Data Schema
===========


ParentId Relationships
----------------------

**Single System**

In a *Single* system, the only star has a parentId of zero.

*Planets* and *Moons* have a parentId of the primary star.

**Binary System**

In a *Binary* system, if the secondary is orbiting the primary, then the secondary
star has a parentId equal to the Id of the primary star. The primary star has a
parentId of zero.

*Planets* have a parentId equal to the star they are orbiting.

It is possible for the stars to be orbiting a common centre of gravity (COG). In
this case both stars have a parentId of -1.

In a *Conjoined* or *Close* binary, *Planets* orbit either the primary or the
COG. If they orbit the COG, they have a parentId of -1.

