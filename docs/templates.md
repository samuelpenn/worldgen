# Text Templates

Text templates are a way of defining the rules for textually describing something,
whether it is a planet, system, port or other type of facility. Each template uses
a Java properties file format, with special syntax for accessing object properties
and controlling the flow of text.

The aim is to allow random text generation, but guided by the actual features
present on the planet.

## Resource File

Take the following properties file for a particular planet type. This file should
be in the `resources/text/planets` directory. It is a `Hermian` type planet, which
is of the `Dwarf` group, so it will be in the `Dwarf` sub-directory and named
`Hermian.properties`.

Star system descriptions are found under `resources/text/systems`.

Facility descriptions are found under `resources/text/facilities`.

## Formatting

### Lines

```
planet=This is a small hot world.
planet.1=This is a barren world close to its star.
planet.2=This world is hot and barren.
```

When the planet's description is generated, all Java property entries beginning
planet.Hermian will be located, and one will be randomly selected. The chosen
one will then be used as the base for the planet's description.

The ability to specify multiple text phrases with .1, .2, .n suffixes applies
everywhere. In all cases, one will be randomly selected. There cannot be any
gaps in the numbering sequence, otherwise any after the gap will be ignored.


### Properties

A property on a planet, system or facility can be accessed by using the $ notation.
At the very basic, use $Property to access the named property on the object. For
example, the following line:

``The world of $Name has a radius of $Radius kilometres.``

might be rendered as:

``The world of Ptavvs has a radius of 7,463 kilometres.``

Note that numeric values will be automatically pretty printed. If you don't
want pretty printing, then you can use `$!Property` to disable it.

Sometimes you might want less precision in the text, in which case you can
specify the number of significant digits using $n_Property. For example the
same world with the following line:

``The world of $Name has a radius of $2_Radius kilometres.``

which would be rendered as:

``The world of Ptavvs has a radius of 7,500 kilometres.``

Sometimes you just don't really want to use numbers at all, and would
prefer a textual approximation. In which case use `$~Property`. So
a world with a population of 76,934,234,123, with the following line:

``The world of $Name has a population of $~Population.``

Would be displayed as so:

``The world of Hive has a population of 77 billion.``

Note that values less than 10 thousand will be displayed using standard
pretty printing.

All these syntax options only apply to numeric properties. They will have no
effect on non-numeric properties.

If a property name is prefixed with Facility, then the property is read from
the facility rather than the planet. This is only applicable when generating
the description of a facility. e.g. ``$FacilityTechLevel``


#### Non-Numeric Properties

Non-numeric properties generally don't require processing, and will be
output as raw text. However, some processing can be forced by using `$$`
notation, e.g. `$$Property`

This is mostly used for printing enums values, which are generally camel
case, e.g. "VeryStrong" or "CommunistState". In this case such values will
be turned lower case and split, so "very strong" and "communist state".

If $$ does result in a purely numeric property, it will be converted to words,
precisely. e.g. "342" becomes "three hundred and forty two"


#### Random Values

You can also specify the generation of random values, by using standard
dice notation in the form ``$xDy``.

For example, $3D6 would generate a number between 3 and 18.

You can also add modifiers, such as `$2D6-2` or `$1D6+4`

If the generated result is large enough, it will automatically be pretty
printed. e.g. `$5D100+1000` could result in `1,332`.

Note that you can use `$$xDy` to output a random number as words.

### Selecting Options

Sometimes you want to have several different variants of a piece of
text using `[|]`. For example, "This planet" or "The planet Minbar". 
You can specify a number of inline options which will be randomly selected
between.

```[This planet|The planet $Name|$Name] has a [big|large] ice cap.```

This will randomly select one of the options given. Properties are
automatically expanded within the option list. This might produce text
such as:

``This planet has a large ice cap.``

or

``Minbar has a big ice cap.``  

Sometimes you need to ensure that the same phrase is used for consisting
within a piece of text. It is possible to save a choice for use later:

``The vats produce [TYPE::algae|protein]. The [TYPE::] is then eaten.``

In this case either 'algae' or 'protein' is selected, and saved to the
constant TYPE. When it is referenced later, the same value is used. The
constant is remembered for the rest of the call, so can be used across
phrases. If the constant has already been set, then its value will be
used instead of selecting a new random value.

### Referencing Lines

You can reference other lines within a line Using `{}`. 

```
planet=This is a small rocky moon. {surface} {geology}
planet.1=The moon of $Name is a barren place. {surface} {geology}
planet.2=This is a boring place. {surface|geology}

surface=The surface is covered in [large|small] craters.
surface.1=The entire surface of $Name is rocky, bare and grey.
surface.2=Tall mountains, unweathered due to the vacuum, cover the world.

geology=$Name has a cold, dead core.
geology.1=The core of $Name is low in metals, and has long since gone cold.
```

This will produce text made up of one of the automatically selected lines,
plus inserting a randomly selected `surface` and `geology` entry in as
well.

Note that this supports the pipe symbol to randomly select different inline
insertions. So `planet.2` will insert *either* a `surface` or a
`geology`, but not both.

You can mix selectors and references. For example:

``This planet has {special.[ruins|aliens|geology]}``

This would reference either `special.ruins`, `special.aliens` or `special.geology`.

The reverse is also true, so

``This planet has [ancient ruins|{alien.artificats}|strange formations]``

This will select either the standard text, or inline `{alien.artificats}`
depending on which one is selected.

Finally, it is possible to use properties as well.

``This world is mostly harmless. {government.$Government}``

This will reference the line based on the planet's *Government* property. So
if it is a CommunistState, then `government.CommunistState`.

In all cases, if a property does not exist, or a line cannot be found, then
an empty string will be inserted.

### Switch Statements

Sometimes you want text that is based on the value of a numeric, normally a
property, but it could also be a dice roll. This uses the following syntax:

`($!Property|Value1=Text1|Value2=Text2|Value3>Text3|Value4<Text4|Default)`

It reads $!Property, and compares it against each of the Values in turn.

* Value= means does the property equal the value?
* Value> means is the property less than the value?
* Value< means is the property greater than the value?

```
This world is ($!Radius|6000>small|7000<large|medium sized).
```

Note that you probably need to use the $!Property form, otherwise the
pretty printing will break the comparison of numeric values (this could
cause problems for very large dice rolls which are always pretty printed,
but this is an unlikely combination).

For the equals operator, you can include comma separated values, e.g.:

```
The waters of this world are ($2D6|2,3=Pink|10,11=Purple|12=Yellow|Brown).
```

This rolls 2D6 (generating a number between 2 and 12, and outputs a result
based on that roll.

### Feature Switch

Similar to a switch, but checks to see if the object (planet, system etc)
has the specified feature. The first feature that is found is returned.

```
This world is (?|Hot|Cold|Warm).
This world is (?just right|Hot=too hot|Cold=too cold|Warm=a bit warm).
{clouds.(?|ThickClouds|DarkClouds|RedClouds)}
```

If a default value is given before the first pipe, then that is returned
if none of the features match.

An alternative value can be specified on a match using =.

It can be used as part of an insert, to select which line to use.

Note that the actual *type* of the feature doesn't matter, and can't
actually be specified at all. If there is a TerrestrialFeature.Warm
and a EoGaianFeature.Warm, the text parser can't tell them apart, and
either will match to 'Warm'.

### Resource Switch

Another type of switch, this one returns the resource which has the highest
density. If none of the resources are present, then it can return a default
value instead.

```
This world is rich in (%|FerricOre|SilicateOre|CarbonicOre).
This world is rich in (%nothing of worth|FerricOre|SilicateOre|CarbonicOre).
{life.(%barren|!Prokaryotes|!Protozoa|!Metazoa)}
```

The first resource with the highest density will be returned. Pre-pending a
resource with "!" will force the enum value to be returned, otherwise the
expanded name is returned.

e.g. "!FerricOre" returns "FerricOre" whilst "FerricOre" returns "Ferric Ore".

### Setters

Sometimes, a description needs to be backed by physical changes to the
planet. This can be done with the `@+` and `@-` notation, which can be
used to add or remove trade codes, and add resources.

```
This world has lots of gold $+PreciousMetals on its surface.
The world is barren $+Ba and has a poor industry$-In.
```

The code (and any trailing space) is automatically removed by the parser,
so it doesn't appear in the final output. If the given code is two letters,
it is taken to be a planetary trade code, and can be added or removed.

If it is longer than two characters, then it is taken to be a commodity name.
If the commodity does not exist as a resource, then it is added as a tertiary
resource. If it does exist, then its density is increased by either +100,
or +25%, whichever is greater.

There are two special properties, @+AMBER and @+RED, which set the trade
safety zone of the world.

#### Feature Setters

It is possible to set a planetary feature. You can do this using `@=`

```
planet=The world of $Name has dark, murky seas. @=MurkySeas
```

This will add the planetary feature enum `MurkySeas` if it exists. It needs to
be defined on either the planet type itself, or one of its super types. This
feature will then be picked up by the mapper so it can be represented on the
map.

However, it is possible to specify your own features in the template, by having
a definition with a `.title` suffix. This will be added to the end of the
planet's description, with its own sub-heading. In this case, there doesn't need
to be an existing enum.

```
MurkySea.title=The Seas of $Name
MurkySea=The seas here are famous.
```

#### Resource Setters

You can add or subtract resources from a world with `@+` and `@-`.

```
planet=This world is quite rich in gold and silver. @+PreciousMetals
planet.1=This world is really rich in gold and silver. @++PreciousMetals
planet.2=This world is lined with precious metals. @+++++PreciousMetals
planet.3=This world is metal poor. @-PreciousMetals @-RareMetals
```

The number of `+` and `-` signs signifies how much to add or subtract.
Five `+` signs is equivalent to the levels of a primary resource.

## Planet Descriptions

Planetary descriptions follow a certain pattern.

If there is a `feature.<featureName>` then this will be used as the
basic template for the planet's description. The first matching
feature that is found is used, and if there are multiple ones then
the order is undefined.

Note that if the above key is found, then none of the following is
done, and they will need to be done manually.

If there are no `feature.*` entries. then it looks for one of the
following in order:

  * `planet.<featureName>`
  * `planet.<H1|H2|H3|H4|H5|H6>`
  * `planet`

It will use the first matching of the above, then it will look in
order for the following items, and append each of these to the above:

  * `planet.temperature.<Temperature>`
  * `planet.atmosphere.<Atmosphere>.<Pressure>`
  * `planet.atmosphere.Any.<Pressure>`
  * `planet.atmosphere.<Atmosphere>`
  * `planet.biosphere.<Life>`
  * `planet.feature.<FeatureName>`
  * `planet.special`

After all the above, *all* planets looks for `biosphere.<Life>`,
even if a major feature was found.


