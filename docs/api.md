API
===

WorldGen is built with RESTful APIs in mind, so that most data can be retrieved
by making API calls rather than needing to go through the UI.

Architecture
------------

Both APIs and Controllers are simply wrappers to the service layer, where all
the logic is. This means that regardless of whether data is accessed from a
Controller or the API should result in the same data and logic.

All API data is returned in JSON format, except for some binary data such
as images.


Image APIs
----------

GET /api/images/

GET /api/images/:name

PUT /api/images/:name