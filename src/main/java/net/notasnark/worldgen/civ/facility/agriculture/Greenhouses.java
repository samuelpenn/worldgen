/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.agriculture;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * Greenhouses are surface facilities which provide intensive agriculture.
 * They tend to produce natural food.
 */
public class Greenhouses extends AbstractFacility {
    public Greenhouses(Planet planet) {
        super(planet);
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        int tl = Math.max(planet.getTechLevel(), 9 + Die.d4());
        if (planet.hasTradeCode(TradeCode.Lo)) {
            tl -= 1;
        } else if (planet.hasTradeCode(TradeCode.Hi)) {
            tl += 1;
        }
        facility.setTechLevel(tl);

        facility.setRating(70 + Die.d20(2) + (int) (Math.log10(planet.getPopulation()) * 3));

        planet.removeTradeCode(TradeCode.Na);
        if (facility.getRating() < 90) {
            planet.addTradeCode(TradeCode.Na);
        }

        return facility;
    }
}
