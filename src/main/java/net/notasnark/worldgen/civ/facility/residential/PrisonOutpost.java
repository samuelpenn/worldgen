/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * A prison outpost is a standalone prison complex designed specifically for maximum security
 * far from civilisation.
 */
public class PrisonOutpost extends AbstractFacility {
    public PrisonOutpost(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(70 + Die.die(40));
        facility.setTechLevel(12);
        planet.setTechLevel(5);
        planet.setGovernment(Government.ImpersonalBureaucracy);
        planet.setLawLevel(6);

        return facility;
    }
}
