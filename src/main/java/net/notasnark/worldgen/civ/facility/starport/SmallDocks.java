/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.starport;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

public class SmallDocks extends AbstractFacility {
    public SmallDocks(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(90 + Die.d10(2));
        facility.setTechLevel(planet.getTechLevel());
        if (facility.getTechLevel() < 8) {
            facility.setTechLevel(8);
        }
        planet.setStarPort(StarPort.Do);

        return facility;
    }
}
