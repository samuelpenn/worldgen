/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.starport;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

public class SmallOrbital extends AbstractFacility {
    public SmallOrbital(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(90 + Die.d10(2));
        switch (planet.getStarPort().getStandard()) {
            case A: case B:
                // We shouldn't be here, but just in case.
                facility.setTechLevel(13);
                break;
            case C:
                facility.setTechLevel(12);
                break;
            case D:
                facility.setTechLevel(11);
                break;
            case E:
                facility.setTechLevel(10);
                break;
            default:
                facility.setTechLevel(9);
                break;
        }
        if (planet.getTechLevel() > facility.getTechLevel()) {
            facility.setTechLevel(planet.getTechLevel());
        }

        return facility;
    }
}
