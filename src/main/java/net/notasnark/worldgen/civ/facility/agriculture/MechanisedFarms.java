/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.agriculture;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * Mechanised farms are high tech farms which make heavy use of machines to do all the heavy
 * work. They still need human labour, but it is greatly reduced over a typical farm. They
 * also tend to use artificial fertilizers and modern intensive techniques.
 *
 * The TL for these range from TL 7 to TL 10.
 *
 * The sort of things needed:
 *   Agricultural Machinery
 *   Fertilizer (TL 7-8)
 *   Advanced Fertilizer (TL 9+)
 *   Agricultural Robotics (minimal, if TL 9+)
 */
public class MechanisedFarms extends AbstractFacility {
    public MechanisedFarms(Planet planet) {
        super(planet);
        minTech = 7;
        maxTech = 10;
    }

    /**
     * Generate a new facility based on the planet. Modifies the agricultural codes
     * for the world as well.
     *
     * @return  Generated facility.
     */
    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setTechLevel(7 + Die.d2());
        facility.setRating(80 + Die.d20(2));

        planet.removeTradeCode(TradeCode.Na);
        planet.addTradeCode(TradeCode.Ag);

        return facility;
    }
}
