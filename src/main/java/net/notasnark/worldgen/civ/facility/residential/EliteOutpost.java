/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * A prison outpost is a standalone prison complex designed specifically for maximum security
 * far from civilisation.
 */
public class EliteOutpost extends AbstractFacility {
    public EliteOutpost(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(100 + Die.die(40));
        switch (Die.d6()) {
            case 1: case 2: case 3:
                facility.setTechLevel(15);
                break;
            case 4: case 5:
                facility.setTechLevel(14);
                break;
            case 6:
                facility.setTechLevel(16);
                break;
        }
        planet.setTechLevel(5);
        planet.setGovernment(Government.Corporation);
        planet.setLawLevel(1);

        return facility;
    }
}
