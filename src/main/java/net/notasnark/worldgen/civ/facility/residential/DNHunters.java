/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Specifically for Deepnight Endeavour.
 */
public class DNHunters extends AbstractFacility {
    public DNHunters(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 2;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(90 + Die.d12(2));
        facility.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(0);

        if (planet.getHabitability() == 2) {
            facility.setRating(facility.getRating() - 15);
        } else if (planet.getHabitability() > 2) {
            facility.setRating(facility.getRating() - 30);
        }

        planet.setGovernment(Government.Anarchy);

        return facility;
    }
}
