/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Specifically for Deepnight Endeavour.
 */
public class DNIndustrial extends AbstractFacility {
    public DNIndustrial(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 2;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(90 + Die.d12(2));
        facility.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(3);
        planet.setGovernment(Government.RepresentativeDemocracy);

        switch (Die.d6(2)) {
            case 2:
                planet.setGovernment(Government.NonCharismaticLeader);
                planet.setLawLevel(3+Die.d3());
                break;
            case 3:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(5+Die.d3());
                break;
            case 4:
                planet.setGovernment(Government.ImpersonalBureaucracy);
                planet.setLawLevel(5+Die.d3());
                planet.setPopulation(planet.getPopulation() * Die.d4());
                break;
            default:
                planet.setGovernment(Government.Balkanization);
                planet.setLawLevel(2+Die.d3());
                break;
        }

        return facility;
    }
}
