/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * A small settlement on a low habitability world. The settlement is subterranean, dug into the rock for
 * protection. There may be a few surface structures, but the bulk of the population lives below ground.
 *
 * They are generally found on Dwarf Terrestrial or Terrestrial worlds.
 */
public class FreeCavers extends AbstractFacility {
    public FreeCavers(Planet planet) {
        super(planet);
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);

        facility.setRating(75 + Die.d20(2));
        planet.setTechLevel(4 + Die.d2());

        // Larger populations have a chance of having a class D starport, but most
        // will be class E.
        if (50_000 + Die.die(100_000, 3) < planet.getPopulation()) {
            planet.setStarPort(StarPort.D);
        } else {
            planet.setStarPort(StarPort.E);
        }
        int modifier = 0;
        if (features.contains(CivilisationFeature.Poor) || features.contains(CivilisationFeature.SmallPopulation)) {
            modifier -= 1;
        }
        if (features.contains(CivilisationFeature.Rich)) {
            modifier += 1;
        }
        if (features.contains(CivilisationFeature.LargePopulation)) {
            modifier += 1;
        } else if (features.contains(CivilisationFeature.HugePopulation)) {
            modifier += 2;
        }
        switch (Die.d6() + modifier) {
            case 0: case 1: case 2:
                planet.setGovernment(Government.Anarchy);
                planet.setLawLevel(Die.d2() - 1);
                break;
            case 3: case 4:
                planet.setGovernment(Government.Communist);
                planet.setLawLevel(Die.d6());
                break;
            case 5:
                switch (Die.d3()) {
                    case 1:
                        planet.setGovernment(Government.TheocraticDictatorship);
                        planet.setLawLevel(Die.d3() + 4);
                        break;
                    case 2:
                        planet.setGovernment(Government.TheocraticOligarchy);
                        planet.setLawLevel(Die.d3() + 3);
                        break;
                    case 3:
                        planet.setGovernment(Government.ImpersonalBureaucracy);
                        planet.setLawLevel(Die.d4() + 4);
                        break;
                }
                break;
            default:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(Die.d2());
                break;
        }
        if (modifier > 0) {
            facility.setTechLevel(planet.getTechLevel() + modifier);
            planet.setTechLevel(planet.getTechLevel() + modifier / 2);
        }

        return facility;
    }
}
