/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Specifically for Deepnight Endeavour.
 */
public class DNBronzeAge extends AbstractFacility {
    public DNBronzeAge(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 2;
    }

    public Facility getFacility(Set<CivilisationFeature> features, CivilisationFeature... extra) {
        System.out.println("DNBronzeAge.getFacility:");
        Facility facility = super.getFacility(features, extra);
        facility.setRating(90 + Die.d12(2));
        facility.setTechLevel(planet.getTechLevel());

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setGovernment(Government.Balkanization);
                planet.setLawLevel(1);
                break;
            case 4: case 5:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(1);
                break;
            case 6:
                planet.setGovernment(Government.Anarchy);
                planet.setLawLevel(0);
                break;
        }
        System.out.println("Government: " + planet.getGovernment() + " at law " + planet.getLawLevel());

        return facility;
    }
}
