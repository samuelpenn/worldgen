/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Specifically for Deepnight Endeavour.
 */
public class DNHighTech extends AbstractFacility {
    public DNHighTech(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 2;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(90 + Die.d12(2));
        facility.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(3);

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setGovernment(Government.Balkanization);
                break;
            case 4: case 5:
                planet.setGovernment(Government.NonCharismaticLeader);
                break;
            case 6:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(4);
                break;
        }

        return facility;
    }
}
