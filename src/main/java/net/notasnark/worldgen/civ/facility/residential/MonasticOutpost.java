/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;

/**
 * This outpost is a monastic retreat, populated entirely by a religious order of monks.
 *
 * Output will be religious texts.
 */
public class MonasticOutpost extends AbstractFacility {
    public MonasticOutpost(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(89 + Die.d10(2));
        planet.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(5);
        planet.setGovernment(Government.TheocraticOligarchy);

        return facility;
    }
}
