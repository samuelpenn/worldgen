/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.starport;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * These are docks built directly into, or onto, an asteroid. They have the advantage of zero gravity
 * docking facilities, coupled with an actual object to build against.
 */
public class AsteroidDocks extends AbstractFacility {
    public AsteroidDocks(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(90 + Die.d10(2));
        facility.setTechLevel(planet.getTechLevel());
        if (facility.getTechLevel() < 6) {
            facility.setTechLevel(6);
        }

        if (planet.getStarPort() == StarPort.X) {
            if (Die.d2() == 1) {
                planet.setStarPort(StarPort.Do);
            } else {
                planet.setStarPort(StarPort.Eo);
            }
        }

        return facility;
    }
}
