/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * Specifically for Deepnight Endeavour. A small colony of religious people.
 */
public class DNReligiousColony extends AbstractFacility {
    public DNReligiousColony(Planet planet) {
        super(planet);
        minTech = 2;
        maxTech = 2;
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);
        facility.setRating(90 + Die.d12(2));
        facility.setTechLevel(planet.getTechLevel());
        planet.setLawLevel(5);

        switch (Die.d6()) {
            case 1: case 2:
                planet.setGovernment(Government.TheocraticOligarchy);
                break;
            case 3: case 4: case 5: case 6:
                planet.setGovernment(Government.TheocraticDictatorship);
                break;
        }

        return facility;
    }
}
