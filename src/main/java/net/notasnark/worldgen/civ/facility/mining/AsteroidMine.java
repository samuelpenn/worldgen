/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.mining;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

public class AsteroidMine extends AbstractFacility {
    public AsteroidMine(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(80 + Die.die(20, 2));

        switch (Die.d6()) {
            case 1: case 2: case 3:
                facility.setTechLevel(10);
                break;
            case 4: case 5:
                facility.setTechLevel(11);
                break;
            case 6:
                facility.setTechLevel(12);
                break;
        }

        return facility;
    }
}
