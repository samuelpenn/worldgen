/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

import java.util.Set;

/**
 * DomedCities are the next step up from DomedHabitats. They are multiple large domed settlements that
 * each have populations in the hundreds of thousands. The main capital may have a population in the
 * millions.
 */
public class DomedCities extends AbstractFacility {
    public DomedCities(Planet planet) {
        super(planet);
    }

    public Facility getFacility(Set<CivilisationFeature> features) {
        Facility facility = super.getFacility(features);

        facility.setRating(75 + Die.d20(2));
        if (planet.hasTradeCode(TradeCode.Ri)) {
            facility.setTechLevel(planet.getTechLevel() + 2);
        } else if (planet.hasTradeCode(TradeCode.Po)) {
            facility.setTechLevel(planet.getTechLevel() + 0);
        } else {
            facility.setTechLevel(planet.getTechLevel() + 1);
        }

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setGovernment(Government.Corporation);
                planet.setLawLevel(Die.d2() + 2);
                break;
            case 4:
                planet.setGovernment(Government.ImpersonalBureaucracy);
                planet.setLawLevel(Die.d2() + 3);
                break;
            case 5:
                planet.setGovernment(Government.TheocraticOligarchy);
                planet.setLawLevel(Die.d2() + 3);
                break;
            case 6:
                planet.setGovernment(Government.ParticipatingDemocracy);
                planet.setLawLevel(Die.d4());
                break;
        }

        return facility;
    }
}
