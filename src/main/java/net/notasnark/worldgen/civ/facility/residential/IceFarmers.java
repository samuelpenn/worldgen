/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.residential;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

/**
 * Class to generate and manage Ice Farmers. Residential facility sometimes found around icy
 * belts far from a star, such as kuiper belts or oort clouds.
 */
public class IceFarmers extends AbstractFacility {
    public IceFarmers(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(50 + Die.d20(2));
        planet.setTechLevel(4 + Die.d2());
        planet.setStarPort(StarPort.X);
        planet.setGovernment(Government.Anarchy);

        return facility;
    }
}
