/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.facility.starport;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.AbstractFacility;

public class IndustrialDocks extends AbstractFacility {
    public IndustrialDocks(Planet planet) {
        super(planet);
    }

    public Facility getFacility() {
        Facility facility = super.getFacility();

        facility.setRating(90 + Die.d10(2));
        facility.setTechLevel(10);

        if (planet.getStarPort() == StarPort.X) {
            if (Die.d2() == 1) {
                planet.setStarPort(StarPort.Ho);
            } else {
                planet.setStarPort(StarPort.Go);
            }
        }

        return facility;
    }
}
