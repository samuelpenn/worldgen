/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.civ.facility.agriculture.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.residential.DomedCities;
import net.notasnark.worldgen.civ.facility.residential.DomedHabitats;
import net.notasnark.worldgen.civ.facility.residential.Farmsteads;
import net.notasnark.worldgen.civ.facility.residential.RuralColony;
import net.notasnark.worldgen.civ.facility.starport.DomedStarPort;
import net.notasnark.worldgen.civ.facility.starport.LandingField;
import net.notasnark.worldgen.civ.facility.starport.OpenStarPort;
import net.notasnark.worldgen.civ.facility.starport.SmallOrbital;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a colony in this star system. Can be applied to systems which have a Class III world or better.
 * Poorer quality worlds tend not to go through a colonial phase.
 */
public class Colony extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Colony.class);

    // An indication of the size of this colony.
    private ColonySize  size;

    // The sort of TL available to the colony. Note it doesn't affect the planet's on TL, just the
    // the type of technology that is being made available to the planet.
    private int         colonyTL = 9;

    private enum ColonySize {
        Tiny,
        Small,
        Medium,
        Large,
        Huge;

        ColonySize getLarger() {
            switch (this) {
                case Tiny: return Small;
                case Small: return Medium;
                case Medium: return Large;
                default: return Huge;
            }
        }

        ColonySize getSmaller() {
            switch (this) {
                case Huge: return Large;
                case Large: return Medium;
                case Medium: return Small;
                default: return Tiny;
            }
        }

        boolean isBiggerThan(ColonySize size) {
            if (size != null) {
                return this.ordinal() > size.ordinal();
            } else {
                return false;
            }
        }

        boolean isSmallerThan(ColonySize size) {
            if (size != null) {
                return this.ordinal() < size.ordinal();
            } else {
                return false;
            }
        }
    }

    public Colony(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding Colony to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        Planet planet = null;
        List<Facility> facilities = new ArrayList<Facility>();

        planet = getBestPlanet(3);
        if (planet == null) {
            // Not really suitable for a full fledged colony.
            notAColony();
        } else {
            colonyTL = Math.max(planet.getTechLevel(), 8 + Die.d3());
            if (hasFeature(CivilisationFeature.Rich)) {
                colonyTL += 1;
            } else if (hasFeature(CivilisationFeature.Poor)) {
                colonyTL -= 1;
            }
            switch (planet.getHabitability()) {
                case 1:
                    idealConditions(planet, facilities);
                    break;
                case 2:
                    okayConditions(planet, facilities);
                    break;
                case 3:
                    harshConditions(planet, facilities);
                    break;
                default:
                    // How did we get here?
                    notAColony();
            }
            if (facilities.size() > 0) {
                generateDescription(planet, facilities);
            }
        }
    }

    /**
     * This system isn't suitable for colonies, so try something else.
     */
    private void notAColony() {
        logger.warn("This system does not make for a good colony");
    }


    private void determineColonySize(int modifier) {
        int roll = Die.d6(2) + modifier;
        size = ColonySize.Medium;
        if (roll < 5) {
            size = ColonySize.Small;
        } else if (roll > 12) {
            size = ColonySize.Huge;
        } else if (roll > 9) {
            size = ColonySize.Large;
        }
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            size = size.getSmaller();
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            size = size.getLarger();
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            size = size.getLarger().getLarger();
        }

        switch (size) {
            case Tiny:
                addFeature(CivilisationFeature.Tiny);
                colonyTL -= 1;
                break;
            case Small:
                addFeature(CivilisationFeature.Small);
                colonyTL -= 1;
                break;
            case Medium:
                addFeature(CivilisationFeature.Medium);
                break;
            case Large:
                addFeature(CivilisationFeature.Large);
                colonyTL += 1;
                break;
            case Huge:
                addFeature(CivilisationFeature.Huge);
                colonyTL += 2;
                break;
        }
    }

    /**
     * This planet has ideal conditions for setting up a colony.
     *
     * @param planet    Planet to test for.
     */
    private void idealConditions(Planet planet, List<Facility> facilities) {
        logger.info("idealConditions:");
        determineColonySize(+2);
        createFarmsteads(planet, facilities);
    }

    private void okayConditions(Planet planet, List<Facility> facilities) {
        logger.info("okayConditions:");
        determineColonySize(0);


        switch (size) {
            case Tiny:
                createFarmsteads(planet, facilities);
                break;
            case Small:
                if (Die.d2() == 1) {
                    createFarmsteads(planet, facilities);
                } else {
                    createRuralColony(planet, facilities);
                }
                break;
            case Medium:
                if (Die.d3() == 1) {
                    createFarmsteads(planet, facilities);
                } else {
                    createRuralColony(planet, facilities);
                }
                break;
            case Large:
                if (Die.d2() == 1) {
                    createRuralColony(planet, facilities);
                } else {
                    createUrbanColony(planet, facilities);
                }
                break;
            case Huge:
                createUrbanColony(planet, facilities);
                break;
        }
    }


    private void harshConditions(Planet planet, List<Facility> facilities) {
        logger.info("hashConditions:");
        determineColonySize(-2);

        switch (size) {
            case Tiny: case Small:
                createDomedHabitats(planet, facilities);
                break;
            case Medium:
                createDomedHabitats(planet, facilities);
                break;
            case Large: case Huge:
                createDomedCities(planet, facilities);
                break;
        }
    }

    private void createAgriculture(Planet planet, List<Facility> facilities) {
        logger.info(String.format("createAgriculture: [%s] [%s]", size, planet.getHabitabilityText()));

        Facility agriculture = null;

        if (planet.getHabitability() > 2) {
            // Uninhabitable world, food must be grown indoors.
            agriculture = new Greenhouses(planet).getFacility();
        } else if (planet.getHabitability() == 2) {
            // Harsh worlds, but still possible to grow things outside.
            if (size.isBiggerThan(ColonySize.Large)) {
                agriculture = new RoboticXenoFarms(planet).getFacility();
            } else if (size.isBiggerThan(ColonySize.Medium)) {
                agriculture = new MechanisedXenoFarms(planet).getFacility();
            } else {
                agriculture = new XenoFarms(planet).getFacility();
            }
        } else if (planet.getHabitability() == 1) {
            // Ideal Earth-like worlds.
            if (size.isBiggerThan(ColonySize.Large)) {
                agriculture = new RoboticFarms(planet).getFacility();
            } else if (size.isBiggerThan(ColonySize.Medium)) {
                agriculture = new MechanisedFarms(planet).getFacility();
            } else {
                agriculture = new Farms(planet).getFacility();
            }
        }
        if (agriculture != null) {
            qualityCheck(agriculture);
            worldGen.getPlanetFactory().setFacility(agriculture);
            facilities.add(agriculture);
        }
    }

    private void createStarPort(Planet planet, List<Facility> facilities) {
        logger.info(String.format("createStarPort: [%s] [%s] [%s]", size,
                planet.getHabitabilityText(), planet.getStarPort()));

        Facility lowPort = null, highPort = null;
        if (planet.getHabitability() > 2) {
            lowPort = new DomedStarPort(planet).getFacility();
            if (planet.getStarPort() == StarPort.C) {
                highPort = new SmallOrbital(planet).getFacility();
            }
        } else {
            switch (planet.getStarPort()) {
                case E: case H:
                    lowPort = new LandingField(planet).getFacility();
                    break;
                case D: case G:
                    lowPort = new OpenStarPort(planet).getFacility();
                    break;
                case C: case F:
                    lowPort = new OpenStarPort(planet).getFacility();
                    highPort = new SmallOrbital(planet).getFacility();
                    break;
            }
        }

        if (lowPort != null) {
            qualityCheck(lowPort);
            worldGen.getPlanetFactory().setFacility(lowPort);
            facilities.add(lowPort);
        }
        if (highPort != null) {
            qualityCheck(highPort);
            worldGen.getPlanetFactory().setFacility(highPort);
            facilities.add(highPort);
        }
    }

    private void createDomedHabitats(Planet planet, List<Facility> facilities) {
        logger.info("Creating Domed Habitats");

        planet.removeTradeCode(TradeCode.In);
        planet.addTradeCode(TradeCode.Ni);
        planet.setStarPort(StarPort.D);

        planet.setTechLevel(6);
        long population = Die.d6(5);
        switch (size) {
            case Tiny:
                population *= 1_000;
                population += Die.die(1_000);
                planet.setStarPort(StarPort.E);
                break;
            case Small:
                population *= 3_000;
                population += Die.die(3_000);
                planet.setStarPort(StarPort.E);
                break;
            case Medium:
                population *= 10_000;
                population += Die.die(10_000);
                break;
            default:
                throw new IllegalArgumentException(("A DomedHabitat can't be larger than Medium in size"));
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new DomedHabitats(planet).getFacility();
        qualityCheck(residential);

        logger.debug(String.format("Setting residential [%s]", residential.getTitle()));
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        createAgriculture(planet, facilities);
        createStarPort(planet, facilities);
    }

    private void createDomedCities(Planet planet, List<Facility> facilities) {
        logger.info("Creating Domed Cities");

        planet.removeTradeCode(TradeCode.In);
        planet.addTradeCode(TradeCode.Ni);

        planet.setTechLevel(6);
        long population = Die.d6(6);
        switch (size) {
            case Large:
                population *= 300_000;
                population += Die.die(300_000);
                planet.setTechLevel(6 + Die.d2());
                if (Die.die(20_000_000) < population) {
                    planet.setStarPort(StarPort.C);
                } else {
                    planet.setStarPort(StarPort.D);
                }
                break;
            case Huge:
                population *= 1_000_000;
                population += Die.die(1_000_000);
                planet.setTechLevel(7 + Die.d2());
                planet.setStarPort(StarPort.C);
                break;
            default:
                throw new IllegalArgumentException("DomedCities must be Large or Huge in size");
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new DomedCities(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        createAgriculture(planet, facilities);
        createStarPort(planet, facilities);
    }

    private void createFarmsteads(Planet planet, List<Facility> facilities) {
        // Tiny or Small.
        logger.info("Creating Farmsteads");

        planet.removeTradeCode(TradeCode.In);
        planet.addTradeCode(TradeCode.Ni);

        // Determine the population of the colony. Farmsteads tend to be Small, so if we've somehow
        // set something larger than that, then still increase the population but at a greatly reduced
        // rate. Colonies like this with populations in the millions will be very unusual.
        long population = Die.d100(5) + 100;
        switch (size) {
            case Tiny:
                planet.setTechLevel(4);
                planet.setStarPort(StarPort.E);
                population *= 5;
                break;
            case Small:
                planet.setStarPort(StarPort.E);
                planet.setTechLevel(4);
                population *= 40;
                break;
            case Medium:
                planet.setStarPort(StarPort.E);
                planet.setTechLevel(5);
                population *= 300;
                break;
            default:
                throw new IllegalArgumentException("Farmsteads cannot be larger than Medium");
        }

        // The point of a colony is to grow quickly, so a rich colony will have more resources
        // to enable this to happen.
        if (hasFeature(CivilisationFeature.Poor)) {
            population *= 0.75;
        } else if (hasFeature(CivilisationFeature.Rich)) {
            population *= 1.25;
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new Farmsteads(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Farmstead residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        createAgriculture(planet, facilities);
        createStarPort(planet, facilities);
    }

    private void createFailedColony(Planet planet, List<Facility> facilities) {
        // Tiny or Small.
        // Determine the population of the colony.
        long population = Die.d100(5) + 100;
        if (hasFeature(CivilisationFeature.Tiny)) {
            planet.setStarPort(StarPort.X);
            planet.setTechLevel(2);
            population *= 3;
        } else if (hasFeature(CivilisationFeature.Small)) {
            planet.setStarPort(StarPort.X);
            planet.setTechLevel(3);
            population *= 10;
        } else if (hasFeature(CivilisationFeature.Large)) {
            planet.setStarPort(StarPort.E);
            planet.setTechLevel(4);
            population *= 100;
        } else if (hasFeature(CivilisationFeature.Huge)) {
            planet.setStarPort(StarPort.E);
            planet.setTechLevel(4);
            population *= 300;
        } else {
            // Medium.
            planet.setStarPort(StarPort.E);
            planet.setTechLevel(4);
            population *= 30;
        }

        // Quality of a Failed Colony will be low to begin with.
        if (hasFeature(CivilisationFeature.Poor)) {
            population *= 0.50;
        } else if (hasFeature(CivilisationFeature.Rich)) {
            population *= 1.10;
        }

        planet.setPopulation(Physics.round(population));
    }

    private void createRuralColony(Planet planet, List<Facility> facilities) {

        // Determine the population of the colony.
        long population = Die.d100(5) + 100;
        switch (size) {
            case Small:
                planet.setStarPort(StarPort.E);
                planet.setTechLevel(6);
                population *= 100;
                break;
            case Medium:
                planet.setStarPort(StarPort.D);
                planet.setTechLevel(7);
                population *= 3_000;
                break;
            case Large:
                planet.setStarPort(StarPort.C);
                planet.setTechLevel(7);
                population *= 1_000;
                break;
            default:
                throw new IllegalArgumentException("RuralColony must be Small, Medium or Large");
        }

        // The point of a colony is to grow quickly, so a rich colony will have more resources
        // to enable this to happen.
        if (hasFeature(CivilisationFeature.Poor)) {
            population *= 0.80;
        } else if (hasFeature(CivilisationFeature.Rich)) {
            population *= 1.20;
        }

        planet.setPopulation(Physics.round(population));

        Facility residential = new RuralColony(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting RuralColony residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        createAgriculture(planet, facilities);
        createStarPort(planet, facilities);
    }

    private void createUrbanColony(Planet planet, List<Facility> facilities) {
        // Large.
        // Determine the population of the colony.
        long population = Die.d100(5) + 100;
        if (hasFeature(CivilisationFeature.Tiny)) {
            planet.setTechLevel(5);
            population *= 10;
        } else if (hasFeature(CivilisationFeature.Small)) {
            planet.setTechLevel(6);
            population *= 100;
        } else if (hasFeature(CivilisationFeature.Large)) {
            planet.setTechLevel(7);
            population *= 10_000;
        } else if (hasFeature(CivilisationFeature.Huge)) {
            planet.setTechLevel(7);
            population *= 100_000;
        } else {
            // Medium.
            planet.setTechLevel(6);
            population *= 1_000;
        }

        // The point of a colony is to grow quickly, so a rich colony will have more resources
        // to enable this to happen.
        if (hasFeature(CivilisationFeature.Poor)) {
            population *= 0.75;
        } else if (hasFeature(CivilisationFeature.Rich)) {
            population *= 1.25;
        }

        planet.setPopulation(Physics.round(population));

        createAgriculture(planet, facilities);
        createStarPort(planet, facilities);
    }
}
