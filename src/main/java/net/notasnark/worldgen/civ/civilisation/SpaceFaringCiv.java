package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a space faring civilising, TL 9 through 12.
 */
public class SpaceFaringCiv extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Colony.class);
    private int techLevel = 0;

    public SpaceFaringCiv(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    @Override
    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding SpaceFaringCiv to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        Planet planet = null;
        List<Facility> facilities = new ArrayList<>();

        planet = getBestPlanet(3);
        if (planet == null) {
            // Not a civ.
        } else {
            techLevel = 8 + Die.d4();
            if (hasFeature(CivilisationFeature.Rich)) {
                techLevel += 1;
            } else if (hasFeature(CivilisationFeature.Poor)) {
                techLevel -= 1;
            }
            techLevel = Math.min(12, techLevel);
            switch (planet.getHabitability()) {
                case 1:
                    idealConditions(planet, facilities);
                    break;
                case 2:
                    okayConditions(planet, facilities);
                    break;
                case 3:
                    harshConditions(planet, facilities);
                    break;
                case 4:
                    unfriendlyConditions(planet, facilities);
                    break;
                default:
                    // How did we get here?
            }
            if (planet.getPopulation() > 0) {
                if (planet.getPopulation() > 1_000_000_000L) {
                    planet.setStarPort(StarPort.A);
                } else if (planet.getPopulation() > 100_000_000L) {
                    planet.setStarPort(StarPort.B);
                } else if (planet.getPopulation() > 10_000_000L) {
                    planet.setStarPort(StarPort.C);
                } else if (planet.getPopulation() > 1_000_000L) {
                    planet.setStarPort(StarPort.D);
                } else {
                    planet.setStarPort(StarPort.E);
                }
                if (Die.d2() == 1 && planet.getStarPort().isBetterThan(StarPort.E)) {
                    planet.setStarPort(planet.getStarPort().getWorse());
                }
            }
        }
    }

    private void idealConditions(Planet planet, List<Facility> facilities) {
        logger.info("idealConditions:");
        planet.setTechLevel(techLevel);
        planet.setPopulation(Die.d100() * 10_000_000L);
        switch (Die.d6()) {
            case 1:
                planet.setGovernment(Government.Balkanization);
                break;
            case 2:
                planet.setGovernment(Government.Corporation);
                break;
            case 3:
                planet.setGovernment(Government.RepresentativeDemocracy);
                break;
            case 4:
                planet.setGovernment(Government.ParticipatingDemocracy);
                break;
            case 5:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                break;
            case 6:
                planet.setGovernment(Government.CharismaticOligarchy);
                break;
        }
        planet.setLawLevel(planet.getGovernment().getLawModifier() + Die.d4() + 2);
    }

    private void okayConditions(Planet planet, List<Facility> facilities) {
        logger.info("idealConditions:");
        planet.setTechLevel(techLevel);
        planet.setPopulation(Die.d100() * 1_000_000L);
        switch (Die.d6()) {
            case 1:
                planet.setGovernment(Government.Balkanization);
                break;
            case 2:
                planet.setGovernment(Government.Corporation);
                break;
            case 3:
                planet.setGovernment(Government.RepresentativeDemocracy);
                break;
            case 4:
                planet.setGovernment(Government.ParticipatingDemocracy);
                break;
            case 5:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                break;
            case 6:
                planet.setGovernment(Government.CharismaticOligarchy);
                break;
        }
        planet.setLawLevel(planet.getGovernment().getLawModifier() + Die.d4() + 2);
        planet.setTechLevel(--techLevel);
    }

    private void harshConditions(Planet planet, List<Facility> facilities) {
        logger.info("idealConditions:");
        planet.setTechLevel(techLevel);
        planet.setPopulation(Die.d100() * 100_000L);
        switch (Die.d6()) {
            case 1: case 2:
                planet.setGovernment(Government.Balkanization);
                break;
            case 3:
                planet.setGovernment(Government.Corporation);
                break;
            case 4:
                planet.setGovernment(Government.RepresentativeDemocracy);
                break;
            case 5:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                break;
            case 6:
                planet.setGovernment(Government.TheocraticDictatorship);
                break;
        }
        planet.setLawLevel(planet.getGovernment().getLawModifier() + Die.d4() + 2);
        planet.setTechLevel(--techLevel);
    }

    private void unfriendlyConditions(Planet planet, List<Facility> facilities) {
        logger.info("unfriendlyConditions:");
        planet.setTechLevel(techLevel);
        planet.setPopulation(Die.d100() * 1_000L);
        switch (Die.d6()) {
            case 1: case 2:
                planet.setGovernment(Government.Balkanization);
                break;
            case 3: case 4:
                planet.setGovernment(Government.FeudalTechnocracy);
                break;
            case 5:
                planet.setGovernment(Government.SelfPerpetuatingOligarchy);
                break;
            case 6:
                planet.setGovernment(Government.TheocraticDictatorship);
                break;
        }
        planet.setLawLevel(planet.getGovernment().getLawModifier() + Die.d3() + 2);
        techLevel -= 3;
        planet.setTechLevel(techLevel);
    }
}
