/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.civ.facility.residential.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a colony in this star system. Can be applied to systems which have a Class III world or better.
 * Poorer quality worlds tend not to go through a colonial phase.
 */
public class DeepnightCiv extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(DeepnightCiv.class);

    // An indication of the size of this colony.

    // The sort of TL available to the colony. Note it doesn't affect the planet's on TL, just the
    // the type of technology that is being made available to the planet.
    private int         colonyTL = 9;


    public DeepnightCiv(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding Colony to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        Planet planet = null;
        List<Facility> facilities = new ArrayList<Facility>();

        planet = getBestPlanet(2);
        if (planet != null) {
            switch (planet.getHabitability()) {
                case 1:
                    idealConditions(planet, facilities);
                    break;
                case 2:
                    okayConditions(planet, facilities);
                    break;
                default:
                    // No colonies.
                    break;
            }
            if (facilities.size() > 0) {
                generateDescription(planet, facilities);
            }
        }
    }


    /**
     * This planet has ideal conditions for setting up a colony.
     *
     * @param planet    Planet to test for.
     */
    private void idealConditions(Planet planet, List<Facility> facilities) {
        logger.info("idealConditions:");

        switch (Die.d6(2)) {
            case 2:
                createFarmsteads(planet, facilities);
                break;
            case 3:
                createHighTech(planet, facilities);
                break;
            case 4:
                createIndustrial(planet, facilities);
                break;
            case 5:
                createPreIndustrial(planet, facilities);
                break;
            case 6:
                createBronzeAge(planet, facilities);
                break;
            case 7:
                createHunters(planet,facilities);
                break;
            case 10:
                createSmallColony(planet, facilities);
                break;
            case 11:
                createFailedColony(planet, facilities);
                break;
            case 12:
                createReligiousColony(planet, facilities);
                break;
        }
    }

    private void okayConditions(Planet planet, List<Facility> facilities) {
        logger.info("okayConditions:");

        switch (Die.d6(2)) {
            case 2:
                createHighTech(planet, facilities);
                break;
            case 3:
                createIndustrial(planet, facilities);
                break;
            case 4:
                createPreIndustrial(planet, facilities);
                break;
            case 5:
                createBronzeAge(planet, facilities);
                break;
            case 6:
                createHunters(planet,facilities);
                break;
            case 12:
                createFailedColony(planet, facilities);
                break;
        }
    }


    /**
     * Small rural settlements, total population is generally less than a hundred thousand. These are
     * normally deliberately settled worlds, small colonies which haven't flourished.
     */
    private void createFarmsteads(Planet planet, List<Facility> facilities) {
        logger.info("createFarmsteads:");

        planet.removeTradeCode(TradeCode.In);
        planet.addTradeCode(TradeCode.Ni);

        long population = (Die.d100(5) * 200) / planet.getHabitability();
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(2);
        planet.setGovernment(Government.Balkanization);
        planet.setStarPort(StarPort.X);

        planet.addTradeCode(TradeCode.Ag, TradeCode.Ni);
        if (Die.d3() == 1) {
            planet.addTradeCode(TradeCode.Po, TradeCode.Ag, TradeCode.Ni);
        }

        Facility residential = new DNFarmsteads(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Farmstead residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createFailedColony(Planet planet, List<Facility> facilities) {
        logger.info("createFailedColony:");

        planet.removeTradeCode(TradeCode.In);
        planet.addTradeCode(TradeCode.Ni);

        long population = (Die.d100(5) * 20) / planet.getHabitability();
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(2);
        planet.setStarPort(StarPort.X);

        planet.addTradeCode(TradeCode.Po, TradeCode.Ni);

        Facility residential = new DNFarmsteads(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Failed Colony residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createPreIndustrial(Planet planet, List<Facility> facilities) {
        long population = Die.d8() * 10_000_000 + Die.die(10_000_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(Die.d2() + 1);
        planet.setStarPort(StarPort.X);

        planet.addTradeCode(TradeCode.Ag, TradeCode.Ni);

        Facility residential = new DNPreIndustrial(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting PreIndustrial residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createIndustrial(Planet planet, List<Facility> facilities) {
        long population = Die.d8() * 75_000_000 + Die.die(100_000_000);
        planet.setTechLevel(Die.d2() + 3);
        if (planet.getTechLevel() == 3) {
            population *= 2;
        }
        planet.setPopulation(Physics.round(population));
        planet.setStarPort(StarPort.X);

        planet.addTradeCode(TradeCode.Ag, TradeCode.Ni);

        Facility residential = new DNIndustrial(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Industrial residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createHighTech(Planet planet, List<Facility> facilities) {
        long population = Die.d8(3) * 10_000_000 + Die.die(10_000_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(Die.d2() + 6);
        planet.setStarPort(StarPort.X);
        planet.addTradeCode(TradeCode.In);
        if (population >= 1_000_000_000) {
            planet.addTradeCode(TradeCode.Hi);
        }

        Facility residential = new DNHighTech(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting High Tech residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createSmallColony(Planet planet, List<Facility> facilities) {
        long population = Die.d6(2) * 1_000 + Die.die(1_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(4);
        planet.setStarPort(StarPort.X);
        planet.addTradeCode(TradeCode.Ni);

        Facility residential = new DNSmallColony(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Small Colony residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createReligiousColony(Planet planet, List<Facility> facilities) {
        long population = Die.d6(3) * 1_000 + Die.die(1_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(4);
        planet.setStarPort(StarPort.X);
        planet.addTradeCode(TradeCode.Ni);

        Facility residential = new DNReligiousColony(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Religious Colony residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createHunters(Planet planet, List<Facility> facilities) {
        long population = Die.d6(2) * 100_000 + Die.die(100_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(0);
        planet.setStarPort(StarPort.X);
        planet.addTradeCode(TradeCode.Ni, TradeCode.Na);
        planet.setGovernment(Government.Anarchy);

        Facility residential = new DNHunters(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Hunter Gatherer residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }

    private void createBronzeAge(Planet planet, List<Facility> facilities) {
        System.out.println("createBronzeAge:");
        long population = Die.d6(4) * 250_000 + Die.die(250_000);
        planet.setPopulation(Physics.round(population));
        planet.setTechLevel(1);
        planet.setStarPort(StarPort.X);
        planet.addTradeCode(TradeCode.Ag, TradeCode.Ni);

        Facility residential = new DNBronzeAge(planet).getFacility();
        qualityCheck(residential);

        logger.debug("Setting Bronze Age residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);
    }
}
