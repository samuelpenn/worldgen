/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.worldgen.astro.Physics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.agriculture.Greenhouses;
import net.notasnark.worldgen.civ.facility.agriculture.XenoFarms;
import net.notasnark.worldgen.civ.facility.residential.DomedHabitats;
import net.notasnark.worldgen.civ.facility.starport.DomedStarPort;
import net.notasnark.worldgen.civ.facility.starport.SmallOrbital;

import java.util.ArrayList;
import java.util.List;

/**
 * A small colony consists of multiple settlements across a world, with a total population
 * less than a million.
 */
public class SmallColony extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Colony.class);

    public SmallColony(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }


    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding SmallColony to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        Planet planet = null;
        List<Facility> facilities = new ArrayList<Facility>();

        planet = getBestPlanet(4);
        if (planet == null) {
            return;
        }

        if (planet.getHabitability() <= 4) {
            createDomedHabitats(planet, facilities, features);
        }

        if (facilities.size() > 0) {
            generateDescription(planet, facilities);
        }
    }

    private void createDomedHabitats(Planet planet, List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating Domed Habitats");
        setFeatures(features);

        long population = Die.d6(5);
        planet.setTechLevel(6);
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 3_000;
            population += Die.die(3_000);
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 30_000;
            population += Die.die(30_000);
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 100_000;
            population += Die.die(100_000);
            planet.setTechLevel(7);
        } else {
            population *= 10_000;
            population += Die.die(10_000);
        }
        planet.setPopulation(Physics.round(population));

        Facility residential = new DomedHabitats(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 10);
        }

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        logger.debug("Setting star ports for " + planet.getStarPort());
        Facility lowPort = new DomedStarPort(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(lowPort);
        facilities.add(lowPort);
        if (planet.getStarPort() == StarPort.C) {
            Facility highPort = new SmallOrbital(planet).getFacility();
            worldGen.getPlanetFactory().setFacility(highPort);
            facilities.add(highPort);
        }

        logger.debug("Agriculture");
        if (planet.getHabitability() > 2) {
            Facility greenhouses = new Greenhouses(planet).getFacility();
            worldGen.getPlanetFactory().setFacility(greenhouses);
            facilities.add(greenhouses);
        } else {
            Facility xenoFarm = new XenoFarms(planet).getFacility();
            worldGen.getPlanetFactory().setFacility(xenoFarm);
            facilities.add(xenoFarm);
        }
    }
}
