/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.worldgen.astro.Physics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.mining.AsteroidMine;
import net.notasnark.worldgen.civ.facility.residential.BelterCity;
import net.notasnark.worldgen.civ.facility.residential.Belters;
import net.notasnark.worldgen.civ.facility.residential.MiningStation;
import net.notasnark.worldgen.civ.facility.starport.AsteroidDocks;
import net.notasnark.worldgen.civ.facility.starport.IndustrialDocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.notasnark.worldgen.civ.CivilisationFeature.*;

/**
 * This is a colony across an entire system, with a mix of Belters and gravity based colonies.
 * The main star port is a D class port. Population is in the hundreds of thousands.
 */
public class LightSystemColony extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(LightSystemColony.class);

    private Government   systemGovernment = Government.None;
    private Map<String,String> extras = new HashMap<>();

    public LightSystemColony(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    @Override
    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding Light System Colony to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        systemGovernment = Government.ParticipatingDemocracy;

        Planet bestPlanet = getBestPlanet(4);
        List<Facility> facilities = new ArrayList<Facility>();

        if (hasAsteroidBelt) {
            logger.debug("There is an asteroid belt");
            for (Planet p : system.getPlanets()) {
                if (p.isBelt()) {
                    coloniseBelt(p);
                }
            }
        } else {
            logger.debug("No asteroid belt");
        }

    }

    private void coloniseBelt(Planet belt) {
        logger.info("coloniseBelt: " + belt.getName());
        Planet biggest = null;

        // Find the biggest planetoid in this belt. This will be the capital.
        for (Planet p : system.getPlanets()) {
            if (p.isMoon() && p.getMoonOf() == belt.getId()) {
                if (biggest == null || p.getRadius() > biggest.getRadius()) {
                    biggest = p;
                }
            }
        }

        if (biggest != null) {
            logger.debug("Biggest planetoid is [" + biggest.getName() + "]");
            // At least one planetoid. Set it to be the main starport. Other asteroids are secondary.
            mainStarport(biggest);
            for (Planet p : system.getPlanets()) {
                if (p.isMoon() && p.getMoonOf() == belt.getId() && p.getId() != biggest.getId()) {
                    if (Die.d2() == 1) {
                        secondaryAsteroid(p);
                    } else {
                        miningSettlement(p);
                    }
                }
            }
            belters(belt);
        } else {
            // No major planetoids, just normal belters.
            belters(belt);
        }
    }

    /**
     * Main starport goes on the largest planetoid in the belt. This has the system's main starport, as
     * well as the largest single population centre.
     *
     * @param planet    Asteroid to put settlement on.
     */
    private void mainStarport(final Planet planet) {
        logger.info("mainStarport: " + planet.getName());
        List<Facility> facilities = new ArrayList<>();
        long population = 100_000 + Die.d6(3) * 10_000 + Die.rollZero(10_000);
        if (hasFeature(SmallPopulation)) {
            population *= 0.667;
        } else if (hasFeature(LargePopulation)) {
            population *= 2;
        } else if (hasFeature(HugePopulation)) {
            population *= 5;
        }

        planet.setPopulation(Physics.round(population, 4));
        planet.setGovernment(systemGovernment);
        Facility residential = new BelterCity(planet).getFacility(getFeatures(), Capital);
        facilities.add(residential);

        planet.setStarPort(StarPort.Do);
        Facility starport = new AsteroidDocks(planet).getFacility();
        facilities.add(starport);

        extras.put("Capital", planet.getName());

        generateDescription(planet, facilities);
    }

    /**
     * Settlement on other planetoids in the belts.
     *
     * @param planet    Asteroid to put settlement on.
     */
    private void secondaryAsteroid(final Planet planet) {
        logger.info("secondaryAsteroid:" + planet.getName());
        List<Facility> facilities = new ArrayList<>();
        long population = Die.d6(3) * 5_000 + Die.rollZero(5_000);
        planet.setPopulation(Physics.round(population, 3));
        planet.setGovernment(systemGovernment);
        Facility residential = new BelterCity(planet).getFacility(getFeatures());
        facilities.add(residential);

        planet.setStarPort(StarPort.Ho);
        Facility starport = new AsteroidDocks(planet).getFacility().features(getFeatures());
        facilities.add(starport);

        generateDescription(planet, facilities);
    }

    private void miningSettlement(final Planet planet) {
        logger.info("miningSettlement:" + planet.getName());

        List<Facility> facilities = new ArrayList<>();
        long population = Die.d6(3) * 300 + Die.rollZero(300);
        planet.setPopulation(Physics.round(population, 3));
        planet.setGovernment(Government.Corporation);
        Facility residential = new MiningStation(planet).getFacility().features(getFeatures());
        facilities.add(residential);

        Facility industrial = new AsteroidMine(planet).getFacility();
        facilities.add(industrial);

        planet.setStarPort(StarPort.Ho);
        Facility starport = new IndustrialDocks(planet).getFacility();
        facilities.add(starport);

        generateDescription(planet, facilities);
    }

    private void belters(final Planet planet) {
        List<Facility> facilities = new ArrayList<>();
        long population = Die.d6(5) * 100_000 + Die.rollZero(100_000);

        planet.setPopulation(Physics.round(population, 4));
        planet.setGovernment(systemGovernment);
        Facility residential = new Belters(planet).getFacility().features(getFeatures());
        if (extras.containsKey("Capital")) {
            residential.feature(Colony).extra("Capital", extras.get("Capital"));
        }
        facilities.add(residential);

        generateDescription(planet, facilities);
    }
}
