/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import net.notasnark.worldgen.astro.Physics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.civ.facility.agriculture.FoodVats;
import net.notasnark.worldgen.civ.facility.residential.Belters;
import net.notasnark.worldgen.civ.facility.residential.FreeCavers;
import net.notasnark.worldgen.civ.facility.residential.FreeHolding;
import net.notasnark.worldgen.civ.facility.starport.LandingField;
import net.notasnark.worldgen.civ.facility.starport.SmallDocks;

import java.util.ArrayList;
import java.util.List;

/**
 * Free Settlers are a range of anarchist or similar groups that have formed disperse communities
 * within a system. They tend to inhabit barren systems, which nobody else wants.
 */
public class FreeSettlers extends CivilisationGenerator {
    private static final Logger logger = LoggerFactory.getLogger(FreeSettlers.class);

    public FreeSettlers(WorldGen worldGen, StarSystem system) {
        super(worldGen, system);
    }

    public void generate(String civ, CivilisationFeature... features) {
        logger.info("Adding FreeSettlers to [" + system.getName() + "]");
        setFeatures(features);
        findPlanetsAvailable();

        Planet planet = null;
        List<Facility> facilities = new ArrayList<Facility>();

        if (hasHabitableWorld) {
            // In a Barren system, habitable worlds are unlikely. At best they would be class III,
            // where a breathing mask is required.
            logger.debug("hasHabitableWorld");
            planet = getBestPlanet(2);
            if (planet != null) {
                createFreeHolding(planet, facilities, features);
            }
        } else if (hasRockWorld) {
            // A rocky world where it's possible to stick a pressurised dome, or dig into tunnels,
            // without too many problems with heat or unpleasant atmosphere.
            logger.debug("hasRockWorld");
            isRockWorld(facilities, features);
        } else if (hasAsteroidBelt) {
            logger.debug("hasAsteroidBelt");
            // Rocky/metallic asteroid belt.
            for (Planet p : system.getPlanets()) {
                if (p.getType().getGroup() == PlanetGroup.Belt) {
                    planet = p;
                    createBelters(planet, facilities, features);
                    break;
                }
            }
        } else if (hasIceBelt) {
            // Icy belt in the outer reaches of the solar system. Least friendly of the options.
            logger.debug("hasIceBelt");
        }

        if (planet != null && facilities.size() > 0) {
            generateDescription(planet, facilities);
        }
    }

    /**
     * The main world of this system is an unfriendly rocky world.
     *
     * @param facilities
     * @param features
     */
    private void isRockWorld(final List<Facility> facilities, CivilisationFeature... features) {
        Planet main = getBestPlanet(6, PlanetGroup.Terrestrial, PlanetGroup.Dwarf);

        createFreeCavers(main, facilities, features);

        if (main != null && facilities.size() > 0) {
            generateDescription(main, facilities);
        }
    }

    private void createBelters(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating Belters");
        setFeatures(features);

        long population = Die.d100(3);
        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 1_000;
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 10_000;
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 30_000;
            planet.setTechLevel(6);
        } else {
            population *= 3_000;
        }
        population += Die.die(10_000); // Make the number look more natural.
        planet.setPopulation(Physics.round(population));

        Facility residential = new Belters(planet).getFacility();
        if (hasFeature(CivilisationFeature.Poor)) {
            residential.setRating(residential.getRating() - 25);
        } else if (hasFeature(CivilisationFeature.Rich)) {
            residential.setRating(residential.getRating() + 10);
        }

        logger.debug("Setting residential");
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        Facility port = new SmallDocks(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(port);
        facilities.add(port);
    }

    private void createFreeCavers(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("createFreeCavers:");
        setFeatures(features);

        planet.setTechLevel(6);
        long population = Die.d100(4);
        switch ((planet.getHabitability() > 4)?Die.d100():Die.d20()) {
            case 1:
                population *= 100;
                break;
            case 2: case 3:
                population *= 30;
                break;
            case 4: case 5: case 6:
                population *= 10;
                break;
            case 7: case 8: case 9: case 10:
                population *= 3;
                break;
            default:
                // No modifier.
        }

        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 100;
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 1_000;
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 3_000;
            planet.setTechLevel(7);
        } else {
            population *= 300;
        }
        int r = (int) Math.max(10_0000, population / 10);
        population += Die.die(r); // Make the number look more natural.
        planet.setPopulation(Physics.round(population));

        logger.debug("Setting residential");
        Facility residential = new FreeCavers(planet).getFacility(getFeatures());
        qualityCheck(residential);
        tradeCheck(planet);
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        logger.debug("Setting starport");
        Facility port = new LandingField(planet).getFacility();
        qualityCheck(port);
        worldGen.getPlanetFactory().setFacility(port);
        facilities.add(port);

        logger.debug("Setting food");
        Facility vats = new FoodVats(planet).getFacility();
        worldGen.getPlanetFactory().setFacility(vats);
        facilities.add(vats);
    }

    /**
     * Settlers on a world. These are generally small colonies of like minded individuals, willing to suffer
     * hardship in order to live how they want.
     *
     * @param planet        Planet to settle.
     * @param facilities    List of existing facilities.
     * @param features      List of features which may modify the new civilisation.
     */
    private void createFreeHolding(final Planet planet, final List<Facility> facilities, CivilisationFeature... features) {
        logger.info("Creating Small Settlement");
        setFeatures(features);

        planet.setTechLevel(5);
        long population = Die.d100(5);
        switch ((planet.getHabitability() > 3)?Die.d100():Die.d20()) {
            case 1:
                population *= 100;
                break;
            case 2: case 3:
                population *= 30;
                break;
            case 4: case 5: case 6:
                population *= 10;
                break;
            case 7: case 8: case 9: case 10:
                population *= 3;
                break;
            default:
                // No modifier.
        }

        if (hasFeature(CivilisationFeature.SmallPopulation)) {
            population *= 100;
        } else if (hasFeature(CivilisationFeature.LargePopulation)) {
            population *= 1_000;
        } else if (hasFeature(CivilisationFeature.HugePopulation)) {
            population *= 3_000;
            planet.setTechLevel(6);
        } else {
            population *= 300;
        }
        int r = (int) Math.max(10_0000, population / 10);
        population += Die.die(r); // Make the number look more natural.
        planet.setPopulation(Physics.round(population));

        logger.debug("Setting residential");
        Facility residential = new FreeHolding(planet).getFacility(getFeatures());
        qualityCheck(residential);
        worldGen.getPlanetFactory().setFacility(residential);
        facilities.add(residential);

        logger.debug("Setting starport");
        Facility port = new LandingField(planet).getFacility();
        qualityCheck(port);
        worldGen.getPlanetFactory().setFacility(port);
        facilities.add(port);
    }
}
