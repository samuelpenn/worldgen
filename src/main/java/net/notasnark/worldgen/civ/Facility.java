/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.civ;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "facilities")
public class Facility {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "planet_id")
    private int planetId;

    @Column
    private String name;

    @Column
    private String title;

    @Column (name="type")
    @Enumerated(EnumType.STRING)
    private FacilityType type;

    @Column (name="rating")
    private int rating;

    @Column (name="tech")
    private int tech;

    @Transient
    private int minTech = 0;
    @Transient
    private int maxTech = 20;

    @Transient
    protected Set<CivilisationFeature> features = new HashSet<>();

    @Transient
    protected Map<String, String> extras = new HashMap<>();

    public Facility() {
        // Empty constructor.
    }

    public Facility(int minTech, int maxTech) {
        this.minTech = minTech;
        this.maxTech = maxTech;
    }

    public int getId() {
        return id;
    }

    public int getPlanetId() {
        return planetId;
    }

    public void setPlanetId(int planetId) {
        this.planetId = planetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public FacilityType getType() {
        return type;
    }

    public void setType(FacilityType type) {
        this.type = type;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = Math.max(0, Math.min(500, rating));
    }

    public void modRating(int modifier) {
        setRating(getRating() + modifier);
    }

    public int getTechLevel() {
        return tech;
    }

    /**
     * Set the technology level of this facility, bounded by the minimum and
     * maximum.
     *
     * @param tech
     */
    public void setTechLevel(int tech) {
        this.tech = Math.max(minTech, Math.min(maxTech, tech));
    }

    /**
     * Modify the tech level by the given amount, bounded by the minimum and maximum for
     * this type of facility.
     *
     * @param modifier  Amount to increase or decrease tech level by.
     */
    public void modTechLevel(int modifier) {
        setTechLevel(getTechLevel() + modifier);
    }

    public void setFeatures(Set<CivilisationFeature> features) {
        if (features == null) {
            features = new HashSet<>();
        } else {
            this.features = features;
        }
    }

    public Facility features(Set<CivilisationFeature> features) {
        setFeatures(features);
        return this;
    }

    public Facility feature(CivilisationFeature feature) {
        addFeature(feature);
        return this;
    }

    public Facility rating(int rating) {
        setRating(rating);
        return this;
    }

    public Facility techlevel(int techLevel) {
        setTechLevel(techLevel);
        return this;
    }

    public Facility title(String title) {
        setTitle(title);
        return this;
    }

    public Facility name(String name) {
        setName(name);
        return this;
    }

    public Set<CivilisationFeature> getFeatures() {
        return features;
    }

    public boolean hasFeature(CivilisationFeature feature) {
        return features.contains(feature);
    }

    public void addFeature(CivilisationFeature feature) {
        features.add(feature);
    }

    public Facility extra(Map<String, String> extras) {
        this.extras = extras;
        return this;
    }

    public Facility extra(String key, String value) {
        extras.put(key, value);
        return this;
    }

    public Map<String, String> getExtras() {
        return extras;
    }

}
