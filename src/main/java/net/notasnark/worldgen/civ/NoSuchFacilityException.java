package net.notasnark.worldgen.civ;

import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.exceptions.NoSuchObjectException;

public class NoSuchFacilityException extends NoSuchObjectException {
    public NoSuchFacilityException(int id) {
        super(String.format("Cannot find facility [%d]", id));
    }

    public NoSuchFacilityException(Planet planet, String name) {
        super(String.format("Cannot find facility [%s] on planet [%d]", name, planet.getId()));
    }
}
