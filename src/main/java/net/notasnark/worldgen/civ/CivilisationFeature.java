/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.civ;

import java.util.Arrays;

public enum CivilisationFeature {
    // Population modifier.
    SmallPopulation,
    LargePopulation,
    HugePopulation,
    // General wealth modifier.
    Rich,
    Poor,
    // Specific sizes.
    Tiny,
    Small,
    Medium,
    Large,
    Huge,
    // Misc.
    Colony,
    Blackmarket,
    Corrupt,
    Unrest,
    Capital,
    Infected,
    Secondary,
    Collapsed;

    public CivilisationFeature[] addTo(CivilisationFeature... features) {
        var list = Arrays.asList(features);
        list.add(this);

        return list.toArray(new CivilisationFeature[0]);
    }
}
