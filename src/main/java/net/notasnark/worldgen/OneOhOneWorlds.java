package net.notasnark.worldgen;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.commodities.Commodity;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.commodities.Frequency;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.astro.planets.generators.dwarf.EuArean;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.EuGaian;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.planets.maps.dwarf.EuAreanMapper;
import net.notasnark.worldgen.astro.planets.maps.terrestrial.EuGaianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OneOhOneWorlds extends PlanetMapper {

    public OneOhOneWorlds(Planet planet, int faceSize) {
        super(planet, faceSize);
    }

    private static String generateGarden(WorldGen worldgen, int i) {
        String name = String.format("Garden%03d", i);
        Planet planet = new Planet();
        planet.setName(name);
        planet.setType(PlanetType.EuGaian);
        planet.setRadius(6400 + Die.dieV(1000));
        planet.setHydrographics(60 + Die.d10(2));
        planet.setTemperature(270 + Die.d6(2));
        planet.setPressure(100_000);
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setLife(Life.Extensive);
        planet.setHabitability(1);

        Commodity woods = new Commodity() {{
            setName(CommodityName.Woods.getName());
            setFrequency(Frequency.COMMON);
        }};
        planet.addResource(woods, 800 + Die.dieV(100));
        planet.addResource(new Commodity() {{
            setName(CommodityName.Grasses.getName());
            setFrequency(Frequency.COMMON);
        }}, 500 + Die.dieV(100));
        planet.addResource(new Commodity() {{
            setName(CommodityName.Shrubs.getName());
            setFrequency(Frequency.COMMON);
        }}, 300 + Die.dieV(50));

        String path = "wonders/" + name;
        try {
            EuGaian world = new EuGaian(worldgen, null, null, null, 0);

            if (Die.d6() == 1) {
                switch (Die.d6(2)) {
                    case 2:
                        planet.addFeature(Terrestrial.TerrestrialFeature.EquatorialOcean);
                        break;
                    case 3:
                        planet.addFeature(Terrestrial.TerrestrialFeature.ManyIslands);
                        break;
                    case 4:
                        planet.addFeature(Terrestrial.TerrestrialFeature.Pangaea);
                        break;
                    case 5:
                        planet.addFeature(Terrestrial.TerrestrialFeature.Dry);
                        planet.addResource(woods, 400 + Die.dieV(100));
                        break;
                    case 6:
                        planet.addFeature(Terrestrial.TerrestrialFeature.Wet);
                        break;
                    case 7:
                        planet.addFeature(Terrestrial.TerrestrialFeature.FractalIslands);
                        break;
                    case 8:
                        planet.addFeature(Terrestrial.TerrestrialFeature.Shallows);
                        break;
                    case 9:
                        planet.addFeature(Terrestrial.TerrestrialFeature.BacterialMats);
                        break;
                    case 10:
                        planet.addFeature(Terrestrial.TerrestrialFeature.BorderedInGreen);
                        break;
                    case 11:
                        planet.addFeature(Terrestrial.TerrestrialFeature.CyanSeas);
                        break;
                    case 12:
                        planet.addFeature(Terrestrial.TerrestrialFeature.Cold);
                        break;
                }
            }
            planet.addFeature(Terrestrial.TerrestrialFeature.Cold);

            PlanetMapper.testOutput(planet,
                    world,
                    new EuGaianMapper(planet, 48),
                    path);

            new File(path+"/"+name+"-Clouds.png").delete();
            new File(path+"/"+name+"-Heat.png").delete();
            new File(path+"/"+name+"-Wet.png").delete();

            File text = new File(path+"/"+name+".txt");
            FileWriter writer = new FileWriter(text);
            writer.write("name: " + name + "\n");
            writer.write("type: " + planet.getType() + "\n");
            writer.write("hydrographics: " + planet.getHydrographics() + "\n");
            writer.write("radius:" + planet.getRadius() + "\n");
            writer.write("temperature:" + planet.getTemperature() + "\n");
            if (planet.getFeatures().size() > 0) {
                writer.write("features:");
                for (PlanetFeature f : planet.getFeatures()) {
                    writer.write(" " + f.toString());
                }
                writer.write("\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return path;
    }

    public static void main(String[] args) {
        WorldGen worldgen = Server.getWorldGen();
        for (int i=61; i <= 101; i++) {
            //generateGarden(worldgen, i);
        }
        generateGarden(worldgen, 157);
    }
}
