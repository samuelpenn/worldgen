/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.Universe;
import net.notasnark.worldgen.astro.planets.NoSuchPlanetException;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.*;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.astro.systems.generators.Deepnight;
import net.notasnark.worldgen.astro.systems.generators.UWPSystem;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import net.notasnark.worldgen.exceptions.NoSuchObjectException;
import net.notasnark.worldgen.web.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Command line based interface to WorldGen. Provides a number of simple commands that allow
 * the universe to be manipulated directly from the command line. Mostly designed for testing
 * purposes, but can be used to manipulate real data.
 */
public class CommandLine extends Main {
    private static final Logger logger = LoggerFactory.getLogger(CommandLine.class);

    private CommandLine() {
    }

    private void usage() {
        System.out.println(String.format("WorldGen (%s)", WorldGen.getFullVersion()));
        System.out.println("<command> <options>");
        System.out.println("  status   - Get status on configured universe.");
        System.out.println("  server   - Start a web application server running.");
        System.out.println("  sectors  - List all known sectors.");
        System.out.println("  sector   - Create one or more new sectors.");
        System.out.println("             <x,y> <name> ...");
        System.out.println("  listsec  - List systems in a sector.");
        System.out.println("             <x,y>");
        System.out.println("  system   - Create one or more new systems.");
        System.out.println("             <sector> <xxyy> [<name>]");
        System.out.println("  populate - Populate a sector.");
        System.out.println("             <sector> <subsector>");
        System.out.println("  sec      - Create from UWP data");
        System.out.println("             <datafile>");
        System.out.println("  print    - Print out data on a star system");
        System.out.println("             <systemid>");
        System.out.println("  time       Display or set the current time");
        System.out.println("             <datetime>");
        System.out.println("  map        Display or set a planet's maps");
        System.out.println("             <planetid> [<map> [<filename>]]");
        System.out.println("  text       Write (or read) description of planet to file");
        System.out.println("             <planetid> [<filename>]");
        System.out.println("  galaxy   - Upload a PNG as galaxy map");
        System.out.println("             <filename>");
        System.out.println("  version  - Print out the current version");
    }

    private void execute(String[] args) {

        if (args.length == 0) {
            usage();
            return;
        }

        String cmd = args[0];
        String[] options;

        options = Arrays.copyOfRange(args, 1, args.length);
        if (args.length > 1) {
            options = Arrays.copyOfRange(args, 1, args.length);
        } else {
            options = new String[]{};
        }

        if (cmd.equals("status")) {
            commandStatus(options);
        } else if (cmd.equals("server")) {
            commandServer(options);
        } else if (cmd.equals("sectors")) {
            commandListSectors(options);
        } else if (cmd.equals("sector")) {
            commandCreateSector(options);
        } else if (cmd.equals("listsec")) {
            commandListSector(options);
        } else if (cmd.equals("system")) {
            commandCreateSystem(options);
        } else if (cmd.equals("populate")) {
            commandPopulateSector(options);
        } else if (cmd.equals("deepnight")) {
            commandDeepnight(options);
        } else if (cmd.equals("print")) {
            commandPrintSystem(options);
        } else if (cmd.equals("addmoon")) {
            commandAddMoon(options);
        } else if (cmd.equals("sec")) {
            commandSecData(options);;
        } else if (cmd.equals("version")) {
            System.out.println(getWorldGen().getFullVersion());
        } else if (cmd.equals("time")) {
            commandTime(options);
        } else if (cmd.equals("map")) {
            commandMap(options);
        } else if (cmd.equals("text")) {
            commandText(options);
        } else if (cmd.equals("galaxy")) {
            commandGalaxy(options);
        } else if (cmd.equals("rename")) {
            commandRename(options);
        } else if (cmd.equals("colour")) {
            commandColour(options);
        } else if (cmd.equals("output")) {
            commandOutput(options);
        }

    }

    private void print(String key, String value) {
        System.out.printf("%s: %s%n", key, value);
    }

    private void print(String key, int value) {
        System.out.printf("%s: %d%n", key, value);
    }

    private void print(String key, long value) {
        System.out.printf("%s: %d%n", key, value);
    }

    private void print(String key, Date value) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        print(key, format.format(value));
    }

    /**
     * Prints out the current status of the universe. This includes current time and some configuration
     * values, as well as some useful statistics.
     *
     * @param options  Options, ignored.
     */
    private void commandStatus(String[] options) {
        try (WorldGen wg = getWorldGen()) {
            Universe u = wg.getUniverse();

            print("Universe", u.getName());
            print("Created Date", u.getCreatedDate());
            print("Last Date", u.getLastDate());
            print("Current Time", u.getCurrentDateTime());

            print("Number of Sectors", wg.getSectorFactory().getSectors().size());
            print("Number of Systems", wg.getStarSystemFactory().getStarSystemCount());
            print("Number of Planets", wg.getPlanetFactory().getPlanetCount());
        }
    }

    /**
     * Starts the web server running.
     *
     * @param options   Options, ignored.
     */
    private void commandServer(String[] options) {
        Server server = new Server();

        server.startServer();
    }

    private void commandListSectors(String[] options) {
        try (WorldGen wg = getWorldGen()) {
            List<Sector> sectors = wg.getSectorFactory().getSectors();

            for (Sector sector : sectors) {
                System.out.println(String.format("%d %d,%d %s",
                        sector.getId(), sector.getX(), sector.getY(), sector.getName()));
            }
        }
    }

    private void commandListSector(String[] options) {
        if (options.length != 1) {
            System.out.println("Usage: listsec <x,y>");
            return;
        }
        try (WorldGen wg = Main.getWorldGen()) {
            SectorFactory factory = wg.getSectorFactory();
            StarSystemFactory systemFactory = wg.getStarSystemFactory();
            String coords = options[0];
            if (!SectorFactory.isCoord(coords)) {
                System.out.println(String.format("Expected coordinate pair, instead got [%s]", coords));
                return;
            }

            int x = SectorFactory.getXCoord(coords);
            int y = SectorFactory.getYCoord(coords);

            Sector sector = factory.getSector(x, y);
            List<StarSystem> systems = systemFactory.getStarSystems(sector);

            for (StarSystem system : systems) {
                System.out.println(String.format("%s %-20s %-2s %2d %12d %s",
                        system.getXY(),
                        system.getName(),
                        system.getStarPort(),
                        system.getTechLevel(),
                        system.getPopulation(),
                        (system.getColour()!=null)?system.getColour():"#FFFFFF")
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an empty sector. Options contains the list of arguments, being the X and Y coordinate of the
     * sector, followed by an optional name. If no name is given, a name is determined based on the location
     * of the sector.
     *
     * @param options   X, Y, [Name]
     */
    private void commandCreateSector(String[] options) {
        if (options.length < 1) {
            System.out.println("Usage: sector <x,y> [<name>] ...");
            return;
        }
        try (WorldGen wg = Main.getWorldGen()) {
            SectorFactory factory = wg.getSectorFactory();
            while (options.length > 0) {
                String coords = options[0];
                if (!SectorFactory.isCoord(coords)) {
                    System.out.println(String.format("Expected coordinate pair, instead got [%s]", coords));
                    return;
                }

                int x = SectorFactory.getXCoord(coords);
                int y = SectorFactory.getYCoord(coords);

                String name = "";

                if (options.length > 1 && !SectorFactory.isCoord(options[1])) {
                    // Needs to cope with sector names with spaces.
                    options = Arrays.copyOfRange(options, 1, options.length);
                    while ( options.length > 0 && !options[0].matches("[-0-9].*") ) {
                        if (name.length() > 0) {
                            name += " ";
                        }
                        name += options[0];
                        options = Arrays.copyOfRange(options, 1, options.length);
                    }
                } else {
                    name = "Sector " + SectorFactory.getSectorNumber(x, y);
                    options = Arrays.copyOfRange(options, 1, options.length);
                }
                if (factory.hasSector(x, y)) {
                    continue;
                }

                Sector sector = factory.createSector(name, x, y);

                System.out.println(String.format("Created sector [%d] [%s]", sector.getId(), sector.getName()));
            }
        } catch (DuplicateObjectException e) {
            e.printStackTrace();
        } finally {
            logger.debug("Finished.");
        }
    }

    /**
     * Searches for an empty parsec within the sector. If one is found, returns x and y coordinates
     * in an array. If not found, returns null. Looks for a random location first, and if that fails
     * starts searching from top left to bottom right.
     *
     * @param sector    Sector to search in.
     * @param factory   Factory for finding systems.
     * @return          Array of { x, y } if empty parsec found, otherwise null.
     */
    private int[] findEmptyParsec(Sector sector, StarSystemFactory factory) {
        int y = Die.die(40);
        int x = Die.die(32);
        try {
            factory.getStarSystem(sector, x, y);
        } catch (NoSuchStarSystemException e) {
            // This is an empty parsec, so return x and y values in array.
            return new int[] { x, y };
        }

        for (y = 1; y <= 40; y++) {
            for (x = 1; x < 32; x++) {
                try {
                    factory.getStarSystem(sector, x, y);
                } catch (NoSuchStarSystemException e) {
                    // This is an empty parsec, so return x and y values in array.
                    return new int[] { x, y };
                }
            }
        }
        return null;
    }

    private void commandCreateSystem(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            SectorFactory sectorFactory = wg.getSectorFactory();
            StarSystemFactory factory = wg.getStarSystemFactory();

            String sectorId = options[0];
            Sector sector = sectorFactory.getSectorByIdentifier(sectorId);

            String  coord = options[1].toLowerCase();
            int     x, y;
            if (coord.startsWith("x")) {
                int[] coords = findEmptyParsec(sector, factory);
                if (coords == null || coords.length != 2) {
                    throw new IllegalStateException("Unable to locate an empty hex within this sector.");
                }
                x = coords[0];
                y = coords[1];
            } else {
                x = StarSystemFactory.getXCoord(coord);
                y = StarSystemFactory.getYCoord(coord);
            }

            String name;

            if (options.length < 4) {
                if (options.length == 3) {
                    name = options[2];
                } else {
                    name = wg.getStarSystemNameGenerator().generateName();
                }
                StarSystemSelector selector = new StarSystemSelector(wg);
                selector.createRandomSystem(sector, name, x, y);
            } else {
                String generator = options[2];
                String type = options[3];
                String civ = null;

                if (options.length > 4) {
                    civ = options[4];
                }

                if (options.length > 5) {
                    name = options[5];
                } else {
                    name = wg.getStarSystemNameGenerator().generateName();
                }
                StarSystemSelector selector = new StarSystemSelector(wg);
                selector.setCivilisationType(civ);
                StarSystem system = selector.createByGeneratorType(sector, name, x, y, generator, type);

                System.out.println(system + ", Id: " + system.getId());
            }

        } catch (NoSuchSectorException e) {
            System.out.println(e.getMessage());
            return;
        } catch (DuplicateObjectException e) {
            e.printStackTrace();
        }
    }

    private void commandPopulateSector(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            SectorFactory sectorFactory = wg.getSectorFactory();

            String      sectorId = options[0];
            SubSector   subSector = null;
            Sector      sector = sectorFactory.getSectorByIdentifier(sectorId);

            if (options.length > 1) {
                subSector = SubSector.valueOf(options[1]);
            }
            SectorGenerator generator = new SectorGenerator(wg);
            generator.createSectorByDensity(sector, subSector);
        } catch (NoSuchSectorException e) {
            System.out.println(e.getMessage());
            return;
        }
    }

    private void commandDeepnight(String[] options) {
        String      sectorId = options[0];

        for (SubSector subSector : SubSector.values()) {
            try (WorldGen wg = Main.getWorldGen()) {
                SectorFactory sectorFactory = wg.getSectorFactory();
                Sector sector = sectorFactory.getSectorByIdentifier(sectorId);

                SectorGenerator generator = new SectorGenerator(wg);
                Deepnight deepnight = new Deepnight(wg);
                generator.createSectorByDensity(sector, subSector, deepnight);

            } catch (NoSuchSectorException e) {
                System.out.println(e.getMessage());
                return;
            }
        }
    }

    private void commandPrintSystem(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            PlanetFactory planetFactory = wg.getPlanetFactory();
            StarSystemFactory systemFactory = wg.getStarSystemFactory();
            int           systemId = Integer.parseInt(options[0]);
            StarSystem    system = systemFactory.getStarSystem(systemId);

            List<Planet> planets = planetFactory.getPlanets(system);

            NumberFormat format = NumberFormat.getInstance();
            for (Planet planet : planets) {
                if (!planet.isMoon()) {
                    System.out.printf("%s%s [%d] %s\n", planet.getName(),
                            (planet.getPopulation() > 0)?" *":"",
                            planet.getId(), planet.getType());
                    System.out.printf("  T: %dK R: %skm D: %skm\n",
                            planet.getTemperature(),
                            format.format(planet.getRadius()),
                            format.format(planet.getDistance()));
                    for (Planet moon : planets) {
                        if (moon.getMoonOf() == planet.getId()) {
                            System.out.printf("    %s%s [%d] %s - T: %dK R: %skm D: %skm\n",
                                    moon.getName(), (moon.getPopulation() > 0)?" *":"",
                                    moon.getId(), moon.getType(),
                                    moon.getTemperature(),
                                    format.format(moon.getRadius()),
                                    format.format(moon.getDistance()));
                        }
                    }
                }
            }

        } catch (NoSuchStarSystemException e) {
            e.printStackTrace();
        }
    }

    private void commandAddMoon(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            int           planetId = Integer.parseInt(options[0]);
            PlanetType type = PlanetType.valueOf(options[1]);

            PlanetFactory planetFactory = wg.getPlanetFactory();
            Planet        planet = planetFactory.getPlanet(planetId);

            planetFactory.addMoon(planet, type);

        } catch (NoSuchObjectException e) {
            e.printStackTrace();
        }
    }

    private void commandTime(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            if (options.length == 0) {
                long time = wg.getCurrentTime();
                System.out.println(Physics.getDate(time));
            } else {
                String arg = options[0];
                boolean relative = false;
                if (arg.startsWith("+") || arg.startsWith("-")) {
                    relative = true;
                }
                long time = Physics.getSeconds(arg);
                if (relative) {
                    long currentTime = wg.getCurrentTime();
                    time = currentTime + time;
                }
                wg.setCurrentTime(time);
                System.out.println(Physics.getDate(time));
            }
        }
    }

    private void commandSecData(String[] options) {
        try {
            String  filename = options[0];
            File    file = new File(filename);

            if (!file.exists()) {
                System.out.println("No such file [" + filename + "]");
                return;
            }

            BufferedReader reader = new BufferedReader(new FileReader(file));

            Sector  sector = null;
            boolean worldData = false;

            String line = reader.readLine();
            while (line != null) {
                if (line.matches("# [-0-9]+,[-0-9]+")) {
                    // Sector coordinates
                    System.out.println(line);
                    line = line.replaceAll("[^-0-9,]", "");
                    int sx = SectorFactory.getXCoord(line);
                    int sy = 0 - SectorFactory.getYCoord(line);
                    System.out.println(sx + "," + sy);

                    try (WorldGen wg = Main.getWorldGen()) {
                        sector = wg.getSectorFactory().getSector(sx, sy);
                    } catch (NoSuchSectorException e) {
                        System.out.println("Cannot find sector at ["+line+"]");
                        return;
                    }
                }
                if (line.startsWith("...")) {
                    if (sector == null) {
                        System.out.println("Found world data without sector identifier");
                        return;
                    }
                    // Start of world data.
                    worldData = true;
                    line = reader.readLine();
                    continue;
                }
                if (worldData && line.length() > 0) {
                    System.out.println(line);
                    try (WorldGen wg = Main.getWorldGen()) {
                        parseUWP(wg, sector, line);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                line = reader.readLine();
            }
            reader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseUWP(WorldGen wg, Sector sector, String uwp) {
        String name = uwp.substring(0, 14).trim();
        String coord = uwp.substring(14, 18);


        int   x = StarSystemFactory.getXCoord(coord);
        int   y = StarSystemFactory.getYCoord(coord);

        if (name.length() == 0) {
            name = SubSector.getSubSector(x, y).toString() + coord;
        }

        StarSystemFactory factory = wg.getStarSystemFactory();

        try {
            factory.getStarSystem(sector, x, y);
            return;
        } catch (Exception e) {
            // No duplicate.
        }
        try {
            while (true) {
                factory.getStarSystem(sector, name);
                name += "i";
            }
        } catch (NoSuchStarSystemException e) {
            // This is good.
        }
        try {
            UWP data = new UWP(name, uwp);
            if ((data.getStars() == null || data.getStars().length == 0) && data.getPopulation() == 0) {
                logger.warn("System has no stars and no people, using random generation");
                Deepnight generator = new Deepnight(wg);
                generator.generate(sector, data.getName(), x, y);
            } else {
                logger.info("Creating UWP system for " + data.getUWP());
                UWPSystem generator = new UWPSystem(wg);
                generator.createUWPSystem(sector, data);
            }
        } catch (DuplicateStarSystemException e) {
            e.printStackTrace();
        } catch (DuplicateObjectException e) {
            e.printStackTrace();
        }
    }

    private void commandText(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            if (options.length == 0) {
                System.out.println("Arguments: <planetid> [<path_to_file>]");
            } else {
                int id = Integer.parseInt(options[0]);
                PlanetFactory factory = wg.getPlanetFactory();
                Planet planet = factory.getPlanet(id);

                if (options.length == 2) {
                    String filename = options[1];
                    File file = new File(filename);
                    InputStream in = new FileInputStream(file);
                    String text = new String(in.readAllBytes());
                    planet.setDescription(text);
                    factory.persist(planet);
                } else {
                    File file = new File(id + ".html");
                    OutputStream out = new FileOutputStream(file);
                    out.write(planet.getDescription().getBytes());
                    out.close();
                    System.out.println(id + ".html");
                }
            }
        } catch (NoSuchPlanetException e) {
            System.out.println("Unable to find planet");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void commandGalaxy(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            if (options.length == 0) {
                System.out.println("Arguments: <path to PNG file>");
            } else {
                String filename = options[0];
                SimpleImage image = new SimpleImage(new File(filename));
                wg.setGalaxyMap(image);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * List, get or set maps for a planet. Given just a planet id, will list all the maps available
     * for that planet. Specifying a map will download the map and write it to a local file. Specifying
     * a filename will upload that file, and replace any existing map.
     */
    private void commandMap(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            if (options.length == 0) {
                System.out.println("Arguments: <planetid> [[<map>] <path_to_image>]");
            } else if (options.length > 0) {
                int id = Integer.parseInt(options[0]);
                PlanetFactory factory = wg.getPlanetFactory();
                try {
                    Planet planet = factory.getPlanet(id);
                    List<String> maps = factory.getPlanetMaps(id);

                    if (options.length == 1) {
                        for (String map : maps) {
                            System.out.println(map);
                        }
                    } else if (options.length == 2) {
                        String mapName = options[1];
                        boolean stretch = false;

                        if (mapName.endsWith("-stretch")) {
                            stretch = true;
                            mapName = mapName.replaceAll("-stretch", "");
                        }

                        if (maps.contains(mapName)) {
                            SimpleImage image = factory.getPlanetMap(id, mapName);
                            String filename = String.format("%d-%s%s.png", id, mapName, stretch?"-stretch":"");

                            if (stretch) {
                                image = Icosahedron.stretchImage(image, 2048);
                            }

                            image.save(new File(filename));
                            System.out.printf("Written map to %s\n", filename);
                        } else {
                            System.out.println("No such map");
                        }
                    } else if (options.length == 3) {
                        String mapName = options[1];
                        String filename = options[2];
                        File   file = new File(filename);
                        if (!file.exists()) {
                            System.out.printf("File [%s] does not exist", filename);
                            return;
                        }
                        SimpleImage image = new SimpleImage(file);
                        factory.setPlanetMap(id, mapName, image);
                    }
                } catch (NoSuchPlanetException | IOException e) {
                    System.out.println("Cannot find planet with id [" + id + "]");
                }
            }
        }
    }

    public void commandRename(String[] options) {
        int systemId = 0;
        int start = 1;

        String sectorId = null, coord = null;
        try {
            systemId = Integer.parseInt(options[0]);
        } catch (NumberFormatException e) {
            sectorId = options[0];
            coord = options[1];
            start = 2;
        }

        String name = options[start++];
        for (int i = start; i < options.length; i++) {
            name += " " + options[i];
        }
        try (WorldGen wg = Main.getWorldGen()) {
            StarSystemFactory starSystemFactory = wg.getStarSystemFactory();
            PlanetFactory planetFactory = wg.getPlanetFactory();

            StarSystem system;
            if (systemId == 0) {
                SectorFactory sectorFactory = wg.getSectorFactory();
                Sector sector = sectorFactory.getSectorByIdentifier(sectorId);
                int x, y;
                x = StarSystemFactory.getXCoord(coord);
                y = StarSystemFactory.getYCoord(coord);
                system = starSystemFactory.getStarSystem(sector, x, y);
            } else {
                system = starSystemFactory.getStarSystem(systemId);
            }

            List<Planet> planets = planetFactory.getPlanets(system);
            system.addPlanets(planets);

            system.rename(name);
            starSystemFactory.persist(system);

        } catch (NoSuchStarSystemException e) {
            System.out.println("No such star system with id [" + systemId + "]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void commandOutput(String[] options) {
        int systemId = 0;

        String sectorId = options[0], coord = options[1];

        try (WorldGen wg = Main.getWorldGen()) {
            StarSystemFactory starSystemFactory = wg.getStarSystemFactory();
            PlanetFactory planetFactory = wg.getPlanetFactory();

            StarSystem system;
            SectorFactory sectorFactory = wg.getSectorFactory();
            Sector sector = sectorFactory.getSectorByIdentifier(sectorId);
            int x, y;
            x = StarSystemFactory.getXCoord(coord);
            y = StarSystemFactory.getYCoord(coord);
            system = starSystemFactory.getStarSystem(sector, x, y);

            List<Planet> planets = planetFactory.getPlanets(system);
            system.addPlanets(planets);

            String baseDir = "output/" + sector.getName().replaceAll(" ", "_") + "/";
            for (Planet p : planets) {
                String name = p.getName().replaceAll("/", "_").replaceAll(" ", "_");
                String dir = baseDir + system.getName().replaceAll(" ", "_") + "/" + p.getName();
                new File(dir).mkdirs();
                List<String> maps = planetFactory.getPlanetMaps(p.getId());
                for (String m : maps) {
                    SimpleImage image = planetFactory.getPlanetMap(p.getId(), m);
                    String filename = String.format("%s-%s.png", name, m);
                    image.save(new File(dir + "/" + filename));
                    image = Icosahedron.stretchImage(image, 2048);
                    filename = String.format("%s-%s_stretch.png", name, m);
                    image.save(new File(dir + "/" + filename));

                    System.out.printf("Written map to %s\n", filename);
                }
            }

        } catch (NoSuchSectorException e) {
            throw new RuntimeException(e);
        } catch (NoSuchStarSystemException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {

        }
    }

    public void commandColour(String[] options) {
        try (WorldGen wg = Main.getWorldGen()) {
            if (options.length < 2) {
                System.out.println("Arguments: <sector> <xxyy> [<colour>]");
                return;
            }

            SectorFactory sectorFactory = wg.getSectorFactory();
            StarSystemFactory factory = wg.getStarSystemFactory();

            String sectorId = options[0];
            Sector sector = sectorFactory.getSectorByIdentifier(sectorId);

            String coord = options[1].toLowerCase();
            int x, y;
            x = StarSystemFactory.getXCoord(coord);
            y = StarSystemFactory.getYCoord(coord);

            StarSystemFactory systemFactory = wg.getStarSystemFactory();
            StarSystem system = systemFactory.getStarSystem(sector, x, y);
            String      name = system.getName() + " (" + system.getId() + ")";
            if (options.length < 3) {
                if (system.getColour() == null) {
                    System.out.println(name + " has no colour set");
                    return;
                }
                System.out.println(name + " " + system.getColour());
            } else {
                String colour = options[2].toUpperCase();
                if (colour.length() < 7) {
                    colour = "#" + colour;
                }
                system.setColour(colour);
                systemFactory.persist(system);
                System.out.println(name + " set to colour " + colour);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        logger.info(String.format("== WorldGen CommandLine (%s) ==", WorldGen.getFullVersion()));

        try {
            CommandLine cmd = new CommandLine();

            cmd.execute(args);
        } catch (Exception e) {
            System.out.println(String.format("Execution exception (%s)", e.getMessage()));
            e.printStackTrace();
        } finally {
            //System.exit(0);
        }

    }
}
