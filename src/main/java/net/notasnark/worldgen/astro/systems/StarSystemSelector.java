/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.systems.generators.*;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.sectors.Sector;

/**
 * Selects what type of star system to generate.
 */
public class StarSystemSelector {
    private static final Logger logger = LoggerFactory.getLogger(StarSystemSelector.class);

    private final WorldGen worldgen;
    private String civName = null;

    public StarSystemSelector(WorldGen worldgen) {
        this.worldgen = worldgen;
    }

    private void validate(Sector sector, String name, int x, int y) {
        if (sector == null || sector.getId() == 0) {
            throw new IllegalArgumentException("StarSystem must be part of an existing Sector");
        }

        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException("StarSystem name cannot be empty");
        }

        if (x < 1 || x > Sector.WIDTH || y < 1 || y > Sector.HEIGHT) {
            throw new IllegalArgumentException(
                    String.format("StarSystem [%s] at [%d,%d] is outside of normal sector boundary",
                            name, x, y));
        }
    }

    public StarSystem createSimpleSystem(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        logger.info(String.format("createSimpleSystem: [%s] [%02d%02d] [%s]", sector.getName(), x, y, name));

        validate(sector, name, x, y);

        StarSystemGenerator generator = new Simple(worldgen);

        return generator.generate(sector, name, x, y);
    }

    /**
     * Generate a new star system using the named generator and type. The generator is the name of the class
     * that should be used. It will be of type StarSystemGenerator, and be part of the
     * net.notasnark.worldgen.astro.systems.generators package. The type will be used to determine the
     * method name, using the pattern (create{TypeName}).
     *
     * @param sector    Sector to create star system in.
     * @param name      Name to give to star system.
     * @param x         X coordinate (01..32)
     * @param y         Y coordinate (01..40)
     * @param generator Name of star system generator to use (class name).
     * @param type      Type of system generator (method name).
     * @return          Newly created star system.
     * @throws UnsupportedException     If the generator is not supported.
     * @throws DuplicateObjectException If there is already a star system here.
     */
    public StarSystem createByGeneratorType(Sector sector, String name, int x, int y, String generator, String type) throws UnsupportedException, DuplicateObjectException {
        logger.info(String.format("createNamedType: [%s] [%02d%02d] [%s]", sector.getName(), x, y, name));

        validate(sector, name, x, y);

        NamedGenerator g = new NamedGenerator(worldgen);
        if (civName != null) {
            g.setCivilisationName(civName);
        }
        return g.generate(sector, name, x, y, generator, type);
    }

    public void setCivilisationType(String civName) {
        this.civName = civName;
    }


    /**
     * Generate a complete random star system. This selects a type of star system from all the
     * available types, so provides the most diverse range of options.
     *
     * @param sector    Sector to create star system in.
     * @param name      Name of system to be created.
     * @param x         X coordinate within the sector (1-32)
     * @param y         Y coordinate within the sector (1-40)
     *
     * @return          A newly created star system.
     * @throws DuplicateObjectException     Star system already exists at location.
     */
    public StarSystem createRandomSystem(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        logger.info(String.format("createRandomSystem: [%s] [%02d%02d] [%s]", sector.getName(), x, y, name));

        validate(sector, name, x, y);

        StarSystemGenerator generator = null;
        switch (Die.d6(3)) {
            case 3: case 4:
                generator = new BrownDwarf(worldgen);
                break;
            case 5:
                generator = new Barren(worldgen);
                break;
            case 6:
                generator = new Barren(worldgen);
                break;
            case 7:
                generator = new Barren(worldgen);
                break;
            case 8: case 9:
                generator = new Simple(worldgen);
                break;
            case 10: case 11: case 12:
                generator = new Barren(worldgen);
                break;
            case 13: case 14:
                generator = new YellowStar(worldgen);
                break;
            case 15: case 16:
                generator = new Simple(worldgen);
                break;
            case 17:
                generator = new Barren(worldgen);
                break;
            case 18:
                generator = new BlueGiant(worldgen);
                break;
        }
        System.out.println("Selected generator " + generator.getClass().getSimpleName());
        return generator.generate(sector, name, x, y);
    }

}
