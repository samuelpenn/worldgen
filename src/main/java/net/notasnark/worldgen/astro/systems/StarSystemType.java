/*
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.systems;

/**
 * Describes the general configuration of a star system, including the number of stars and the orbital
 * structure which binds them together. This limits star systems to certain configurations for purposes
 * of simplicity.
 */
public enum StarSystemType {
    /**
     * An EMPTY system has no stars or planets. It may be created for book keeping purposes only.
     * Generally they are named simply after their sector coordinates, e.g. 0431.
     */
    EMPTY(0),
    /**
     * A SINGLE system is the classic single star at the centre of the system, with planets orbiting
     * around it. This is the most common type of system for implementation purposes.
     */
    SINGLE(1),
    /**
     * A CONJOINED_BINARY consists of two stars in very close orbit around each other (within a few
     * million kilometres at most, often a hundred thousand kilometres). The stars are close enough
     * that their atmospheres are touching.
     *
     * The most common configuration is two very similar stars orbiting a common centre of gravity.
     * Planets orbit the centre of gravity, at a distance at least ten times the distance between
     * the two stars.
     *
     * Some options have a giant or super giant star with a white dwarf or similar small companion.
     * In this case the giant star is at the centre, and the dwarf star orbits it. Planets orbit
     * the giant at at least 1 AU distance, probably much more.
     */
    CONJOINED_BINARY(2),
    /**
     * A CLOSE_BINARY is where two stars orbit within 1 AU of each other. They normally orbit a common
     * centre of gravity, and planets orbit the centre of gravity. Planets are at least ten times the
     * distance between the two stars.
     */
    CLOSE_BINARY(2),
    /**
     * A MEDIUM_BINARY consists of two stars between 1 AU and 10 AU of each other. They are often
     * similar in mass, and orbit a common centre of gravity. Any planets orbit outside both stars
     * at least ten times the distance between the two stars.
     */
    MEDIUM_BINARY(2),
    /**
     * A FAR_BINARY consists of two stars more than 10 AU apart. Any planets orbit each individual
     * star, and cannot be more than 1/8th the distance between the stars in order to remain stable.
     * The stars are just as likely to be different as similar.
     */
    FAR_BINARY(2),
    TRIPLE(3),
    /**
     * A ROGUE_PLANET system has a single planet with no stars. Such planets are generally large frigid
     * worlds, and may have moons. The planet sits at the centre of the system.
     */
    ROGUE_PLANET(0);

    private final int numStars;

    private StarSystemType(final int numStars) {
        this.numStars = numStars;
    }

    public int getNumberOfStars() {
        return numStars;
    }
}
