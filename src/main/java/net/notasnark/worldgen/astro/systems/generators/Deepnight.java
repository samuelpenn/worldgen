/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Belt;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.astro.systems.generators.deepnight.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.civilisation.DeepnightCiv;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;

import java.util.List;

/**
 * Generate a barren, type M or K, star system. These systems have no native life, the worlds are often
 * cold and uninhabitable. Quite simple systems.
 */
public class Deepnight extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Deepnight.class);

    public Deepnight(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        logger.info("Deepnight system generator");
        StarSystem system = null;

        Deepnight generator = null;
        switch (Die.d6(3)) {
            case 3:
                generator = new DNBarrenM(worldgen);
                break;
            case 4:
                generator = new DNBarrenA(worldgen);
                break;
            case 5:
                generator = new DNBarrenF(worldgen);
                break;
            case 6: case 7:
                generator = new DNBarrenG(worldgen);
                break;
            case 8: case 9:
                generator = new DNBarrenK(worldgen);
                break;
            case 10: case 11:
            case 12: case 13:
                generator = new DNHabitableG(worldgen);
                break;
            default:
                generator = new DNBarrenM(worldgen);
                break;
        }
        if (generator != null) {
            logger.info("Generate " + generator.getClass().getSimpleName());
            system = generator.generate(sector, name, x, y);
            updateStarSystem(system);
        }
        //createDummy(system);

        return system;
    }

    /**
     * Generate a rogue star system. This will probably be a Planemo.
     */
    public StarSystem generateRogue(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        createRogue(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise star system " + system.getName());

        CivilisationGenerator generator = new DeepnightCiv(worldgen, system);
        generator.generate();
    }

    public void createTest(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Dummy] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        int  orbit = 1;
        long distance = Physics.getOrbitWithTemperature(primary, 250);
        PlanetType type = PlanetType.EuGaian;
        String name = StarSystemFactory.getPlanetName(primary, orbit++);

        List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
        system.addPlanets(planets);

        setDescription(system, null);

    }

    public void createDummy(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Dummy] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Luminosity luminosity = new Roller<Luminosity>(Luminosity.VI, Luminosity.V, Luminosity.V, Luminosity.V, Luminosity.IV).roll();
        SpectralType hr;

        switch (luminosity) {
            case VI:
                hr = new Roller<SpectralType>().add(SpectralType.G5, 1).add(SpectralType.K5, 6).add(SpectralType.M5, 9).roll();
                break;
            case V:
                hr = new Roller<SpectralType>().add(SpectralType.F5, 1).add(SpectralType.G5, 9).add(SpectralType.K5, 6).add(SpectralType.M5, 3).roll();
                break;
            case IV:
                hr = new Roller<SpectralType>(SpectralType.A5).add(SpectralType.F5, 3).add(SpectralType.G5, 1).add(SpectralType.K5, 3).add(SpectralType.M5, 6).roll();
                break;
            default:
                hr = new Roller<SpectralType>().add(SpectralType.F5, 1).add(SpectralType.G5, 3).add(SpectralType.K5, 1).roll();
                break;
        }
        Star primary = starGenerator.generatePrimary(luminosity, hr.getSpectralType(Die.dieV(5)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        int  orbit = 1;
        long distance = Physics.getOrbitWithTemperature(primary, 250);

        String      name;
        PlanetType  type;
        List<Planet> planets;

        if (Die.d4() > 1) {
            type = PlanetType.EuArean;
            if (Die.d6() == 1) {
                type = PlanetType.EoGaian;
            }
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d4() > 1) {
            distance *= 5;
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            type = PlanetType.Jovic;
            planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
        }

        setDescription(system, null);
    }

    /**
     * This system is empty, it only has a single star and no planets. It may have oort clouds.
     */
    protected void createEmpty(StarSystem system, Luminosity luminosity, SpectralType type) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Empty/%s %s] system [%s]", type, luminosity, system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(luminosity, type);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        addOortClouds(planetFactory, system, primary);

        system.addTradeCode(StarSystemCode.Ba);
    }

    protected void createAsteroids(StarSystem system, Luminosity luminosity, SpectralType type) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Asteroids/%s %s] system [%s]", type, luminosity, system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(luminosity, type);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Place the belt around 1AU from the star.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        String name = StarSystemFactory.getBeltName(primary, 1);
        List<Planet> planets;

        planets = planetFactory.createPlanet(system, primary, name, PlanetType.AsteroidBelt, distance,
                Belt.BeltFeature.WideRing);
        system.setPlanets(planets);
        // Belts may increase their distance based on width.
        distance = planets.get(0).getDistance();

        distance *= 2;
        if (Die.d2() == 1) {
            name = StarSystemFactory.getPlanetName(primary, 1);
            planets = planetFactory.createPlanet(system, primary, name, PlanetType.Jovic, distance);
            system.addPlanets(planets);
        }

        distance *= 3;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        addOortClouds(planetFactory, system, primary);

        system.addTradeCode(StarSystemCode.Ba);
    }

    public void createRogue(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Rogue] system [%s]", system.getName()));

        system.setType(StarSystemType.ROGUE_PLANET);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        List<Planet> planets;
        PlanetType  type;
        switch (Die.d6(2)) {
            case 2: case 3:
                type = PlanetType.Kauketean;
                break;
            case 4: case 5:
                type = PlanetType.Hauhetean;
                break;
            case 6: case 7:
                type = PlanetType.Nyxian;
                break;
            default:
                type = PlanetType.Odyssian;
        }

        planets = planetFactory.createPlanet(system, null, system.getName(), type, 0);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ro);
    }

    public void createGasRogue(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [GasRogue] system [%s]", system.getName()));

        system.setType(StarSystemType.ROGUE_PLANET);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        List<Planet> planets;
        PlanetType  type = PlanetType.Odyssian;

        planets = planetFactory.createPlanet(system, null, system.getName(), type, 0);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ro);
    }

    protected static void addOortClouds(PlanetFactory factory, StarSystem system, Star primary) {
        long distance = (long) (Physics.AU * 10_000 * primary.getMass());
        int  chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
        if (primary.getMass() < 1) {
            chance = (int) (primary.getMass() * 100);
        }

        if (Die.d100() <= chance) {
            PlanetType type = PlanetType.OortCloud;
            String     name = StarSystemFactory.getOortCloudName(primary, 1);
            List<Planet> planets = factory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 5;
            chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
            if (Die.d100() <= chance) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                planets = factory.createPlanet(system, primary, name, type, distance);
                system.addPlanets(planets);
            }
        }
    }



}
