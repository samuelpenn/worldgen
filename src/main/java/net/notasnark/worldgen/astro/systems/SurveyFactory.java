package net.notasnark.worldgen.astro.systems;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.sectors.NoSuchSectorException;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.survey.*;
import net.notasnark.worldgen.web.Server;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.sectors.SectorFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.*;
import java.util.stream.Collectors;

public class SurveyFactory {
    private static final Logger logger = LoggerFactory.getLogger(SurveyFactory.class);
    private final EntityManager session;
    private final WorldGen worldGen;

    private static final String SURVEY_QUERY = "FROM Survey WHERE systemId = :system";

    public SurveyFactory(EntityManager session, WorldGen worldGen) {
        this.session = session;
        this.worldGen = worldGen;
    }

    /**
     * Gets the survey data for the given system. Returns a new survey object with a value of zero
     * if none is found. For now, we always return the global survey data, without specifiying a
     * the ship identifier.
     *
     * @param systemId      Star system to obtain survey data on.
     * @return              Survey object
     */
    public Survey getSurvey(int systemId) {
        Query query = session.createQuery(SURVEY_QUERY);
        query.setParameter("system", systemId);

        try {
            Survey survey = (Survey) query.getSingleResult();

            return survey;
        } catch (NoResultException e) {
            return new Survey(systemId, 0);
        }
    }

    public void persist(Survey survey) {
        try {
            session.persist(survey);
            session.flush();
        } catch (ConstraintViolationException e) {
            //throw new DuplicateSurveyException(survey);
        }
    }

    public static int getDistance(Sector sector1, int x1, int y1, Sector sector2, int x2, int y2) {
        int dx = (x2 - x1);
        int dy = (y2 - y1);

        dx = Math.abs(dx);
        dy = dy + (dx / 2);

        if (x1 % 2 != 0 && x2 % 2 == 0) {
            dy++;
        }

        return Math.max(dx - dy, Math.max(dy, dx));
    }

    private SystemDensity calculateDensity(List<Planet> planets) {
        int density = 0;
        boolean hasJovian = false;
        boolean hasTerrestrial = false;
        boolean hasBelt = false;

        for (Planet planet : planets) {
            if (planet.isMoon()) {
                // Nothing.
            } else if (planet.isBelt()) {
                hasBelt = true;
                density++;
            } else if (planet.isJovian()) {
                hasJovian = true;
                density++;
            } else if (planet.isTerrestrial()) {
                hasTerrestrial = true;
                density++;
            }
        }
        if (hasJovian) density += 2;
        if (hasTerrestrial) density += 1;
        if (hasBelt) density += 1;

        return SystemDensity.getByDensity(density);
    }

    /**
     * Perform a scan of a region, centred on the given coordinates, with the given level of effect, at the
     * radius given. Systems with radius-2 and radius will be scanned (inclusive).
     *
     * The effect is reduced by distance, according to the luminosity of the brightest star.
     * VI : -1 / parsec
     * V : -1 / 3 parsecs
     * IV : -1 / 5 parsecs
     * III: -1 / 7 parsecs
     * II: -1 / 9 parsecs
     * I: -1 / 12 parsecs
     *
     * @param oSector
     * @param ox
     * @param oy
     * @param radius
     * @param scan
     * @return
     */
    public int performSurvey(Sector oSector, int ox, int oy, int radius, int scan) {
        logger.info("performSurvey: Running a survey with radius [" + radius + "] and effect [" + scan + "]");

        int count = 0;

        StarSystemFactory systemFactory = worldGen.getStarSystemFactory();
        SectorFactory sectorFactory = worldGen.getSectorFactory();

        for (int y = oy - radius; y <= oy + radius; y++) {
            for (int x = ox - radius; x <= ox + radius; x++) {
                // Local x and y coordinates within the sector.
                int lx = x, ly = y;

                Sector sector = oSector;
                // Calculate coordinates outside the source sector.
                if (lx < 1 || lx > Sector.WIDTH || ly < 1 || ly > Sector.HEIGHT) {
                    int sectorX = oSector.getX(), sectorY = oSector.getY();
                    while (lx < 1) {
                        sectorX--;
                        lx += Sector.WIDTH;
                    }
                    while (lx > Sector.WIDTH) {
                        sectorX++;
                        lx -= Sector.WIDTH;
                    }
                    while (ly < 1) {
                        sectorY--;
                        ly += Sector.HEIGHT;
                    }
                    while (ly > Sector.HEIGHT) {
                        sectorY++;
                        ly -= Sector.HEIGHT;
                    }
                    try {
                        sector = sectorFactory.getSector(sectorX, sectorY);
                    } catch (NoSuchSectorException e) {
                        // If there isn't a sector, there's no stars here.
                        continue;
                    }
                }

                try {
                    StarSystem system = systemFactory.getStarSystem(sector, lx, ly);

                    int     d = getDistance(oSector, ox, oy, oSector, x, y);
                    double  rangeBand = 12;
                    if (d > radius - 3 && d <= radius) {
                        double totalLuminosity = 0;
                        for (Star star : system.getStars()) {
                            totalLuminosity += Physics.getSolarLuminosity(star);
                        }
                        if (totalLuminosity > 100) {
                            rangeBand = 100;
                        } else if (totalLuminosity > 30) {
                            rangeBand = 50;
                        } else if (totalLuminosity > 10) {
                            rangeBand = 25;
                        } else if (totalLuminosity > 3) {
                            rangeBand = 18;
                        } else if (totalLuminosity > 0.5) {
                            rangeBand = 12;
                        } else if (totalLuminosity > 0.1) {
                            rangeBand = 6;
                        } else if (totalLuminosity > 0.03) {
                            rangeBand = 3;
                        } else if (totalLuminosity > 0.01) {
                            rangeBand = 2;
                        } else if (totalLuminosity > 0.003) {
                            rangeBand = 1;
                        } else {
                            rangeBand = 0.5;
                        }

                        logger.debug(String.format("Scanning system [%s/%02d%02d] at distance of [%d] (L%.1f)",
                                system.getName(), system.getX(), system.getY(), d, rangeBand));

                        count++;
                        int quality = (int)Math.round(scan - d / rangeBand);
                        if (quality >= worldGen.getConfig().getSurveySensitivity()) {
                            quality = Math.max(quality, 1);
                            int level = (int) Math.max(0, (Math.log(quality) / Math.log(2)) + 1);
                            logger.debug("Scanned system [" + system.getName() + "] SI [" + quality + "] level [" + level + "]");
                            Survey survey = getSurvey(system.getId());
                            if (level > survey.getScan()) {
                                survey.setScan(level);
                                persist(survey);
                            } else if (level >= survey.getScan() / 2) {
                                survey.setScan(survey.getScan() + 1);
                                persist(survey);
                            }
                        }
                    }
                } catch (NoSuchStarSystemException e) {
                    // Nothing to do.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return count;
    }

    /**
     * Scan a sector whole sector to set preliminary survey data on it. Used for setting up previously scanned
     * sectors rather than for scanning by exploration vessels.
     */
    public int scanSector(final Sector sector, final int scan, final int distance) {
        int count = 0;

        StarSystemFactory systemFactory = Server.getWorldGen().getStarSystemFactory();
        SectorFactory sectorFactory = Server.getWorldGen().getSectorFactory();

        for (StarSystem system : systemFactory.getStarSystems(sector)) {
            try {
                int lm = 0;
                if (system.getStars().size() > 0) {
                    // Get a luminosity modifier based on the log of the star's power output.
                    double l = Physics.getSolarLuminosity(system.getStars().get(0));
                    lm = (int)(l * Math.sqrt(10));
                }
                logger.debug(String.format("Scanning system [%s/%02d%02d] at distance of [%d] (L%d)",
                        system.getName(), system.getX(), system.getY(), distance, lm));

                count++;
                double r = 3.0 + lm;
                if (r < 1) {
                    r = -3 / (lm + 1);
                }
                int quality = (int)Math.round(scan - distance / r);
                if (quality > 0) {
                    int level = (int) Math.max(0, (Math.log(quality) / Math.log(2)) + 1);
                    if (distance == 0) {
                        level = scan;
                    }
                    logger.debug("Scanned system [" + system.getName() + "] SI [" + quality + "] level [" + level + "] luminosity [" + r + "]");
                    Survey survey = getSurvey(system.getId());
                    if (level > survey.getScan()) {
                        survey.setScan(level);
                        persist(survey);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return count;
    }

    private static long getEstimate(long value, long accuracy) {
        return (long) (Math.round((1.0 * value) / (1.0 * accuracy)) * accuracy);
    }

    private long getMinimumSize(int scanLevel) {
        switch (scanLevel) {
            case 0:
                return 5_000_000;
            case 1:
                return 1_000_000;
            case 2:
                return 500_000;
            case 3:
                return 250_000;
            case 4:
                return 100_000;
            case 5:
                return 50_000;
            case 6:
                return 25_000;
            case 7:
                return 10_000;
            case 8:
                return 5_000;
            case 9:
                return 2_000;
            case 10:
                return 500;
            case 11:
                return 100;
            default:
                return 10;
        }
    }

    /**
     * Get the estimated diameter of an object, based on the level of scan detail available.
     */
    private long getEstimatedDiameter(long diameter, int scanLevel) {
        return getEstimate(diameter, getMinimumSize(scanLevel));
    }

    /**
     * Get the estimated distance of an object, based on the level of scan detail available.
     */
    private long getEstimatedDistance(long distance, int scanLevel) {
        switch (scanLevel) {
            case 0:
                return getEstimate(distance, 100 * Physics.AU);
            case 1:
                return getEstimate(distance, 30 * Physics.AU);
            case 2:
                return getEstimate(distance, 10 * Physics.AU);
            case 3:
                return getEstimate(distance, 3 * Physics.AU);
            case 4:
                return getEstimate(distance, 1 * Physics.AU);
            case 5:
                return getEstimate(distance, 50 * Physics.MKM);
            case 6:
                return getEstimate(distance, 15 * Physics.MKM);
            case 7:
                return getEstimate(distance, 5 * Physics.MKM);
            case 8:
                return getEstimate(distance, 1 * Physics.MKM);
            case 9:
                return getEstimate(distance, 250_000);
            case 10:
                return getEstimate(distance, 100_000);
            case 11:
                return getEstimate(distance, 10_000);
            default:
                return getEstimate(distance, 1_000);
        }
    }

    public long getEstimatedPopulation(long population, int scanLevel) {
        switch (scanLevel) {
            case 0:
                return getEstimate(population, 100_000_000_000L);
            case 1:
                return getEstimate(population, 50_000_000_000L);
            case 2:
                return getEstimate(population, 25_000_000_000L);
            case 3:
                return getEstimate(population, 10_000_000_000L);
            case 4:
                return getEstimate(population, 5_000_000_000L);
            case 5:
                return getEstimate(population, 2_500_000_000L);
            case 6:
                return getEstimate(population, 1_000_000_000L);
            case 7:
                return getEstimate(population, 500_000_000L);
            case 8:
                return getEstimate(population, 250_000_000L);
            case 9:
                return getEstimate(population, 100_000_000L);
            case 10:
                return getEstimate(population, 25_000_000L);
            case 11:
                return getEstimate(population, 5_000_000L);
            default:
                return getEstimate(population, 100_000L);
        }
    }

    public SystemSurvey getSystemSurvey(StarSystem system) {
        return getSystemSurvey(system , 0);
    }

    /**
     * Get survey data for an entire star system. Data returned is based on the current survey level the
     * system is known at. If it's zero, then no data will be returned.
     */
    public SystemSurvey getSystemSurvey(StarSystem system, int minimumScan) {
        logger.info("getSystemSurvey: " + system.getName());
        if (system == null || system.getId() == 0) {
            throw new IllegalArgumentException("StarSystem cannot be null");
        }
        SystemSurvey survey = new SystemSurvey();
        int scanLevel = Math.max(worldGen.getSurveyFactory().getSurvey(system.getId()).getScan(), minimumScan);

        logger.debug("getSystemSurvey: Scan " + scanLevel);
        if (scanLevel > 0) {
            survey.name = system.getName();
            survey.type = system.getType();
            survey.x = system.getX();
            survey.y = system.getY();
            survey.starPort = StarPort.X;
            survey.scan = scanLevel;

            List<Planet> planets = worldGen.getPlanetFactory().getPlanets(system);
            survey.density = calculateDensity(planets);

            logger.debug("getSystemSurvey: Stars " + system.getStars().size() + ", Planets " + planets.size());

            if (system.getStars().size() == 0) {
                logger.debug("getSystemSurvey: No stars");
                COG cog = new COG();
                for (Planet planet : planets) {
                    PlanetSurvey pSurvey = getPlanetSurvey(planet, planets, scanLevel - 1);
                    if (pSurvey != null) {
                        cog.planets.add(pSurvey);
                    }
                }
                if (cog.planets.size() == 0) {
                    return survey;
                }
                survey.cogs.add(cog);
                return survey;
            }

            for (Star star : system.getStars()) {
                logger.debug("getSystemSurvey: " + star.getName());
                if (star.getParentId() == 0) {
                    logger.debug("getSystemSurvey: Primary star");
                    // This is the primary star in a system.
                    COG cog = new COG();
                    StarSurvey starSurvey = new StarSurvey();
                    starSurvey.name = star.getName();
                    starSurvey.distance = getEstimatedDistance(star.getDistance(), scanLevel) / Physics.AU;
                    if (scanLevel > 0) {
                        starSurvey.luminosity = star.getLuminosity();
                        starSurvey.diameter = getEstimatedDiameter(star.getRadius() * 2, scanLevel);
                    }
                    if (scanLevel > 1) {
                        starSurvey.type = star.getSpectralType();
                    }
                    cog.stars.add(starSurvey);
                    planets.stream().filter(p -> p.getParentId() == star.getId()).collect(Collectors.toList());
                    addPlanets(cog, planets, star.getId(), survey, scanLevel);
                    survey.cogs.add(cog);
                } else if (star.getParentId() > 0) {
                    logger.debug("getSystemSurvey: Secondary star");
                    COG cog = new COG();
                    StarSurvey starSurvey = new StarSurvey();
                    starSurvey.name = star.getName();
                    starSurvey.distance = getEstimatedDistance(star.getDistance(), scanLevel) / Physics.AU;
                    System.out.println("Star distance " + starSurvey.distance);
                    if (scanLevel > 1) {
                        starSurvey.luminosity = star.getLuminosity();
                        starSurvey.diameter = getEstimatedDiameter(star.getRadius() * 2, scanLevel);
                    }
                    if (scanLevel > 2) {
                        starSurvey.type = star.getSpectralType();
                    }
                    cog.stars.add(starSurvey);
                    planets.stream().filter(p -> p.getParentId() == star.getId()).collect(Collectors.toList());
                    addPlanets(cog, planets, star.getId(), survey, scanLevel);
                    survey.cogs.add(cog);
                } else if (star.getParentId() == -1) {
                    logger.debug("getSystemSurvey: Shared stellar orbit [" + star.getName() + "]");
                    COG cog = new COG();
                    StarSurvey starSurvey = new StarSurvey();
                    starSurvey.name = star.getName();
                    starSurvey.distance = getEstimatedDistance(star.getDistance(), scanLevel) / Physics.AU;
                    if (scanLevel > 0) {
                        starSurvey.luminosity = star.getLuminosity();
                        starSurvey.diameter = getEstimatedDiameter(star.getRadius() * 2, scanLevel);
                    }
                    if (scanLevel > 1) {
                        starSurvey.type = star.getSpectralType();
                    }
                    cog.stars.add(starSurvey);
                    planets.stream().filter(p -> p.getParentId() == -1).collect(Collectors.toList());
                    addPlanets(cog, planets, -1, survey, scanLevel);
                    survey.cogs.add(cog);
                }
            }
        } else {
            return null;
        }

        return survey;
    }

    private void addPlanets(COG cog, List<Planet> planets, int starId, SystemSurvey survey, int scanLevel) {
        for (Planet planet : planets) {
            if (planet.getStarPort().isBetterThan(survey.starPort)) {
                switch (planet.getStarPort().getStandard()) {
                    case A:
                        if (scanLevel >= 1) {
                            survey.starPort = StarPort.A;
                        }
                        break;
                    case B:
                        if (scanLevel >= 2) {
                            survey.starPort = StarPort.B;
                        }
                        break;
                    case C:
                        if (scanLevel >= 3) {
                            survey.starPort = StarPort.C;
                        }
                        break;
                    case D:
                        if (scanLevel >= 5) {
                            survey.starPort = StarPort.D;
                        }
                        break;
                    case E:
                        if (scanLevel >= 7) {
                            survey.starPort = StarPort.E;
                        }
                        break;
                }
            }
            long population = 0;
            if (planet.getTechLevel() >= survey.techLevel) {
                if (planet.getTechLevel() >= 13 && scanLevel >= 1) {
                    survey.techLevel = 13;
                    if (scanLevel >= 5) survey.techLevel = planet.getTechLevel();
                    population = getEstimatedPopulation(planet.getPopulation(), scanLevel);
                } else if (planet.getTechLevel() >= 9 && scanLevel >= 2) {
                    survey.techLevel = 9;
                    if (scanLevel >= 7) survey.techLevel = planet.getTechLevel();
                    population = getEstimatedPopulation(planet.getPopulation(), scanLevel - 1);
                } else if (planet.getTechLevel() >= 7 && scanLevel >= 3) {
                    survey.techLevel = 7;
                    if (scanLevel >= 9) survey.techLevel = planet.getTechLevel();
                    population = getEstimatedPopulation(planet.getPopulation(), scanLevel - 2);
                } else if (planet.getTechLevel() >= 5 && scanLevel >= 4) {
                    survey.techLevel = 5;
                    population = getEstimatedPopulation(planet.getPopulation(), scanLevel - 3);
                }
            }
            if (planet.getParentId() == starId && !planet.isMoon()) {
                if (planet.isBelt()) {
                    for (Planet p : planets) {
                        if (p.getMoonOf() == planet.getId()) {
                            PlanetSurvey pSurvey = getPlanetSurvey(p, planets, scanLevel);
                            if (pSurvey != null) {
                                pSurvey.population = population;
                                cog.planets.add(pSurvey);
                            }
                        }
                    }
                } else {
                    PlanetSurvey pSurvey = getPlanetSurvey(planet, planets, scanLevel);
                    if (pSurvey != null) {
                        pSurvey.population = population;
                        cog.planets.add(pSurvey);
                    }
                }
            }
        }
    }

    private PlanetSurvey getPlanetSurvey(Planet planet, List<Planet> planets, int scanLevel) {
        PlanetSurvey survey = new PlanetSurvey();

        long distance = planet.getDistance();
        if (planet.isMoon()) {
            Optional<Planet> belt = planets.stream().filter(i -> i.getId() == planet.getMoonOf()).findFirst();
            if (belt.isPresent()) {
                distance = belt.get().getDistance() + planet.getDistance();
            }
        }

        survey.distance = (long) Math.round(1.0 * getEstimatedDistance(distance, scanLevel) / Physics.AU);
        survey.diameter = getEstimatedDiameter(planet.getRadius() * 2, scanLevel);

        if (survey.diameter == 0 || scanLevel < 1) {
            // Planet is too small to be detected.
            return null;
        }
        long min = getMinimumSize(scanLevel);
        survey.pclGroup = planet.getType().getGroup();

        if (planet.getRadius() > min) {
            survey.pclClass = planet.getType().getClassification();
        }
        if (planet.getRadius() > min * 2) {
            survey.pclClass = planet.getType().getClassification();
            survey.atmosphere = planet.getAtmosphere();
            survey.hydrographics = (int) (Math.round(planet.getHydrographics() / 25.0) * 25);
            survey.pressure = planet.getPressureText();
        }
        if (planet.getRadius() > min * 3) {
            survey.pclType = planet.getType();
            survey.pressure = planet.getPressureText();
            survey.hydrographics = (int) (Math.round(planet.getHydrographics() / 5.0) * 5);
        }

        return survey;
    }

    public static void test(int x1, int y1, int x2, int y2, int exp) {
        int d= getDistance(null, x1, y1, null, x2, y2);

        String wrong = "";
        if (d != exp) {
            wrong = " != " + exp;
        }
        System.out.println("["+x1+","+y1+"] -> ["+x2+","+y2+"] = " + d + wrong);
    }

    public static void main(String[] args) {
        /*
        test(4,4, 2,2, 3);
        test(4,4, 1, 1, 5);
        test(5,5, 2,2, 4);
        test(5,5, 1,1, 6);
        test(5, 5, 7, 2, 4);
        test(5, 5, 8, 1, 5);
        test(5, 5, 8, 2, 4);
        test(4, 1, 6, 5, 5);
        test(8, 4, 1, 1, 7);
        test(8, 4, 15, 1, 7);
         */

        System.out.println(getEstimate(100, 300));
        System.out.println(getEstimate(200, 300));
        System.out.println(getEstimate(500, 300));
        System.out.println(getEstimate(1000, 300));

    }

}
