/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.civilisation.SmallColony;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemGenerator;
import net.notasnark.worldgen.astro.systems.StarSystemType;

import java.util.List;

import static net.notasnark.worldgen.astro.systems.StarSystemFactory.getBeltName;
import static net.notasnark.worldgen.astro.systems.StarSystemFactory.getPlanetName;

/**
 * Generates a simple system which has a habitable planet in the goldilocks zone of the system.
 * There is a chance of a second star, but it will be far out and have non-habitable planets.
 */
public class Goldilocks extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Goldilocks.class);

    public Goldilocks(WorldGen worldgen) {
        super(worldgen);
    }

    @Override
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            default:
                createDwarf(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    @Override
    public void colonise(StarSystem system) {
        logger.info("Colonising system [" + system.getName() + "]");
        CivilisationGenerator generator = new SmallColony(worldgen, system);

        generator.generate();
    }

    private Star createStars(StarSystem system) throws DuplicateStarException {
        StarGenerator starGenerator;

        switch (Die.d3()) {
            case 1:
                starGenerator = new StarGenerator(worldgen, system, true);
                system.setType(StarSystemType.FAR_BINARY);
                break;
            default:
                starGenerator = new StarGenerator(worldgen, system, false);
                system.setType(StarSystemType.SINGLE);
                break;
        }

        // On average a yellow main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.G2.getSpectralType(Die.dieV(3)));
        system.addStar(primary);

        if (system.getType() == StarSystemType.FAR_BINARY) {
            Star secondary = starGenerator.generateSecondary(Luminosity.VI,
                    SpectralType.M5.getSpectralType(Die.dieV(4)));
            secondary.setDistance(500 * Physics.AU + Die.die(500) * Physics.AU + Die.die(Physics.AU));
            system.addStar(secondary);
        }

        return primary;
    }


    private void addSecondaryPlanets(StarSystem system, Star secondary) {

    }

    /**
     * This system has a dwarf terrestrial as the main world. It is reasonably warm, but has a thin atmosphere
     * and low gravity. It isn't ideal for life, but people can walk on its surface without a suit.
     *
     * @param system
     * @throws DuplicateObjectException
     */
    public void createDwarf(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Goldilocks] [Dwarf] system [%s]", system.getName()));

        PlanetFactory factory = worldgen.getPlanetFactory();
        Star primary = createStars(system);
        List<Planet> planets;

        long   distance = 40_000_000 + Die.d20(2) * 1_000_000 + Die.die(1_000_000);
        int    numBelts = 0, numPlanets = 0;

        switch (Die.d6(2)) {
            case 2: case 3:
                // Asteroid belt. Placed slightly closer to the star than planets are.
                distance *= 0.8;
                planets = factory.createPlanet(system, primary, getBeltName(primary, ++numBelts),
                        PlanetType.VulcanianBelt, distance);
                system.addPlanets(planets);
                break;
            case 4: case 5:
                planets = factory.createPlanet(system, primary, getPlanetName(primary, ++numPlanets),
                        PlanetType.Ferrinian, distance);
                system.addPlanets(planets);
                break;
            case 6: case 7: case 8:
                planets = factory.createPlanet(system, primary, getPlanetName(primary, ++numPlanets),
                        PlanetType.Hermian, distance);
                system.addPlanets(planets);
                break;
            default:
                // No planet in this spot.
        }

        switch (Die.d6()) {
            default:
                distance = Physics.AU;
                planets = factory.createPlanet(system, primary, getPlanetName(primary, ++numPlanets),
                        PlanetType.MesoArean, distance);
                system.addPlanets(planets);
                break;

        }


        setDescription(system, null);
    }
}
