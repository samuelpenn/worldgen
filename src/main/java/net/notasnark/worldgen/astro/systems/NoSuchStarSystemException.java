/**
 * SpectralType.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.systems;

import net.notasnark.worldgen.exceptions.NoSuchObjectException;
import net.notasnark.worldgen.astro.sectors.Sector;

public class NoSuchStarSystemException extends NoSuchObjectException {
    public NoSuchStarSystemException(int id) {
        super(String.format("Cannot find star system [%d]", id));
    }

    public NoSuchStarSystemException(Sector sector, int x, int y) {
        super(String.format("Cannot find star system at [%s]/[%02d%02d]",
                sector.getName(), x, y));
    }

    public NoSuchStarSystemException(Sector sector, String name) {
        super(String.format("Cannot find star system [%s]/[%s]",
                sector.getName(), name));
    }

}
