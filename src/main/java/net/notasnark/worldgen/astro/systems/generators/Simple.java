/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;


import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.*;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.civilisation.Colony;
import net.notasnark.worldgen.civ.civilisation.SpaceFaringCiv;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.systems.CircumstellarZone;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemGenerator;
import net.notasnark.worldgen.astro.systems.StarSystemType;

import java.util.List;

/**
 * Creates a simple star system, primarily for testing boring and predictable systems.
 */
public class Simple extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Simple.class);

    public Simple(WorldGen worldgen) {
        super(worldgen);
    }

    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        logger.info("generate: Simple System");
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            case 1:
                createSol(system);
                break;
            case 2:
                createRockyWorlds(system);
                break;
            case 3:
                createHotWorlds(system);
                break;
            default:
                createSingleStar(system);
                break;
        }
        if (Die.d6() > 5) {
            //colonise(system);
        }
        updateStarSystem(system);

        return system;
    }

    /**
     * This is an empty system, only used as a marker. It has no stars and no planets.
     * @param system    System to be set.
     */
    public void createEmpty(StarSystem system) {
        system.setType(StarSystemType.EMPTY);
        setDescription(system, null);
    }

    private PlanetType getPlanetType(CircumstellarZone zone) {
        switch (zone) {
            case Exclusion:
                // No planets here.
                return null;
            case Epistellar:
                return PlanetType.Hermian;
            case Hot:
                return PlanetType.Hermian;
            case Inner:
                return new Roller<>(PlanetType.Cytherean, PlanetType.Lutian, PlanetType.BathyPelagic).roll();
            case Middle:
                return PlanetType.EuGaian;
            case Outer:
                return new Roller<>(PlanetType.EuArean, PlanetType.AsteroidBelt, PlanetType.EoArean).roll();
            case Cold:
                return PlanetType.Jovic;
            case Stygia:
                return PlanetType.Saturnian;
            case Tartarus:
                return new Roller<>(PlanetType.Neptunian, PlanetType.IceBelt).roll();
        }
        return null;
    }


    /**
     * This constructs a star system very similar to that of Sol. It is designed for testing purposes
     * to see how traditional worlds look under the generation system. Worlds of specific types will
     * be created, with variations only within the type.
     *
     * @param system    Star system to populate.
     * @throws DuplicateStarException       Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSol(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary;
        switch (Die.d6(1)) {
            case 1:
                // Hotter.
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G1);
                break;
            case 6:
                // Cooler.
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G3);
                break;
            default:
                primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);
                break;
        }

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;
        int     orbit = 1;

        // Mercury.
        long distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        // Earth.
        distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        // Mars.
        distance = 230 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EuArean, distance);
        system.addPlanets(planets);

        // Asteroids.
        distance = (390 + Die.d10(2)) * Physics.MKM;
        planetName = StarSystemFactory.getBeltName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt, distance);
        system.addPlanets(planets);

        // Jupiter.
        distance = 750 * Physics.MKM + Die.dieV(30_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Jovic, distance);
        system.addPlanets(planets);

        // Saturn.
        distance = 1500 * Physics.MKM + Die.dieV(100_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Saturnian, distance);
        system.addPlanets(planets);

        // Uranus.
        distance = (19L + Die.d4()) * Physics.AU + Die.die(Physics.AU);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian, distance);
        system.addPlanets(planets);

        // Neptune.
        distance = (long)((28L + (long)Die.d6()) * Physics.AU) + Die.die(Physics.AU);
        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian, distance);
        system.addPlanets(planets);

        // Kuiper Belt
        distance = (38L + Die.d6()) * Physics.AU + Die.die(Physics.AU);
        planetName = StarSystemFactory.getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.IceBelt, distance,
                planets.get(planets.size() - 1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Creates a small star system with small rocky worlds similar to the inner system of Sol.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createRockyWorlds(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2.getSpectralType(Die.dieV(2)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        // Vulcan.
        long distance = 20 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ferrinian, distance);
        system.addPlanets(planets);

        // Mercury.
        distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 3);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        // Earth.
        distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 4);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian, distance);
        system.addPlanets(planets);

        // Mars.
        distance = 230 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 5);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoArean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }


    private void createMainWorld(StarSystem system, PlanetType type) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G2);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName = StarSystemFactory.getPlanetName(primary, 1);

        long distance = 150 * Physics.MKM + Die.dieV(5_000_000);
        planets = planetFactory.createPlanet(system, primary, planetName, type, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }
    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createEoGaian(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.EoGaian);
    }

    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createMesoGaian(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.MesoGaian);
    }

    public void createEuGaian(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.EuGaian);
    }

    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createMesoArean(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.MesoArean);
    }
    /**
     * Creates a small star system a single EoGaian world.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createNecroGaian(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.NecroGaian);
    }

    public void createGaianPelagic(StarSystem system) throws DuplicateObjectException {
        createMainWorld(system, PlanetType.GaianPelagic);
    }

    /**
     * Creates a small star system with small rocky worlds similar to the inner system of Sol.
     *
     * @param system    Star system to populate.
     * @throws DuplicateObjectException     Duplicate object was created.
     */
    @SuppressWarnings("WeakerAccess")
    public void createHotWorlds(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a hot star.
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.F5.getSpectralType(Die.dieV(3)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String  planetName;

        // Vulcan.
        long distance = 20 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 1);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.ProtoLithian, distance);
        system.addPlanets(planets);

        // Mercury.
        distance = 50 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian, distance);
        system.addPlanets(planets);

        // Venus.
        distance = 110 * Physics.MKM + Die.dieV(5_000_000);
        planetName = StarSystemFactory.getPlanetName(primary, 3);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean, distance);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    /**
     * Generates a simple predictable star system that is similar to Sol, with rocky planets making
     * up the inner worlds, an asteroid belt and some large Jovian worlds in the outer system. There
     * is more variety here than for the 'Sol' system.
     */
    @SuppressWarnings("WeakerAccess")
    public void createSingleStar(StarSystem system) throws DuplicateObjectException {
        logger.info("createSingleStar:");
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Luminosity l = Luminosity.V;
        SpectralType hr = SpectralType.G2.getSpectralType(Die.dieV(2));
        Star primary = starGenerator.generatePrimary(l, hr);

        int orbit = 1;
        int belts = 1;
        String planetName;

        switch (Die.d6(2)) {
            case 4:
                planetName = StarSystemFactory.getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.VulcanianBelt,
                        (30 + Die.d12()) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 5:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Janian,
                        (40 + Die.d6(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 6: case 7: case 8:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Hermian,
                        (40 + Die.d12(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 9: case 10:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ferrinian,
                        (40 + Die.d8(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
        }

        if (Die.d3() > 1) {
            planetName = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cytherean,
                    (70 + Die.d10(2) + orbit * 15) * Physics.MKM);
            system.addPlanets(planets);
        }

        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EoGaian,
                (140 + Die.d10(2)) * Physics.MKM);
        system.addPlanets(planets);

        switch (Die.d6()) {
            case 1: case 2:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.MesoArean,
                        (230 + Die.d20(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 3: case 4: case 5:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.EuArean,
                        (230 + Die.d20(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
        }

        switch (Die.d3()) {
            case 1:
                planetName = StarSystemFactory.getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt,
                        (400 + Die.d20(5)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 2:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Cerean,
                        (400 + Die.d20(5)) * Physics.MKM, Dwarf.DwarfFeature.Large);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
                break;
        }

        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Jovic,
                (700 + Die.d100(1)) * Physics.MKM);
        system.addPlanets(planets);

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Saturnian,
                    (1400 + Die.d100(2)) * Physics.MKM);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian,
                    (20 + Die.d6( 2)) * Physics.AU + Die.die(Physics.AU));
            system.addPlanets(planets);
        }

        // Kuiper Belt
        long distance = (40L + Die.d6()) * Physics.AU + Die.die(Physics.AU);
        planetName = StarSystemFactory.getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.IceBelt, distance,
                planets.get(planets.size() - 1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    private void addSomePlanets(StarSystem system, Star star, int numPlanets) {
        long distance = Physics.getOrbitWithTemperature(star, 400);

        int orbit = 1, belt = 1;
        for (int p=0; p < numPlanets; p++) {

        }

    }

    public void createCloseBinary(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.CLOSE_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        long starDistance = Die.d6(2) * 1_000_000 + Die.die(1_000_000);
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G0.getSpectralType(-Die.d6()));
        primary.setDistance(starDistance);

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6(2));
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);

        // Work out orbital distance and period of the pair.
        double totalMass = primary.getMass() + secondary.getMass();

        long period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, starDistance * 1_000);
        primary.setPeriod(period);
        secondary.setPeriod(period);

        primary.setDistance(Physics.round(starDistance * secondary.getMass() / totalMass));
        secondary.setDistance(Physics.round(starDistance * primary.getMass() / totalMass));

        primary.setParentId(StarSystem.PRIMARY_COG);
        secondary.setParentId(StarSystem.PRIMARY_COG);

        primary.setStandardMass();
        secondary.setStandardMass();
        primary.setStandardRadius();
        secondary.setStandardRadius();

        system.addStar(primary);
        system.addStar(secondary);

        String name = String.format("%s %s/%s", system.getName(),
                primary.getName().replaceAll(".* ", ""),
                secondary.getName().replaceAll(".* ", ""));
        Star centre = new Star(name, system, primary, secondary);
        centre.setCentreOfGravity(StarSystem.PRIMARY_COG);

        List<Planet> planets;
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        System.out.println(Physics.getSolarConstant(centre));

        int numPlanets = Die.d4() + 1;
        long distance = Physics.getOrbitWithTemperature(centre, 400);
        distance = Math.max(distance, starDistance * 2);

        int orbit = 1, belt = 1;
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(centre, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(centre, belt++);
                planets = planetFactory.createPlanet(system, centre, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(centre, orbit++);
                planets = planetFactory.createPlanet(system, centre, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
        }

        setDescription(system, null);
    }

    public void createMediumBinary(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.MEDIUM_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        long starDistance = (10 + Die.d20(2)) * Physics.AU + Die.die(Physics.AU);
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G0.getSpectralType(-Die.d6()));
        primary.setDistance(0);
        primary.setPeriod(0);
        primary.setStandardMass();
        primary.setStandardRadius();

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6(2));
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);
        secondary.setDistance(starDistance);
        secondary.setStandardMass();
        secondary.setStandardRadius();

        long period = Physics.getOrbitalPeriod(primary.getMass() * Physics.SOL_MASS, starDistance * 1_000);
        secondary.setPeriod(period);
        secondary.setParentId(primary.getId());
        system.addStar(primary);
        system.addStar(secondary);

        List<Planet> planets;
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Planets for first star
        int numPlanets = Die.d4() + 1;
        String name = null;
        long distance = Physics.getOrbitWithTemperature(primary, 400);

        int orbit = 1, belt = 1;
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(primary, belt++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        // Planets for second star
        numPlanets = Die.d4();
        orbit = 1; belt = 1;
        distance = Physics.getOrbitWithTemperature(secondary, 400);
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(secondary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(secondary, belt++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(secondary, orbit++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        setDescription(system, null);
    }

    public void createFarBinary(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.FAR_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        long starDistance = (10 + Die.d20(2)) * Physics.AU + Die.die(Physics.AU);
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G0.getSpectralType(-Die.d6()));
        primary.setDistance(0);
        primary.setPeriod(0);
        primary.setStandardMass();
        primary.setStandardRadius();

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6(2));
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);
        secondary.setDistance(starDistance);
        secondary.setStandardMass();
        secondary.setStandardRadius();

        long period = Physics.getOrbitalPeriod(primary.getMass() * Physics.SOL_MASS, starDistance * 1_000);
        secondary.setPeriod(period);

        secondary.setParentId(primary.getId());


        system.addStar(primary);
        system.addStar(secondary);

        List<Planet> planets;
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Planets for first star
        int numPlanets = Die.d4() + 3;
        String name = null;
        long distance = Physics.getOrbitWithTemperature(primary, 400);

        int orbit = 1, belt = 1;
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(primary, belt++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        // Planets for second star
        numPlanets = Die.d4() + 1;
        orbit = 1; belt = 1;
        distance = Physics.getOrbitWithTemperature(secondary, 400);
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(secondary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(secondary, belt++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(secondary, orbit++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        setDescription(system, null);
    }

    public void createTriple(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.TRIPLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        long starDistance = (10 + Die.d20(2)) * Physics.AU + Die.die(Physics.AU);
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.G0.getSpectralType(-Die.d6()));
        primary.setDistance(0);
        primary.setPeriod(0);
        primary.setStandardMass();
        primary.setStandardRadius();

        // Get a possibly cooler variant of the primary star.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6(2));
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);
        secondary.setDistance(starDistance);
        secondary.setStandardMass();
        secondary.setStandardRadius();

        long period = Physics.getOrbitalPeriod(primary.getMass() * Physics.SOL_MASS, starDistance * 1_000);
        secondary.setPeriod(period);
        secondary.setParentId(primary.getId());

        Star tertiary = starGenerator.generateTertiary(Luminosity.VII, SpectralType.D5);
        tertiary.setDistance(starDistance * (Die.d3() + 1));
        tertiary.setStandardMass();
        tertiary.setStandardRadius();
        period = Physics.getOrbitalPeriod(primary.getMass() * Physics.SOL_MASS, tertiary.getDistance() * 1_000);
        tertiary.setPeriod(period);
        tertiary.setParentId(primary.getId());

        system.addStar(primary);
        system.addStar(secondary);
        system.addStar(tertiary);

        List<Planet> planets;
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Planets for first star
        int numPlanets = Die.d4() + 3;
        String name = null;
        long distance = Physics.getOrbitWithTemperature(primary, 400);

        int orbit = 1, belt = 1;
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(primary, belt++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        // Planets for second star
        numPlanets = Die.d4() + 1;
        orbit = 1; belt = 1;
        distance = Physics.getOrbitWithTemperature(secondary, 400);
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(secondary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(secondary, belt++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(secondary, orbit++);
                planets = planetFactory.createPlanet(system, secondary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 5) {
                break;
            }
        }

        // Planets for third star
        numPlanets = Die.d2();
        orbit = 1; belt = 1;
        distance = Physics.getOrbitWithTemperature(tertiary, 200);
        for (int p = 0; p < numPlanets; p++) {
            PlanetType type = getPlanetType(CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(tertiary, distance)));
            if (type == null) {
                // Do nothing
            } else if (type.getGroup() == PlanetGroup.Belt) {
                name = StarSystemFactory.getBeltName(tertiary, belt++);
                planets = planetFactory.createPlanet(system, tertiary, name, type, distance, GeneralFeature.NoMoons);
                distance += planets.get(0).getRadius();
                system.addPlanets(planets);
            } else {
                name = StarSystemFactory.getPlanetName(tertiary, orbit++);
                planets = planetFactory.createPlanet(system, tertiary, name, type, distance, GeneralFeature.NoMoons);
                system.addPlanets(planets);
            }
            distance += Die.die(distance, 2);
            if (distance > starDistance / 2) {
                break;
            }
        }

        setDescription(system, null);
    }

    public void createNeutronStar(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        List<Planet> planets;

        // Generate a Sol-like star.
        Luminosity l = Luminosity.N;
        SpectralType hr = SpectralType.D2.getSpectralType(Die.dieV(2));
        Star primary = starGenerator.generatePrimary(l, hr);

        int orbit = 1;
        int belts = 1;
        String planetName;

        switch (Die.d6(2)) {
            case 4: case 5:
                planetName = StarSystemFactory.getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.VulcanianBelt,
                        (90 + Die.d20()) * Physics.MKM);
                system.addPlanets(planets);
                break;
            case 6: case 7: case 8:
                planetName = StarSystemFactory.getPlanetName(primary, orbit++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ferrinian,
                        (50 + Die.d12(2)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
        }

        if (Die.d3() > 1) {
            planetName = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Ymirian,
                    (170 + Die.d10(2)) * Physics.MKM);
            system.addPlanets(planets);
        }

        switch (Die.d3()) {
            case 1:
                planetName = StarSystemFactory.getBeltName(primary, belts++);
                planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.AsteroidBelt,
                        (400 + Die.d20(5)) * Physics.MKM);
                system.addPlanets(planets);
                break;
            default:
                // No planet.
                break;
        }

        planetName = StarSystemFactory.getPlanetName(primary, orbit++);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Jovic,
                (7 + Die.d6()) * Physics.AU + Die.die(Physics.AU));
        system.addPlanets(planets);

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.Neptunian,
                    (30 + Die.d6( 2)) * Physics.AU + Die.die(Physics.AU));
            system.addPlanets(planets);
        }

        // Kuiper Belt
        long distance = (50L + Die.d6()) * Physics.AU + Die.die(Physics.AU);
        planetName = StarSystemFactory.getBeltName(primary, 2);
        planets = planetFactory.createPlanet(system, primary, planetName, PlanetType.IceBelt, distance,
                planets.get(planets.size() - 1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }



    public void colonise(StarSystem system) {
        logger.info("Colonise Simple star system " + system.getName());

        CivilisationGenerator generator = null;
        switch (Die.d6()) {
            case 1:
            case 2:
            case 3:
                generator = new SpaceFaringCiv(worldgen, system);
                break;
            case 4:
            case 5:
                generator = new Colony(worldgen, system);
                break;
            default:
                break;
        }
        if (generator != null) {
            generator.generate();
        }
    }
}
