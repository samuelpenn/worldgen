/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Belt;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.*;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.astro.systems.generators.deepnight.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.civilisation.DeepnightCiv;
import net.notasnark.worldgen.civ.civilisation.SpaceFaringCiv;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Generate a barren, type M or K, star system. These systems have no native life, the worlds are often
 * cold and uninhabitable. Quite simple systems.
 */
public class FarSideNowhere extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(FarSideNowhere.class);

    public FarSideNowhere(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        logger.info("Far Side Of Nowhere system generator");
        StarSystem system = createEmptySystem(sector, name, x, y);

        FarSideNowhere generator = null;
        switch (Die.d6(3)) {
            default:
                createYellow(system);
                break;
        }
        if (Die.d6() > 4) {
            colonise(system);
        }
        updateStarSystem(system);

        return system;
    }

    /**
     * Generate a rogue star system. This will probably be a Planemo.
     */
    public StarSystem generateRogue(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        createRogue(system);

        return system;
    }

    private Roller<PlanetType> getEpistellarType() {
        return new Roller<>(PlanetType.MetallicBelt, PlanetType.VulcanianBelt)
                .add(PlanetType.Ferrinian, PlanetType.Vulcanian, PlanetType.Metallic)
                .add(PlanetType.ProtoLithian, PlanetType.ProtoFerrinian, PlanetType.ProtoCarbonian)
                .add(PlanetType.Janian)
                .add(PlanetType.Osirian)
                .add(PlanetType.Sokarian)
                .add(PlanetType.Junic, 3)
                .add(PlanetType.SuperJunic);
    }

    private Roller<PlanetType> getHotType() {
        return new Roller<PlanetType>()
                .add(PlanetType.Hermian,3)
                .add(PlanetType.Ferrinian)
                .add(PlanetType.VulcanianBelt)
                .add(PlanetType.Janian);
    }

    private Roller<PlanetType> getInnerType() {
        return new Roller<PlanetType>()
                .add(PlanetType.Cytherean)
                .add(PlanetType.BathyPelagic)
                .add(PlanetType.BathyGaian)
                .add(PlanetType.Lutian, 3);
    }

    private Roller<PlanetType> getMiddleType() {
        return new Roller<PlanetType>()
                .add(PlanetType.EuGaian, 3)
                .add(PlanetType.MesoGaian)
                .add(PlanetType.NecroGaian);
    }
    private Roller<PlanetType> getOuterType() {
        return new Roller<PlanetType>()
                .add(PlanetType.EuArean);
    }

    private Roller<PlanetType> getColdType() {
        return new Roller<PlanetType>()
                .add(PlanetType.Saturnian, 3)
                .add(PlanetType.Jovic)
                .add(PlanetType.Poseidonic);
    }

    private Roller<PlanetType> getStygiaType() {
        return new Roller<PlanetType>()
                .add(PlanetType.Neptunian);
    }

    private Roller<PlanetType> getTartarusType() {
        return new Roller<PlanetType>()
                .add(PlanetType.IceBelt);
    }

    private PlanetType getPlanetType(CircumstellarZone zone) {
        switch (zone) {
            case Epistellar:
                return getEpistellarType().roll();
            case Hot:
                return getHotType().roll();
            case Inner:
                return getInnerType().roll();
            case Middle:
                return getMiddleType().roll();
            case Outer:
                return getOuterType().roll();
            case Cold:
                return getColdType().roll();
            case Stygia:
                return getStygiaType().roll();
            case Tartarus:
                return getTartarusType().roll();
        }

        return null;
    }


    public void createYellow(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);

        // Generate a Sol-like star
        Star primary = starGenerator.generatePrimary(
                Luminosity.V,
                SpectralType.G7.getSpectralType(Die.dieV(6)));

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        int     orbit = 1, belt = 1;
        long    distance = 50 * Physics.MKM + Die.dieV(10_000_000);

        while (Die.d6() + orbit < 10) {
            CircumstellarZone   zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
            PlanetType          type = getPlanetType(zone);
            if (type != null && Die.d3() != 1) {
                if (type.getGroup() == PlanetGroup.Belt) {
                    String name = StarSystemFactory.getBeltName(primary, belt++);
                    List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
                    system.addPlanets(planets);

                    if (planets.size() > 0) {
                        distance += planets.get(0).getRadius();
                    }
                } else {
                    String name = StarSystemFactory.getPlanetName(primary, orbit++);
                    List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
                    system.addPlanets(planets);

                    if (planets.size() > 0 && planets.get(0).getRadius() > 20_000) {
                        distance *= 1.4;
                    }
                }
            }
            distance *= (1.0 + Die.d4(2) / 10.0);
            distance += Die.d100() * 100_000;
        }
        addOortClouds(planetFactory, system, primary);

        setDescription(system, null);
    }



    public void colonise(StarSystem system) {
        logger.info("Colonise star system " + system.getName());

        CivilisationGenerator generator = new SpaceFaringCiv(worldgen, system);
        generator.generate();
    }


    /**
     * This system is empty, it only has a single star and no planets. It may have oort clouds.
     */
    protected void createEmpty(StarSystem system, Luminosity luminosity, SpectralType type) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Empty/%s %s] system [%s]", type, luminosity, system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(luminosity, type);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        addOortClouds(planetFactory, system, primary);

        system.addTradeCode(StarSystemCode.Ba);
    }


    public void createRogue(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [Rogue] system [%s]", system.getName()));

        system.setType(StarSystemType.ROGUE_PLANET);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        List<Planet> planets;
        PlanetType  type;
        switch (Die.d6(2)) {
            case 2: case 3:
                type = PlanetType.Kauketean;
                break;
            case 4: case 5:
                type = PlanetType.Hauhetean;
                break;
            case 6: case 7:
                type = PlanetType.Nyxian;
                break;
            default:
                type = PlanetType.Odyssian;
        }

        planets = planetFactory.createPlanet(system, null, system.getName(), type, 0);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ro);
    }

    public void createGasRogue(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [Deepnight] [GasRogue] system [%s]", system.getName()));

        system.setType(StarSystemType.ROGUE_PLANET);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        List<Planet> planets;
        PlanetType  type = PlanetType.Odyssian;

        planets = planetFactory.createPlanet(system, null, system.getName(), type, 0);
        system.addPlanets(planets);

        system.addTradeCode(StarSystemCode.Ro);
    }

    protected static void addOortClouds(PlanetFactory factory, StarSystem system, Star primary) {
        long distance = (long) (Physics.AU * 10_000 * primary.getMass());
        int  chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
        if (primary.getMass() < 1) {
            chance = (int) (primary.getMass() * 100);
        }

        if (Die.d100() <= chance) {
            PlanetType type = PlanetType.OortCloud;
            String     name = StarSystemFactory.getOortCloudName(primary, 1);
            List<Planet> planets = factory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 5;
            chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
            if (Die.d100() <= chance) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                planets = factory.createPlanet(system, primary, name, type, distance);
                system.addPlanets(planets);
            }
        }
    }



}
