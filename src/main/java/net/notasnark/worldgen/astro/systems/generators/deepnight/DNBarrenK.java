/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators.deepnight;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.*;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.planets.generators.belt.OortCloud;
import net.notasnark.worldgen.astro.sectors.Sector;

import java.util.ArrayList;
import java.util.List;

/**
 * Generate a barren, type M system for the Deepnight campaign.
 */
public class DNBarrenK extends DNBarren {
    private static final Logger logger = LoggerFactory.getLogger(DNBarrenK.class);

    public DNBarrenK(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6(2)) {
            case 2:
                // A system with just a single star and nothing else.
                createEmpty(system);
                break;
            case 3:
                // Hot gas giants close to the star.
                createHotGiants(system);
                break;
            case 4:
                // Some asteroid belts. High chance of a gas giant nearby.
                createAsteroids(system);
                break;
            case 5:
                // A system with a few gas giants some distance out.
                createColdGiants(system);
                break;
            case 6: case 7:
                // A class system with rock worlds and gas giants further out.
                createSimple(system);
                break;
            case 8:
                // A small system with a few rocky worlds close in.
                createRockWorlds(system);
                break;
            case 9:
                // A close binary pair of stars.
                createCloseBinary(system);
                break;
            case 10:
                createMediumBinary(system);
                break;
            case 11:
                createFarBinary(system);
                break;
            case 12:
                // A red giant system, with planets.
                createGiant(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmpty(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [Empty] system [%s]", system.getName()));

        createEmpty(system, new Roller<Luminosity>(Luminosity.V).add(Luminosity.VI, 3).roll(),
                SpectralType.K5.getSpectralType(Die.dieV(5)));

        setDescription(system, null);
    }

    public void createAsteroids(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [Asteroids] system [%s]", system.getName()));

        createAsteroids(system, new Roller<Luminosity>(Luminosity.V).add(Luminosity.VI, 2).roll(),
                SpectralType.K5.getSpectralType(Die.dieV(5)));

        if (Die.d3() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }
    /**
     * Creates a simple system around an M class main sequence star. A simple system is one with a pretty traditional
     * layout, with rocky worlds in the inner system, and gassy worlds towards the outer edge.
     */
    public void createSimple(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [Simple] system [%s]", system.getName()));

        Luminosity luminosity = Luminosity.V;
        SpectralType hrType = SpectralType.M5.getSpectralType(Die.dieV(6));

        createSimple(system, luminosity, hrType);

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     *
     * @param system
     * @throws DuplicateObjectException
     */
    public void createRockWorlds(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RockWorlds] system [%s]", system.getName()));

        Luminosity luminosity = new Roller<>(Luminosity.IV).add(Luminosity.V, 3).
                add(Luminosity.VI, 6).roll();
        SpectralType hrType = SpectralType.M5.getSpectralType(Die.dieV(5));

        createRockWorlds(system, luminosity, hrType);

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }


    /**
     * This type of star system only has a dust belt and a type M sub-dwarf. No major planets have formed.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createDustOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [DustOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M3.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 75_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.DustDisc, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    public void createAsteroidsOnly(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [AsteroidsOnly] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 60_000_000 + Die.dieV(25_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        List<Planet> allPlanets;

        allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }


    public void createHotGiant(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [EpiStellarJovian.properties] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 1_500_000 + Die.dieV(500_000);
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> allPlanets;
        if (Die.d3() == 1) {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Sokarian, distance);
        } else {
            allPlanets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Osirian, distance);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        system.setPlanets(allPlanets);
        setDescription(system, null);
    }

    /**
     * A star system where the main world is a GeoHelian world.
     */
    public void createHelian(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [Helian] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 2_500_000 + Die.die(500_000);
        int  numPlanets = Die.d4() + 1;
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> planets;
        if (Die.d3() == 1) {
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Halcyonic, distance);
        } else {
            distance *= 3;
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Hyperionic, distance);
        }
        system.addPlanets(planets);

        for (int i = 2; i <= numPlanets; i++) {
            distance *= 1.2;
            distance += Die.die(2_000_000, 5);

            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
            PlanetType type = getDwarfTerrestrial(zone);

            name = StarSystemFactory.getPlanetName(primary, i);
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, type, distance);
            system.addPlanets(planets);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }
    /**
     * Create a system with a single red dwarf star, with some worlds in the outer system. Most
     * worlds are small Jovians or icy worlds.
     *
     * @param system    Star system to create planets in.
     */
    public void createColdWorlds(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M9.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long distance = (2 + Die.d4(2)) * Physics.AU + Die.die(Physics.AU);
        String name = StarSystemFactory.getPlanetName(primary, 1);
        List<Planet> planets = null;

        if (Die.d2() == 1) {
            // A small, cold, jovian world.
            planets = factory.createPlanet(system, primary, name, PlanetType.Neptunian, distance);
        } else {
            // A huge, icy, super-Earth.
            planets = factory.createPlanet(system, primary, name, PlanetType.Thean, distance);
        }
        system.addPlanets(planets);

        distance *= 2;
        name = StarSystemFactory.getBeltName(primary, 2);
        planets = factory.createPlanet(system, primary, name, PlanetType.IceBelt, distance,
                planets.get(planets.size()-1), IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        setDescription(system, null);
    }

    public void createProto(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);

        // Create a cool red dwarf star.
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star          primary = starGenerator.generatePrimary(Luminosity.VI, SpectralType.M2.getSpectralType(Die.d6()));
        system.addStar(primary);

        PlanetFactory factory = worldgen.getPlanetFactory();

        long    distance = 5_000_000 + Die.die(5_000_000, 2);
        String  name;
        int     orbit = 1;

        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoFerrinian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoLithian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoCarbonian, distance));
        distance += 5_000_000;
        name = StarSystemFactory.getPlanetName(primary, orbit++);
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoGelidian, distance));

        setDescription(system, null);
    }

    private void createHotGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 1500);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3();
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d4() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    private void createColdGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 5, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    public void createCloseBinary(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.CLOSE_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        long distance = 5_000_000 + Die.d6(2) * 200_000;
        Star primary = starGenerator.generatePrimary(Luminosity.V, SpectralType.K5.getSpectralType(Die.dieV(5)));
        primary.setDistance(distance);

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d8());
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hr);

        // Work out orbital distance and period of the pair.
        double totalMass = primary.getMass() + secondary.getMass();

        long period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        primary.setPeriod(period);
        secondary.setPeriod(period);

        primary.setDistance(Physics.round(distance * secondary.getMass() / totalMass));
        secondary.setDistance(Physics.round(distance * primary.getMass() / totalMass));


        primary.setParentId(StarSystem.PRIMARY_COG);
        secondary.setParentId(StarSystem.PRIMARY_COG);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Set up a 'fake' combined star with the total mass and heat output of the binary pair.
        String name = String.format("%s %s/%s", system.getName(),
                primary.getName().replaceAll(".* ", ""),
                secondary.getName().replaceAll(".* ", ""));

        Star cog = new Star(name, system, primary, secondary);

        String planetName;
        List<Planet> planets = new ArrayList<>();
        int belt = 1, orbit = 1;
        distance *= (8 + Die.d4());

        int numPlanets = Die.d4() + 1;
        while (numPlanets-- > 0) {
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(cog, distance));
            System.out.println(zone);
            PlanetType type;

            switch (Die.d6()) {
                case 1:
                    type = getJovian(zone);
                    break;
                case 2:
                case 3:
                    type = getTerrestrial(zone);
                    break;
                default:
                    type = getDwarfTerrestrial(zone);
                    break;
            }

            planetName = StarSystemFactory.getPlanetName(cog, orbit++);
            planets = planetFactory.createPlanet(system, cog, planetName, type, distance);
            system.addPlanets(planets);

            distance *= 2;
        }

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getBeltName(cog, belt);
            planets = planetFactory.createPlanet(system, cog, planetName, PlanetType.AsteroidBelt, distance);
            logger.info(String.format("Created world [%s]", planetName));
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }

    public void createGiant(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RedGiant] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Create a red giant star.
        Star primary = starGenerator.generatePrimary(Luminosity.III, SpectralType.K3.getSpectralType(Die.dieV(4)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        long distance;
        int  orbit = 1;

        PlanetType type;

        if (Die.d2() == 1) {
            type = PlanetType.Hermian;
            distance = (50 + Die.dieV(6)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            type = PlanetType.Lutian;
            distance = (100 + Die.dieV(8)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            type = PlanetType.NecroGaian;
            distance = (150 + Die.dieV(12)) * Physics.MKM;
            String name = StarSystemFactory.getPlanetName(primary, orbit++);
            List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        distance = (750 + Die.dieV(100)) * Physics.MKM;
        type = PlanetType.Poseidonic;
        String name = StarSystemFactory.getPlanetName(primary, orbit++);
        List<Planet> planets = planetFactory.createPlanet(system, system.getStars().get(0),
                name, type, distance);
        system.addPlanets(planets);

        if (Die.d2() == 1) {
            distance = (1800 + Die.dieV(400)) * Physics.MKM;
            type = PlanetType.Saturnian;
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            distance = (3800 + Die.dieV(800)) * Physics.MKM;
            type = PlanetType.Neptunian;
            name = StarSystemFactory.getPlanetName(primary, orbit++);
            planets = planetFactory.createPlanet(system, system.getStars().get(0),
                    name, type, distance);
            system.addPlanets(planets);
        }

        // Kuiper Belt
        distance = (70 + Die.dieV(10)) * Physics.AU;
        type = PlanetType.IceBelt;
        name = StarSystemFactory.getBeltName(primary, 1);
        planets = planetFactory.createPlanet(system, system.getStars().get(0),
                name, type, distance, IceBelt.IceBeltFeature.KuiperBelt);
        system.addPlanets(planets);

        // Oort Cloud
        if (Die.d100() <= 90) {
            name = StarSystemFactory.getOortCloudName(primary, 1);
            type = PlanetType.OortCloud;

            distance = (long)(Physics.AU * 10_000 * primary.getMass());
            planets = planetFactory.createPlanet(system, primary, name, type, distance,
                    OortCloud.OortCloudFeatures.InnerOort);
            system.addPlanets(planets);

            if (Die.d100() <= 90) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                distance = (long)(Physics.AU * 40_000 * primary.getMass());
                planets = planetFactory.createPlanet(system, primary, name, type, distance,
                        OortCloud.OortCloudFeatures.OuterOort);
                system.addPlanets(planets);
            }
        }
        system.addTradeCode(StarSystemCode.Ba);

        setDescription(system, null);
    }

    public void createMediumBinary(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [MediumBinary] system [%s]", system.getName()));

        Luminosity      luminosity = Luminosity.V;
        SpectralType    hrType = SpectralType.K4.getSpectralType(Die.dieV(5));
        long            starDistance = (30 + Die.die(60)) * Physics.AU + Die.die(Physics.AU);

        createBinary(system, luminosity, hrType, starDistance);

        // Good chance of it suffering from solar flares.
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d8() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }

    public void createFarBinary(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenK] [MediumBinary] system [%s]", system.getName()));

        Luminosity      luminosity = Luminosity.V;
        SpectralType    hrType = SpectralType.K4.getSpectralType(Die.dieV(5));
        long            starDistance = (100 + 25 * Die.die(6, 2)) * Physics.AU + Die.die(Physics.AU * 25);

        createBinary(system, luminosity, hrType, starDistance);

        // Good chance of it suffering from solar flares.
        if (Die.d4() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d8() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }
}
