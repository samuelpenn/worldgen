/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.systems.generators.deepnight;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.systems.generators.Deepnight;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines random planet tables for barren star systems.
 */
public abstract class DNBarren extends Deepnight {
    private static final Logger logger = LoggerFactory.getLogger(DNBarren.class);

    public DNBarren(WorldGen worldgen) {
        super(worldgen);
    }

    protected PlanetType getBelt(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                // Nothing here.
                break;
            case Epistellar:
                roller.add(PlanetType.VulcanianBelt);
                break;
            case Hot:
                roller.add(PlanetType.MetallicBelt, PlanetType.VulcanianBelt);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.AsteroidBelt, 3).add(PlanetType.MetallicBelt);
                break;
            case Outer:
                roller.add(PlanetType.AsteroidBelt, 3).add(PlanetType.IceBelt);
                break;
            case Cold:
                roller.add(PlanetType.IceBelt, 3).add(PlanetType.AsteroidBelt);
                break;
            default:
                roller.add(PlanetType.IceBelt);
                break;
        }
        return roller.roll();
    }


    protected PlanetType getDwarfTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Janian, PlanetType.Ferrinian);
                break;
            case Hot:
                roller.add(PlanetType.Hermian, 3).add(PlanetType.Ferrinian);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.EuArean, 3).add(PlanetType.Selenian, 2).
                        add(PlanetType.AreanLacustric).add(PlanetType.Bathic);
                break;
            case Outer:
                roller.add(PlanetType.EuArean, 9).add(PlanetType.Selenian, 3).
                        add(PlanetType.AreanLacustric).add(PlanetType.MesoArean).
                        add(PlanetType.Bathic).add(PlanetType.Cerean).add(PlanetType.Vestian);
                break;
            case Cold:
                roller.add(PlanetType.Cerean, PlanetType.Vestian, PlanetType.Gelidian);
                break;
            default:
                roller.add(PlanetType.Gelidian);
        }
        return roller.roll();
    }

    protected PlanetType getTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.JaniLithic);
                break;
            case Hot:
                roller.add(PlanetType.Lutian).add(PlanetType.BathyGaian);
                break;
            case Inner:
                roller.add(PlanetType.Cytherean, 3).add(PlanetType.Lutian).add(PlanetType.BathyPelagic).
                        add(PlanetType.MesoGaian).add(PlanetType.EuGaian);
                break;
            case Middle:
                roller.add(PlanetType.NecroGaian, 3).add(PlanetType.Cytherean, PlanetType.BathyPelagic);
                break;
            case Outer: case Cold:
                roller.add(PlanetType.Ymirian);
                break;
            default:
                roller.add(PlanetType.NecroGaian);
        }

        return roller.roll();
    }

    protected PlanetType getJovian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                roller.add(PlanetType.Junic, 3).add(PlanetType.SuperJunic);
                break;
            case Epistellar:
                roller.add(PlanetType.Junic).add(PlanetType.Osirian, 3).add(PlanetType.Sokarian, 6);
                break;
            case Hot:
                roller.add(PlanetType.Junic).add(PlanetType.Osirian, 3).add(PlanetType.Sokarian, 6);
                break;
            case Inner: case Middle:
                roller.add(PlanetType.Jovic).add(PlanetType.Poseidonic, 3);
                break;
            case Outer:
                roller.add(PlanetType.Poseidonic).add(PlanetType.Jovic, 3);
                break;
            case Cold:
                roller.add(PlanetType.Jovic).add(PlanetType.Saturnian);
                break;
            default:
                roller.add(PlanetType.Neptunian);
        }
        return roller.roll();
    }

    protected PlanetType getHelian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Halcyonic, 3).add(PlanetType.Thetusean);
                break;
            case Hot: case Inner: case Middle:
                roller.add(PlanetType.Hyperionic);
                break;
            case Outer:
                roller.add(PlanetType.Hyperionic).add(PlanetType.Thean);
                break;
            default:
                roller.add(PlanetType.Thean);
                break;
        }
        return roller.roll();
    }

    protected void createSimple(StarSystem system, Luminosity luminosity, SpectralType hrType) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarren] [Simple] system [%s] %s %s", system.getName(), hrType, luminosity));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M5.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        int  orbit = 1;
        int  belt = 1;
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int num = Die.d4() + 1;
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));
            if (Die.d3() == 1) {
                type = getTerrestrial(CircumstellarZone.getZone(k));
            }

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        if (Die.d4() == 1) {
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
            distance *= 1.2;
        } else {
            distance += Die.die(distance) / 2;
        }

        num = Die.d3();
        while (num-- > 0) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 1.5;
            distance += Die.die(distance) / 5;
        }

        if (Die.d2() == 1) {
            // Possibly a kuiper belt at this point.
            String      name = StarSystemFactory.getPlanetName(primary, belt++);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getBelt(CircumstellarZone.getZone(k));

            distance *= 1.2;
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance,
                    IceBelt.IceBeltFeature.KuiperBelt);
            system.addPlanets(planets);
        }

        // Oort Clouds
        addOortClouds(planetFactory, system, primary);

        // Good chance of it suffering from solar flares.
        if (Die.d3() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d8() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }
    }

    /**
     * Create a binary star system, with the two stars a dozen or so AU distant.
     */
    protected void createBinary(StarSystem system, Luminosity luminosity, SpectralType hrType) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarren] [Binary] system [%s] %s %s", system.getName(), hrType, luminosity));

        system.setType(StarSystemType.CLOSE_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        Star primary = starGenerator.generatePrimary(luminosity, hrType);

        // Get a possibly cooler variant of the primary star. There is a 50% chance that
        // it is at least one step cooler, and 50% chance for each step down beyond that.
        SpectralType hr = primary.getSpectralType().getSpectralType(-Die.d6());
        Star secondary = starGenerator.generateSecondary(luminosity.getCompanionStar(), hr.getSpectralType(-Die.d6(2)));

        // Work out orbital distance and period of the pair.
        long    distance = Physics.MKM * Die.d6(2) * 10;
        double  totalMass = primary.getMass() + secondary.getMass();

        long period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        primary.setPeriod(period);
        secondary.setPeriod(period);
        primary.setDistance(Physics.round(distance * secondary.getMass() / totalMass));
        secondary.setDistance(Physics.round(distance * primary.getMass() / totalMass));
        primary.setParentId(StarSystem.PRIMARY_COG);
        secondary.setParentId(StarSystem.PRIMARY_COG);

        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // Set up a 'fake' combined star with the total mass and heat output of the binary pair.
        String name = String.format("%s %s/%s", system.getName(),
                primary.getName().replaceAll(".* ", ""),
                secondary.getName().replaceAll(".* ", ""));

        Star cog = new Star(name, system, primary, secondary);

        String planetName;
        List<Planet> planets = new ArrayList<>();
        int belt  = 1, orbit = 1;
        distance *= (4 + Die.d4());

        int numPlanets = Die.d4() + 1;
        while (numPlanets-- > 0) {
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(cog, distance));
            PlanetType        type;

            switch (Die.d6()) {
                case 1:
                    type = getJovian(zone);
                    break;
                case 2: case 3:
                    type = getTerrestrial(zone);
                    break;
                default:
                    type = getDwarfTerrestrial(zone);
                    break;
            }

            planetName = StarSystemFactory.getPlanetName(cog, orbit++);
            planets = planetFactory.createPlanet(system, cog, planetName, type, distance);
            system.addPlanets(planets);

            distance *= 2;
        }

        if (Die.d2() == 1) {
            planetName = StarSystemFactory.getBeltName(cog, belt);
            planets = planetFactory.createPlanet(system, cog, planetName, PlanetType.IceBelt, distance,
                    IceBelt.IceBeltFeature.KuiperBelt);
            logger.info(String.format("Created world [%s]", planetName));
            system.addPlanets(planets);
        }

        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);

    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     */
    protected void createRockWorlds(StarSystem system, Luminosity luminosity, SpectralType hrType) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarren] [RockWorlds] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(luminosity, hrType);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d4() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getDwarfTerrestrial(CircumstellarZone.getZone(k));

            logger.info(String.format("createRockWorlds: [%d] [%dK] [%s] [%s]",
                    orbit, k, CircumstellarZone.getZone(k), type));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 3;
        }

        // Potentially, add an asteroid belt in as well. Probably an icebelt by this point.
        if (Die.d2() == 1) {
            distance *= 2;
            String      name = StarSystemFactory.getBeltName(primary, 1);
            int k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType type = getBelt(CircumstellarZone.getZone(k));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);

            system.addPlanets(planets);
        }

        addOortClouds(planetFactory, system, primary);
    }

    protected void createGasWorlds(StarSystem system, Luminosity luminosity, SpectralType hrType) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarren] [GasWorlds] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(luminosity, hrType);
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = 0;
        switch (Die.d6()) {
            case 1: case 2: case 3:
                distance = Physics.getOrbitWithTemperature(primary, 500);
                break;
            case 4: case 5:
                distance = Physics.getOrbitWithTemperature(primary, 1_000);
                break;
            case 6:
                distance = Physics.getOrbitWithTemperature(primary, 2_000);
                break;
        }
        distance += Die.die(distance / 5, 2);

        // Create a random number of Jovian worlds.
        int numPlanets = Die.d4() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            if (orbit > 1 && Die.d3() == 1) {
                type = getHelian(CircumstellarZone.getZone(k));
            }
            while (type == null) {
                distance = 1 + distance * 2;
                k = Physics.getTemperatureOfOrbit(primary, distance);
                type = getJovian(CircumstellarZone.getZone(k));
            }

            logger.info(String.format("createGasWorlds: [%d] [%dK] [%s] [%s]",
                    orbit, k, CircumstellarZone.getZone(k), type));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 2) + distance / 2;
            if (distance < Physics.AU) {
                // If the system is really small, expand it a bit quicker.
                distance *= 2;
            }
        }

        // Potentially, add an asteroid belt in as well. Probably an icebelt by this point.
        if (Die.d2() == 1) {
            distance *= (Die.d2() + 1);
            String      name = StarSystemFactory.getBeltName(primary, 1);
            int k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType type = getBelt(CircumstellarZone.getZone(k));
            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance,
                    IceBelt.IceBeltFeature.KuiperBelt);
            system.addPlanets(planets);
        }

        addOortClouds(planetFactory, system, primary);
    }

    protected void createPlanets(StarSystem system, Star star, PlanetFactory factory, int ambient, long maxDistance, int numPlanets) {
        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(star, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        factory.setAmbient(ambient);
        int mod = 0;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(star, orbit);
            int         k = Physics.getTemperatureOfOrbit(ambient, star, distance);
            PlanetType  type = null;

            CircumstellarZone zone = CircumstellarZone.getZone(k);
            switch (Die.d6(2) + mod) {
                case 2: case 3:
                    type = getHelian(zone);
                    mod += 1;
                    break;
                case 4: case 5: case 6:
                    type = getJovian(zone);
                    if (orbit > 1) {
                        distance *= 1.2;
                    }
                    mod += 2;
                    break;
                case 8: case 9:
                    type = getTerrestrial(zone);
                    break;
                case 10:
                    type = getBelt(zone);
                    break;
                default:
                    type = getDwarfTerrestrial(zone);
                    break;
            }

            List<Planet> planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);
            if (type.getGroup() == PlanetGroup.Jovian) {
                distance *= 1.2;
            }

            distance += Die.die(distance / 3, 2) + distance / 3;
            if (maxDistance > 0 && Die.die(distance, 3) / 3 > maxDistance) {
                break;
            }
        }

    }

    public void createBinary(StarSystem system, Luminosity luminosity, SpectralType hrType, long starDistance) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarren] [Binary] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        system.setType(StarSystemType.MEDIUM_BINARY);
        Star primary = starGenerator.generatePrimary(luminosity, hrType);
        primary.setDistance(0);
        primary.setPeriod(0);
        primary.setStandardMass();
        primary.setStandardRadius();

        system.addStar(primary);
        Star secondary = starGenerator.generateSecondary(Luminosity.VI, hrType.getCompanion());
        secondary.setDistance(starDistance);
        secondary.setStandardMass();
        secondary.setStandardRadius();

        long period = Physics.getOrbitalPeriod(primary.getMass() * Physics.SOL_MASS, starDistance * 1_000);
        secondary.setPeriod(period);

        secondary.setParentId(primary.getId());
        system.addStar(secondary);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 500);
        distance += Die.die(distance / 5, 2);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d6() + 2;
        int ambient = Physics.getTemperatureOfOrbit(secondary, starDistance);
        planetFactory.setAmbient(ambient);
        createPlanets(system, primary, planetFactory, ambient, starDistance / 10, numPlanets);

        numPlanets = Die.d6();
        ambient = Physics.getTemperatureOfOrbit(primary, starDistance);
        planetFactory.setAmbient(ambient);
        createPlanets(system, secondary, planetFactory, ambient, starDistance / 10, numPlanets);

        // Good chance of it suffering from solar flares.
        if (Die.d3() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        if (Die.d8() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }


    public static void main(String[] args) {
        Star star = new Star();
        star.setSpectralType(SpectralType.M5);
        star.setLuminosity(Luminosity.V);
        StarGenerator.defineMassAndRadius(star);

        System.out.println(Physics.getSolarConstant(star));
        System.out.println(star.getMass());
    }
}
