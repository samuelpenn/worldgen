/*
 * Copyright (c) 2019, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.*;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.Sector;

import java.util.List;

import static net.notasnark.worldgen.astro.planets.generators.Belt.BeltFeature.HotGas;
import static net.notasnark.worldgen.astro.planets.generators.Belt.BeltFeature.ProtoPlanets;

/**
 * Generate a young star system, either with proto-planetary discs or proto-planets.
 */
public class YoungStar extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(YoungStar.class);

    public YoungStar(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            case 1: case 2: case 3:
                createHotGas(system);
                break;
            case 4: case 5:
                createProtoPlanets(system);
                break;
            default:
                createHotGas(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise " + system.getZone() + " star system " + system.getName());

        CivilisationGenerator generator = null;

        if (civName != null) {
            logger.info(String.format("Civilisation name set to be [%s]", civName));
            generator = getCivilisationByName(system);
        } else {

        }
        if (generator != null) {
            generator.generate();
        }
    }

    private List<Planet> addVulcanianBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.VulcanianBelt, distance);
    }

    private List<Planet> addAsteroidBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);
    }

    private List<Planet> addIceBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.IceBelt, distance);
    }

    /**
     * Creates a star typical for younger star systems.
     */
    private Star createYoungStar(StarSystem system) throws DuplicateStarException {
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        Star primary;

        switch (Die.d6()) {
            case 1: case 2: case 3:
                primary = starGenerator.generatePrimary(Luminosity.VI,
                        SpectralType.K5.getSpectralType(Die.dieV(6)));
                break;
            case 4: case 5:
                primary = starGenerator.generatePrimary(Luminosity.V,
                        SpectralType.G5.getSpectralType(Die.dieV(6)));
                break;
            default:
                primary = starGenerator.generatePrimary(Luminosity.IV,
                        SpectralType.F5.getSpectralType(Die.dieV(6)));
                break;
        }
        if (Die.d3() == 1) system.addTradeCode(StarSystemCode.Sf);
        if (Die.d10() == 1) system.addTradeCode(StarSystemCode.Ji);
        system.addStar(primary);
        if (system.hasTradeCode(StarSystemCode.Sf, StarSystemCode.Ji)) {
            system.setZone(Zone.AMBER);
        }
        system.addTradeCode(StarSystemCode.Ba);

        return primary;
    }

    /**
     * Creates a star system with a single star and no proto-planetary disc or planets.
     */
    public void createLoneStar(StarSystem system) throws DuplicateStarException {
        Star primary = createYoungStar(system);

        setDescription(system, null);
    }

    /**
     * A star system with rings of dust and gas around them. No planets have formed yet.
     */
    public void createHotGas(StarSystem system) throws DuplicateStarException {
        Star primary = createYoungStar(system);

        PlanetFactory factory = worldgen.getPlanetFactory();
        int orbit = 1;
        String name = StarSystemFactory.getBeltName(primary, orbit++);
        long   distance = primary.getHotDistance() * 2;

        PlanetFeature metalType = GeneralFeature.NoMetals;
        switch (Die.d6()) {
            case 1: case 2: case 3:
                metalType = GeneralFeature.LowMetals;
                break;
            case 4: case 5:
                metalType = GeneralFeature.NoMetals;
                break;
            case 6:
                metalType = GeneralFeature.HighMetals;
                break;
        }

        // Disc of gas and dust.
        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.DustDisc, distance,
                HotGas, metalType));
        int numDiscs = 3 + Die.d3();
        while (numDiscs-- > 0) {
            Planet furthest = system.getFurthestPlanet();
            distance = furthest.getDistance() + furthest.getRadius() * 3;
            name = StarSystemFactory.getBeltName(primary, orbit++);
            system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.DustDisc, distance,
                    HotGas, metalType));
        }

        setDescription(system, null);
    }

    /**
     * Creates a young star system which hasn't yet formed planets. It just has a proto-planetary
     * dust disc surrounding it, which is still cooling.
     */
    @SuppressWarnings("WeakerAccess")
    public void createProtoPlanets(StarSystem system) throws DuplicateStarException {
        Star primary = createYoungStar(system);

        PlanetFactory factory = worldgen.getPlanetFactory();
        int orbit = 1;
        int disc = 1;
        String name;
        long   distance;

        PlanetFeature metalType = GeneralFeature.HighMetals;

        int numDiscs = 3 + Die.d3();
        while (numDiscs-- > 0) {
            Planet furthest = system.getFurthestPlanet();
            if (furthest == null) {
                distance = primary.getHotDistance() * 2;
            } else {
                distance = furthest.getDistance() + furthest.getRadius() * 2;
                if (furthest.getType().getGroup() != PlanetGroup.Belt) {
                    distance += 5_000_000;
                }
            }

            name = StarSystemFactory.getBeltName(primary, disc++);
            system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.DustDisc, distance, furthest,
                    ProtoPlanets, metalType));

            if (numDiscs > 0) {
                if (Die.d2() == 1) {
                    PlanetType type = PlanetType.ProtoLithian;
                    furthest = system.getFurthestPlanet();
                    distance = furthest.getDistance() + furthest.getRadius() * 2;
                    name = StarSystemFactory.getPlanetName(primary, orbit++);
                    system.addPlanets(factory.createPlanet(system, primary, name, type, distance));
                } else {

                }
            }
        }

        setDescription(system, null);
    }

    @SuppressWarnings("WeakerAccess")
    public void createYoungSystem(StarSystem system) throws DuplicateStarException {
        Star primary = createYoungStar(system);

        PlanetFactory factory = worldgen.getPlanetFactory();
        int orbit = 1;
        String name = StarSystemFactory.getPlanetName(primary, orbit++);
        long   distance = primary.getHotDistance() * 2;

        system.addPlanets(factory.createPlanet(system, primary, name, PlanetType.ProtoLithic, distance));

        setDescription(system, null);
    }
}
