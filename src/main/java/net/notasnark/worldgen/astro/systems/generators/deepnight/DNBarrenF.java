/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators.deepnight;

import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemCode;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.sectors.Sector;

/**
 * Generate a barren, type F system for the Deepnight campaign.
 */
public class DNBarrenF extends DNBarren {
    private static final Logger logger = LoggerFactory.getLogger(DNBarrenF.class);

    public DNBarrenF(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6()) {
            case 1:
                // A system with just a single star and nothing else.
                createEmpty(system);
                break;
            case 2:
                // Some asteroid belts. High chance of a gas giant nearby.
                createAsteroids(system);
                break;
            case 3:
                // A close binary pair of stars.
                createBinary(system);
                break;
            case 4:
                // A small system with a few rocky worlds close in.
                createRockWorlds(system);
                break;
            default:
                // A class system with rock worlds and gas giants further out.
                createSimple(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        // No colonies here.
    }

    /**
     * This system is empty, it only has a single red dwarf star and no planets.
     */
    public void createEmpty(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenF] [Empty] system [%s]", system.getName()));

        createEmpty(system, new Roller<Luminosity>(Luminosity.IV).add(Luminosity.V, 3).roll(),
                SpectralType.F5.getSpectralType(Die.dieV(5)));

        setDescription(system, null);
    }

    public void createAsteroids(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenF] [Asteroids] system [%s]", system.getName()));

        createAsteroids(system, Luminosity.III, SpectralType.F5.getSpectralType(Die.dieV(5)));
        if (Die.d3() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }

    public void createBinary(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenF] [Binary] system [%s]", system.getName()));

        createBinary(system, Luminosity.IV, SpectralType.F3.getSpectralType(Die.dieV(4)));

        setDescription(system, null);
    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     */
    public void createRockWorlds(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RockWorlds] system [%s]", system.getName()));

        Luminosity luminosity = new Roller<>(Luminosity.IV, Luminosity.V).roll();
        SpectralType hrType = SpectralType.F5.getSpectralType(Die.dieV(5));

        createRockWorlds(system, luminosity, hrType);

        setDescription(system, null);
    }


    /**
     * Creates a simple system around an M class main sequence star. A simple system is one with a pretty traditional
     * layout, with rocky worlds in the inner system, and gassy worlds towards the outer edge.
     */
    public void createSimple(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenF] [SimpleSystem] system [%s]", system.getName()));

        Luminosity luminosity = Luminosity.V;
        SpectralType hrType = SpectralType.F5.getSpectralType(Die.dieV(6));

        createSimple(system, luminosity, hrType);

        setDescription(system, null);
    }
}
