package net.notasnark.worldgen.astro.systems;

import net.notasnark.worldgen.astro.sectors.SubSector;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Government;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.codes.StarPort;

import java.util.ArrayList;
import java.util.List;

public class UWP {
    private final String line;
    private final String uwp;
    private String name;

    public UWP(String name, String line) {
        this.name = name;
        this.line = line;
        this.uwp = line.substring(19, 28);
    }

    public String getLine() {
        return line;
    }

    public String getUWP() {
        return uwp;
    }

    public String getName() {
        if (this.name != null) {
            return name;
        } else {
            String name = line.substring(0, 14).trim();

            if (name.length() == 0) {
                int x = getX();
                int y = getY();
                name = SubSector.getSubSector(x, y) + "-" + line.substring(14, 18);
            }
            return name;
        }
    }

    public int getX() {
        String coord = line.substring(14, 18);
        return StarSystemFactory.getXCoord(coord);
    }

    public int getY() {
        String coord = line.substring(14, 18);
        return StarSystemFactory.getYCoord(coord);
    }

    public StarPort getStarPort() {
        return StarPort.valueOf(uwp.substring(0, 1));
    }

    public int getSize() {
        return Integer.parseInt(uwp.substring(1, 2), 24) * 1600;
    }

    public Atmosphere getAtmosphere() {
        Atmosphere atmosphere = Atmosphere.Vacuum;
        int code = Integer.parseInt(uwp.substring(2, 3), 16);

        switch (code) {
            case 0:
                atmosphere = Atmosphere.Vacuum;
                break;
            case 2: case 4: case 7: case 9:
                atmosphere = Atmosphere.Tainted;
                break;
            case 10: case 15:
                atmosphere = Atmosphere.Exotic;
                break;
            case 11:
                atmosphere = Atmosphere.Chlorine;
                break;
            case 12:
                atmosphere = Atmosphere.Flourine;
                break;
            default:
                atmosphere = Atmosphere.Standard;
                break;
        }

        return atmosphere;
    }

    public Pressure getPressure() {
        Pressure pressure = Pressure.None;
        int code = Integer.parseInt(uwp.substring(2, 3), 16);

        switch (code) {
            case 0:
                pressure = Pressure.None;
                break;
            case 1:
                pressure = Pressure.Trace;
                break;
            case 2: case 3:
                pressure = Pressure.VeryThin;
                break;
            case 4: case 5:
                pressure = Pressure.Thin;
                break;
            case 8: case 9:
                pressure = Pressure.Dense;
                break;
            case 12: case 13:
                pressure = Pressure.VeryDense;
                break;
            case 14:
                pressure = Pressure.Thin;
                break;
            default:
                pressure = Pressure.Standard;
                break;
        }

        return pressure;
    }

    public int getHydrosphere() {
        return Integer.parseInt(uwp.substring(3, 4), 16) * 10;
    }

    public long getPopulation() {
        int code = Integer.parseInt(uwp.substring(4, 5), 16);
        int p = Integer.parseInt(line.substring(51, 52));

        if (code == 0) {
            return 0;
        } else if (code == 1) {
            return p;
        }
        return (long) Math.pow(10, code) * p;
    }

    public Government getGovernment() {
        Government government = Government.Anarchy;
        int code = Integer.parseInt(uwp.substring(5, 6), 16);

        switch (code) {
            case 0:
                government = Government.Anarchy;
                break;
            case 1:
                government = Government.Corporation;
                break;
            case 2:
                government = Government.ParticipatingDemocracy;
                break;
            case 3:
                government = Government.SelfPerpetuatingOligarchy;
                break;
            case 4:
                government = Government.RepresentativeDemocracy;
                break;
            case 5:
                government = Government.FeudalTechnocracy;
                break;
            case 6:
                government = Government.Captive;
                break;
            case 7:
                government = Government.Balkanization;
                break;
            case 8:
                government = Government.CivilService;
                break;
            case 9:
                government = Government.ImpersonalBureaucracy;
                break;
            case 10:
                government = Government.CharismaticLeader;
                break;
            case 11:
                government = Government.NonCharismaticLeader;
                break;
            case 12:
                government = Government.CharismaticOligarchy;
                break;
            case 13:
                government = Government.TheocraticDictatorship;
                break;
            case 14:
                government = Government.TheocraticOligarchy;
                break;
            case 15:
                government = Government.TotalitarianOligarchy;
                break;
        }
        return government;
    }

    public int getLawLevel() {
        return Integer.parseInt(uwp.substring(6, 7), 24);
    }

    public int getTechLevel() {
        return Integer.parseInt(uwp.substring(8, 9), 24);
    }

    public String[] getStars() {
        try {
            return line.substring(58).split(" ");
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public List<Star> getStarList() {
        List<Star> stars = new ArrayList<>();
        String[] data = line.substring(58).split(" ");

        int i = 0;
        while (i < data.length) {
            Star star = new Star();
            star.setLuminosity(Luminosity.VII);
            star.setSpectralType(SpectralType.D5);
            try {
                star.setSpectralType(SpectralType.valueOf(data[i]));
            } catch (IllegalArgumentException e) {
                if (data[i].equals("D")) {
                    star.setSpectralType(SpectralType.D5.getSpectralType(Die.d3()));
                }
                stars.add(star);
                i++;
                continue;
            }
            i++;
            try {
                star.setLuminosity(Luminosity.valueOf(data[i]));
            } catch (IllegalArgumentException e) {

            } catch (Throwable e) {
                // No entry, assume V type star.
                star.setLuminosity(Luminosity.V);
            }
            stars.add(star);
            i++;
        }

        return stars;
    }

    private String getBaseCode() {
        return line.substring(30, 31);
    }

    // See https://wiki.travellerrpg.com/Base_Code
    private boolean isCodeA(String codes) {
        for (char c : codes.toCharArray()) {
            if (getBaseCode().equals("" + c)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasNavyBase() {
        return isCodeA("NCADFGHKLPZTU");
    }

    public boolean hasScoutBase() {
        return isCodeA("ASVTU");
    }

    public boolean hasMilitaryBase() {
        return isCodeA("FMQTUZ");
    }

    public boolean hasResearchBase() {
        return getBaseCode().equals("R");
    }

    public boolean hasTradeCode(String code) {
        String codes = " " + line.substring(32, 47) + " ";

        return codes.indexOf(" " + code + " ") > -1;
    }

    public int getOtherWorlds() {
        return Integer.parseInt(line.substring(51, 52));
    }

    public int getBelts() {
        return Integer.parseInt(line.substring(52, 53));
    }

    public int getGasGiants() {
        return Integer.parseInt(line.substring(53, 54));
    }

    public Zone getZone() {
        String c = line.substring(48, 49);
        if (c.equals("A")) {
            return Zone.AMBER;
        } else if (c.equals("R")) {
            return Zone.RED;
        } else {
            return Zone.GREEN;
        }
    }

    public static void main(String[] args) {
        UWP uwp = new UWP("Narval", "Narval        0805 D525688-7  M Ni Da           A  603 Cz G4 V M6 V");

        System.out.println(uwp.getPopulation());
        System.out.println(uwp.getTechLevel());
        System.out.println(uwp.getZone());
        System.out.println(uwp.getBaseCode());
    }
}
