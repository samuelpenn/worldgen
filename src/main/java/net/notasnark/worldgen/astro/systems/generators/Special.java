/*
 * Copyright (c) 2022, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.civ.civilisation.FreeSettlers;
import net.notasnark.worldgen.civ.civilisation.Hermits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;

import java.util.List;

/**
 * Special generators for specific star system designs.
 */
public class Special extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(Special.class);

    public Special(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {
        logger.info("Colonise " + system.getZone() + " star system " + system.getName());

        CivilisationGenerator generator = null;

        if (civName != null) {
            logger.info(String.format("Civilisation name set to be [%s]", civName));
            generator = getCivilisationByName(system);
        } else {
            if (system.getZone() == Zone.GREEN) {
                switch (Die.d6()) {
                    case 1: case 5: case 6:
                        generator = new Hermits(worldgen, system);
                        break;
                    case 2: case 3: case 4:
                        generator = new FreeSettlers(worldgen, system);
                        break;
                    default:
                        break;
                }
            }
        }
        if (generator != null) {
            generator.generate();
        }
    }

    /**
     * Black hole system.
     *
     * @param system        Star System to create belt in.
     */
    @SuppressWarnings("WeakerAccess")
    public void createVilaakasii(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating Vilaakasii"));
        system.rename("Vilaakasii");

        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.B, SpectralType.X3);
        system.addStar(primary);

        Star secondary = starGenerator.generateSecondary(Luminosity.IV, SpectralType.G0);
        secondary.setDistance(5 * Physics.AU);
        system.addStar(secondary);
        system.setType(StarSystemType.CLOSE_BINARY);


        // Place the belt around 2AU from the star.
        long distance = 3 * Physics.AU + Die.dieV(50_000_000);
        String name = StarSystemFactory.getBeltName(primary, 1);

        PlanetFactory factory = worldgen.getPlanetFactory();
        List<Planet> allPlanets;
        allPlanets =  factory.createPlanet(system, system.getStars().get(0),
                name, PlanetType.DustDisc, distance);

        name = StarSystemFactory.getBeltName(secondary, 1);
        distance = 1 * Physics.AU + Die.dieV(30_000_000);
        allPlanets.addAll(factory.createPlanet(system, system.getStars().get(1),
                name, PlanetType.DustDisc, distance));

        name = StarSystemFactory.getPlanetName(secondary, 1);
        distance = 30 * Physics.AU;
        allPlanets.addAll(factory.createPlanet(system, system.getStars().get(1),
                name, PlanetType.Jovic, distance));

        system.addTradeCode(StarSystemCode.Ba);

        system.setPlanets(allPlanets);
        system.setZone(Zone.AMBER);
        setDescription(system, null);
    }

    private List<Planet> addVulcanianBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.VulcanianBelt, distance);
    }

    private List<Planet> addAsteroidBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.AsteroidBelt, distance);
    }

    private List<Planet> addIceBelt(StarSystem system, String name, long distance) {
        return worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, PlanetType.IceBelt, distance);
    }

}
