/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.Sector;

import java.util.ArrayList;
import java.util.List;

/**
 * Generates a star system with a hot blue or white giant as its primary star. Such systems will have few,
 * if any, planets.
 */
public class BlueGiant extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(BlueGiant.class);
    public BlueGiant(WorldGen worldgen) {
        super(worldgen);
    }

    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        createSuperGiant(system);
        updateStarSystem(system);

        return system;
    }

    public void colonise(StarSystem system) {

    }

    /**
     * Create a typical hot super giant. Such a system is low in metals and has no planets of any type.
     *
     * @param system    System to create star in.
     * @return          Updated star system object.
     */
    public void createSuperGiant(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.SINGLE);

        Star primary = new Star();
        primary.setSystem(system);
        primary.setName(system.getName());

        SpectralType type = SpectralType.B5;
        switch (Die.d6()) {
            case 1: case 2: case 3:
                type = SpectralType.valueOf("B" + (Die.d4() + 5));
                break;
            case 4: case 5: case 6:
                type = SpectralType.valueOf("A" + (Die.d10() - 1));
                break;
        }
        primary.setSpectralType(type);

        switch (Die.d6()) {
            case 1: case 2:
                primary.setLuminosity(Luminosity.Ia);
                break;
            default:
                primary.setLuminosity(Luminosity.Ib);
                break;
        }

        logger.debug(String.format("Creating %s %s super giant",
                primary.getLuminosity(), primary.getSpectralType()));

        starFactory.persist(primary);

        setDescription(system, null);
    }

    /**
     * Creates a blue giant star with a dwarf companion. The dwarf companion may have a planetary system.
     *
     * @param system
     * @throws DuplicateObjectException
     */
    public void createSuperGiantBinary(StarSystem system) throws DuplicateObjectException {
        system.setType(StarSystemType.FAR_BINARY);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);

        Star primary = starGenerator.generatePrimary(Luminosity.Ib, SpectralType.A0.getSpectralType(Die.d6()));
        system.addStar(primary);
        starFactory.persist(primary);

        Star secondary = starGenerator.generateSecondary(Luminosity.VI, SpectralType.M2.getSpectralType(Die.d6(2)));
        secondary.setDistance((500 + Die.d100()) * Physics.AU);
        secondary.setParentId(primary.getId());
        secondary.setPeriod(primary.getPeriod(secondary.getDistance()));
        system.addStar(secondary);
        starFactory.persist(secondary);

        List<Planet> planets = new ArrayList<Planet>();


        // Planets around the primary star.
        PlanetFactory planetFactory = worldgen.getPlanetFactory();
        String planetName;
        long distance = distance = Physics.getOrbitWithTemperature(primary, CircumstellarZone.Hot.getKelvin());
        distance += Die.die(100) * 5_000_000;

        int numPlanets = 0;
        int numBelts = 0;
        while (distance < secondary.getDistance() / 8) {
            if (Die.d3() == 1) {
                int kelvin = Physics.getTemperatureOfOrbit(primary, distance);
                PlanetType type = PlanetFactory.determinePlanetType(kelvin);

                if (type.getGroup() == PlanetGroup.Belt) {
                    planetName = StarSystemFactory.getBeltName(primary, ++numBelts);
                } else {
                    planetName = StarSystemFactory.getPlanetName(primary, ++numPlanets);
                }
                planets.addAll(planetFactory.createPlanet(system, primary, planetName, type, distance));
            }
            distance += Die.die((int) Physics.AU * 5, 2 + numPlanets + numBelts);
        }


        // Planets around the secondary star.
        distance = 50_000_000 + Die.d6(2) * 2_500_000L;
        planetName = StarSystemFactory.getBeltName(secondary, 1);
        planets.addAll(planetFactory.createPlanet(system, secondary, planetName, PlanetType.AsteroidBelt, distance));

        system.setPlanets(planets);
        setDescription(system, null);

    }


}
