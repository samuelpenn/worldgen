/*
 * Copyright (c) 2018, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.systems;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;

import java.text.DecimalFormat;

/**
 * Divides a solar system into zones based on the type of worlds that are likely to be found there.
 * This is based on the orbital temperature in the zone.
 *
 * https://en.wikipedia.org/wiki/Effective_temperature
 * https://en.wikipedia.org/wiki/Planetary_equilibrium_temperature
 *
 * VeryHot  - Too hot for most planets to form.
 * Hot      - Hot, barren worlds.
 * Warm     - Warm end of Goldilocks.
 * Standard - Goldilocks.
 * Cool     - Cool end of Goldilocks.
 * Cold     - Too cold for liquid water.
 * VeryCold - Outer solar system.
 */
public enum CircumstellarZone {
    Exclusion(10_000, "#FFFFFF"), // Metal and rock boil. Highly unlikely to have planets in this zone.
    Epistellar(850, "#800000"),   // Most worlds will be molten.
    Hot(550, "#FF8000"),          // Liquid water will not be present.
    Inner(350, "#FFFF00"),        // Warm side of Goldilocks. Liquid water is possible, but only under pressure.
    Middle(300, "#00FF00"),       // Middle of Goldilocks zone.
    Outer(250, "#00FFFF"),        // Cool side of Goldilocks. Liquid water is possible, but ice is more common.
    Cold(200, "#008080"),         // Beyond the snow line, water and similar liquids are frozen.
    Stygia(75, "#000080"),        // Most liquids are frozen.
    Tartarus(50, "#000040");      // Outer edges of the system. Most gases are frozen.

    private final int kelvin;
    private final String colour;

    /**
     * Define a Circumstellar zone around a star system.
     *
     * @param kelvin    Upper temperature limit of this zone.
     * @param colour    Colour to use when drawing zone boundary.
     */
    private CircumstellarZone(int kelvin, String colour) {
        this.kelvin = kelvin;
        this.colour = colour;
    }

    public boolean isColderThan(CircumstellarZone zone) {
        return this.kelvin < zone.kelvin;
    }

    public boolean isHotterThan(CircumstellarZone zone) {
        return this.kelvin > zone.kelvin;
    }

    /**
     * Gets the upper bound temperature for this zone in Kelvin. Temperatures higher than this will be
     * the next hotter zone.
     *
     * @return  Upper bound in kelvin.
     */
    public int getKelvin() {
        return kelvin;
    }

    public String getColour() {
        return colour;
    }

    /**
     * Gets the lower bound temperature for this zone in Kelvin. Returns 1 K if there are no zones
     * beyond this.
     *
     * @return  Lower bound in kelvin.
     */
    public int getLowerKelvin() {
        int k = 1;
        if (ordinal() < CircumstellarZone.values().length - 1) {
            k = CircumstellarZone.values()[ordinal() + 1].kelvin;
        }
        return k;
    }

    public int getMiddleKelvin() {
        return (getLowerKelvin() + getKelvin()) / 2;
    }

    /**
     * Gets the zone that this orbital temperature equates to.
     *
     * @param kelvin    Orbital temperature.
     * @return          Zone, or null if too hot.
     */
    public static CircumstellarZone getZone(int kelvin) {
        CircumstellarZone zone = null;

        for (CircumstellarZone z : CircumstellarZone.values()) {
            if (kelvin <= z.kelvin) {
                zone = z;
            }
        }

        return zone;
    }

    public static void main(String args[]) {
        Star star = new Star("Foo", null, 0, 0, Luminosity.V, SpectralType.G2);
        StarGenerator.defineMassAndRadius(star);

        for (CircumstellarZone z : CircumstellarZone.values()) {
            long d = Physics.getOrbitWithTemperature(star, z.kelvin);
            System.out.println(z.toString() + ": " + new DecimalFormat().format(d) + " (" + (d / 150_000_000L) + " AU)");
        }

        System.out.println(Physics.getTemperatureOfOrbit(star, 40L * 150_000_000L));
    }
}
