package net.notasnark.worldgen.astro.systems.generators;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.belt.IceBelt;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;

import java.util.ArrayList;
import java.util.List;

public class UWPSystem extends StarSystemGenerator {
    private static final Logger logger = LoggerFactory.getLogger(UWPSystem.class);

    private int orbit = 1;
    private int belt = 1;

    public UWPSystem(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector Sector to generate system in.
     * @param name   Name of the system.
     * @param x      X coordinate of the system in the sector (1-32).
     * @param y      Y coordinate of the system in the sector (1-40).
     * @return Created and persisted star system.
     * @throws DuplicateObjectException If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        return factory.createStarSystem(sector, name.trim(), x, y, StarSystemType.EMPTY);
    }

    @Override
    public void colonise(StarSystem system) {

    }

    private PlanetType getDwarfTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                roller.add(PlanetType.Ferrinian);
                break;
            case Epistellar:
                roller.add(PlanetType.Janian, PlanetType.Ferrinian);
                break;
            case Hot:
                roller.add(PlanetType.Hermian);
                break;
            case Inner:
            case Middle:
                roller.add(PlanetType.AreanLacustric, PlanetType.Selenian);
                break;
            case Outer:
                roller.add(PlanetType.AreanLacustric, 2).add(PlanetType.EuArean, 3).
                        add(PlanetType.MesoArean, PlanetType.EoArean, PlanetType.Bathic);
                break;
            case Cold:
                roller.add(PlanetType.Cerean, PlanetType.Vestian, PlanetType.Gelidian);
                break;
            default:
                roller.add(PlanetType.Gelidian);
        }
        return roller.roll();
    }

    private PlanetType getTerrestrial(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();

        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.JaniLithic);
                break;
            case Hot:
                roller.add(PlanetType.Lutian);
                break;
            case Inner:
                roller.add(PlanetType.Cytherean);
                break;
            case Outer:
            case Cold:
                roller.add(PlanetType.Ymirian);
                break;
            default:
                roller.add(PlanetType.NecroGaian);
        }

        return roller.roll();
    }

    private PlanetType getJovian(CircumstellarZone zone) {
        Roller<PlanetType> roller = new Roller<>();
        switch (zone) {
            case Exclusion:
                break;
            case Epistellar:
                roller.add(PlanetType.Osirian, PlanetType.Sokarian);
                break;
            case Hot:
                roller.add(PlanetType.Poseidonic);
                break;
            case Inner:
                roller.add(PlanetType.Poseidonic);
                break;
            case Middle:
                roller.add(PlanetType.Poseidonic);
                break;
            case Outer:
                roller.add(PlanetType.Jovic, PlanetType.Poseidonic);
                break;
            case Cold:
                roller.add(PlanetType.Saturnian);
                break;
            default:
                roller.add(PlanetType.Neptunian);
        }

        return roller.roll();
    }


    private PlanetType getSmallBody(UWP uwp) {
        return PlanetType.Cerean;
    }

    private PlanetType getDwarfTerrestrial(UWP uwp) {
        if (uwp.hasTradeCode("Va")) {
            return PlanetType.Selenian;
        } else if (uwp.hasTradeCode("Wa")) {
            return PlanetType.Bathic;
        } else if (uwp.hasTradeCode("De")) {
            return PlanetType.EuArean;
        }
        return PlanetType.MesoArean;
    }

    private PlanetType getTerrestrial(UWP uwp) {
        if (uwp.hasTradeCode("Wa")) {
            if (Die.d4() == 1) {
                return PlanetType.BathyGaian;
            } else {
                return PlanetType.GaianPelagic;
            }
        } else if (uwp.hasTradeCode("De")) {
            return PlanetType.Lutian;
        } else if (uwp.getHydrosphere() > 80) {
            return PlanetType.GaianPelagic;
        }
        return new Roller<PlanetType>(PlanetType.EuGaian, PlanetType.EuGaian, PlanetType.MesoGaian).roll();
    }

    private PlanetType getJovian() {
        return new Roller<PlanetType>(PlanetType.Saturnian, PlanetType.Jovic, PlanetType.Poseidonic, PlanetType.Poseidonic).roll();
    }

    private PlanetType getMoon(UWP uwp) {
        int size = uwp.getSize();

        if (uwp.hasTradeCode("Wa")) {
            if (size < 8000) {
                return PlanetType.Bathic;
            } else {
                return PlanetType.BathyGaian;
            }
        }
        if (uwp.hasTradeCode("De")) {
            if (size < 8000) {
                return PlanetType.EuArean;
            } else {
                return PlanetType.Lutian;
            }
        }
        if (uwp.hasTradeCode("Va")) {
            if (size < 2500) {
                return PlanetType.Cerean;
            } else if (size < 5000) {
                return PlanetType.Selenian;
            }
        }

        if (size < 2500) {
            return PlanetType.Cerean;
        } else if (size < 5000) {
            return PlanetType.Enceladusian;
        } else if (size < 9000) {
            return PlanetType.MesoArean;
        } else {
            return PlanetType.EuGaian;
        }
    }

    public StarSystem createUWPSystem(Sector sector, UWP uwp) throws DuplicateObjectException {
        if (sector == null || sector.getId() == 0) {
            throw new IllegalArgumentException("StarSystem must be part of an existing Sector");
        }

        logger.info(String.format("createUWPSystem: [%s] [%s]", sector.getName(), uwp.getUWP()));

        String name = uwp.getName();
        StarSystem system = factory.createStarSystem(sector, name, uwp.getX(), uwp.getY(), StarSystemType.EMPTY);
        system.setUWP(uwp.getLine());

        String[] starData = uwp.getStars();
        List<Star> starList = uwp.getStarList();

        switch (starList.size()) {
            case 0:
                Star star = new Star();
                star.setLuminosity(Luminosity.V);
                star.setSpectralType(new Roller<SpectralType>(SpectralType.M5, SpectralType.M3, SpectralType.M0, SpectralType.K7, SpectralType.K4, SpectralType.K1, SpectralType.G7, SpectralType.G4).roll().getSpectralType(5));
                starList.add(star);
                createSingle(system, uwp, starList);
                break;
            case 1:
                createSingle(system, uwp, starList);
                break;
            case 2:
                createBinary(system, uwp, starList);
                break;
            case 3:
                createTriple(system, uwp, starList);
                break;
            default:
                // Do nothing for now.
                logger.error("System [" + system.getName() + "] cannot support more than 3 stars, limiting to 3");
                createTriple(system, uwp, starList);
                break;
        }
        system.setZone(uwp.getZone());

        return system;
    }

    private void setTradeCodes(Planet planet, UWP uwp) {
        if (uwp.hasNavyBase()) {
            planet.addTradeCode(TradeCode.NV);
        }
        if (uwp.hasScoutBase()) {
            planet.addTradeCode(TradeCode.SC);
        }
        if (uwp.hasMilitaryBase()) {
            planet.addTradeCode(TradeCode.MI);
        }
        if (uwp.hasResearchBase()) {
            planet.addTradeCode(TradeCode.RE);
        }
        for (TradeCode code : TradeCode.values()) {
            if (uwp.hasTradeCode(code.toString())) {
                planet.addTradeCode(code);
            }
        }
    }

    private List<Planet> createMainWorld(PlanetFactory factory, StarSystem system, Star parent, UWP uwp, long distance) {
        List<Planet> planets;
        PlanetType type;
        String planetName;
        List<PlanetFeature> features = new ArrayList();

        int size = uwp.getSize();
        if (size > 8000) {
            type = getTerrestrial(uwp);
        } else if (size > 2000) {
            type = getDwarfTerrestrial(uwp);
        } else {
            type = getSmallBody(uwp);
        }
        boolean isSatellite = false;
        boolean isBelt = false;
        if (uwp.hasTradeCode("Sa")) {
            isSatellite = true;
        } else if (uwp.hasTradeCode("As")) {
            isBelt = true;
        }

        if (size > 0) {
            size = Math.max(0, size + Die.dieV(800));
        }

        switch (uwp.getAtmosphere()) {
            case Standard: case Tainted:
                features.add(GeneralFeature.HighLife);
                if (uwp.getPressure() == Pressure.Standard || uwp.getPressure() == Pressure.Dense) {
                    features.add(GeneralFeature.VeryHighLife);
                }
                break;
        }
        features.add(GeneralFeature.UWP);

        if (isSatellite) {
            planetName = StarSystemFactory.getPlanetName(parent, orbit++);
            planets = factory.createPlanet(system, parent, planetName, getJovian(), distance, features.toArray(new PlanetFeature[0]));
        } else if (size == 0) {
            planetName = StarSystemFactory.getBeltName(parent, belt++);
            factory.setUWP(uwp);
            planets = factory.createPlanet(system, parent, planetName, PlanetType.AsteroidBelt, distance, GeneralFeature.NoMoons);
            setTradeCodes(planets.get(0), uwp);
        } else {
            planetName = StarSystemFactory.getPlanetName(parent, orbit++);
            factory.setUWP(uwp);
            planets = factory.createPlanet(system, parent, planetName, type, distance, features.toArray(new PlanetFeature[0]));
            planets.get(0).setRadius(size / 2);
            setTradeCodes(planets.get(0), uwp);
        }
        if (isSatellite) {
            int count = 1;
            long moonDistance = 0;
            int parentId = 0;
            Planet parentPlanet = null;
            String baseSatelliteName = null;
            for (Planet p : planets) {
                // Are we counting rings or moons?
                if (p.isMoon()) {
                    if ((size > 0 && !p.isBelt()) || (size == 0 && p.isBelt())) {
                        count++;
                    }
                    moonDistance = p.getDistance();
                } else if (parentId == 0) {
                    parentId = p.getId();
                    parentPlanet = p;
                    baseSatelliteName = p.getName();
                }
            }
            moonDistance *= 1.5;
            if (moonDistance == 0) {
                moonDistance = planets.get(0).getRadius() * 10;
            }
            if (size == 0) {
                planetName = StarSystemFactory.getRingName(baseSatelliteName, count);
                type = PlanetType.IceRing;
            } else {
                planetName = StarSystemFactory.getMoonName(baseSatelliteName, count);
                type = getMoon(uwp);
            }
            factory.setUWP(uwp);
            Planet satellite = factory.createMoon(system, parent, planetName, type, moonDistance, parentPlanet,
                    GeneralFeature.NoMoons, GeneralFeature.UWP);
            satellite.setMoonOf(parentId);
            satellite.setDistance(moonDistance);
            setTradeCodes(satellite, uwp);
            planets.add(satellite);
        }
        system.addPlanets(planets);

        return planets;
    }

    private void createInnerWorlds(PlanetFactory factory, StarSystem system, Star star, long mainWorldDistance, int innerWorlds, boolean hasBelt) {
        long baseDistance = mainWorldDistance;

        if (innerWorlds >= 3) {
            baseDistance = mainWorldDistance / 4;
        } else if (innerWorlds == 2) {
            baseDistance = mainWorldDistance / 3;
        } else if (innerWorlds == 1) {
            baseDistance = mainWorldDistance / 2;
        }

        int modifier = 0;

        if (hasBelt) {
            PlanetType type = PlanetType.VulcanianBelt;
            String name = StarSystemFactory.getBeltName(star, belt++);
            List<Planet> planets = factory.createPlanet(system, star, name, type, mainWorldDistance / 5);
            system.addPlanets(planets);
            modifier++;
        }

        for (int p = 0; p < innerWorlds; p++) {
            PlanetType type;
            long distance = baseDistance * (p + 1) + Die.dieV(baseDistance / 4);
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(star, distance));

            switch (Die.d6() + modifier) {
                case 1:
                    type = PlanetType.Halcyonic;
                    modifier += 2;
                    break;
                case 2:
                case 3:
                    type = getTerrestrial(zone);
                    modifier += 1;
                    break;
                default:
                    type = getDwarfTerrestrial(zone);
                    break;
            }
            if (type != null) {
                String name = StarSystemFactory.getPlanetName(star, orbit++);
                List<Planet> planets = factory.createPlanet(system, star, name, type, distance);
                system.addPlanets(planets);
            }
        }
    }

    /**
     * Create all the outer worlds in the system.
     */
    private void createOuterWorlds(PlanetFactory factory, StarSystem system, Star star, long mainWorldDistance, long maxDistance, int outerWorlds, int gasGiants, int belts) {
        List<Planet> planets;

        long distance = mainWorldDistance;
        while (Die.d6() <= outerWorlds) {
            outerWorlds--;
            distance *= 1.5;
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(star, distance));
            PlanetType type = getDwarfTerrestrial(zone);
            String name = StarSystemFactory.getPlanetName(star, orbit++);
            planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);
        }

        if (belts > 0 && Die.d2() == 1) {
            belts--;
            if (maxDistance > 0) {
                distance *= 1.5;
            } else {
                distance *= 2;
            }
            PlanetType type = PlanetType.AsteroidBelt;
            String name = StarSystemFactory.getBeltName(star, belt++);
            planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);
        }

        while (gasGiants > 0) {
            gasGiants--;
            if (maxDistance > 0) {
                if (distance > maxDistance) {
                    distance *= 1.2;
                } else if (distance > maxDistance / 2) {
                    distance *= 1.5;
                } else {
                    distance *= 1.75;
                }
            } else {
                distance *= 2;
            }
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(star, distance));

            PlanetType type = getJovian(zone);
            String name = StarSystemFactory.getPlanetName(star, orbit++);
            planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);
        }

        while (outerWorlds > 0) {
            outerWorlds--;
            if (maxDistance > 0) {
                if (distance > maxDistance) {
                    distance *= 1.1;
                } else if (distance > maxDistance / 2) {
                    distance *= 1.3;
                } else {
                    distance *= 1.4;
                }
            } else {
                distance *= 1.5;
            }
            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(star, distance));

            PlanetType type = getDwarfTerrestrial(zone);
            String name = StarSystemFactory.getPlanetName(star, orbit++);
            planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);

        }

        while (belts > 0) {
            belts--;
            if (maxDistance > 0) {
                if (distance > maxDistance) {
                    distance *= 1.25;
                } else if (distance > maxDistance / 2) {
                    distance *= 1.5;
                } else {
                    distance *= 1.75;
                }
            } else {
                distance *= 2;
            }
            PlanetType type = PlanetType.IceBelt;
            String name = StarSystemFactory.getBeltName(star, belt++);
            if (maxDistance == 0) {
                planets = factory.createPlanet(system, star, name, type, distance, IceBelt.IceBeltFeature.KuiperBelt);
            } else {
                planets = factory.createPlanet(system, star, name, type, distance);
            }
            system.addPlanets(planets);

        }
    }

    /**
     * Create a system with a single star. This is the simplest setup.
     */
    private void createSingle(StarSystem system, UWP uwp, List<Star> starList) throws DuplicateObjectException {
        logger.info("createSingle: [" + system.getName() + "]");
        system.setType(StarSystemType.SINGLE);
        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);

        Star primary = starGenerator.generatePrimary(starList.get(0).getLuminosity(),
                starList.get(0).getSpectralType());
        system.addStar(primary);

        int gasGiants = uwp.getGasGiants();
        int belts = uwp.getBelts();
        int otherWorlds = Die.d4(2) - 1;

        int innerWorlds = otherWorlds / 3;
        if (otherWorlds > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        int outerWorlds = otherWorlds - innerWorlds;

        long mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);

        PlanetFactory factory = worldgen.getPlanetFactory();
        createInnerWorlds(factory, system, primary, mainWorldDistance, innerWorlds, false);
        createMainWorld(factory, system, primary, uwp, mainWorldDistance);
        createOuterWorlds(factory, system, primary, mainWorldDistance, 0, outerWorlds, gasGiants, belts);

        // Chance of there being Oort clouds.
        long distance = (long) (Physics.AU * 10_000 * primary.getMass());
        int  chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
        if (primary.getMass() < 1) {
            chance = (int) (primary.getMass() * 100);
        }

        if (Die.d100() <= chance) {
            PlanetType type = PlanetType.OortCloud;
            String     name = StarSystemFactory.getOortCloudName(primary, 1);
            List<Planet> planets = factory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance *= 5;
            chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
            if (Die.d100() <= chance) {
                name = StarSystemFactory.getOortCloudName(primary, 2);
                planets = factory.createPlanet(system, primary, name, type, distance);
                system.addPlanets(planets);
            }
        }

        // Smaller dwarf stars have a habit of suffering from solar flares.
        if (Physics.getSolarConstant(primary) < 1) {
            if (Die.d100() > Physics.getSolarConstant(primary) * 1000) {
                system.addTradeCode(StarSystemCode.Sf);
            }
        }
        // A small chance of jump instabilities.
        if (Die.d1000() == 1) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }

    private void createBinary(StarSystem system, UWP uwp, List<Star> starList) throws DuplicateObjectException {
        logger.info("createBinary: [" + system.getName() + "]");

        int numGasGiants = uwp.getGasGiants();
        int numBelts = uwp.getBelts();

        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);
        Star primary = starGenerator.generatePrimary(starList.get(0).getLuminosity(), starList.get(0).getSpectralType());
        Star secondary = starGenerator.generateSecondary(starList.get(1).getLuminosity(), starList.get(1).getSpectralType());

        primary.setStandardRadius();
        primary.setStandardMass();
        secondary.setStandardRadius();
        secondary.setStandardMass();

        if (primary.getLuminosity() == secondary.getLuminosity() && primary.getLuminosity().isSmallerThan(Luminosity.III) && Die.d2() == 1) {
            // If the stars are the same Luminosity, make them close binaries.
            system.setType(StarSystemType.CLOSE_BINARY);
        } else {
            // If they are different, make them far binaries, with a small chance of medium binaries.
            system.setType(StarSystemType.FAR_BINARY);
            if (Die.d4() == 1 && primary.getLuminosity().isSmallerThan(Luminosity.IV) && numGasGiants + numBelts < 4) {
                system.setType(StarSystemType.MEDIUM_BINARY);
            }
        }

        long distance, period;
        double totalMass = primary.getMass() + secondary.getMass();

        switch (system.getType()) {
            case CLOSE_BINARY:
                // Less than 1 AU
                primary.setParentId(StarSystem.PRIMARY_COG);
                secondary.setParentId(StarSystem.PRIMARY_COG);
                distance = Die.d6(3) * 1_000_000 + Die.die(1_000_000);
                if (Die.d10() == 1) {
                    distance /= 2;
                }
                if (distance < 1.1 * (primary.getRadius() + secondary.getRadius())) {
                    distance = (long) (1.1 * (primary.getRadius() + secondary.getRadius()));
                }
                period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
                primary.setPeriod(period);
                secondary.setPeriod(period);
                primary.setDistance(Physics.round(distance * secondary.getMass() / totalMass));
                secondary.setDistance(Physics.round(distance * primary.getMass() / totalMass));
                system.addStar(primary, secondary);
                createCloseBinary(system, uwp, primary, secondary, numGasGiants, numBelts);
                break;
            case MEDIUM_BINARY:
                // Tens of AU
                secondary.setParentId(primary.getId());
                if (Physics.getSolarConstant(primary) < 1.5) {
                    distance = Die.d4(2) * Physics.AU * 10 + Die.die(Physics.AU * 10);
                } else if (Physics.getSolarConstant(primary) < 3) {
                    distance = (4 + Die.d4()) * Physics.AU * 10 + Die.die(Physics.AU * 10);
                } else if (Physics.getSolarConstant(primary) < 10) {
                    distance = (6 + Die.d4()) * Physics.AU * 10 + Die.die(Physics.AU * 10);
                } else {
                    distance = (8 + Die.d4()) * Physics.AU * 10 + Die.die(Physics.AU * 10);
                }
                secondary.setDistance(distance);
                period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
                secondary.setPeriod(period);
                system.addStar(primary, secondary);
                createMediumBinary(system, uwp, primary, secondary, numGasGiants, numBelts);
                break;
            case FAR_BINARY:
                // Hundreds of AU
                secondary.setParentId(primary.getId());
                distance = Die.d6(3) * 50 * Physics.AU + Die.die(Physics.AU * 50);
                if (Die.d3() == 1) {
                    distance *= Die.d10(2);
                }
                secondary.setDistance(distance);
                period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
                secondary.setPeriod(period);
                system.addStar(primary, secondary);
                createFarBinary(system, uwp, primary, secondary, numGasGiants, numBelts);
                break;
        }
    }

    /**
     * A binary star system with both stars in close orbit around each other, and the planetary system orbiting
     * both stars. This is the simplest type of binary system.
     */
    private void createCloseBinary(StarSystem system, UWP uwp, Star primary, Star secondary, int gasGiants, int belts) {
        String baseName = system.getName();
        Star star = new Star(baseName, system, primary, secondary);

        int otherWorlds = Die.d4(2);

        int innerWorlds = otherWorlds / 3;
        if (otherWorlds > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        int outerWorlds = otherWorlds - innerWorlds;

        long mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);

        PlanetFactory factory = worldgen.getPlanetFactory();
        createInnerWorlds(factory, system, star, mainWorldDistance, innerWorlds, false);
        createMainWorld(factory, system, star, uwp, mainWorldDistance);
        createOuterWorlds(factory, system, star, mainWorldDistance, 0, outerWorlds, gasGiants, belts);

        // Add possible Oort clouds.
        long distance = (long) (Physics.AU * 10_000 * star.getMass());
        int  chance = 75 - (int) (distance / (Physics.AU * 1_000.0));
        if (star.getMass() < 1) {
            chance = (int) (star.getMass() * 100);
        }

        if (Die.d100() <= chance) {
            PlanetType type = PlanetType.OortCloud;
            String     name = StarSystemFactory.getOortCloudName(star, 1);
            List<Planet> planets = factory.createPlanet(system, star, name, type, distance);
            system.addPlanets(planets);

            distance *= 5;
            chance = 100 - (int) (distance / (Physics.AU * 1_000.0));
            if (Die.d100() <= chance) {
                name = StarSystemFactory.getOortCloudName(star, 2);
                planets = factory.createPlanet(system, star, name, type, distance);
                system.addPlanets(planets);
            }
        }

        // Solar flares are more common in close binary systems.
        if (Die.d100() > Physics.getSolarConstant(primary) * 500 || Die.d100() > Physics.getSolarConstant(secondary) * 500) {
            system.addTradeCode(StarSystemCode.Sf);
        }

        // Chance of jump instability goes up, the close the stars are to each other.
        long starDistance = primary.getDistance() + secondary.getDistance();
        if (Die.die(10_000_000) > starDistance) {
            system.addTradeCode(StarSystemCode.Ji);
        }

        setDescription(system, null);
    }

    /**
     * A binary star system with both stars within 100AU of each other, with each star potentially having its
     * own planetary system. This is potentially the most complicated type of binary, since both stars can
     * influence each other's planets.
     * <p>
     * The main world is always around the primary star. Both planetary systems get an ambient temperature
     * from the other star (assumed to be constant).
     */
    private void createMediumBinary(StarSystem system, UWP uwp, Star primary, Star secondary, int gasGiants, int belts) {
        int otherWorlds = Die.d4();
        long maxDistance = secondary.getDistance() / 5;

        int gasGiants0 = gasGiants / 2 + 1;
        int belts0 = belts / 2 + 1;
        int otherWorlds0 = otherWorlds / 2 + 1;

        int gasGiants1 = gasGiants - gasGiants0;
        int belts1 = belts - belts0;
        int otherWorlds1 = otherWorlds - otherWorlds0;

        if (secondary.getLuminosity().equals(Luminosity.VII)) {
            if (gasGiants1 > 0) {
                gasGiants0++;
                gasGiants1--;
            }
            if (otherWorlds1 > 0) {
                otherWorlds0++;
                otherWorlds1--;
            }
        }

        PlanetFactory factory = worldgen.getPlanetFactory();

        // Primary Star
        int innerWorlds = otherWorlds0 / 3;
        if (otherWorlds0 > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        int outerWorlds = otherWorlds0 - innerWorlds;
        int ambientTemperature = Physics.getTemperatureOfOrbit(secondary, secondary.getDistance());
        long mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);

        createInnerWorlds(factory, system, primary, mainWorldDistance, innerWorlds, belts0 > 0);
        if (belts0 > 0) {
            belts0--;
        }
        createMainWorld(factory, system, primary, uwp, mainWorldDistance);
        createOuterWorlds(factory, system, primary, mainWorldDistance, maxDistance, outerWorlds, gasGiants0, belts0);

        // Secondary Star
        // Need to reset planetary orbit counters.
        belt = 1;
        orbit = 1;
        innerWorlds = otherWorlds1 / 3;
        if (otherWorlds1 > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        outerWorlds = otherWorlds0 - innerWorlds;
        ambientTemperature = Physics.getTemperatureOfOrbit(primary, secondary.getDistance());
        mainWorldDistance = Physics.getOrbitWithTemperature(secondary, 220);

        createInnerWorlds(factory, system, secondary, mainWorldDistance, innerWorlds, false);
        if (belts1 > 0) {
            // Don't have a main world around the secondary, so can we fit an asteroid belt instead?
            PlanetType type = PlanetType.AsteroidBelt;
            String name = StarSystemFactory.getBeltName(secondary, belt++);
            List<Planet> planets = factory.createPlanet(system, secondary, name, type, mainWorldDistance);
            system.addPlanets(planets);
            belts1--;
        } else {
            // If no belt, then bring outer worlds in close to fill the gap.
            mainWorldDistance *= 0.75;
        }
        createOuterWorlds(factory, system, secondary, mainWorldDistance, maxDistance, outerWorlds, gasGiants1, belts1);


        // Small chance of a single Oort cloud.
        long distance = (long) (Physics.AU * 15_000 * primary.getMass());
        int  chance = 100 - (int) (distance / (Physics.AU * 1_500.0));
        if (primary.getMass() < 1) {
            chance = (int) (primary.getMass() * 100);
        }
        // Chance is reduced the further away the companion is.
        chance -= secondary.getDistance() / Physics.AU / 2;

        if (Die.d100() <= chance) {
            PlanetType type = PlanetType.OortCloud;
            String     name = StarSystemFactory.getOortCloudName(primary, 1);
            List<Planet> planets = factory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);
        }

        setDescription(system, null);
    }

    private void createFarBinary(StarSystem system, UWP uwp, Star primary, Star secondary, int gasGiants, int belts) {
        int otherWorlds = Die.d6(2);
        long maxDistance = secondary.getDistance() / 5;

        int gasGiants0 = (gasGiants + 1) / 2;
        int belts0 = (belts + 1) / 2;
        int otherWorlds0 = (otherWorlds + 1) / 2;

        int gasGiants1 = gasGiants - gasGiants0;
        int belts1 = belts - belts0;
        int otherWorlds1 = otherWorlds - otherWorlds0;

        if (secondary.getLuminosity().equals(Luminosity.VII)) {
            if (gasGiants1 > 0) {
                gasGiants0++;
                gasGiants1--;
            }
            if (otherWorlds1 > 0) {
                otherWorlds0++;
                otherWorlds1--;
            }
        }

        PlanetFactory factory = worldgen.getPlanetFactory();

        // Primary Star
        int innerWorlds = otherWorlds0 / 3;
        if (otherWorlds0 > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        int outerWorlds = otherWorlds0 - innerWorlds;
        long mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);

        createInnerWorlds(factory, system, primary, mainWorldDistance, innerWorlds, belts0 > 0);
        if (belts0 > 0) {
            belts0--;
        }
        createMainWorld(factory, system, primary, uwp, mainWorldDistance);
        createOuterWorlds(factory, system, primary, mainWorldDistance, maxDistance, outerWorlds, gasGiants0, belts0);

        // Secondary Star
        // Need to reset planetary orbit counters.
        belt = 1;
        orbit = 1;
        innerWorlds = otherWorlds1 / 3;
        if (otherWorlds1 > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        outerWorlds = otherWorlds0 - innerWorlds;
        mainWorldDistance = Physics.getOrbitWithTemperature(secondary, 255);

        createInnerWorlds(factory, system, secondary, mainWorldDistance, innerWorlds, false);
        if (belts1 > 0) {
            // Don't have a main world around the secondary, so can we fit an asteroid belt instead?
            PlanetType type = PlanetType.AsteroidBelt;
            String name = StarSystemFactory.getBeltName(secondary, belt++);
            List<Planet> planets = factory.createPlanet(system, secondary, name, type, mainWorldDistance);
            system.addPlanets(planets);
            belts1--;
        } else {
            // If no belt, then bring outer worlds in close to fill the gap.
            mainWorldDistance *= 0.75;
        }
        createOuterWorlds(factory, system, secondary, mainWorldDistance, maxDistance, outerWorlds, gasGiants1, belts1);

        setDescription(system, null);
    }

    /**
     * A triple system has one star at the centre, and the other two stars orbit it at a large distance.
     * In this case, it's similar to a Far Binary, but with three stars.
     */
    private void createTriple(StarSystem system, UWP uwp, List<Star> stars) throws DuplicateObjectException {
        logger.info("createTriple: [" + system.getName() + "]");

        int numGasGiants = uwp.getGasGiants();
        int numBelts = uwp.getBelts();

        system.setType(StarSystemType.TRIPLE);

        StarGenerator starGenerator = new StarGenerator(worldgen, system, true);
        Star primary = starGenerator.generatePrimary(stars.get(0).getLuminosity(), stars.get(0).getSpectralType());
        Star secondary = starGenerator.generateSecondary(stars.get(1).getLuminosity(), stars.get(1).getSpectralType());
        Star tertiary = starGenerator.generateTertiary(stars.get(2).getLuminosity(), stars.get(2).getSpectralType());

        primary.setStandardRadius();
        primary.setStandardMass();
        secondary.setStandardRadius();
        secondary.setStandardMass();
        tertiary.setStandardRadius();
        tertiary.setStandardMass();

        long distance, period;
        double totalMass = primary.getMass() + secondary.getMass();

        // Hundreds of AU
        secondary.setParentId(primary.getId());
        distance = Die.d6(3) * 60 * Physics.AU + Die.die(Physics.AU * 60);
        if (Die.d3() == 1) {
            distance *= Die.d6(2);
        }
        secondary.setDistance(distance);
        period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        secondary.setPeriod(period);

        tertiary.setParentId(primary.getId());
        distance *= Die.d3() + 1;
        tertiary.setDistance(distance);
        totalMass = primary.getMass() + tertiary.getMass();
        period = Physics.getOrbitalPeriod(totalMass * Physics.SOL_MASS, distance * 1_000);
        tertiary.setPeriod(period);

        system.addStar(primary, secondary, tertiary);
        createTriple(system, uwp, primary, secondary, tertiary, numGasGiants, numBelts);
    }

    private void createTriple(StarSystem system, UWP uwp, Star primary, Star secondary, Star tertiary, int gasGiants, int belts) {
        long maxDistance = secondary.getDistance() / 5;

        int gasGiants0 = (gasGiants + 1) / 2;
        int belts0 = (belts + 1) / 2;

        int gasGiants1 = (1 + gasGiants - gasGiants0) / 2;
        int belts1 = (1 + belts - belts0) / 2;

        int gasGiants2 = gasGiants - gasGiants0 - gasGiants1;
        int belts2 = belts - belts0 - belts1;

        PlanetFactory factory = worldgen.getPlanetFactory();

        // Primary Star
        int otherWorlds = Die.d6() + 1;
        int innerWorlds = otherWorlds / 3;
        if (otherWorlds > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        int outerWorlds = otherWorlds - innerWorlds;
        long mainWorldDistance = Physics.getOrbitWithTemperature(primary, 255);

        createInnerWorlds(factory, system, primary, mainWorldDistance, innerWorlds, belts0 > 0);
        if (belts0 > 0) {
            belts0--;
        }
        createMainWorld(factory, system, primary, uwp, mainWorldDistance);
        createOuterWorlds(factory, system, primary, mainWorldDistance, maxDistance, outerWorlds, gasGiants0, belts0);

        // Secondary Star
        // Need to reset planetary orbit counters.
        belt = 1;
        orbit = 1;
        otherWorlds = Die.d4() + 1;
        innerWorlds = otherWorlds / 3;
        if (otherWorlds > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        outerWorlds = otherWorlds - innerWorlds;
        mainWorldDistance = Physics.getOrbitWithTemperature(secondary, 255);

        createInnerWorlds(factory, system, secondary, mainWorldDistance, innerWorlds, false);
        if (belts1 > 0) {
            // Don't have a main world around the secondary, so can we fit an asteroid belt instead?
            PlanetType type = PlanetType.AsteroidBelt;
            String name = StarSystemFactory.getBeltName(secondary, belt++);
            List<Planet> planets = factory.createPlanet(system, secondary, name, type, mainWorldDistance);
            system.addPlanets(planets);
            belts1--;
        } else {
            // If no belt, then bring outer worlds in close to fill the gap.
            mainWorldDistance *= 0.75;
        }
        createOuterWorlds(factory, system, secondary, mainWorldDistance, maxDistance, outerWorlds, gasGiants1, belts1);


        // Tertiary Star
        // Need to reset planetary orbit counters.
        belt = 1;
        orbit = 1;
        otherWorlds = Die.d3();
        innerWorlds = otherWorlds / 3;
        if (otherWorlds > 0) {
            innerWorlds = Math.min(1, innerWorlds);
        }
        outerWorlds = otherWorlds - innerWorlds;
        mainWorldDistance = Physics.getOrbitWithTemperature(tertiary, 255);

        createInnerWorlds(factory, system, tertiary, mainWorldDistance, innerWorlds, false);
        if (belts1 > 0) {
            // Don't have a main world around the secondary, so can we fit an asteroid belt instead?
            PlanetType type = PlanetType.AsteroidBelt;
            String name = StarSystemFactory.getBeltName(tertiary, belt++);
            List<Planet> planets = factory.createPlanet(system, tertiary, name, type, mainWorldDistance);
            system.addPlanets(planets);
            belts1--;
        } else {
            // If no belt, then bring outer worlds in close to fill the gap.
            mainWorldDistance *= 0.75;
        }
        createOuterWorlds(factory, system, tertiary, mainWorldDistance, maxDistance, outerWorlds, gasGiants2, belts2);


        setDescription(system, null);
    }


}

