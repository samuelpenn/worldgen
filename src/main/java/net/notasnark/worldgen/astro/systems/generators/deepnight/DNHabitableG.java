/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.systems.generators.deepnight;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.stars.StarGenerator;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.civ.CivilisationGenerator;
import net.notasnark.worldgen.civ.civilisation.Colony;
import net.notasnark.worldgen.civ.civilisation.SpaceFaringCiv;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;

import java.util.List;

/**
 * Generate a barren, type M system for the Deepnight campaign.
 */
public class DNHabitableG extends DNHabitable {
    private static final Logger logger = LoggerFactory.getLogger(DNHabitableG.class);

    public DNHabitableG(WorldGen worldgen) {
        super(worldgen);
    }

    /**
     * Creates a barren star system. Such a system has one or more stars, with planets, but the
     * planets are barren with no civilisation or life of any kind. If the system has multiple stars,
     * the second star is much smaller and a long way away.
     *
     * @param sector    Sector to generate system in.
     * @param name      Name of the system.
     * @param x         X coordinate of the system in the sector (1-32).
     * @param y         Y coordinate of the system in the sector (1-40).
     *
     * @return          Created and persisted star system.
     * @throws DuplicateObjectException     If a duplicate system is created.
     */
    public StarSystem generate(Sector sector, String name, int x, int y) throws DuplicateObjectException {
        StarSystem system = createEmptySystem(sector, name, x, y);

        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5:
            case 6: case 7: case 8:
                // A class system with rock worlds and gas giants further out.
                createSimple(system);
                break;
            case 9:
                // A small system with a few rocky worlds close in.
                createRockWorlds(system);
                break;
            case 10:
                // A close binary pair of stars.
                createBinary(system);
                break;
            case 11:
                // Hot gas giants close to the star.
                createHotGiants(system);
                break;
            case 12:
                // A red giant system, with planets.
                createSuperEarths(system);
                break;
        }

        colonise(system);

        updateStarSystem(system);

        return system;
    }


    public void createBinary(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenG] [Binary] system [%s]", system.getName()));

        createBinary(system, Luminosity.IV, SpectralType.G3.getSpectralType(Die.dieV(4)));

        setDescription(system, null);
    }

    /**
     * Creates a simple system around an G class main sequence star. A simple system is one with a pretty traditional
     * layout, with rocky worlds in the inner system, and gassy worlds towards the outer edge.
     */
    public void createSimple(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenG] [SimpleSystem] system [%s]", system.getName()));

        Luminosity luminosity = Luminosity.V;
        SpectralType hrType = SpectralType.G5.getSpectralType(Die.dieV(6));

        createSimple(system, luminosity, hrType);

        setDescription(system, null);
    }

    /**
     * Create a system with some (probably) rocky worlds. There is a chance of an asteroid/ice belt.
     */
    public void createRockWorlds(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [RockWorlds] system [%s]", system.getName()));

        Luminosity luminosity = new Roller<>(Luminosity.IV).add(Luminosity.V, 3).
                add(Luminosity.VI, 6).roll();
        SpectralType hrType = SpectralType.M5.getSpectralType(Die.dieV(5));

        createRockWorlds(system, luminosity, hrType);

        setDescription(system, null);
    }



    /**
     * A star system where the main world is a GeoHelian world.
     */
    public void createHelian(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [BarrenMK] [Helian] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.VI,
                SpectralType.M1.getSpectralType(Die.dieV(6)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Place the belt around 1AU from the star.
        long distance = 2_500_000 + Die.die(500_000);
        int  numPlanets = Die.d4() + 1;
        String name = StarSystemFactory.getPlanetName(primary, 1);

        List<Planet> planets;
        if (Die.d3() == 1) {
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Halcyonic, distance);
        } else {
            distance *= 3;
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                    name, PlanetType.Hyperionic, distance);
        }
        system.addPlanets(planets);

        for (int i = 2; i <= numPlanets; i++) {
            distance *= 1.2;
            distance += Die.die(2_000_000, 5);

            CircumstellarZone zone = CircumstellarZone.getZone(Physics.getTemperatureOfOrbit(primary, distance));
            PlanetType type = getDwarfTerrestrial(zone);

            name = StarSystemFactory.getPlanetName(primary, i);
            planets = worldgen.getPlanetFactory().createPlanet(system, system.getStars().get(0),
                name, type, distance);
            system.addPlanets(planets);
        }

        system.addTradeCode(StarSystemCode.Ba);
        if (Die.d2() == 1) {
            system.addTradeCode(StarSystemCode.Sf);
            system.setZone(Zone.AMBER);
        }

        setDescription(system, null);
    }

    private void createHotGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 1500);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3();
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 3, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d4() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    private void createColdGiants(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getJovian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 5, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }

    private void createSuperEarths(StarSystem system) throws DuplicateObjectException {
        logger.info(String.format("Generating [DNBarrenM] [HotGiants] system [%s]", system.getName()));

        StarGenerator starGenerator = new StarGenerator(worldgen, system, false);
        PlanetFactory planetFactory = worldgen.getPlanetFactory();

        // On average a cool main-sequence star will be generated.
        Star primary = starGenerator.generatePrimary(Luminosity.V,
                SpectralType.M7.getSpectralType(Die.dieV(3)));
        system.addStar(primary);
        system.setType(StarSystemType.SINGLE);

        // Start at a reasonably hot orbit, but not too hot. Add a bit of variety.
        long distance = Physics.getOrbitWithTemperature(primary, 200);
        distance += Die.dieV(distance / 5);

        // Create a random number of Dwarf Terrestrial worlds.
        int numPlanets = Die.d3() + 1;
        for (int orbit = 1; orbit <= numPlanets; orbit++) {
            String      name = StarSystemFactory.getPlanetName(primary, orbit);
            int         k = Physics.getTemperatureOfOrbit(primary, distance);
            PlanetType  type = getHelian(CircumstellarZone.getZone(k));

            List<Planet> planets = planetFactory.createPlanet(system, primary, name, type, distance);
            system.addPlanets(planets);

            distance += Die.die(distance / 5, 3);
        }

        // Good chance of it suffering from solar flares.
        if (Die.d3() != 1) {
            system.addTradeCode(StarSystemCode.Sf);
        }
        setDescription(system, null);
    }
    public void colonise(StarSystem system) {
        logger.info("Colonise star system " + system.getName());

        CivilisationGenerator generator = null;

        switch (Die.d6()) {
            case 1: case 2: case 3:
                generator = new SpaceFaringCiv(worldgen, system);
                break;
            case 4: case 5:
                generator = new Colony(worldgen, system);
                break;
            default:
                break;
        }
        if (generator != null) {
            generator.generate();
        }
    }}
