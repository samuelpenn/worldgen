/**
 * NoSuchPlanetException.java
 *
 * Copyright (c) 2011, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.commodities;

/**
 * Enum for defining all the common commodity names. Code should refer to commodities by their enum
 * rather than by their name, in order to guarantee a level of type safety.
 */
public enum CommodityName {
    Hydrogen("Hydrogen"),
    Helium("Helium"),
    OrganicGases("Organic Gases"),
    CorrosiveGases("Corrosive Gases"),
    ExoticGases("Exotic Gases"),
    SilicateOre("Silicate Ore"),
    CarbonicOre("Carbonic Ore"),
    FerricOre("Ferric Ore"),
    HeavyMetals("Heavy Metals"),
    Radioactives("Radioactives"),
    RareMetals("Rare Metals"),
    ExoticMetals("Exotic metals"),
    PreciousMetals("Precious Metals"),
    SilicateCrystals("Silicate Crystals"),
    ExoticCrystals("Exotic Crystals"),
    CarbonicCrystals("Carbonic Crystals"),
    Water("Water"),
    Oxygen("Oxygen"),
    // Archaean Life
    OrganicChemicals("Organic Chemicals"),
    OrganicToxins("Organic Toxins"),
    InorganicToxins("Inorganic Toxins"),
    Prokaryotes("Prokaryotes"),
    Protozoa("Protozoa"),
    Algae("Algae"),
    // Aerobic Life
    Sponges("Sponges"),
    Metazoa("Metazoa"),
    Kelp("Kelp"),
    Jellies("Jellies"), LargeJellies("Large Jellies"), HugeJellies("Huge Jellies"),
    Molluscs("Molluscs"), LargeMolluscs("Large Molluscs"), HugeMolluscs("Huge Molluscs"),
    // Complex Ocean
    Crawlers("Crawlers"), LargeCrawlers("Large Crawlers"), HugeCrawlers("Huge Crawlers"),
    Swimmers("Swimmers"), LargeSwimmers("Large Swimmers"), HugeSwimmers("Huge Swimmers"), GiganticSwimmers("Gigantic Swimmers"),

    Arthropods("Arthropods"),
    Fish("Fish"), LargeFish("Large Fish"), HugeFish("Huge Fish"),

    // Simple Land
    SlimeMold("Slime Mold"),
    Lichen("Lichen"),
    Fungus("Fungus"),
    Shrubs("Shrubs"),
    Woods("Woods"),
    Grasses("Grasses"),

    Insects("Insects"),
    Rodents("Rodents"), LargeRodents("Large Rodents"),
    SmallAmphibians("Small Amphibians"), Amphibians("Amphibians"), LargeAmphibians("Large Amphibians"),
    SmallBurrowers("Small Burrowers"), Burrowers("Burrowers"), LargeBurrowers("Large Burrowers"),
    SmallGrazers("Small Grazers"), Grazers("Grazers"), LargeGrazers("Large Grazers"), HugeGrazers("Huge Grazers"), GiganticGrazers("Gigantic Grazers"),
    SmallHunters("Small Hunters"), Hunters("Hunters"), LargeHunters("Large Hunters"), HugeHunters("Huge Hunters"), GiganticHunters("Gigantic Hunters"),
    SmallFlyers("Small Flyers"), Flyers("Flyers"), LargeFlyers("Large Flyers"),
    SmallRaptors("Small Raptors"), Raptors("Raptors"), LargeRaptors("Large Raptors"),

    Salvage("Salvage"),
    HistoricalItems("Historical Items"),
    AlienItems("Alien Items"),
    Unobtanium("Unobtanium");

    private String name;

    CommodityName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
