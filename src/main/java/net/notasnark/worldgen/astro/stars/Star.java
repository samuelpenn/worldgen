/**
 * Star.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.stars;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.systems.CircumstellarZone;
import net.notasnark.worldgen.astro.systems.StarSystem;

import javax.persistence.*;

import java.text.NumberFormat;

import static net.notasnark.worldgen.astro.Physics.AU;

/**
 * Represents a Star in a solar system. A star system will consist of zero or more stars, plus any
 * planets which orbit them. In practice, the limit of Stars in a star system is probably about three,
 * due to the complexity of working out orbital mechanics.
 *
 * A Star can be a typical star, a stellar remnant (such as black hole, neutron star or white dwarf)
 * or a brown dwarf. Brown dwarfs are considered to be stars since they tend to be the primary object
 * in the star system.
 *
 * Nomenclature: If there is a single star in the system then it is named after the system. If there
 * are multiple stars, they have a suffix of Alpha (for the largest/brightest), Beta, Gamma etc.
 * The closest companion of the primary is always Beta.
 *
 * @author Samuel Penn
 */
@Entity
@Table(name = "stars")
public class Star {
    // Unique identifier used as primary key.
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    // Persisted fields.
    @Column(name = "name")
    private String name;

    // Astronomical data
    @ManyToOne
    @JoinColumn(name = "system_id", referencedColumnName = "id")
    private StarSystem system;

    @Column(name = "parent_id")
    private int parentId;
    @Column(name = "distance")
    private long distance;

    @Enumerated(EnumType.STRING)
    @Column(name = "luminosity")
    private Luminosity luminosity;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private SpectralType type;

    @Column(name = "mass")
    private double mass;

    @Column(name = "radius")
    private long radius;

    @Column(name = "period")
    private long period;

    /**
     * Empty constructor.
     */
    public Star() {
    }

    public Star(Star copy) {
        this.name = copy.name;
        this.system = copy.system;
        this.parentId = copy.parentId;
        this.distance = copy.distance;
        this.luminosity = copy.luminosity;
        this.type = copy.type;
        this.mass = copy.mass;
        this.radius = copy.radius;
        this.period = copy.period;
    }

    /**
     * Define a new Star with a full set of details.
     */
    public Star(String name, StarSystem system, int parentId, int distance, Luminosity luminosity, SpectralType type) {
        this.id = 0;
        this.name = name;
        this.system = system;
        this.parentId = parentId;
        this.distance = distance;
        this.luminosity = luminosity;
        this.type = type;
        setStandardMass();
    }

    /**
     * Create a combined star from a close orbiting pair. Used to calculate heat coming from the two.
     */
    public Star(String name, StarSystem system, Star one, Star two) {
        this.name = name;
        this.system = system;
        setLuminosity(one.getLuminosity());
        setSpectralType(one.getSpectralType());
        setMass(one.getMass() + two.getMass());
        setRadius(one.getRadius() + two.getRadius() / 2);
        setCentreOfGravity(StarSystem.PRIMARY_COG);
    }

    /**
     * Gets the unique id of the star. All star ids are unique across the entire universe.
     *
     * @return Unique id of this star.
     */
    public int getId() {
        return id;
    }

    public void setCentreOfGravity(int cog) {
        this.id = cog;
    }

    /**
     * Gets the star system to which this star belongs. Once persisted, a Star will always have a
     * StarSystem defined.
     *
     * @return  Star system object, not null.
     */
    public StarSystem getSystem() {
        return system;
    }

    public void setSystem(StarSystem system) {
        this.system = system;
    }

    /**
     * Gets the name of the star. Names should be unique within a star system.
     * If there are multiple stars, normally the first is named Alpha, the
     * second Beta etc.
     *
     * @return Name of the star.
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException("Name is not valid");
        }
        this.name = name.trim();
    }

    public void rename(String oldName, String name) {
        if (this.name == null || this.name.length() == 0) {
            this.name = name;
        } else {
            this.name = this.name.replaceAll(oldName, name);
        }
    }

    /**
     * Gets the id of the parent around which this star orbits. If the star has
     * no parent, this is zero. If it is non-zero, then this star is in orbit
     * around another star. It is possible that two stars orbit each other around
     * a common centre of gravity, in which case they will each have a parent of
     * the other.
     *
     * @return Gets the parent id of this star.
     */
    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    /**
     * Gets the orbit distance of this star, in kilometres. If the
     * star has no parent, this will normally be zero. Support for multiple
     * stars orbiting a common centre of gravity is not yet supported.
     *
     * @return Distance from parent star, in KM.
     */
    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        if (distance < 0) {
            throw new IllegalArgumentException("Distance cannot be negative");
        }
        this.distance = distance;
    }

    /**
     * Gets the orbital period of this star in seconds. If the star is at the centre
     * of the system, then it's orbital period will be zero.
     *
     * @return  Period in seconds.
     */
    public long getPeriod() {
        return period;
    }

    public void setPeriod(long seconds) {
        this.period = seconds;
    }

    /**
     * Gets the classification of this star, from class VI dwarfs up to class Ia
     * super giants. Most stars are class V.
     *
     * @return Star class, from VII up to Ia.
     */
    public Luminosity getLuminosity() {
        return luminosity;
    }

    public Star setLuminosity(Luminosity luminosity) {
        this.luminosity = luminosity;
        return this;
    }

    /**
     * Gets the spectral type of the star, using the Hertzsprung Russell
     * diagram. This is a two character code, e.g. our sun is G2.
     *
     * @return Spectral type of star.
     */
    public SpectralType getSpectralType() {
        return type;
    }

    public Star setSpectralType(SpectralType type) {
        this.type = type;
        return this;
    }


    /**
     * Gets the multiplier for different temperature bands around this star
     * based on that of Sol. A star four times as luminous as Sol would have
     * range bands which are twice as far.
     *
     * Luminosity is considered to increase by the square of the radius and
     * linearly with surface temperature. See:
     *
     * https://en.wikipedia.org/wiki/Circumstellar_habitable_zone
     *
     * @return  Constant to multiply temperature range bands by.
     */
    private double getSolarConstant() {
        return Physics.getSolarConstant(this);
    }

    public int getSurfaceTemperature() {
        return getSpectralType().getSurfaceTemperature();
    }

    /**
     * Gets the minimum distance at which a planet of some kind can exist around this
     * star. This is mostly based on the size and temperature of the star, but remnants
     * will have a larger distance on the assumption that anything closer would have
     * been destroyed in the star's death throes.
     *
     * @return  Distance, in kilometres.
     */
    public long getMinimumDistance() {
        long distance;

        if (luminosity == Luminosity.B || luminosity == Luminosity.N || luminosity == Luminosity.VII) {
            distance = 250_000_000;
        } else {
            distance = Physics.getOrbitWithTemperature(this, CircumstellarZone.Epistellar.getKelvin());
        }

        return Math.min(getRadius() * 2, distance);
    }

    /**
     * Gets the distance around a star that is considered the point at which things get too
     * hot for most worlds. Worlds closer than this are scorched by the star and will lack
     * any volatiles. Worlds further out than this but up to the InnerWarm distance will be
     * too hot for life, but may contain some volatiles.
     *
     * @return  Distance in kilometres.
     */
    public long getHotDistance() {
        return Physics.getOrbitWithTemperature(this, 500);
    }

    // Length of a standard year, in seconds.
    private static final long STANDARD_YEAR = 31557600L;

    /**
     * Given a distance from the star, get the orbital period of that orbit in seconds.
     *
     * @param distance  Distance in millions of kilometres.
     * @return          Orbital period in seconds.
     */
    public long getPeriod(long distance) {
        long period = (long) (STANDARD_YEAR * Math.pow((1.0 * distance) / AU, 3.0/2.0));

        return (long) (period / Math.sqrt(getMass()));
    }

    /**
     * Sets the mass of the star in solar masses, based on the Luminosity Class and Spectral Type
     * of this star.
     */
    public Star setStandardMass() {
        if (type != null && luminosity != null) {
            this.mass = type.getMass() * luminosity.getMass();
        } else if (type != null) {
            this.mass = type.getMass();
        } else if (luminosity != null) {
            this.mass = luminosity.getMass();
        } else {
            this.mass = 0;
        }
        return this;
    }

    /**
     * Sets the mass of the star in solar masses, to a specific value.
     *
     * @param solarMasses  Mass in solar masses.
     */
    public Star setMass(final double solarMasses) {
        if (solarMasses < 0) {
            throw new IllegalArgumentException("Stellar mass must be positive.");
        }
        this.mass = solarMasses;
        return this;
    }

    /**
     * Gets the mass of this star in solar masses.
     *
     * @return      Star mass in solar masses.
     */
    public double getMass() {
        return mass;
    }

    public Star setStandardRadius() {
        this.radius = (int) (Physics.SOL_RADIUS * luminosity.getRadius() * type.getRadius());

        switch (luminosity) {
            case O:
                for (SpectralType t : new SpectralType[] { SpectralType.G0, SpectralType.F0, SpectralType.A0, SpectralType.B0 } ) {
                    if (type.getSurfaceTemperature() > t.getSurfaceTemperature()) {
                        this.radius *= 0.5;
                    }
                }
                break;
            case Ia: case Ib:
                for (SpectralType t : new SpectralType[] { SpectralType.G0, SpectralType.F0, SpectralType.A0, SpectralType.B0 } ) {
                    if (type.getSurfaceTemperature() > t.getSurfaceTemperature()) {
                        this.radius *= 0.6;
                    }
                }
                break;
            case II:
                for (SpectralType t : new SpectralType[] { SpectralType.G0, SpectralType.F0, SpectralType.A0, SpectralType.B0 } ) {
                    if (type.getSurfaceTemperature() > t.getSurfaceTemperature()) {
                        this.radius *= 0.7;
                    }
                }
                break;
            case III:
                for (SpectralType t : new SpectralType[] { SpectralType.G0, SpectralType.F0, SpectralType.A0, SpectralType.B0 } ) {
                    if (type.getSurfaceTemperature() > t.getSurfaceTemperature()) {
                        this.radius *= 0.8;
                    }
                }
                break;
            case IV:
                for (SpectralType t : new SpectralType[] { SpectralType.F0, SpectralType.A0, SpectralType.B0 } ) {
                    if (type.getSurfaceTemperature() > t.getSurfaceTemperature()) {
                        this.radius *= 0.9;
                    }
                }
                break;
        }
        return this;
    }

    /**
     * Set the radius of this star in kilometres.
     *
     * @param km    Radius in kilometres.
     */
    public Star setRadius(long km) {
        this.radius = km;
        return this;
    }

    /**
     * Gets the radius of this star in kilometres.
     *
     * @return  Radius in kilometres.
     */
    public long getRadius() {
        return radius;
    }


    public String toString() {
        return name + " " + type + luminosity;
    }

    public static void main(String[] args) {

        Star s = new Star();

        System.out.println(String.format("%-8s %13s %12s %12s %12s %12s %12s %12s", "Class", "M5", "K5", "G5", "F5", "A5", "B5", "O5"));

        for (Luminosity l : new Luminosity[] { Luminosity.VII, Luminosity.VI, Luminosity.V, Luminosity.IV, Luminosity.III, Luminosity.II, Luminosity.Ib, Luminosity.Ia, Luminosity.O }) {
            s.setLuminosity(l);

            System.out.print(String.format("%-8s ", l.name()));

            for (SpectralType hr : new SpectralType[] { SpectralType.M5, SpectralType.K5, SpectralType.G5, SpectralType. F5, SpectralType.A5, SpectralType.B5, SpectralType.O5 }) {
                s.setSpectralType(hr);

                s.setStandardMass();
                s.setStandardRadius();
                StarGenerator.defineMassAndRadius(s);

                NumberFormat format = NumberFormat.getNumberInstance();

                //System.out.print(String.format("% 7.2f ", s.getMass() ));
                System.out.print(String.format("%13s", format.format(s.getRadius()) ));
            }
            System.out.println("");
        }


    }
}
