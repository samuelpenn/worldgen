/**
 * SectorFactory.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.sectors;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.astro.systems.generators.FarSideNowhere;
import net.notasnark.worldgen.exceptions.DuplicateObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;


/**
 * Class for generating new sectors.
 */
public class SectorGenerator {
    private static final Logger logger = LoggerFactory.getLogger(SectorGenerator.class);
    private final WorldGen worldgen;

    /**
     * Constructor for a new Generator.
     *
     * @param worldgen  WorldGen session object.
     */
    public SectorGenerator(WorldGen worldgen) {
        this.worldgen = worldgen;
    }

    public void createEmptySector(String name, int x, int y) throws DuplicateSectorException {
        Sector sector = new Sector(name, x, y);

        worldgen.getSectorFactory().persist(sector);
    }

    public void createRandomSector(Sector sector, int density) {
        StarSystemFactory factory = worldgen.getStarSystemFactory();

        for (int y=1; y <= 40; y++) {
            for (int x=1; x <= 32; x++) {
                if (Die.d100() <= density) {
                    // Create a new star system.
                    try {
                        factory.createStarSystem(sector, "" + Die.die(10000000), x, y, StarSystemType.EMPTY);
                    } catch (DuplicateStarSystemException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Gets the density of a particular point in a sector according to the galaxy density map.
     * The density is a number from 0 to 1000, and is the chance of a hex containing
     * a star system. The minimum and maximum values can be configured differently.
     * A 'standard' density is considered to be about 300.
     *
     * @param sector    Sector
     * @param x         X coordinate within the sector (1..32).
     * @param y         Y coordinate within the sector (1..40).
     * @return          A value between 0 and 1000.
     */
    private int getDensity(Sector sector, int x, int y) {
        SimpleImage image = worldgen.getGalaxyMap();

        int     sx = sector.getX();
        int     sy = sector.getY();

        int w = image.getWidth();
        int h = image.getHeight();
        int sw = w / Sector.WIDTH;
        int sh = h / Sector.HEIGHT;

        int ox = sw / 2;
        int oy = sh / 2;
        int px = (ox + sx) * Sector.WIDTH + x - 1;
        int py = (oy + sy) * Sector.HEIGHT + y - 1;

        int colour = image.getColour(px, py) & 0xFF;
        logger.debug(String.format("Colour at [%d,%d] is [%d]", x, y, colour));

        int density = (int)(colour * 4);

        density = Math.max(density, worldgen.getConfig().getDensityMinimum());
        density = Math.min(density, worldgen.getConfig().getDensityMaximum());
        System.out.printf("  Density at [%02d,%02d] is [%d]\n", x, y, density);

        return density;
    }

    /**
     * Fills an existing sector with new star systems, according to the galactic density map.
     * Assumes that the sector is empty, but will work if it already has systems in it. The
     * chance of any given hex having a system is based on the density map.
     *
     * If a sub-sector is provided, then systems are only created in that sub sector.
     *
     * @param sector    Sector to create systems in.
     * @param subSector Optional sub sector to limit creation to.
     */
    public void createSectorByDensity(Sector sector, SubSector subSector, StarSystemGenerator generator) {
        StarSystemFactory systemFactory = worldgen.getStarSystemFactory();

        int minX = 1, maxX = 32;
        int minY = 1, maxY = 40;

        if (subSector != null) {
            minX = subSector.getMinX();
            maxX = subSector.getMaxX();

            minY = subSector.getMinY();
            maxY = subSector.getMaxY();
        }

        int totalHexes = (1 + maxX - minX) * (1 + maxY - minY);
        int checkedHexes = 0;
        int count = 0;
        for (int y=minY; y <= maxY; y++) {
            for (int x=minX; x <= maxX; x++) {
                checkedHexes += 1;
                System.out.println("createSectorByDensity: [" + subSector.toString() + "] " + (int)((100 * checkedHexes) / (totalHexes)) + "%");
                int     density = getDensity(sector, x, y);
                boolean create = false;
                boolean rogue = false;

                if (Die.d1000() <= density) {
                    create = true;
                } else if (Die.d1000() <= density / 2) {
                    create = true;
                    rogue = true;
                }

                if (create) {
                    // Create a new star system.
                    try {
                        if (!systemFactory.hasStarSystem(sector, x, y)) {
                            while (true) {
                                String name = worldgen.getStarSystemNameGenerator().generateName();

                                try {
                                    // See if a system with this name already exists in the system.
                                    systemFactory.getStarSystem(sector, name);
                                } catch (NoSuchStarSystemException e) {
                                    // We have a unique system name.
                                    if (generator != null) {
                                        System.out.println("Using generator " + generator.getClass().getSimpleName());
                                        if (rogue) {
                                            generator.generateRogue(sector, name, x, y);
                                        } else {
                                            generator.generate(sector, name, x, y);
                                        }
                                    } else {
                                        System.out.println("Using null generator");
                                        StarSystemSelector selector = new StarSystemSelector(worldgen);
                                        selector.createRandomSystem(sector, name, x, y);
                                    }
                                    break;
                                }
                            }
                            worldgen.flush();
                        }
                    } catch (DuplicateObjectException e) {
                        logger.warn("Duplicate star system creation");
                    }
                    count++;
                }
            }
        }
        int stellarDensity = (count * 100) / checkedHexes;
        logger.info(String.format("Created [%d] systems, density %d%%.", count, stellarDensity));
        System.out.printf("Created [%d] systems, density %d%%.", count, stellarDensity);
    }

    public void createSectorByDensity(Sector sector, SubSector subSector) {
        createSectorByDensity(sector, subSector, null);
    }

}
