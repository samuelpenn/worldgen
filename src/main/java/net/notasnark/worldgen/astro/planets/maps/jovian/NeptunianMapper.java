/**
 * JovicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.jovian.Neptunian;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A cold Jovian world, found in the outer solar system.
 */
public class NeptunianMapper extends JovianMapper {

    protected static final Tile DARK = new Tile("Blue", "#4E82FC", false, 6);
    protected static final Tile LIGHT = new Tile("Pale", "#C9EFF2", false, 4);
    protected static final Tile GREEN = new Tile("Green", "#D0EFD0", false, 4);
    protected static final Tile WHITE = new Tile("White", "#E9EFF2", false, 2);
    protected static final Tile SAPPHIRE = new Tile("Sapphire", "#4040E0").random(4);

    public NeptunianMapper(final Planet planet, final int size) {
        super(planet, size);
    }
    public NeptunianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        if (planet.hasFeature(Neptunian.NeptunianFeatures.DarkClouds)) {
            return NeptunianMapper.DARK;
        } else if (planet.hasFeature(Neptunian.NeptunianFeatures.GreenClouds)) {
            return NeptunianMapper.GREEN;
        } else if (planet.hasFeature(Neptunian.NeptunianFeatures.SapphireClouds)) {
            return NeptunianMapper.SAPPHIRE;
        }
        return NeptunianMapper.LIGHT;
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        return previousColour.getVariant(Die.d8() - Die.d8());
    }

    private static final int H = 52;

    public void cloudFormations() {
        if (planet.hasFeature(Neptunian.NeptunianFeatures.WhiteBands)) {
            int s = getNumRows() / 2 + Die.dieV(20);
            int h = Die.d4(2);

            Tile mix = getTile(0, s);
            for (int y = s; y < s + h; y++) {
                mix = mix.getMix(WHITE);
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
            for (int y = s + h; y < s + h * 2; y++) {
                mix = mix.getMix(getTile(0, y));
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Neptunian");
        planet.setType(PlanetType.Neptunian);
        planet.setRadius(50_000);

        //planet.addFeature(Neptunian.NeptunianFeatures.SapphireClouds);
        //planet.addFeature(WhiteBands);

        testOutput(planet, new Neptunian(Server.getWorldGen(), null, null, null, 0),
                new NeptunianMapper(planet));
    }
}
