/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets;

/**
 * Defines features that are unique to moons.
 */
public enum MoonFeature implements PlanetFeature {
    Moon,
    SmallMoon,
    LargeMoon,
    TidallyLocked,
    AlmostLocked
}
