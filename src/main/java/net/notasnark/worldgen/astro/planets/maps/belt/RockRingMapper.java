/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.stars.Star;

import java.util.List;
import java.util.Random;

/**
 * Mapper for a rock ring around a planet.
 */
public class RockRingMapper extends AsteroidBeltMapper {
    protected static final Logger logger = LoggerFactory.getLogger(RockRingMapper.class);

    public RockRingMapper(final Planet planet, final int size) {
        super(planet, size);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public RockRingMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public void generate() {
        // Nothing do do here.
    }

    /**
     * Draw an orbital map of this ring. The ring consists of discrete particles rather than a single solid
     * object. Individual particles are positioned according to their own orbital velocity.
     *
     * @param image         Image to draw ring onto.
     * @param star          Ignored, may be null.
     * @param cx            Centre point (in pixels) to position ring around.
     * @param cy            Centre point (in pixels) to position ring around.
     * @param kmPerPixel    Scale to draw at, number of KM per pixel.
     * @param moons         List of any moons for this ring. Ignored.
     */
    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random  random = new Random(planet.getId());

        final int number = getNumberOfPlanetesimals(planet, kmPerPixel) * 100;

        String colour = planet.getType().getColour();
        if (isFaded) {
            colour = SimpleImage.getLighter(colour, 3);
        }
        for (int a = 0; a < number; a++) {
            double  d = planet.getDistance() + random.nextFloat() * planet.getRadius() - random.nextFloat() * planet.getRadius();
            double  angle = random.nextDouble() * 360.0 + getAngleOffset(planet);
            int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
            int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

            image.rectangleFill(x, y, 1, 1, colour);
        }
    }

}
