/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.smallbody;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.smallbody.Kuiper;
import net.notasnark.worldgen.astro.planets.maps.SmallBodyMapper;
import net.notasnark.worldgen.astro.planets.tiles.Icy;

/**
 * An icy comet, in a stable
 */
public class KuiperMapper extends SmallBodyMapper {
    public KuiperMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public KuiperMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile ICE = new Tile("Ice", "#F0F0F0", false);
    private static final Tile METHANE = new Tile("Methane", "#D0D090", false);
    private static final Tile THOLINS = new Tile("Tholins", "#DDA090", false);
    private static final Tile SOOT = new Tile("Soot", "#101010", false);

    public void generate() {
        super.generate();

        Tile surface = ICE;
        Tile crater = ICE;
        if (planet.hasFeature(Kuiper.KuiperFeature.Tholins)) {
            surface = THOLINS;
            crater = METHANE;
        }

        int baseGrey = 60;
        for (int y = 0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int h = 25 + (int) (getHeight(x, y) * 0.75);
                setTile(x, y, new Icy(surface.getShaded(h)));
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 3;
        if (planet.getRadius() > 240) {
            heightDividor = 1 + (int) (planet.getRadius() / 80);
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }
}
