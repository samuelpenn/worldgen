/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.smallbody;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.smallbody.Gelidaceous;
import net.notasnark.worldgen.astro.planets.maps.SmallBodyMapper;
import net.notasnark.worldgen.astro.planets.tiles.Icy;
import net.notasnark.worldgen.astro.planets.tiles.Rough;

/**
 * An icy asteroid, with some rocky parts.
 */
public class GelidaceousMapper extends SmallBodyMapper {
    public GelidaceousMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public GelidaceousMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile SILICATES = new Tile("Silicates", "#A09070", false);
    private static final Tile ICE = new Tile("Ice", "#F0F0F0", false);
    private static final Tile SOOT = new Tile("Soot", "#101010", false);

    public void generate() {
        super.generate();

        int baseGrey = 60;
        for (int y = 0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                if (planet.hasFeature(Gelidaceous.GelidaceousFeature.RockSurface)) {
                    setTile(x, y, new Icy(SILICATES.getShaded(h)));
                } else {
                    if (h < baseGrey) {
                        int grey = h + (100 - baseGrey);
                        if (planet.hasFeature(Gelidaceous.GelidaceousFeature.SootSurface)) {
                            setTile(x, y, new Icy(SOOT.getShaded(grey)));
                        } else {
                            setTile(x, y, new Rough(ICE.getShaded(grey)));
                        }
                    } else {
                        int grey = 2 * (getHeight(x, y) - baseGrey);
                        if (planet.hasFeature(Gelidaceous.GelidaceousFeature.SootSurface)) {
                            grey /= 2;
                        }
                        if (planet.hasFeature(Gelidaceous.GelidaceousFeature.SnowCovered)) {
                            setTile(x, y, new Icy(SILICATES.getShaded(grey)));
                        } else {
                            setTile(x, y, new Rough(SILICATES.getShaded(grey)));
                        }
                    }
                }
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 1 + (int) (planet.getRadius() / 80);
        if (planet.hasFeature(Gelidaceous.GelidaceousFeature.RockSurface)) {
            heightDividor += 5;
        } else if (planet.hasFeature(Gelidaceous.GelidaceousFeature.SootSurface)) {
            heightDividor += 2;
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }
}
