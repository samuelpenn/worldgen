/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.MoonFeature;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Gelidian type worlds are largely composed of ices, and are found beyond the snowline. They may
 * be moons of gas giants, or exist as worlds in their own right. Unlike Plutonian worlds, these
 * are not heated by tidal flexing, so tend to be solid ice.
 */
public class Gelidian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Gelidian.class);

    public Gelidian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        // No features to add at the moment.
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Gelidian);
        int radius = 1200 + Die.die(400, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(Pressure.None);
        planet.setMagneticField(MagneticField.None);

        addFeatures(planet);

        if (planet.hasFeature(MoonFeature.SmallMoon, DwarfFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon, DwarfFeature.Large)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked)) {
            planet.setDayLength(planet.getPeriod());
        } else if (planet.hasFeature(MoonFeature.AlmostLocked)) {
            planet.setDayLength(planet.getPeriod() * (94 + Die.dieV(6)) / 100);
        }

        // Define resources for this world.
        addPrimaryResource(planet, CommodityName.Water);
        addTertiaryResource(planet, CommodityName.ExoticCrystals);

        return planet;
    }
}
