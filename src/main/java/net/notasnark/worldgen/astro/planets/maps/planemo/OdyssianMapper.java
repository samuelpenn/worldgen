/**
 * Copyright (C) 2021 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.planemo;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.planemo.Odyssian;

import java.io.IOException;

import static net.notasnark.worldgen.astro.planets.generators.planemo.Odyssian.OdyssianFeature.*;

/**
 * A Planemo world, which is actually a type of Jovian world if it was orbiting a star.
 * This uses the JovianMapper as the super class, since it looks more like one of them.
 */
public class OdyssianMapper extends JovianMapper {

    protected static final Tile PALE_CREAM = new Tile("Pale Cream", "#EEEECC", false, 2);
    protected static final Tile LIGHT_CREAM = new Tile("Cream", "#DDDDAA", false, 5);
    protected static final Tile DARK_CREAM = new Tile("Dark Brown", "#CCCC99", false, 3);

    protected static final Tile DARK_BLUE = new Tile("Blue", "#4E82FC", false, 6);
    protected static final Tile LIGHT_BLUE = new Tile("Pale", "#C9EFF2", false, 4);
    protected static final Tile GREEN = new Tile("Green", "#D0EFD0", false, 4);
    protected static final Tile WHITE = new Tile("White", "#E9EFF2", false, 2);
    protected static final Tile SAPPHIRE = new Tile("Sapphire", "#4040E0").random(4);
    protected static final Tile YELLOW = new Tile("Yellow", "#DCDC70", false, 4);

    public OdyssianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public OdyssianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }


    /**
     * Gets a random band colour for the clouds, based on the type of clouds prominent in the atmosphere.
     */
    private Tile getRandomColour() {
        if (planet.hasFeature(YellowClouds)) {
            return new Roller<Tile>(DARK_CREAM, GREEN).add(LIGHT_CREAM, 3).add(YELLOW,3).roll();
        } else if (planet.hasFeature(GreenClouds)) {
            return new Roller<Tile>(YELLOW, LIGHT_CREAM).add(GREEN, 3).roll();
        } else if (planet.hasFeature(BlueClouds)) {
            return new Roller<Tile>(WHITE, LIGHT_BLUE).add(DARK_BLUE, 3).roll();
        } else if (planet.hasFeature(PaleClouds)) {
            return new Roller<Tile>(PALE_CREAM, LIGHT_CREAM, DARK_CREAM).roll();
        } else if (planet.hasFeature(DarkClouds)) {
            return new Roller<Tile>(LIGHT_BLUE).add(SAPPHIRE, 3).add(DARK_BLUE, 3).roll();
        } else {
            return new Roller<Tile>(LIGHT_CREAM, DARK_CREAM).roll();
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d4() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.d8() - Die.d8());
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Odyssian");
        planet.setType(PlanetType.Odyssian);
        planet.setRadius(50_000);
        //planet.addFeature(DarkClouds);
        //planet.addFeature(PaleClouds);
        //planet.addFeature(YellowClouds);
        //planet.addFeature(GreenClouds);
        planet.addFeature(BlueClouds);

        testOutput(planet, new Odyssian(Server.getWorldGen(), null, null, null, 0),
                new OdyssianMapper(planet));
    }
}
