/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * JaniVesperian worlds are tidally locked to their parent star, existing at the inner edge of where
 * planets can form. They have some ice/water content. though it is mostly frozen on the night side.
 * There may be a viable biome in the Twilight zone.
 */
public class JaniVesperian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(JaniVesperian.class);

    public JaniVesperian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.JaniVesperian);

        planet.setHabitability(2);
        planet.setHydrographics(Die.d20(2));
        planet.setAtmosphere(Atmosphere.HighCarbonDioxide);
        planet.setPressure(Die.die(30_000, 2) + 15_000);
        planet.setLife(Life.SimpleLand);
        planet.setDayLength(planet.getPeriod());
        planet.setTemperature((int) (planet.getTemperature() * 1.4));
        planet.setNightTemperature(planet.getTemperature() / 2);

        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, FerricOre);
        addSecondaryResource(planet, Water);

        addSecondaryResource(planet, Grasses);
        addSecondaryResource(planet, Shrubs);

        planet.addFeature(GeneralFeature.TideLocked);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
