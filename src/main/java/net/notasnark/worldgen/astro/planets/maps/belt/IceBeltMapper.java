/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.worldgen.astro.planets.Planet;

/**
 * Mapper for Ice Belts. This is functionally identical to the mapper for Asteroid Belts.
 * It just needs its own class so that it can be found by the reflection methods.
 */
public class IceBeltMapper extends AsteroidBeltMapper {
    protected static final Logger logger = LoggerFactory.getLogger(IceBeltMapper.class);

    public IceBeltMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public IceBeltMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }
}
