/*
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.Commodity;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.commodities.Resource;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.systems.Zone;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * A planet represents any notable celestial object that is not a star. This includes actual planets, moons,
 * asteroid belts, planetary rings and asteroids. They have a PlanetType, which determines the type of planet
 * that the object represents.
 *
 * Where a planet refers to other objects (stars, star systems and planets), it does so by id, not directly
 * to an object of that type. This is done to simplify JSON data structures.
 */
@Entity
@Table(name="planets")
public class Planet {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column (name = "system_id")
    private int systemId;

    @Column (name = "parent_id")
    private int parentId;

    @Column (name = "moon_of")
    private int moonOf;

    @Column
    private long distance;

    @Column
    private long radius;

    @Column (name = "day")
    private long dayLength;

    @Column (name = "period")
    private long period;

    @Column @Enumerated (EnumType.STRING)
    private PlanetType type;

    @Column
    private int temperature;

    @Column (name = "night")
    private int nightTemperature;

    @Column @Enumerated (EnumType.STRING)
    private Atmosphere atmosphere = Atmosphere.Vacuum;

    @Column
    private int pressure;

    @Column
    private int density;

    @Column @Enumerated (EnumType.STRING)
    private MagneticField field = MagneticField.None;

    @Column (name = "hydro")
    private int hydrographics;

    @Column
    private int tilt;

    @Column
    private int eccentricity;

    @Column
    private int inclination;

    @Column (name = "habitability")
    private int habitability;

    @Column @Enumerated (EnumType.STRING)
    private Life life = Life.None;

    @Column @Enumerated (EnumType.STRING)
    private StarPort port = StarPort.X;

    @Column (name = "population")
    private long population;

    @Column @Enumerated (EnumType.STRING)
    private Government government = Government.None;

    @Column (name = "tech")
    private int techLevel;
    @Column (name = "law")
    private int law;

    @Column
    private String codes;

    @Column
    private Zone zone = Zone.GREEN;

    @Column
    private String description;

    @Transient
    private Set<PlanetFeature> features = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "resources", joinColumns= @JoinColumn(name="planet_id"))
    private List<Resource> resources;

    @Transient
    private Map<String, String> extra;

    public Planet() {
        extra = new HashMap<>();
        codes = "";
    }

    /**
     * Gets the unique id of this planet.
     *
     * @return  Planet Id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the unique id of this planet. Cannot be changed after it has been set.
     *
     * @param id    Planet id.
     */
    void setId(int id) {
        if (this.id != 0) {
            throw new IllegalStateException("Can't change the id of a planet after setting it.");
        }
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException("Planet name must be non-empty.");
        }
        this.name = name.trim();
    }

    /**
     * Gets the id of the star system this planet belongs to.
     *
     * @return  Id of star system.
     */
    public int getSystemId() {
        return systemId;
    }

    /**
     * Sets the id of the star system this planet belongs to.
     *
     * @param systemId  Star System id.
     */
    void setSystemId(int systemId) {
        this.systemId = systemId;
    }

    /**
     * Gets the id of the parent star for this planet. If this is a rogue planet
     * with no stellar parent, then the id will be zero. If this planet is a moon,
     * then the parent is still the star.
     *
     * @return  Id of planet's parent star.
     */
    public int getParentId() {
        return parentId;
    }

    void setParentId(int parentId) {
        this.parentId = parentId;
    }


    /**
     * Is this planet a belt or ring of some kind. If so, it won't have the usual planetary map.
     *
     * @return  True iff this is a stellar or planetary ring or belt.
     */
    public boolean isBelt() {
        return type.getGroup() == PlanetGroup.Belt;
    }

    public boolean isSmallBody() { return type.getGroup() == PlanetGroup.SmallBody; }

    public boolean isJovian() { return type.getGroup() == PlanetGroup.Jovian; }

    public boolean isTerrestrial() { return type.getGroup() == PlanetGroup.Terrestrial; }

    public boolean isDwarf() { return type.getGroup() == PlanetGroup.Dwarf; }

    /**
     * Returns true iff this planet is a moon of another planet. A planet is a moon if MoonOf is
     * non-zero.
     *
     * @return  True iff this is a moon.
     */
    public boolean isMoon() {
        return moonOf > 0;
    }

    public void setMoonOf(int planetId) {
        this.moonOf = planetId;
    }

    /**
     * If this is a moon, then gets the id of the planet around which this moon orbits.
     * If this is not a moon, then returns zero.
     *
     * @return      Id of the planet around which this planet orbits, if it is a moon.
     */
    public int getMoonOf() {
        return moonOf;
    }

    /**
     * Distance is always in kilometres. This is the distance from the planet's primary. If the
     * planet is a moon, this is the distance from the parent parent. If it's a planet, then it's
     * distance from the star.
     *
     * If a moon is part of a Belt, then the distance may be negative.
     *
     * @return  Distance of planet from primary.
     */
    public long getDistance() {
        return distance;
    }

    public void setDistance(long km) {
        this.distance = km;
    }

    public PlanetType getType() {
        return type;
    }

    public void setType(PlanetType type) {
        if (type == null) {
            throw new IllegalArgumentException("Planet type must be non-null");
        }
        this.type = type;
    }

    public long getRadius() {
        return radius;
    }

    public void setRadius(long km) {
        this.radius = Math.max(km, 0);
    }

    public long getBeltWidth() {
        return radius * 2;
    }

    /**
     * Gets the density of this planet. A density of 1000 is equal to that of water.
     * For belts, the density is handled differently.
     *
     * @return  Density in kg per cubic metre.
     */
    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = Math.max(0, density);
    }

    /**
     * Gets the length of the day of this planet in seconds.
     *
     * @return      Day length in seconds.
     */
    public long getDayLength() {
        return dayLength;
    }

    public void setDayLength(long seconds) {
        this.dayLength = Math.max(seconds, 0);
    }

    /**
     * Gets the length of the year of this planet in seconds.
     *
     * @return      Year length in seconds.
     */
    public long getPeriod() { return period; }

    public void setPeriod(long seconds) {
        this.period = Math.max(seconds, 0);
    }

    /**
     * Gets the axial tilt of this planet. Zero means that the planet rotates with its pole pointing directly
     * upwards relative to the system's plane. It will always be between 0° and 90° inclusive.
     *
     * @return  Axial tilt of the planet, in degrees.
     */
    public int getAxialTilt() {
        return tilt;
    }

    public String getAxialTiltText() {
        return tilt + "°";
    }

    /**
     * Sets the axial tilt of this planet. This is always a positive number, from 0° to 90°.
     *
     * @param tilt  Axial tilt, between 0° and 90°.
     */
    public void setAxialTilt(final int tilt) {
        this.tilt = Math.min(Math.abs(tilt), 90);
    }

    /**
     * Gets the orbital eccentricity of this planet, as a percentage from 0 to 100. 0 means that this planet has
     * a perfectly circular orbit. 100 means that it is on a parabolic escape trajectory, so is not actually in
     * an orbit. No planet will therefore have an eccentricity higher than 99. Most planets are between 1 and 5.
     *
     * @return  Orbital eccentricity as a percentage, from 0 to 99.
     */
    public int getEccentricity() {
        return eccentricity;
    }

    public String getEccentricityText() {
        DecimalFormat format = new DecimalFormat();
        format.setMinimumFractionDigits(3);
        format.setMaximumFractionDigits(3);
        return format.format (eccentricity / 100.0);
    }

    /**
     * Sets the eccentricity of this planet's orbit. 0 is a perfectly circular orbit.
     *
     * @param eccentricity  Eccentricity, between 0 and 99 inclusive.
     */
    public void setEccentricity(final int eccentricity) {
        this.eccentricity = Math.min(Math.max(eccentricity, 0), 99);
    }

    /**
     * Gets the orbital inclination of this planet. This is the number of degrees the planet's orbit is inclined
     * to the ecliptic, which is normally defined as the stars equator.
     *
     * @return  Inclination in degrees.
     */
    public int getInclination() {
        return inclination;
    }

    public String getInclinationText() {
        return inclination + "°";
    }

    /**
     * Sets the orbital inclination of this planet.
     *
     * @param inclination   Inclination in degrees, 0° to 180°.
     */
    public void setInclination(final int inclination) {
        this.inclination = Math.min(Math.max(inclination, 0), 180);
    }

    /**
     * Gets the surface temperature of this planet in Kelvin. This is the average surface
     * temperature - there will be wide fluctuations based on latitude and time of year.
     * A standard temperature is considered to be around 285K - 290K.
     *
     * @return  Surface temperature in Kelvin.
     */
    public int getTemperature() {
        return temperature;
    }

    public String getTemperatureText() {
        return String.format("%,dK (%,d°C)", temperature, temperature - 273);
    }

    /**
     * Sets the temperature of this world, in Kelvin. The minimum temperature that can be set is 3K. Setting
     * it any lower than this will set it to be 3K.
     *
     * If this planet is a belt, then the night temperature is also set to the same value.
     *
     * @param k     Temperature to set to, in kelvin.
     */
    public void setTemperature(int k) {
        this.temperature = Math.max(k, 3);
        if (isBelt()) {
            this.nightTemperature = this.temperature;
        }
    }

    public void addTemperature(int k) {
        this.temperature = Math.max(this.temperature + k, 3);
    }

    /**
     * Gets the average night time surface temperature of this planet in Kelvin. For many
     * planets it will be a bit below the day time temperature. For planets which have
     * very long nights, it will be considerably lower.
     *
     * If the planet is a belt, then the night side temperature cannot be set differently
     * to the day side temperature. Instead, setting either will automatically ignore any
     * given night temperature and set it instead to the day temperature.
     *
     * @return  Night time surface temperature in Kelvin.
     */
    public int getNightTemperature() {
        return nightTemperature;
    }

    public void setNightTemperature(int k) {
        if (isBelt()) {
            this.nightTemperature = this.temperature;
        } else {
            this.nightTemperature = Math.max(k, 3);
        }
    }

    public Atmosphere getAtmosphere() {
        return atmosphere;
    }

    public void setAtmosphere(Atmosphere atmosphere) {
        this.atmosphere = atmosphere;
    }

    /**
     * Gets the atmospheric pressure of this planet, in Pascals. 100,000 Pa is considered
     * to be standard pressure.
     *
     * @return      Pressure in Pascals.
     */
    public int getPressure() {
        return pressure;
    }

    public Pressure getPressureText() {
        Pressure value = Pressure.None;
        for (Pressure p : Pressure.values()) {
            if (pressure > p.getPascals()) {
                value = p;
            }
        }
        return value;
    }

    /**
     * Sets the atmospheric pressure of the planet, in Pascals. 100,000 Pa is considered
     * to be standard pressure.
     *
     * @param pascals   Pressure in pascals.
     */
    public void setPressure(int pascals) {
        this.pressure = Math.max(pascals, 0);
    }

    public void setPressure(Pressure pressure) {
        if (pressure != null) {
            this.pressure = pressure.getPascals();
        } else {
            this.pressure = 0;
        }
    }

    public void addPressure(int pascals) {
        this.pressure = Math.max(this.pressure + pascals, 0);
    }

    /**
     * Gets the proportion of the planet's surface that is covered by liquid water, as a
     * percentage from 0 - 100.
     *
     * @return      Surface water percentage, 0-100.
     */
    public int getHydrographics() {
        return hydrographics;
    }

    /**
     * Sets the proportion of the planet's surface that is covered by liquid water, as a
     * percentage from 0 - 100. Values outside this range will be capped to 0 or 100.
     *
     * @param hydrographics Water cover as a percentage.
     */
    public void setHydrographics(int hydrographics) {
        this.hydrographics = Math.max(Math.min(hydrographics, 100), 0);
    }

    /**
     * Gets the magnetic field strength for this planet.
     *
     * @return  Strength of the magnetic field.
     */
    public MagneticField getMagneticField() {
        if (field != null) {
            return field;
        } else {
            return MagneticField.None;
        }
    }

    /**
     * Sets the magnetic field strength of this planet.
     *
     * @param field     Strength of the magnetic field.
     */
    public void setMagneticField(MagneticField field) {
        if (field != null) {
            this.field = field;
        } else {
            throw new IllegalArgumentException("MagneticField cannot be set to a null value");
        }
    }

    /**
     * Gets the habitability class of this world.
     * Class 1 worlds are very habitable and easy to live on.
     * Class 2 worlds are a struggle to live on, though life support isn't required.
     * Class 3 worlds are difficult to live on, and requiring at least breathing masks.
     * Class 4 worlds require a full body suit to survive on.
     * Class 5 worlds require special survival gear beyond a simple space suit.
     * Class 6 worlds are extremely hostile.
     *
     * @return  The habitability class of the world.
     */
    public int getHabitability() {
        return habitability;
    }

    public String getHabitabilityText() {
        switch (habitability) {
            case 0: return "O";
            case 1: return "I";
            case 2: return "II";
            case 3: return "III";
            case 4: return "IV";
            case 5: return "V";
            case 6: return "VI";
        }
        return "" + habitability;
    }

    public void setHabitability(int habitability) {
        this.habitability = Math.min(6, Math.max(1, habitability));
    }

    public Life getLife() {
        return life;
    }

    public void setLife(Life life) {
        this.life = life;
    }

    public StarPort getStarPort() {
        return port;
    }

    public void setStarPort(StarPort port) {
        this.port = port;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = Math.max(population, 0);
    }

    public Government getGovernment() {
        return government;
    }

    public void setGovernment(Government government) {
        this.government = government;
    }

    public int getTechLevel() {
        return techLevel;
    }

    public void setTechLevel(int techLevel) {
        this.techLevel = Math.max(techLevel, 0);
    }

    public int getLawLevel() {
        return law;
    }

    public void setLawLevel(int law) {
        this.law = Math.max(Math.min(law, 6), 0);
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(final Zone zone) {
        if (zone != null) {
            this.zone = zone;
        } else {
            this.zone = Zone.GREEN;
        }
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Add the specified feature to the planet. Features are special aspects of the world
     * which may appear on the map, and can be described in the planet's text description.
     * Features themselves aren't stored against the planet object, they are only used to
     * modify maps and descriptions at generation time.
     *
     * @param feature   One or more features to set.
     */
    public void addFeature(PlanetFeature... feature) {
        if (feature != null && feature.length > 0) {
            for (PlanetFeature f : feature) {
                features.add(f);
            }
        }
    }

    public void removeFeature(PlanetFeature... feature) {
        if (feature != null && feature.length > 0) {
            for (PlanetFeature f : feature) {
                features.remove(f);
            }
        }
    }

    /**
     * Does the planet have any of the requested features? A feature is an enum which implements
     * the PlanetFeature interface.
     *
     * @param list  List of features to check for.
     * @return      Returns true iff the planet has this feature.
     */
    public boolean hasFeature(PlanetFeature... list) {
        for (PlanetFeature feature : list) {
            if (features.contains(feature)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasFeature(String... featureNames) {
        for (String featureName : featureNames) {
            for (PlanetFeature feature : features) {
                if (feature.toString().equals(featureName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<PlanetFeature> getFeatures() {
        return new ArrayList<PlanetFeature>(features);
    }

    public void setFeatures(HashSet<PlanetFeature> features) {
        this.features = features;
    }

    /**
     * Gets the diameter of the planet. This is simply the radius * 2. This is sometimes used
     * by the TextGenerator which can't do the arithmetic itself.
     *
     * @return  Diameter of the planet in km, or width of a Belt.
     */
    public long getDiameter() {
        return radius * 2;
    }

    /**
     * Gets the list of resources available on this world.
     *
     * @return  List of resources. May be empty, never null.
     */
    public List<Resource>  getResources() {
        if (resources == null) {
            resources = new ArrayList<Resource>();
        }
        return resources;
    }

    public int getResource(CommodityName name) {
        try {
            for (Resource r : resources) {
                if (r.getCommodity().getName().equals(name.getName())) {
                    return r.getDensity();
                }
            }
        } catch (NullPointerException e) {
            // No resource found.
            //System.out.println("Null");
        }

        return 0;
    }

    /**
     * Adds a resource with the specified density to the list of resources this planet has.
     * If the resource already exists, then its density is overwritten. If the density is below 1,
     * then no resource is added, and any existing resource of that type is removed.
     *
     * The provided density is modified by the frequency of the commodity type, so will probably
     * end up being lower than the given density (and may end up not being added if the density
     * drops below 1).
     *
     * @param commodity     Commodity this resource is for.
     * @param density       Density, generally 1 - 1000.
     */
    public void addResource(Commodity commodity, int density) {
        int f = commodity.getFrequency().getBaseFrequency();
        density = (density * f) / 100;

        if (resources == null) {
            resources = new ArrayList<Resource>();
        }
        for (Resource r : resources) {
            if (r.getCommodity().equals(commodity)) {
                if (density > 0) {
                    r.setDensity(density);
                } else {
                    resources.remove(r);
                }
                return;
            }
        }
        if (density > 0) {
            resources.add(new Resource(commodity, density));
        }
    }

    /**
     * Gets the mass of the planet in kg.
     *
     * @return  Mass in kilogrammes.
     */
    public double getMass() {
        double volume = (Math.PI * 4.0 / 3.0) * Math.pow(radius * 1000.0, 3);
        return volume * (density);
    }

    public double getEarthMass() {
        return getMass() / Physics.EARTH_MASS;
    }

    /**
     * Gets the surface gravity of this planet in metres per second per second.
     *
     * @return  Surface gravity in m/s/s.
     */
    public double getSurfaceGravity() {
        return Physics.round(Physics.G * Math.PI * 4 * density * radius * 1000 / 3.0, 2);
    }

    public double getEarthGravity() {
        return getSurfaceGravity() / Physics.EARTH_GRAVITY;
    }

    /**
     * Gets the escape velocity from the surface of the planet in metres per second.
     *
     * @return  Escape velocity in metres per second.
     */
    public long getEscapeVelocity() {
        return Physics.round(Math.sqrt((2 * Physics.G * getMass()) / (radius * 1000.0) ));
    }

    /**
     * Gets the artificial gravity due to spin for this planet. Generally used for SmallBody planets that
     * have been hollowed out and are being spun to produce gravity.
     *
     * @return  Artificial gravity in m/s/s.
     */
    public double getSpinGravity() {
        return Physics.round(Physics.getGravityOfSpin(getRadius() * 1000, getDayLength()), 2);
    }

    /**
     * Extra information is stored on the 'extra' property. This consists of calculated
     * data which is easy for the server to determine, but difficult for clients to. Before
     * being transformed to JSON, updateExtras() should be called to populate the map with
     * information which can be used by the client.
     */
    public void updateExtras(Planet parent) {
        extra.put("Group", type.getGroup().toString());
        extra.put("g", "" + getSurfaceGravity());
        extra.put("escapeVelocity", "" + getEscapeVelocity());
        if (parent != null && parent.getType().getGroup() != PlanetGroup.Belt) {
            // If this is a moon, use the orbital period of the parent body.
            extra.put("solarDay", "" + Physics.getSolarDay(parent.period, dayLength));
        } else {
            extra.put("solarDay", "" + Physics.getSolarDay(period, dayLength));
        }
        extra.put("albedo", "" + getType().getAlbedo());
        if (parent != null) {
            extra.put("parentName", parent.getName());
        }
        if (getTradeCodes().contains(TradeCode.Sg)) {
            extra.put("spinGravity", "" + getSpinGravity());
        }
    }

    public void updateExtras() {
        updateExtras(null);
    }

    /**
     * Get the list of extra information. The information is always returned as a string,
     * so it is up to the caller to figure out how to interpret it.
     *
     * @return  Map of extras, as string pairs.
     */
    public Map<String,String> getExtras() {
        return extra;
    }

    public Set<TradeCode> getTradeCodes() {
        Set<TradeCode> tradeCodes = EnumSet.noneOf(TradeCode.class);

        if (codes.length() > 0) {
            for (String code : codes.split(" ")) {
                tradeCodes.add(TradeCode.valueOf(code));
            }
        }

        return tradeCodes;
    }

    public void setTradeCodes(String codes) {
        this.codes = codes;
    }

    public boolean hasTradeCode(TradeCode code) {
        return getTradeCodes().contains(code);
    }

    public void addTradeCode(TradeCode... list) {
        if (codes != null) {
            for (TradeCode code : list) {
                if (!getTradeCodes().contains(code)) {
                    codes = (codes + " " + code.name()).trim();
                }
            }
        }
    }

    public void removeTradeCode(TradeCode code) {
        if (getTradeCodes().contains(code)) {
            codes = codes.replaceAll(code.toString(), "").replaceAll("  ", " ").trim();
        }
    }


    /**
     * Returns true iff the planet is one of the listed planet types.
     *
     * @param types     List of planet types to check.
     * @return          True iff planet's type is one of those listed.
     */
    public boolean isA(PlanetType... types) {
        for (PlanetType t: types) {
            if (getType() == t) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true iff the planet is one of the listed planet classes.
     *
     * @param classes   List of planet classes to check.
     * @return          True iff planet's class is one of those listed.
     */
    public boolean isA(PlanetClass... classes) {
        for (PlanetClass c: classes) {
            if (getType().getClassification() == c) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true iff the planet is one of the listed planet groups.
     *
     * @param groups    List of planet groups to check.
     * @return          True iff planet's group is one of those listed.
     */
    public boolean isA(PlanetGroup... groups) {
        for (PlanetGroup g: groups) {
            if (getType().getGroup() == g) {
                return true;
            }
        }
        return false;
    }

    public void rename(String oldName, String newName) {
        this.name = this.name.replaceAll(oldName, newName);
        this.description = this.description.replaceAll(oldName, newName);
    }

    public static void main(String[] args) {
        Planet p = new Planet();
        p.setRadius(7200);
        p.setDensity(5700);
        System.out.println(p.getSurfaceGravity());
    }
}
