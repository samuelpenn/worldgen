/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Jovian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Jovian.JovianFeature.SilicateClouds;

/**
 * Sokarian worlds of the SubJovian class of the Jovian group. They are very hot worlds, close
 * to their star, often with silicate clouds (Sudarsky class V). They tend to be tidally locked.
 */
public class Sokarian extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Sokarian.class);

    public Sokarian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        planet.addFeature(SilicateClouds);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(18_000 + Die.die(3_000, 4));

        // Set to be tidally locked.
        planet.setDayLength(planet.getPeriod());
        planet.addFeature(GeneralFeature.TideLocked);
        planet.setNightTemperature(planet.getTemperature());
        planet.setTemperature(planet.getTemperature() * 3);

        if (planet.getTemperature() > Temperature.SilicatesMelt.getKelvin() + 500) {
            planet.addFeature(SilicateClouds);
            planet.setAtmosphere(Atmosphere.Exotic);
        } else {
            planet.setAtmosphere(Atmosphere.Hydrogen);
        }
        planet.setPressure(Pressure.SuperDense);

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.Weak);
                break;
            case 4: case 5: case 6: case 7: case 8: case 9: case 10:
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 11: case 12:
                planet.setMagneticField(MagneticField.Strong);
                break;
        }

        generateFeatures(planet);

        addPrimaryResource(planet, Hydrogen);
        addPrimaryResource(planet, SilicateOre);
        addPrimaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, ExoticCrystals);
        addTertiaryResource(planet, Helium);

        return planet;
    }
}
