/**
 * Copyright (C) 2017-2021 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Jovian;
import net.notasnark.worldgen.astro.planets.generators.jovian.Saturnian;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A type of Jovian world which tends to consist of rather bland, light coloured clouds.
 */
public class SaturnianMapper extends JovianMapper {

    protected static final Tile PALE_CREAM = new Tile("Pale Cream", "#EEEECC", false, 2);
    protected static final Tile LIGHT_CREAM = new Tile("Cream", "#DDDDAA", false, 5);
    protected static final Tile DARK_CREAM = new Tile("Dark Brown", "#CCCC99", false, 3);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#D3AD6E", false, 3);
    protected static final Tile YELLOW = new Tile("Yellow", "#DCDC70", false, 4);
    protected static final Tile GREEN = new Tile("Pale Green", "#CCEECC", false, 2);

    public SaturnianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public SaturnianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }


    /**
     * Gets a random band colour for the clouds, based on the type of clouds prominent in the atmosphere.
     */
    private Tile getRandomColour() {
        if (planet.hasFeature(Jovian.JovianFeature.AmmoniaClouds)) {
            return new Roller<Tile>(YELLOW, LIGHT_BROWN).add(LIGHT_CREAM, 3).add(DARK_CREAM,3).roll();
        } else if (planet.hasFeature(Jovian.JovianFeature.MethaneClouds)) {
            return new Roller<Tile>(GREEN).add(LIGHT_CREAM, 3).roll();
        } else if (planet.hasFeature(Jovian.JovianFeature.WaterClouds)) {
            return new Roller<Tile>(DARK_CREAM).add(PALE_CREAM, 2).add(DARK_CREAM, 3).roll();
        } else {
            return new Roller<Tile>(PALE_CREAM, LIGHT_CREAM, DARK_CREAM).roll();
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d4() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.d8() - Die.d8());
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Saturnian");
        planet.setType(PlanetType.Saturnian);
        planet.setRadius(50_000);
        //planet.addFeature(MethaneClouds);
        planet.addFeature(Jovian.JovianFeature.AmmoniaClouds);

        testOutput(planet, new Saturnian(Server.getWorldGen(), null, null, null, 0),
                new SaturnianMapper(planet));
    }
}
