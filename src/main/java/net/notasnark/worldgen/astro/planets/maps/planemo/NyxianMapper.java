/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.planemo;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.planemo.Nyxian;
import net.notasnark.worldgen.astro.planets.maps.HelianMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.Snow;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.List;

public class NyxianMapper extends HelianMapper {

    protected static final Tile PLAINS = new Tile("Plains", "#B0A0A0");
    protected static final Tile MOUNTAINS = new Tile("Mountains", "#705050").mountain(true);

    protected static final Tile ICE_PLAINS = new Tile("Ice Plains", "#B0B0C0");
    protected static final Tile ICE_MOUNTAINS = new Tile("Ice Mountains", "#FEFEFF").mountain(true);

    public NyxianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public NyxianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour(int height) {
        int mod = height / 24 + (planet.hasFeature(Nyxian.NyxianFeature.NoVolatiles)?1:0);

        if (Die.d4(2) + mod > 8) {
            return NyxianMapper.MOUNTAINS;
        } else {
            return NyxianMapper.PLAINS;
        }
    }

    public void generate() {
        super.generate();

        // Basic barren landscape.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getRandomColour(getHeight(x, y));
                setTile(x, y, tile);
            }
        }
        flood(MOUNTAINS, 2);
        flood(PLAINS, 3);

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getTile(x, y).getShaded(50 + getHeight(x, y) / 2);
                int  h = getHeight(x, y);

                if (planet.hasFeature(Nyxian.NyxianFeature.NoVolatiles, Nyxian.NyxianFeature.FewVolatiles)) {
                    if (tile.isMountain()) {
                        setHeight(x, y, 95);
                    } else {
                        setHeight(x, y, 15);
                        if (planet.hasFeature(Nyxian.NyxianFeature.NoVolatiles)) {
                            setTile(x, y, new Rough(tile));
                        } else if (planet.hasFeature(Nyxian.NyxianFeature.FewVolatiles) && h < 15) {
                            setTile(x, y, new Snow(tile));
                        } else {
                            setTile(x, y, new Rough(tile));
                        }
                    }
                } else {
                    if (tile.isMountain()) {
                        setHeight(x, y, 95);
                        setTile(x, y, new Rough(ICE_MOUNTAINS.getShaded(50 + h)));
                    } else {
                        setHeight(x, y, 15);
                        setTile(x, y, new Snow(ICE_PLAINS.getShaded(25 + h)));
                    }
                }
            }
        }

        cleanBumpMap();

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
    }


    private void generateFeatures(List<PlanetFeature> features) {
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Nyxian");
        planet.setType(PlanetType.Nyxian);
        planet.setTemperature(3);
        planet.setPressure(3_000);
        planet.setAtmosphere(Atmosphere.InertGases);
        planet.setLife(Life.None);
        //planet.addFeature(Nyxian.NyxianFeature.NoVolatiles);
        planet.addFeature(Nyxian.NyxianFeature.FewVolatiles);

        testOutput(planet, new Nyxian(Server.getWorldGen(), null, null, null, 0),
                new NyxianMapper(planet));
    }
}
