/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.planemo;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.planets.generators.Planemo;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * These are small Planemo worlds, frozen balls of rock and ice with little to no atmosphere.
 */
public class Nyxian extends Planemo {
    private static final Logger logger = LoggerFactory.getLogger(Nyxian.class);

    public enum NyxianFeature implements PlanetFeature {
        NoVolatiles,
        FewVolatiles
    }

    public Nyxian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(1_500 + Die.die(500, 3));

        planet.setAtmosphere(Atmosphere.InertGases);
        planet.setPressure(1_000 + Die.die(5_000, 2));
        planet.setMagneticField(MagneticField.None);

        if (Die.d2() == 1) {
            planet.setPressure(planet.getPressure() / 5);
        }

        planet.setDayLength(6 * Physics.HOUR + Die.die(60) * Physics.HOUR + Die.die(Physics.HOUR));

        planet.setTemperature(3);
        planet.setNightTemperature(planet.getTemperature());

        planet.addTradeCode(TradeCode.Ba);
        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.addFeature(NyxianFeature.NoVolatiles);
                addTraceResource(planet, CarbonicOre);
                break;
            case 5: case 6: case 7:
                planet.addFeature(NyxianFeature.FewVolatiles);
                addTertiaryResource(planet, Water);
                addTertiaryResource(planet, CarbonicOre);
                planet.addTradeCode(TradeCode.Ic);
                break;
            default:
                planet.addTradeCode(TradeCode.Ic);
                addPrimaryResource(planet, Water);
                addTertiaryResource(planet, CarbonicOre);
                break;
        }

        addTraceResource(planet, Helium);
        addSecondaryResource(planet, SilicateOre);

        return planet;
    }
}
