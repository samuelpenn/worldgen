/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.smallbody;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.maps.SmallBodyMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;

/**
 * A Carbonaceous asteroid has a dark, rocky surface. They may have a few volatiles.
 */
public class CarbonaceousMapper extends SmallBodyMapper {
    public CarbonaceousMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public CarbonaceousMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile CARBON = new Tile("Carbon", "#404040", true);

    public void generate() {
        super.generate();

        int baseGrey = 25;
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int grey = baseGrey + getHeight(x, y) / 2;
                setTile(x, y, new Rough(CARBON.getShaded(grey)));
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 3;
        if (planet.getRadius() > 300) {
            heightDividor = 1 + (int) (planet.getRadius() / 100);
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }
}
