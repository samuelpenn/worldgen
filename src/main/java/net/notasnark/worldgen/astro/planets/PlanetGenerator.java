/**
 * PlanetGenerator.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

import net.notasnark.worldgen.astro.commodities.*;
import net.notasnark.worldgen.astro.planets.codes.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.UWP;
import net.notasnark.worldgen.civ.CivilisationFeature;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.worldgen.text.TextGenerator;
import net.notasnark.worldgen.web.Server;

import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Abstract class which defines planet generators. There is a planet generator class
 * for each type of planet, as defined by the PlanetType enum.
 */
public abstract class PlanetGenerator {
    protected static final Logger logger = LoggerFactory.getLogger(PlanetGenerator.class);
    protected final StarSystem system;
    protected final Star star;
    protected final Planet previousPlanet;
    protected long  distance;
    protected long  parentDistance;
    protected Planet parentPlanet;
    protected final WorldGen worldGen;
    private final CommodityFactory commodityFactory;
    private int ambient;
    private UWP uwp;

    private List<PlanetFeature> features = new ArrayList<PlanetFeature>();

    public PlanetGenerator(WorldGen worldGen, StarSystem system, Star primary, Planet previous, long distance) {
        this.worldGen = worldGen;
        this.system = system;
        this.star = primary;
        this.distance = distance;
        this.previousPlanet = previous;

        this.commodityFactory = worldGen.getCommodityFactory();
    }

    public void setUWP(UWP uwp) {
        this.uwp = uwp;
    }

    protected void setFromUWP(Planet planet) {
        if (planet != null && uwp != null) {
            planet.setRadius(uwp.getSize() / 2);
            planet.setHydrographics(uwp.getHydrosphere());
            planet.setAtmosphere(uwp.getAtmosphere());
            planet.setPressure(uwp.getPressure());
        }
    }

    /**
     * If this is to be a moon, set the parent planet that this is a moon of.
     *
     * @param parentPlanet  Parent planet for this moon.
     */
    void setMoonOf(Planet parentPlanet) {
        this.parentPlanet = parentPlanet;
    }

    /**
     * For moons, we need to know the distance the parent is from the star so we can correctly
     * determine orbital temperature. If this is not a moon, then it should be zero.
     *
     * @param km    Distance of parent from the star in kilometres.
     */
    public void setParentDistance(long km) {
        this.parentDistance = km;
    }

    public void setAmbientTemperature(final int ambient) {
        this.ambient = ambient;
    }

    public abstract Planet getPlanet(String name, PlanetType type);

    public abstract Planet getPlanet(String name);

    /**
     * Specify a feature that should be applied to the planet when it is created. Normally a generator
     * will determine its own features, but it is possible to override this at a higher level.
     *
     * @param feature       Feature to apply.
     */
    public void addFeature(PlanetFeature feature) {
        features.add(feature);
    }

    public void clearFeatures() {
        features = new ArrayList<PlanetFeature>();
    }

    public List<PlanetFeature> getFeatures() {
        return features;
    }

    void generateDescription(Planet planet) {
        TextGenerator text = new TextGenerator(planet);
        text.setGenerator(this);
        planet.setDescription(text.getFullDescription());
    }

    /**
     * Slowly rotating planets will have a higher daytime surface temperature.
     *
     * @param planet    Planet to modify the temperature of.
     */
    protected void modifyTemperatureByRotation(Planet planet) {
        setTemperature(planet);

        if (planet.isBelt()) {
            return;
        }

        long solarDay = Physics.getSolarDay(planet.getPeriod(), planet.getDayLength());
        if (parentPlanet != null && parentPlanet.getType().getGroup() != PlanetGroup.Belt) {
            // This is in orbit around a star.
            solarDay = Physics.getSolarDay(parentPlanet.getPeriod(), planet.getDayLength());
        }
        if (solarDay > Physics.STANDARD_DAY) {
            double modifier = Math.pow((1.0 * solarDay) / Physics.STANDARD_DAY, 0.5) / 15.0;
            modifier = Math.min(2.0, modifier + 1);
            planet.setNightTemperature((int) (planet.getTemperature() / modifier));
            planet.setTemperature((int) (modifier * planet.getTemperature()));
        } else if (solarDay == 0) {
            // Tidally locked.
            planet.setNightTemperature(planet.getTemperature() / 2);
            planet.setTemperature(planet.getTemperature() * 2);
            planet.addTradeCode(TradeCode.Lk);
        }
    }

    protected void setTemperature(Planet planet) {
        if (star == null) {
            planet.setTemperature(Temperature.DeepSpace.getKelvin());
            planet.setNightTemperature(Temperature.DeepSpace.getKelvin());
            return;
        }
        if (parentPlanet != null && parentPlanet.isBelt()) {
            planet.setTemperature(Physics.getTemperatureOfOrbit(ambient, star,
                    parentPlanet.getDistance() + distance, planet.getType().getAlbedo()));
        } else if (parentPlanet != null) {
            planet.setTemperature(Physics.getTemperatureOfOrbit(ambient, star, parentPlanet.getDistance(),
                    planet.getType().getAlbedo()));
        } else {
            planet.setTemperature(Physics.getTemperatureOfOrbit(ambient, star, distance,
                    planet.getType().getAlbedo()));
        }
        if (planet.isBelt()) {
            planet.setNightTemperature(planet.getTemperature());
        } else {
            planet.setNightTemperature(planet.getTemperature() + Die.dieV(4));
            planet.setTemperature(planet.getTemperature() + Die.dieV(6));
        }
        if (planet.getTemperature() < planet.getNightTemperature()) {
            planet.setTemperature(planet.getNightTemperature() + Die.d2());
        }

        if (planet.getTemperature() > 700) {
            planet.setHabitability(Math.max(6, planet.getHabitability()));
        } else if (planet.getTemperature() > 400) {
            planet.setHabitability(Math.max(5, planet.getHabitability()));
        } else if (planet.getTemperature() > 350) {
            planet.setHabitability(Math.max(4, planet.getHabitability()));
        } else if (planet.getTemperature() < 250) {
            planet.setHabitability(Math.max(3, planet.getHabitability()));
        } else if (planet.getTemperature() < 200) {
            planet.setHabitability(Math.max(4, planet.getHabitability()));
        } else if (planet.getTemperature() < 150) {
            planet.setHabitability(Math.max(5, planet.getHabitability()));
        }
    }

    protected Planet definePlanet(Planet planet) {
        planet.setSystemId(system.getId());
        if (star != null) {
            planet.setParentId(star.getId());
        }
        if (parentPlanet != null) {
            planet.setMoonOf(parentPlanet.getId());
        }
        if (planet.getDistance() == 0) {
            planet.setDistance(distance);
        }
        if (planet.getRadius() == 0) {
            planet.setRadius(500 + Die.d100() * 10);
        }
        planet.setDensity((int) ( planet.getType().getDensity() * 1000) + Die.dieV(100));

        // Set orbital period (year) for this planet.
        if (parentPlanet != null && parentPlanet.isBelt()) {
            planet.setPeriod((star != null) ? star.getPeriod(parentPlanet.getDistance() + distance) : 0);
        } else if (parentPlanet != null) {
            planet.setPeriod(Physics.getOrbitalPeriod(parentPlanet.getMass() + planet.getMass(), distance * 1000));
        } else {
            planet.setPeriod((star != null) ? star.getPeriod(planet.getDistance()) : 0);
        }
        setTemperature(planet);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(0);
        planet.setMagneticField(MagneticField.None);
        planet.setStarPort(StarPort.X);
        planet.setPopulation(0);
        planet.setGovernment(Government.None);
        planet.setHabitability(4);
        planet.setLife(Life.None);
        planet.addTradeCode(TradeCode.Ba);

        // Set the length of day for this planet, and modify temperature based on this.
        if (planet.getType() != null) {
            switch (planet.getType().getGroup()) {
                case SmallBody:
                    planet.setDayLength(planet.getRadius() * 25 + Die.die(1800, 3));
                    break;
                case Dwarf:
                    planet.setDayLength(40_000 + Die.die(25_000, 2));
                    break;
                case Terrestrial: case Planemo:
                    planet.setDayLength(60_000 + Die.die(25_000, 2));
                    break;
                case Jovian: case Helian:
                    planet.setDayLength(20_000 + Die.die(25_000, 2));
                    break;
                default:
                    planet.setDayLength(0);
            }
            modifyTemperatureByRotation(planet);
        }

        // If any features have been pre-specified, then apply them now.
        if (features != null && features.size() != 0) {
            for (PlanetFeature f : features) {
                planet.addFeature(f);
            }
        }

        planet.setDescription("<p>Unexplored.</p>");

        return planet;
    }

    protected Planet definePlanet(String name, PlanetType type) {
        Planet planet = new Planet();
        planet.setName(name);
        planet.setType(type);

        return definePlanet(planet);
    }

    /**
     * Gets the distance of the previous planet in the system. This is used for working
     * out whether we need to migrate this planet outwards in case of collision. If there
     * is no previous planet, the distance returned is zero.
     *
     * @return  Distance from star of previous planet, in millions of km.
     */
    protected long getPreviousDistance() {
        if (previousPlanet == null) {
            return 0;
        } else {
            return previousPlanet.getDistance();
        }
    }

    public static PlanetMapper getMapper(Planet planet) throws UnsupportedException {
        String mapRoot = "net.notasnark.worldgen.astro.planets.maps";
        String typeName = String.format("%s.%s.%sMapper", mapRoot, planet.getType().getGroup().name().toLowerCase(),
                planet.getType().name());

        try {
            Constructor c = Class.forName(typeName).getConstructor(Planet.class);
            return (PlanetMapper) c.newInstance(planet);
        } catch (Exception e) {
            logger.error(String.format("Unable to find map class for planet type [%s]", planet.getType().name()));
        }
        throw new UnsupportedException(String.format("Planet type [%s] does not support maps",
                planet.getType().name()));
    }

    protected SimpleImage getPlanetMap(Planet planet) throws UnsupportedException {
        try {
            PlanetMapper map = getMapper(planet);

            map.generate();
            return map.draw(Server.getConfiguration().getPlanetMapResolution());
        } catch (UnsupportedException e) {
            throw e;
        } catch (Exception e) {
            throw new UnsupportedException("Unable to create world map (" + e.getMessage() + ")", e);
        }
    }

    protected Map<String,SimpleImage> getPlanetMaps(Planet planet) throws UnsupportedException {
        try {
            Map<String,SimpleImage> maps = new HashMap<>();

            PlanetMapper mapper = getMapper(planet);

            int width = Server.getConfiguration().getPlanetMapResolution();
            if (mapper.hasMainMap()) {
                mapper.generate();
                maps.put(PlanetMap.MAIN, mapper.draw(width));
            }
            if (mapper.hasHeightMap()) {
                maps.put(PlanetMap.HEIGHT, mapper.drawHeightMap(width));
            }
            if (mapper.hasDeformMap()) {
                maps.put(PlanetMap.DEFORM, mapper.drawHeightMap(width));
            }
            if(mapper.hasCloudMap()) {
                List<SimpleImage> clouds = mapper.drawClouds(width);
                int height = mapper.getBaseCloudHeight();
                for (int i=0; i < clouds.size(); i++) {
                    maps.put(PlanetMap.CLOUD + "-" + height, clouds.get(i));
                    height += mapper.getBaseCloudHeight();
                }
            }
            if (mapper.hasRingMap()) {
                maps.put(PlanetMap.RING, mapper.drawRing(width));
            }
            if (mapper.hasOrbitMap()) {

            }

            return maps;
        } catch (UnsupportedException e) {
            throw e;
        } catch (Exception e) {
            throw new UnsupportedException("Unable to create world map (" + e.getMessage() + ")", e);
        }
    }

    /**
     * Adds a resource to the planet. A resource is a density of a commodity. The frequency
     * of the commodity affects the final density of the resource.
     *
     * @param planet    Planet to add resource to.
     * @param name      Name of the commodity this resource is for.
     * @param density   Base density for this resource.
     */
    private void addResource(Planet planet, CommodityName name, int density) {
        try {
            Commodity commodity = commodityFactory.getCommodity(name.getName());
            planet.addResource(commodity, density);
        } catch (NoSuchCommodityException e) {
            // This shouldn't happen, since use of the enum should be well defined.
            logger.error(String.format("Well defined commodity [%s] cannot be found", name.getName()));
        }
    }

    public final void addPrimaryResource(Planet planet, CommodityName commodity) {
        addResource(planet, commodity, 800 + Die.d100(4));
    }

    public final void addSecondaryResource(Planet planet, CommodityName commodity) {
        addResource(planet, commodity, 400 + Die.d100(2));
    }

    public final void addTertiaryResource(Planet planet, CommodityName commodity) {
        addResource(planet, commodity, 200 + Die.d100());
    }

    public final void addTraceResource(Planet planet, CommodityName commodity) {
        addResource(planet, commodity, 100 + Die.d20(2));
    }

    /**
     * Boost the amount of resource of a given type that is available, or add it in if
     * it doesn't exist. If it isn't there, then it is added as a tertiary resource. If
     * it is there, it is increased by 25%, or +100 whichever is greatest.
     *
     * @param planet    Planet to add it to.
     * @param name      Name of commodity to be boosted as a resource.
     * @param count     From 1-5, affects size of increase of the value
     */
    public final void addExtraResource(Planet planet, CommodityName name, int count) {
        int     add = 0;
        double  mult = 1.0;
        switch (count) {
            case 1:
                add = 100;
                mult = 1.3;
                break;
            case 2:
                add = 300;
                mult = 1.3;
                break;
            case 3:
                add = 500;
                mult = 1.4;
                break;
            case 4:
                add = 750;
                mult = 1.4;
                break;
            case 5:
                add = 1_000;
                mult = 1.5;
                break;
            default:
                add = count * 200;
                mult = 1.5;
                break;
        }

        try {
            List<Resource> resources = planet.getResources();
            Commodity commodity = commodityFactory.getCommodity(name.getName());
            // Modify by the frequency of this resource.
            add = (add * commodity.getFrequency().getBaseFrequency()) / 100;

            for (Resource r : resources) {
                if (r.getCommodity().equals(commodity)) {
                    int density = r.getDensity();
                    r.setDensity(Math.max(add + Die.dieV(add / 5), (int)(density * mult)));
                    System.out.println("Set [" + r.getCommodity().getName() +"] to [" + r.getDensity() + "]");
                    return;
                }
            }
            // Doesn't exist, so add it in as a tertiary resource.
            addResource(planet, name, add + Die.dieV(add/5));
        } catch (NoSuchCommodityException e) {
            logger.warn(String.format("Commodity [%s] cannot be found.", name.getName()));
        }
    }

    public final void reduceResource(Planet planet, CommodityName name) {
        try {
            List<Resource> resources = planet.getResources();
            Commodity commodity = commodityFactory.getCommodity(name.getName());

            for (Resource r : resources) {
                if (r.getCommodity().equals(commodity)) {
                    int density = r.getDensity();
                    r.setDensity(density / 2);
                    return;
                }
            }
        } catch (NoSuchCommodityException e) {
            logger.warn(String.format("Commodity [%s] cannot be found.", name.getName()));
        }
    }

    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        return new ArrayList<Planet>();
    }

    /**
     * Try to colonise a world. Determines whether the world should be colonised, and
     * creates a new civilisation on it if so.
     *
     * @param planet    Planet to try to colonise.
     * @param features  List of features of any civilisation.
     * @return          Population (if any) of the new world.
     */
    public long colonise(Planet planet, CivilisationFeature... features) {
        // By default, a planet is unlikely to be colonised.

        return 0;
    }

}
