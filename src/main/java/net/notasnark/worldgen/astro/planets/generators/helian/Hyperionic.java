package net.notasnark.worldgen.astro.planets.generators.helian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Helian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * These are massive terrestrial worlds, up to 13 Earth masses in size. They are too hot from internal heating
 * to have a hydrosphere, and have a thick atmosphere of helium and sulphur. Some may have water clouds.
 */
public class Hyperionic extends Helian {
    private static final Logger logger = LoggerFactory.getLogger(Hyperionic.class);

    public Hyperionic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(9_000 + Die.die(2_500, 2));

        planet.setHabitability(6);
        planet.setAtmosphere(Atmosphere.SulphurCompounds);
        int sizeMod = (int) planet.getRadius() / 1000 - 8;
        planet.setPressure(Physics.STANDARD_PRESSURE * Die.d6(sizeMod * 2) + Die.die(Physics.STANDARD_PRESSURE));
        int k = planet.getTemperature() + 300;
        planet.setTemperature(k);
        planet.setNightTemperature((int)(k * 0.9));

        switch (Die.d6(2) + sizeMod) {
            case 2: case 3: case 4:
                planet.setMagneticField(MagneticField.Standard);
                planet.addFeature(HelianFeature.LightVolcanic);
                break;
            case 5: case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.Strong);
                planet.addFeature(HelianFeature.MediumVolcanic);
                break;
            default:
                planet.setMagneticField(MagneticField.VeryStrong);
                planet.addFeature(HelianFeature.HeavyVolcanic);
                break;
        }

        planet.setDayLength(6 * Physics.HOUR + Die.die(6, 1 + sizeMod) * 3 * Physics.HOUR + Die.die(12 * Physics.HOUR));

        addPrimaryResource(planet, Helium);
        addPrimaryResource(planet, SilicateOre);
        addPrimaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, FerricOre);
        addSecondaryResource(planet, Radioactives);
        addTertiaryResource(planet, CarbonicOre);
        addTertiaryResource(planet, HeavyMetals);
        addTraceResource(planet, PreciousMetals);
        addTertiaryResource(planet, CorrosiveGases);
        addTertiaryResource(planet, ExoticCrystals);

        return planet;
    }

}
