/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.smallbody;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.smallbody.Aggregate;
import net.notasnark.worldgen.astro.planets.maps.SmallBodyMapper;
import net.notasnark.worldgen.web.Server;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * An Aggregate asteroid is more of a collection of boulders held together loosely by gravity,
 * rather than being a single entity.
 */
public class AggregateMapper extends SmallBodyMapper {
    protected static final int    DEFAULT_FACE_SIZE = 6;

    public AggregateMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public AggregateMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile CARBON = new Tile("Carbon", "#909090", true);
    private static final Tile SILICATES = new Tile("Silicates", "#A09070", false);
    private static final Tile METALS = new Tile("Metals", "#E0E0F0", false);

    private static final Tile ROCK = new Tile("Rock", "#A0A0A0");


    private String getRockColour() {
        switch (Die.d6()) {
            case 1: case 2: case 3:
                return "#A09070";
            case 4: case 5:
                return "#909090";
            case 6:
                return "#E0E0F0";
        }
        return "#A09070";
    }

    private void aggregateShaper() {
        // Set everything to default.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                setTile(x, y, ROCK.copy().rgb("#202020"));
                setHeight(x, y, 0);
            }
        }

        List<Tile> list = new ArrayList<>();

        // Seed the top and bottom faces.
        int y = getNumRows() / 6;
        int w = getWidthAtY(y) / 5;
        for (int x = 0; x < 5; x++) {
            if (Die.d4() > 1) {
                Tile r = ROCK.copy("Rock " + list.size()).rgb(getRockColour());
                list.add(r);
                setTile(x * w + w/2, y, r);
                setHeight(x * w + w/2, y, 100);
            }
            if (Die.d4() > 1) {
                Tile r = ROCK.copy("Rock " + list.size()).rgb(getRockColour());
                list.add(r);
                setTile(x * w + w/2, getNumRows() - y - 1, r);
                setHeight(x * w + w/2, getNumRows() - y - 1, 100);
            }
        }

        // Seed the middle faces.
        y *= 3;
        w = getWidthAtY(y) / 10;
        for (int x = 0; x < 10; x++) {
            if (Die.d3() > 1) {
                Tile r = ROCK.copy("Rock " + list.size()).rgb(getRockColour());
                list.add(r);
                setTile(x * w + w/2, y, r);
                setHeight(x * w + w/2, y, 100);
            }
        }

        // Now expand
        int tx, ty;
        int count = 1;
        while (count > 0) {
            count = 0;
            for (y=0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (!getTile(x, y).equals(ROCK)) {
                        switch (Die.d3()) {
                            case 1:
                                // West.
                                tx = getWest(x, y);
                                if (getTile(tx, y).equals(ROCK)) {
                                    setTile(tx, y, getTile(x, y).copy().shaded(93));
                                    setHeight(tx, y, (int) (getHeight(x, y) * 0.95));
                                    count++;
                                }
                                break;
                            case 2:
                                // East.
                                tx = getEast(x, y);
                                if (getTile(tx, y).equals(ROCK)) {
                                    setTile(tx, y, getTile(x, y).copy().shaded(93));
                                    setHeight(tx, y, (int) (getHeight(x, y) * 0.95));
                                    count++;
                                }
                                break;
                            case 3:
                                // North/South.
                                Point p = getUpDown(x, y);
                                tx = (int) p.getX();
                                ty = (int) p.getY();
                                if (getTile(tx, ty).equals(ROCK)) {
                                    setTile(tx, ty, getTile(x, y).copy().shaded(93));
                                    setHeight(tx, ty, (int) (getHeight(x, y) * 0.95));
                                    count++;
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    public void generate() {
        super.generate();

        aggregateShaper();
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Aggregate");
        planet.setType(PlanetType.Aggregate);
        planet.setTemperature(200);

        testOutput(planet, new Aggregate(Server.getWorldGen(), null, null, null, 0),
                new AggregateMapper(planet));

    }
}
