/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * ProtoLithian worlds are still in a process of forming. This may be due to being very young, or
 * because they haven't had the opportunity to cool down due to intense solar radiation. They may
 * have an atmosphere due to it being replenished due to outgassing.
 */
public class ProtoGelidian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(ProtoGelidian.class);

    public ProtoGelidian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.ProtoGelidian);
        int radius = 2000 + Die.die(500, 2);

        if (planet.hasFeature(DwarfFeature.Small)) {
            radius *= 0.67;
        } else if (planet.hasFeature(DwarfFeature.Large)) {
            radius *= 1.5;
        }

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.WaterVapour);
        planet.addTradeCode(TradeCode.Ba);
        planet.setPressure(Die.die(100_000, 3));
        planet.setHabitability(4);

        if (planet.hasFeature(GeneralFeature.VeryYoung)) {
            planet.setDayLength(30_000 + Die.die(30_000, 2));
        } else if (planet.hasFeature(GeneralFeature.Young)) {
            planet.setDayLength(50_000 + Die.die(100_000, 2));
        } else {
            switch (Die.d6()) {
                case 1:
                    planet.setDayLength((int) (planet.getPeriod() * 1.5));
                    break;
                case 2:
                    planet.setDayLength((int) (planet.getPeriod() * (2.0 / 3.0)));
                    break;
                default:
                    planet.setDayLength(planet.getPeriod() / 3 + Die.die((int) planet.getPeriod()) / 2);
                    break;
            }
        }
        modifyTemperatureByRotation(planet);

        // Shift night temperature to be more like the day, just in case it isn't. Enough convection
        // processes in the mantle to shift heat around.
        planet.setNightTemperature((planet.getTemperature() + planet.getNightTemperature()) / 2);

        setAutomaticFeatures(planet);
        addFeatures(planet);

        // Molten metallic core provides some degree of magnetic field.
        switch (Die.d6(3)) {
            case 3:
                planet.setMagneticField(MagneticField.Weak);
                break;
            case 4: case 5:
                planet.setMagneticField(MagneticField.VeryWeak);
                break;
            case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.Minimal);
            default:
                planet.setMagneticField(MagneticField.None);
        }

        // Define resources for this world.
        addPrimaryResource(planet, Water);
        addTertiaryResource(planet, SilicateOre);

        return planet;
    }
}
