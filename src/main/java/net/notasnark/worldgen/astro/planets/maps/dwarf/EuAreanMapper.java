/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Arean;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.web.Server;
import net.notasnark.worldgen.astro.planets.generators.dwarf.EuArean;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * EuArean worlds are similar to Mars.
 */
public class EuAreanMapper extends AreanMapper {


    public EuAreanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public EuAreanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    /**
     * Generate a Hermian surface landscape. This will be grey, barren and cratered.
     */
    public void generate() {
        super.generate();

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
    }

    private void generateFeatures(List<PlanetFeature> features) {
        for (PlanetFeature f : features) {
            if (f == Dwarf.DwarfFeature.GreatRift) {
                // A single rift split across the world.
                addRift(RIFT,50 + Die.d20(2));
                flood(RIFT, 2);
            } else if (f == Dwarf.DwarfFeature.BrokenRifts) {
                // A number of small rifts in the surface of the world.
                int numRifts = 6 + Die.d4(2);
                for (int r = 0; r < numRifts; r++) {
                    addRift(RIFT,6 + Die.d4(2));
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        WorldGen worldgen = Server.getWorldGen();
        for (int i=0; i < 101; i++) {
            String name = String.format("Barren%02d", i);
            Planet       planet = new Planet();
            planet.setName(name);
            planet.setType(PlanetType.EuArean);
            planet.setRadius(4000);
            planet.setHydrographics(0);
            planet.setTemperature(250 + Die.d20(2));
            if (Die.d10() == 1) {
                planet.setTemperature(200 + Die.d20(2));
            }
            planet.setPressure(1000);
            planet.setAtmosphere(Atmosphere.CarbonDioxide);
            planet.setLife(Life.None);
            //planet.addFeature(Arean.AreanFeature.GreyDesert);
            //planet.addFeature(Dwarf.DwarfFeature.GreatRift);
            //planet.addFeature(Dwarf.DwarfFeature.EquatorialRidge);
            //planet.addFeature(Arean.AreanFeature.NorthernSea);

            switch (Die.d6(3)) {
                case 3:
                    planet.addFeature(Arean.AreanFeature.WhiteDesert);
                    break;
                case 4:
                    planet.addFeature(Arean.AreanFeature.YellowDesert);
                    break;
                case 5:
                    planet.addFeature(Arean.AreanFeature.RedDesert);
                    break;
                case 6:
                    planet.addFeature(Arean.AreanFeature.BrownDesert);
                    break;
                case 15:
                    planet.addFeature(Dwarf.DwarfFeature.GreatRift);
                    break;
                case 16:
                    planet.addFeature(Dwarf.DwarfFeature.EquatorialRidge);
                    break;
                case 17:
                    planet.addFeature(Arean.AreanFeature.NorthernSea);
                    break;
                case 18:
                    planet.addFeature(Arean.AreanFeature.SouthernSea);
                    break;
            }

            String path = "wonders/" + name;
            PlanetMapper.testOutput(planet,
                    new EuArean(worldgen, null, null, null, 0),
                    new EuAreanMapper(planet, 48),
                    path);

            new File(path+"/"+name+"-Clouds.png").delete();
            new File(path+"/"+name+"-Heat.png").delete();
            new File(path+"/"+name+"-Wet.png").delete();
        }
    }
}
