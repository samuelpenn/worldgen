/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * Lutian worlds are hot, dry and barren Terrestrial worlds with no extant geological cycle.
 * They have enough of an atmosphere to produce erosion and dust storms.
 */
public class Lutian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(Lutian.class);

    public Lutian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Lutian);

        planet.setHabitability(4);
        planet.setRadius(5000 + Die.die(1000, 2));
        planet.setHydrographics(0);
        planet.setAtmosphere(Atmosphere.Nitrogen);
        planet.setPressure(Die.die(40_000) + 20_000);
        planet.setLife(Life.None);
        planet.setDayLength(30_000 + Die.die(30_000, 2));

        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, FerricOre);

        planet.addTradeCode(TradeCode.De, TradeCode.Ba);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
