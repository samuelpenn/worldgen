/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.planemo;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.planemo.Hauhetean;
import net.notasnark.worldgen.astro.planets.maps.HelianMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.Snow;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.List;

public class HauheteanMapper extends HelianMapper {

    protected static final Tile PLAINS = new Tile("Plains", "#B0B0C0");
    protected static final Tile MOUNTAINS = new Tile("Mountains", "#303020").mountain(true);

    protected static final Tile ICE = new Tile("Ice", "#FEFEFF").water(true);

    public HauheteanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public HauheteanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour(int height) {
        switch (Die.d4(2) + height / 24) {
            case 2:
                return HauheteanMapper.ICE;
            case 3: case 4: case 5: case 6: case 7:
                return HauheteanMapper.PLAINS;
            case 8: case 9: case 10: case 11: case 12:
                return HauheteanMapper.MOUNTAINS;
        }
        throw new IllegalStateException("getRandomColour: Invalid switch value.");
    }

    /**
     * Generate a Hauhetean surface landscape. This will be grey and barren with few craters, possibly covered in ice.
     */
    public void generate() {
        super.generate();

        // Basic barren landscape.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getRandomColour(getHeight(x, y));
                setTile(x, y, tile);
            }
        }
        flood(MOUNTAINS, 2);
        flood(PLAINS, 3);

        if (planet.getHydrographics() > 0) {
            for (int i = 0; i < planet.getHydrographics() / 3; i++) {
                int y = Die.rollZero(getNumRows() / 2) + getNumRows() / 4;
                int x = Die.rollZero(getWidthAtY(y));
                setTile(x, y, ICE);
                setHeight(x, y, 0);
            }
            floodToPercentage(ICE, planet.getHydrographics());
        }
        // Shade
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x,y).isWater()) {
                    setHeight(x, y, 0);
                    setTile(x, y, new Rough(getTile(x, y)));
                } else {
                    Tile tile = getTile(x, y).getShaded(50 + getHeight(x, y) / 2);
                    setTile(x, y, new Snow(tile));
                    if (tile.isMountain()) {
                        setHeight(x, y, 95);
                    } else {
                        setHeight(x, y, 15);
                    }
                }
            }
        }

        cleanBumpMap();

        if (planet != null) {
            List<PlanetFeature> features = planet.getFeatures();
            if (features != null && features.size() != 0) {
                generateFeatures(features);
            }
        }
    }


    private void generateFeatures(List<PlanetFeature> features) {
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Hauhetean");
        planet.setType(PlanetType.Hauhetean);
        planet.setHydrographics(00);
        planet.setTemperature(100);
        planet.setPressure(320_000);
        planet.setAtmosphere(Atmosphere.InertGases);
        planet.setLife(Life.None);
        planet.setHydrographics(45);

        testOutput(planet, new Hauhetean(Server.getWorldGen(), null, null, null, 0),
                new HauheteanMapper(planet));
    }
}
