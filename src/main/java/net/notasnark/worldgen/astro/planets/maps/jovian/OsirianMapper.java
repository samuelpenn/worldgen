/**
 * SaturnianMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Jovian;
import net.notasnark.worldgen.astro.planets.generators.jovian.Saturnian;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A Sub-Jovian world (0.03 to 0.48 Jupiter masses) locked into a tight solar orbit. They may have silicate clouds.
 */
public class OsirianMapper extends JovianMapper {

    protected static final Tile RED = new Tile("Red", "#802020", false, 2);
    protected static final Tile ORANGE = new Tile("Orange", "#D08020", false, 5);
    protected static final Tile YELLOW = new Tile("Yellow", "#F0F020", false, 3);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#D3AD6E", false, 3);
    protected static final Tile DARK_BROWN = new Tile("Dark Brown", "#CCCC99", false, 4);

    public OsirianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public OsirianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }


    /**
     * Gets a random band colour for the clouds, based on the type of clouds prominent in the atmosphere.
     */
    private Tile getRandomColour() {
        switch (Die.d8() + (planet.hasFeature(Jovian.JovianFeature.SilicateClouds)?0:2)) {
            case 1: case 2: case 3:
                return RED;
            case 4: case 5:
                return ORANGE;
            case 6:
                return YELLOW;
            case 7:
                return LIGHT_BROWN;
            default:
                return DARK_BROWN;
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d4() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.d8() - Die.d8());
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Osirian");
        planet.setType(PlanetType.Osirian);
        planet.setRadius(35_000 + Die.dieV(10_000));
        planet.addFeature(Jovian.JovianFeature.DarkClouds, Jovian.JovianFeature.SilicateClouds);

        testOutput(planet, new Saturnian(Server.getWorldGen(), null, null, null, 0),
                new OsirianMapper(planet));
    }
}
