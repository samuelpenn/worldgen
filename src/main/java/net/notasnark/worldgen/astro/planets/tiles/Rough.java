/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.tiles;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;

/**
 * Generates a dappled pattern on the tiles when they are displayed, giving a rougher
 * look to the surface.
 */
public class Rough extends Tile {

    public Rough(Tile tile) {
        super(tile);
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        textured(image, x, y, w, h);
    }
}
