/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.MoonFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * Selenian worlds are a subtype of Dwarf Terrestrial Lithic worlds. They are similar to our Moon.
 * Airless, rocky and barren they may harbour water ice in the shadows of craters.
 */
public class Selenian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Selenian.class);

    public Selenian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        // No features to add at the moment.
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Selenian);
        int radius = 1000 + Die.die(400);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(Pressure.None);
        planet.setMagneticField(MagneticField.None);
        planet.addTradeCode(TradeCode.Va);

        addFeatures(planet);

        if (planet.hasFeature(MoonFeature.SmallMoon)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked)) {
            planet.setDayLength(planet.getPeriod());
            planet.addTradeCode(TradeCode.Lk);
        } else if (planet.hasFeature(MoonFeature.AlmostLocked)) {
            planet.setDayLength(planet.getPeriod() * (94 + Die.dieV(6)) / 100);
        }

        // Define resources for this world.
        addSecondaryResource(planet, SilicateOre);
        addTertiaryResource(planet, SilicateCrystals);

        return planet;
    }
}
