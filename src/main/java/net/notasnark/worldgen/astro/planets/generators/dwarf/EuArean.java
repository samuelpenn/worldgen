/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A GeoCyclic MesoArean world of the Dwarf Terrestrial Group. GeoCylic worlds go through
 * a cycle of active/passive periods. An EuArean world has become cold and dry.
 */
public class EuArean extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(EuArean.class);

    public EuArean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
        switch (Die.d6(2)) {
            case 5:
                planet.addFeature(DwarfFeature.SouthCrater);
                break;
            case 6:
                planet.addFeature(DwarfFeature.EquatorialRidge);
                break;
            case 7:
                planet.addFeature(DwarfFeature.GreatRift);
                break;
            case 8:
                planet.addFeature(DwarfFeature.NorthCrater);
                break;
            case 9:
                planet.addFeature(DwarfFeature.BrokenRifts);
                break;
            case 10:
                planet.addFeature(
                        new Roller<Arean.AreanFeature>(
                                Arean.AreanFeature.YellowDesert,
                                Arean.AreanFeature.PinkDesert,
                                Arean.AreanFeature.PurpleDesert,
                                Arean.AreanFeature.BlackRocks,
                                Arean.AreanFeature.GreenRocks).roll());
                break;
            default:
                // No special features.
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EuArean);
        int radius = 2800 + Die.die(500, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(determinePressure(-2));
        planet.setMagneticField(MagneticField.None);

        addFeatures(planet);

        // Define resources for this world.
        addPrimaryResource(planet, CommodityName.SilicateOre);
        addSecondaryResource(planet, CommodityName.SilicateCrystals);
        addTertiaryResource(planet, CommodityName.FerricOre);
        addTraceResource(planet, CommodityName.Water);

        return planet;
    }
}
