/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Dwarf.DwarfFeature.MetallicSea;
import static net.notasnark.worldgen.astro.planets.generators.Dwarf.DwarfFeature.MoltenMetals;

/**
 * ProtoLithic worlds are terrestrial worlds still in the process of forming. They have molten surfaces
 * and atmospheres mostly consisting of hydrogen and helium.
 */
public class ProtoLithic extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(ProtoLithic.class);

    public ProtoLithic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
        if (planet.getTemperature() > Temperature.SilicatesMelt.getKelvin()) {
            planet.addFeature(TerrestrialFeature.MoltenSurface);
        } else if (planet.getTemperature() > Temperature.IronMelts.getKelvin()) {
            planet.addFeature(TerrestrialFeature.MoltenMetals);
        } else if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            planet.addFeature(TerrestrialFeature.MetallicSea);
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.ProtoLithic);
        int radius = 5000 + Die.die(1200, 2);

        if (planet.hasFeature(TerrestrialFeature.Small)) {
            radius *= 0.67;
        } else if (planet.hasFeature(TerrestrialFeature.Large)) {
            radius *= 1.5;
        }

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.addTradeCode(TradeCode.Ba);
        planet.setPressure(50_000 + Die.die(100_000) + Die.die(100_000));
        planet.setHabitability(5);

        if (planet.hasFeature(GeneralFeature.VeryYoung)) {
            planet.setDayLength(20_000 + Die.die(10_000, 2));
        } else if (planet.hasFeature(GeneralFeature.Young)) {
            planet.setDayLength(30_000 + Die.die(20_000, 2));
        } else {
            planet.setDayLength(40_000 + Die.die(25_000, 2));
        }
        modifyTemperatureByRotation(planet);

        // This is a hot world, regardless of where it is relative to its star.
        if (planet.hasFeature(GeneralFeature.VeryYoung)) {
            int kelvin = Temperature.SilicatesMelt.getKelvin() + Die.die(500, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));
            planet.setHabitability(6);
        } else if (planet.hasFeature(GeneralFeature.Young)) {
            int kelvin = Temperature.IronMelts.getKelvin() + Die.die(300, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));
            planet.setHabitability(6);
        } else {
            int kelvin = Temperature.LeadMelts.getKelvin() + Die.die(150, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));
        }

        // Shift night temperature to be more like the day, just in case it isn't. Enough convection
        // processes in the mantle to shift heat around.
        planet.setNightTemperature((planet.getTemperature() + planet.getNightTemperature()) / 2);

        addFeatures(planet);

        // Molten metallic core provides some degree of magnetic field.
        switch (Die.d6(3)) {
            case 3: case 4: case 5:
                planet.setMagneticField(MagneticField.Weak);
                break;
            case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 9: case 10: case 11:
                planet.setMagneticField(MagneticField.Strong);
            default:
                planet.setMagneticField(MagneticField.VeryWeak);
        }

        // Define resources for this world.
        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, FerricOre);
        addTertiaryResource(planet, Radioactives);
        if (planet.hasFeature(MetallicSea) || planet.hasFeature(MoltenMetals)) {
            addPrimaryResource(planet, HeavyMetals);
            addSecondaryResource(planet, Radioactives);
            addTertiaryResource(planet, PreciousMetals);
        } else {
            addSecondaryResource(planet, HeavyMetals);
        }

        return planet;
    }
}
