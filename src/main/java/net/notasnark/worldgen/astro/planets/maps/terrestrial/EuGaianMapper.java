/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.Commodity;
import net.notasnark.worldgen.astro.commodities.Frequency;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.generators.dwarf.EuArean;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.EuGaian;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.maps.dwarf.EuAreanMapper;
import net.notasnark.worldgen.web.Server;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.tiles.Rough;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static net.notasnark.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.*;

public class EuGaianMapper extends TerrestrialMapper {
    public EuGaianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public EuGaianMapper(final Planet planet, final int faces) {
        super(planet, faces);
    }

    protected static final Tile MESOGAIAN = new Tile("Land", "#806030", false, 3);
    protected static final Tile SANDY = new Tile("Land", "#A08050", false, 3);
    protected static final Tile VOLCANO = new Tile("Volcano", "#F05050", false, 3);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        if (planet.hasFeature(CyanSeas)) {
            WATER.rgb("#00F0E0");
        }
        if (planet.hasFeature(VolcanicFlats, Volcanoes)) {
            LAND_ICE.rgb("#E0E0E0");
            SEA_ICE.rgb("#E0E0F0");
            SANDY.rgb("#101010");
        }

        if (planet.hasFeature(FractalIslands)) {
            setWater(WATER);
            int height = getSeaLevel(planet.getHydrographics());
            for (int y=0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (getHeight(x, y) >= height) {
                        setTile(x, y, LAND);
                    }
                }
            }
        } else {
            setWater();

            if (planet.hasFeature(ManyIslands)) {
                createContinents(12 + Die.d6(3));
            } else if (planet.hasFeature(Pangaea)) {
                createContinents(1);
            } else if (planet.hasFeature(EquatorialOcean)) {
                for (int y=0; y < getNumRows(); y++) {
                    for (int x = 0; x < getWidthAtY(y); x++) {
                        if (Math.abs(y - getNumRows()/2) < 2) {
                            setTile(x, y, WATER);
                            setHeight(x, y, 0);
                        } else {
                            setTile(x, y, LAND);
                        }
                    }
                }
                floodToPercentage(WATER, planet.getHydrographics(), true);
            } else {
                createContinents(3 + Die.d6());
            }
        }
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y)/10 + 80)));
                    setHeight(x, y, 0);
                } else {
                    if (getHeight(x, y) > 75 && planet.hasFeature(Volcanoes) && Die.d10() == 1) {
                        setTile(x, y, new Rough(VOLCANO));
                    } else if (getHeight(x, y) > 50) {
                        setTile(x, y, new Rough(MESOGAIAN.getShaded((getHeight(x, y) + 100) / 2)));
                    } else {
                        setTile(x, y, new Rough(SANDY.getShaded((getHeight(x, y) + 100) / 2)));
                    }
                }
            }
        }
        if (planet.hasFeature(GeneralFeature.VeryHighLife)) {
            planet.setLife(Life.Extensive);
        } else if (planet.hasFeature(GeneralFeature.HighLife)) {
            planet.setLife(Life.ComplexLand);
        }

        generateFeatures(planet.getFeatures());
        setIceCaps(getIce(), getIce());
        generateWetness();
        generateBiomes();
        cleanBumpMap();

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        String cloudColour = "#FEFEFE";
        int lowerLimit = 45;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, h);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    private static void generate(WorldGen worldgen, String name) throws IOException {
        Planet planet = new Planet();
        planet.setName(name);
        planet.setType(PlanetType.EuGaian);
        planet.setTemperature(280 + Die.d6(2));
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setPressure(110_000);
        planet.setHydrographics(40 + Die.d10(3));
        planet.setLife(Life.Extensive);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Woods.getName());
            setFrequency(Frequency.COMMON);
        }}, 800);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Grasses.getName());
            setFrequency(Frequency.COMMON);
        }}, 500);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Shrubs.getName());
            setFrequency(Frequency.COMMON);
        }}, 300);

        String path = "wonders/" + name;
        PlanetMapper.testOutput(planet,
                new EuGaian(worldgen, null, null, null, 0),
                new EuGaianMapper(planet, 48),
                path);

        new File(path+"/"+name+"-Clouds.png").delete();
        new File(path+"/"+name+"-Heat.png").delete();
        new File(path+"/"+name+"-Wet.png").delete();
    }

    public static void main(String[] args) throws IOException {

        WorldGen worldgen = Server.getWorldGen();
        for (int i=0; i < 100; i++) {
            String name = String.format("Garden_%02d", i);

            generate(worldgen, name);
        }
    }
}
