/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.belt;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Belt;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A Planetesimal Disc is a Circumstellar class object of the Belt group. It represents a wide ring
 * of small planetesimals not more than a few metres in radius.
 */
public class PlanetesimalDisc extends Belt {
    public PlanetesimalDisc(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.PlanetesimalDisc);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet = definePlanet(name, PlanetType.PlanetesimalDisc);
        long radius = (int) (distance / 3);

        radius = checkDistance(planet, radius);
        planet.setRadius(radius);
        planet.setDensity(4000 + Die.d100(2) * 10);

        planet.setTemperature((int) (planet.getTemperature() * 1.3));
        planet.setNightTemperature(planet.getTemperature());

        addSecondaryResource(planet, SilicateOre);
        addSecondaryResource(planet, CarbonicOre);
        if (planet.getTemperature() < 400) {
            addTertiaryResource(planet, Water);
        }

        return planet;
    }
}
