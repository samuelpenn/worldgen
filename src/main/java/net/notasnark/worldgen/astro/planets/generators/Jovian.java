/**
 * Jovian.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.*;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.CircumstellarZone;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.utils.rpg.Die;

import java.util.ArrayList;
import java.util.List;

/**
 * Planet Generator for the Jovian group of planet types.
 */
public class Jovian extends PlanetGenerator {

    // Some notes taken from here: https://en.wikipedia.org/wiki/Sudarsky%27s_gas_giant_classification
    public enum JovianFeature implements PlanetFeature {
        DarkClouds,
        MainSatellite,
        MethaneClouds, // Whitish - Blue
        AmmoniaClouds, // Pale Brown
        WaterClouds,   // White
        Cloudless,     // Blue
        AlkaliMetals,  // Dark
        SilicateClouds // Greenish
    }

    public Jovian(WorldGen worldgen, StarSystem system, Star primary, Planet previous, long distance) {
        super(worldgen, system, primary, previous, distance);
    }

    /**
     * Get a generated planet. Can't be called directly on the Belt class, because
     * we don't know exactly what type of planet to create.
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }


    @Override
    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(Die.d6(3) * 5000);

        // Set default day length to be around 10 hours.
        planet.setDayLength(9 * 86400 + Die.die(3600, 2));

        addPrimaryResource(planet, CommodityName.Hydrogen);
        addTertiaryResource(planet, CommodityName.Helium);

        return planet;
    }

    /**
     * Randomly determine how many moons this planet has.
     *
     * @return  Number of moons, up to a dozen or so.
     */
    private int getNumberOfMoons() {
        int numberOfMoons;

        switch (Die.d6(3)) {
            case 2:
                numberOfMoons = Die.d12(2);
                break;
            case 3:
                numberOfMoons = Die.d10(2);
                break;
            case 4: case 5:
                numberOfMoons = Die.d8(2);
                break;
            case 6: case 7: case 8:
                numberOfMoons = Die.d6(2);
                break;
            case 9: case 10: case 11: case 12:
                numberOfMoons = Die.d4(2);
                break;
            case 13: case 14: case 15: case 16: case 17:
                numberOfMoons = Die.d4() + 1;
                break;
            default:
                numberOfMoons = Die.d3() - 1;
                break;
        }
        return numberOfMoons;
    }

    private PlanetType getTidalMoon(int kelvin) {
        CircumstellarZone zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Cold: case Stygia: case Tartarus:
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        return PlanetType.Europan;
                    case 4: case 5: case 6:
                        return PlanetType.Enceladusian;
                }
                break;
        }
        return null;
    }

    private PlanetType getNonTidalMoon(int kelvin) {
        CircumstellarZone zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Epistellar:
                return PlanetType.Ferrinian;
            case Hot:
                return PlanetType.Hermian;
            case Inner: case Middle: case Outer:
                return PlanetType.Selenian;
            case Cold: case Stygia: case Tartarus:
                return PlanetType.Gelidian;
        }
        return null;
    }

    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<Planet>();

        int numberOfMoons = getNumberOfMoons();
        if (primary.hasFeature(GeneralFeature.NoMoons)) {
            numberOfMoons = 0;
        } else if (primary.hasFeature(GeneralFeature.FewMoons)) {
            numberOfMoons /= 2;
        }
        if (primary.hasFeature(JovianFeature.MainSatellite)) {
            numberOfMoons = Math.max(1, numberOfMoons);
        }
        if (numberOfMoons == 0) {
            // Nothing to do.
            return moons;
        }

        logger.info(String.format("Creating %d moons for [%s]", numberOfMoons, primary.getName()));

        int  kelvin = Physics.getTemperatureOfOrbit(star, primary.getDistance());
        long distance = primary.getRadius() * (Die.d2(2));

        int  moonOrbit = 1;
        int  ringOrbit = 1;
        int  moonCount = 0;
        for (int m=0; m < numberOfMoons; m++) {
            String name = StarSystemFactory.getMoonName(primary.getName(), m + 1);

            logger.info("Adding moon " + name);

            List<PlanetFeature> features = new ArrayList<PlanetFeature>();
            features.add(MoonFeature.Moon);

            PlanetType type = PlanetFactory.determineSmallBodyType(kelvin);
            if (Die.die(m + 2 + moonCount) == 1 && Physics.getTemperatureOfOrbit(star, primary.getDistance()) < 250) {
                type = PlanetType.IceRing;
                name = StarSystemFactory.getRingName(primary.getName(), ringOrbit++);
                if (m == 0) {
                    distance *= 0.6;
                }
            } else {
                moonCount++;
                if (Die.die(m) < 4) {
                    if (distance < primary.getRadius() * 20) {
                        type = getTidalMoon(kelvin);
                    } else {
                        type = getNonTidalMoon(kelvin);
                    }
                }
                name = StarSystemFactory.getMoonName(primary.getName(), moonOrbit++);
            }

            if (type == null) {
                logger.error("Trying to create a moon of type null, bailing out.");
                break;
            }

            Planet moon = factory.createMoon(system, star, name, type,
                    distance, primary,
                    features.toArray(new PlanetFeature[0]));

            moons.add(moon);
            distance += Die.die((int) primary.getRadius(), 3);
        }

        return moons;
    }
}
