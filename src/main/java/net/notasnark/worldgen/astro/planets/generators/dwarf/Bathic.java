/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.MoonFeature;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;

import static net.notasnark.worldgen.astro.commodities.CommodityName.Water;

/**
 * Bathic type worlds are covered with a layer of liquid water, with an atmosphere mostly of water vapour.
 * They are only found in the circumstellar habitable zone, since any closer and they'll lose their water
 * too quickly to space, and further out they freeze.
 */
public class Bathic extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Bathic.class);

    public enum BathicFeature implements PlanetFeature {
        PolarCaps,
        Cold,
        MostlyFrozen,
        Frozen
    }

    public Bathic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        // No features to add at the moment.
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Bathic);
        int radius = 1400 + Die.die(400, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Nitrogen);
        planet.setPressure(new Roller<Pressure>().add(Pressure.Thin, Pressure.Thin, Pressure.VeryThin).roll());

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setAtmosphere(Atmosphere.CarbonDioxide);
                planet.addPressure(planet.getPressure()  / 4);
                planet.addTemperature(20);
                break;
            case 4: case 5:
                planet.setAtmosphere(Atmosphere.Nitrogen);
                break;
            case 6:
                planet.setAtmosphere(Atmosphere.InertGases);
                planet.addPressure(planet.getPressure() / 2);
                planet.addTemperature(-10);
                break;
        }
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));
        // If the temperature is warm enough, the atmosphere is mostly water vapour.
        if (planet.getTemperature() > 340) {
            planet.setAtmosphere(Atmosphere.WaterVapour);
            planet.addPressure((planet.getTemperature() - 340) * 2_000);
        }

        planet.setMagneticField(MagneticField.None);
        planet.setHydrographics(100);

        addFeatures(planet);

        if (planet.hasFeature(MoonFeature.SmallMoon, DwarfFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon, DwarfFeature.Large)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked, GeneralFeature.TideLocked)) {
            planet.setDayLength(planet.getPeriod());
        }

        int lifeBonus = 0;
        if (planet.getTemperature() < 240) {
            planet.addFeature(BathicFeature.Frozen);
            planet.setPressure(planet.getPressure() / 5);
        } else if (planet.getTemperature() < 255) {
            planet.addFeature(BathicFeature.MostlyFrozen);
            lifeBonus = 1;
        } else if (planet.getTemperature() < 270) {
            planet.addFeature(BathicFeature.Cold);
            lifeBonus = 2;
        } else if (planet.getTemperature() < 285) {
            planet.addFeature(BathicFeature.PolarCaps);
            lifeBonus = 3;
        } else {
            planet.setPressure((int)(planet.getPressure() * 1.2));
            lifeBonus = 2;
        }
        if (planet.getAtmosphere() == Atmosphere.InertGases) {
            lifeBonus --;
        }

        if (planet.getAtmosphere() != Atmosphere.WaterVapour) {
            switch (Die.d6() + lifeBonus) {
                case 0: case 1: case 2: case 3:
                    break;
                case 4: case 5:
                    planet.setLife(Life.Archaean);
                    break;
                case 6: case 7:
                    planet.setLife(Life.Aerobic);
                    planet.setAtmosphere(Atmosphere.LowOxygen);
                    break;
                default:
                    planet.setLife(Life.ComplexOcean);
                    planet.setAtmosphere(Atmosphere.Standard);
                    break;
            }
        }

        // Define resources for this world.
        addPrimaryResource(planet, Water);

        return planet;
    }
}
