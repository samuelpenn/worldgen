/*
 * Copyright (C) 2018 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.codes;

public enum TradeCode {
    NV, // Navy Base
    SC, // Scout Base
    MI, // Military Base
    RE, // Research base
    Ba, // Barren: Has no permanent population
    Va, // Vacuum: Has no atmosphere
    An, // Ancient Site
    Tw, // Twilight Zone
    Tu, // Tunda
    Co, // Cold
    Pi, // Preindustrial
    Hi,
    Lo,
    Ri,
    Po,
    De,
    Ic,
    Wa,
    As,
    Ag,
    Na,
    In,
    Ni,
    Fl,
    Brd, Bgr, Bbl, Bbk, // Red, Green, Blue or Dark shade to things.
    Bsp, // Belt, spokes.
    Bva, // Belt, variable density.
    Sg, // Spin gravity
    Lk  // Tide Locked
}
