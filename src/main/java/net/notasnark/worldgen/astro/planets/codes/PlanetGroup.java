/**
 * PlanetGroup.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.codes;

/**
 * A PlanetGroup is the highest level of classification in the PCL. A group contains classes, and within
 * classes are types. Groups are very broad categories for lumping planetary objects.
 */
public enum PlanetGroup {
    // Belt.
    Belt("Belt", 0),
    // Asteroids, Comets and Vulcanoids.
    SmallBody("Small Body", 2),
    // Dwarf Terrestrial Group.
    Dwarf("Dwarf Terrestrial", 3),
    // Terrestrial Worlds.
    Terrestrial("Terrestrial", 4),
    // Massive Terrestrial Worlds.
    Helian("Helian", 5),
    // Gas Giants.
    Jovian("Jovian", 7),
    // Rogue Planets.
    Planemo("Planemo", 4),
    // Artificial Worlds.
    Construct("Construct", 0);

    private final String title;
    private final int size;

    private PlanetGroup(final String title, final int size) {
        this.title = title;
        this.size = size;
    }

    /**
     * Gets the human readable title for this group. May contain spaces
     * and is properly capitalised.
     *
     * @return  Title of the group.
     */
    public String getTitle() {
        return title;
    }

    public int getSize() {
        return size;
    }
}
