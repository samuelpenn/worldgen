/**
 * PlanetFeature.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

/**
 * A planet feature is some aspect of a planet that appears on the map, and which can be described
 * by the text generator. It should be extended by an enum by each type of planet.
 */
public interface PlanetFeature {

    /**
     * Gets the notability (chance of being displayed) for primary features. These are features
     * which completely replace the planet's text. Only the first matching (and selected) notable
     * feature will be used.
     *
     * @return  A percentage value between 1 and 100.
     */
    default int getPrimaryNotability() {
        return 100;
    }

    /**
     * Gets the notability (chance of being displayed) for secondary features. These are included
     * after the main body of text.
     *
     * @return  A percentage value between 1 and 100.
     */
    default int getNotability() {
        return 50;
    }
}
