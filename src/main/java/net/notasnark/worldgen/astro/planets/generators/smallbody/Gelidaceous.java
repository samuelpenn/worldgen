/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.smallbody;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Temperature;
import net.notasnark.worldgen.astro.planets.generators.SmallBody;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A Gelidaceous asteroid is an ice-rich body with volatile content greater than 50%. They tend to be
 * stable, and volatile loss is minimal.
 *
 * There may be some unusual examples of such asteroids that have recently migrated to the inner system.
 * Most don't survive long, but a few have picked up characteristics that allow them to survive. They
 * tend to be larger than normal. The smaller ones evaporate too quickly.
 */
public class Gelidaceous extends SmallBody {

    public enum GelidaceousFeature implements PlanetFeature {
        RockSurface,
        SootSurface,
        SnowCovered
    }

    public Gelidaceous(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.Gelidaceous);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(getRadius(planet));
        planet.setTemperature(planet.getTemperature() + Die.dieV(4));

        // Set default day length to be 2-3 hours.
        planet.setDayLength(3600 + Die.die(3600, 2));

        if (planet.getTemperature() > Temperature.WaterBoils.getKelvin()) {
            // Unusual to find such an object in the inner system. In this case, the outer surface
            // is rocky, and the inner core is liquid water, if not super-heated.
            planet.addFeature(GelidaceousFeature.RockSurface);
            planet.setPressure(Die.d100(2));
            planet.setAtmosphere(Atmosphere.WaterVapour);
            if (planet.getRadius() < 200) {
                planet.setRadius(planet.getRadius() + 100);
            }
        } else if (planet.getTemperature() > Temperature.WaterFreezes.getKelvin()) {
            // A bit too warm for such rocks, but a sooty layer on top of the ice protects it from
            // the heat and evaporation. May have a partially liquid core.
            planet.addFeature(GelidaceousFeature.SootSurface);
            planet.setPressure(Die.d20(2));
            planet.setAtmosphere(Atmosphere.WaterVapour);
            if (planet.getRadius() < 100) {
                planet.setRadius(planet.getRadius() + 50);
            }
        } else {
            if (Die.d3() == 1) {
                planet.addFeature(GelidaceousFeature.SnowCovered);
            }
        }

        addPrimaryResource(planet, Water);
        addSecondaryResource(planet, SilicateOre);
        addSecondaryResource(planet, CarbonicOre);

        switch (Die.d8()) {
            case 1: case 2:
                addTraceResource(planet, ExoticCrystals);
                break;
            case 3: case 4:
                addTraceResource(planet, SilicateCrystals);
                break;
            case 5: case 6:
                addTraceResource(planet, CarbonicCrystals);
                break;
            default:
                // Nothing special.
        }

        return planet;
    }
}
