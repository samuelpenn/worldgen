package net.notasnark.worldgen.astro.planets.maps;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetGenerator;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.astro.planets.tiles.Climate;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.SnowCover;
import net.notasnark.worldgen.astro.planets.tiles.Wetness;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.text.TextGenerator;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PlanetMapper extends Icosahedron {
    protected final Planet planet;
    protected static final int    DEFAULT_FACE_SIZE = 24;

    protected boolean isFaded = false;

    protected boolean hasMainMap = true;
    protected boolean hasHeightMap = false;
    protected boolean hasDeformMap = false;
    protected boolean hasCloudMap = false;
    protected boolean hasOrbitMap = false;
    protected boolean hasRingMap = false;

    protected int     cloudHeight = 5;

    private long      currentTime = 0;

    private final Tile WATER    = new Tile("Water", "#7777FF").water(true).random(2);
    private final Tile ICE      = new Tile("Ice", "#FEFEFE").random(2);
    private final Tile MOUNTAIN = new Tile("Mountain", "#A07020").mountain(true).random(2);
    private final Tile PLAINS   = new Tile("Plains", "#F0A0000").random(2);

    protected Tile getPlains() {
        return PLAINS;
    }
    protected Tile getMountains() {
        return MOUNTAIN;
    }
    protected Tile getWater() {
        return WATER;
    }
    protected Tile getIce() {
        return ICE;
    }

    public PlanetMapper(final Planet planet, final int faceSize) {
        super(faceSize);
        this.planet = planet;
    }

    public PlanetMapper(final Planet planet) {
        super(DEFAULT_FACE_SIZE);
        this.planet = planet;
    }

    public boolean hasMainMap() { return hasMainMap; }

    public boolean hasHeightMap() {
        return hasHeightMap;
    }

    public boolean hasCloudMap() {
        return hasCloudMap;
    }

    public boolean hasOrbitMap() { return hasOrbitMap; }

    public boolean hasDeformMap() { return hasDeformMap; }

    public boolean hasRingMap() { return hasRingMap; }

    public void setTime(final long currentTime) {
        this.currentTime = currentTime;
    }

    public long getTime() {
        return currentTime;
    }

    public void setFaded(boolean faded) {
        this.isFaded = faded;
    }

    /**
     * Gets the height of the first cloud layer. Subsequent layers increase with the
     * same increment. We name the layers according to the height, so on display we
     * can calculate how far above the surface to draw each layer.
     *
     * @return  Integer, strictly positive.
     */
    public int getBaseCloudHeight() { return cloudHeight; }

    /**
     * Gets the latitude of the current tileY in degrees, between 0 and 90.
     * This does not differentiate between Northern and Southern hemispheres, it is mostly
     * for use determining how close to the poles the position is.
     *
     * @param tileY     Y coordinate of the tile.
     * @return          Latitude in degrees, between 0 and 90.
     */
    protected int getLatitude(int tileY) {
        int lat;
        if (tileY < getNumRows() / 2) {
            lat = (int) (90 * (1 - (2.0 * tileY) / getNumRows()));
        } else {
            lat = (int) (90 * (1 - (2.0 * (getNumRows() - tileY)) / getNumRows()));
        }
        return lat;
    }

    /**
     * Gets the longitude of the specified tile. 0° is considered the middle of the map, negative
     * is West and positive is East.
     *
     * @param tileX     X coordinate of tile.
     * @param tileY     Y coordinate of tile.
     * @return          Longitude, between -180° and +180°.
     */
    protected int getLongitude(int tileX, int tileY) {
        int x = tileX;

        if (tileY > (getNumRows() * 2) / 3) {
            //x -= getNumRows() / 3;
            x -= getWidthAtY(tileY) / 10;
        } else if (tileY > getNumRows() / 3) {
            x -= (tileY - getNumRows() / 3);
        }
        if (x < 0) {
            x += getWidthAtY(tileY);
        }

        int l = (360 * x) / getWidthAtY(tileY);
        return l - 180;
    }

    /**
     * Gets the average temperature of a given tile. Based on planet temperature, latitude
     * and height.
     *
     * @param x     X coordinate of the tile.
     * @param y     Y coordinate of the tile.
     * @return      Temperature in Kelvin.
     */
    protected int getTemperature(int x, int y) {
        return planet.getTemperature() + 35 - (getLatitude(y) / 2) - getHeight(x, y) / 5;
    }

    protected void generateHeightMap(int variation, int finalSize) {
        Icosahedron parent = new Icosahedron(3);
        parent.fractal();

        generateHeightMap(parent, variation, finalSize);
        hasHeightMap = true;
    }

    protected void generateHeightMap(Icosahedron parent, int variation, int finalSize) {
        int size = parent.getFaceSize();

        while (size < finalSize) {
            size *= 2;
            Icosahedron  map = new Icosahedron(size);
            if (size < finalSize / 4) {
                map.copy(parent);
            } else {
                map.fractal(parent, variation);
            }
            variation /= 2;
            parent = map;
        }

        copyHeightMap(parent);
    }

    public SimpleImage drawHeightMap(int width) throws IOException {
        return drawHeight(width);
    }

    public List<SimpleImage> drawClouds(int width) throws IOException {
        return new ArrayList<SimpleImage>();
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {

    }

    public SimpleImage drawRing(int width) throws IOException {
        return null;
    }

    private int craterHeight = 10;
    private double craterMultiplier = 0;

    protected void setCraterHeight(int height) {
        this.craterHeight = height;
        this.craterMultiplier = 0.0;
    }

    protected void setCraterModifier(double multiplier) {
        this.craterMultiplier = multiplier;
    }

    /**
     * Set an individual tile to be a crater. Darkens the tile colour, and sets the height
     * to be lower than normal land (but higher than water).
     *
     * @param tileX     X coordinate of tile to be set.
     * @param tileY     Y coordinate of tile to be set.
     */
    private void setCraterTile(int tileX, int tileY) {
        if (!getTile(tileX, tileY).isWater()) {
            if (craterMultiplier > 0) {
                setHeight(tileX, tileY, (int) (getHeight(tileX, tileY) * craterMultiplier));
                setTile(tileX, tileY, new Rough(getTile(tileX, tileY).getShaded(85)));
            } else {
                if (getHeight(tileX, tileY) != craterHeight) {
                    setHeight(tileX, tileY, craterHeight);
                    setTile(tileX, tileY, new Rough(getTile(tileX, tileY).getShaded(85)));
                }
            }
        }
    }

    /**
     * Creates a small crater, a single tile in size.
     *
     * @param tileX     X coordinate of centre tile of crater.
     * @param tileY     Y coordinate of centre tile of crater.
     */
    private void createSmallCrater(int tileX, int tileY) {
        setCraterTile(tileX, tileY);
    }


    private void setCraterLine(int tileX, int tileY, int width) {
        for (int x = tileX - width; x <= tileX + width; x++) {
            setCraterTile(x, tileY);
        }
    }

    /**
     * Creates a medium crater, a hexagon covering six tiles. The
     * centre of the crater will be either the top or bottom centre
     * tile of the hexagon.
     *
     * @param tileX     X coordinate of centre tile of crater.
     * @param tileY     Y coordinate of centre tile of crater.
     */
    private void createMediumCrater(int tileX, int tileY) {
        try {
            setCraterLine(tileX, tileY, 1);
            Point p = getOpposite(tileX, tileY);
            tileX = (int) p.getX();
            tileY = (int) p.getY();
            setCraterLine(tileX, tileY, 1);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index out of bounds for " + tileX + "," + tileY);
        }
    }

    protected void createLargeCrater(int tileX, int tileY) {
        try {
            setCraterLine(tileX, tileY, 3);
            Point p = getOpposite(tileX, tileY);
            int x = (int) p.getX();
            int y = (int) p.getY();
            setCraterLine(x, y, 3);
            p = getUpDown(x, y);
            x = (int) p.getX();
            y = (int) p.getY();
            setCraterLine(x, y, 2);
            p = getUpDown(tileX, tileY);
            x = (int) p.getX();
            y = (int) p.getY();
            setCraterLine(x, y, 2);

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array index out of bounds for " + tileX + "," + tileY);
        }
    }

    /**
     * Create random craters across the world's surface. The size should be between -2 and +2
     * for best results. For medium sized craters, a few hundred is a good number.
     *
     * @param size      Size of the craters.
     * @param number    Number of craters.
     */
    protected void createCraters(int size, int number) {
        size = Math.max(-2, Math.min(2, size));
        for (int c = 0; c < number; c++) {
            int tileY = getNumRows() / 5 + Die.rollZero((getNumRows() * 3) / 5);
            int tileX = 2 + Die.rollZero(getWidthAtY(tileY) - 4);

            // If planet is tidally locked, re-roll any craters on the near side of the planet.
            if (planet.hasFeature(GeneralFeature.TideLocked)) {
                if (Math.abs(getLongitude(tileX, tileY)) < 90) {
                    tileX = 2 + Die.rollZero(getWidthAtY(tileY) - 4);
                }
            }

            switch (Die.d6() + size) {
                case -5: case -4: case -3:
                    // No crater.
                    break;
                case -2: case -1: case 0: case 1:
                    createSmallCrater(tileX, tileY);
                    break;
                case 2: case 3: case 4: case 5:
                    createMediumCrater(tileX, tileY);
                    break;
                default:
                    createLargeCrater(tileX, tileY);
            }
        }
    }

    /**
     * Add a rift to the world map. This will be of the specified length and tile type.
     * Rifts will tend to run East/West, and tend towards equatorial regions, though
     * they can drift towards the poles.
     *
     * @param tile      Type of tile to set rift to.
     * @param length    Length of the rift.
     */
    protected void addRift(Tile tile, int length) {
        int y = getNumRows() / 2 + Die.dieV(getNumRows() / 4);
        int x = Die.rollZero(getWidthAtY(y));

        for (int l = 0; l < length; l++) {
            if (Die.d3() == 1) {
                x = getWest(x, y);
                Point p = getUpDown(x, y);
                x = (int) p.getX();
                y = (int) p.getY();
            }
            setTile(x, y, tile);
            setHeight(x, y, 10);
            x = getEast(x, y);
        }
    }

    /**
     * Add ice caps to the world, based on average surface temperature. Also adds
     * snow and ice cover to high mountain ranges.
     *
     * @param landIce   Type of tile to set for land ice.
     * @param seaIce    Type of tile to set for sea ice.
     */
    protected void setIceCaps(Tile landIce, Tile seaIce) {
        int k = planet.getTemperature();
        int iceLine = 0;

        if (k > 250) {
            iceLine += (k - 250) * 2;
        }
        int w = planet.getHydrographics();
        if (w < 5) {
            iceLine = Math.max(iceLine, 85);
        } else if (w < 10) {
            iceLine = Math.max(iceLine, 80);
        } else if (w < 30) {
            iceLine = Math.max(iceLine, 70);
        } else if (w < 50) {
            iceLine = Math.max(iceLine, 60);
        }

        for (int y=0; y < getNumRows(); y++) {
            int lat = getLatitude(y);

            for (int x=0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                if (getTile(x, y).isWater()) {
                    if (lat + h / 5 > iceLine) {
                        setTile(x, y, seaIce);
                    }
                } else {
                    int effectiveLat = lat + h/5;
                    if (effectiveLat > iceLine) {
                        setTile(x, y, landIce);
                    } else if (effectiveLat > iceLine - 30) {
                        int cover = (int) Math.pow(effectiveLat - (iceLine - 30), 2) / 10;
                        setTile(x, y, new SnowCover(getTile(x, y)).cover(cover));
                    }
                }

            }
        }
    }


    /**
     * Draws fractal clouds which can be applied to a cloud layer.
     *
     * @return  New high resolution icosahedron map.
     */
    protected Icosahedron getCloudLayer() {
        Icosahedron cloud = new Icosahedron(12);
        cloud.fractal();
        int size = cloud.getFaceSize();

        int variation = 48;
        while (size < 48) {
            size *= 2;
            Icosahedron  map = new Icosahedron(size);
            map.fractal(cloud, variation);
            variation /= 2;
            cloud = map;
        }

        return cloud;
    }

    /**
     * Get the angular position of an object in orbit around its primary. This is based on the
     * current simulation time (or map time if that has been explicitly set). Assumes that planets
     * orbit counter-clockwise.
     *
     * @param primary       Primary star.
     * @param distance      Distance in Mkm.
     * @return              Angle, between 0 and 360 degrees.
     */
    protected double getAngleOffset(Star primary, double distance) {
        double angle = (distance + ((primary != null)?primary.getId():0)) % 360;

        long period = Math.max(1, primary.getPeriod((int)distance));

        angle += (360.0 * (getTime() % period)) / period;
        angle %= 360;

        // Switch to counter clockwise.
        angle = (360 - angle) % 360;

        return angle;
    }

    protected double getAngleOffset(Planet planet) {
        double angle = (planet.getDistance() + planet.getId()) % 360;

        long period = Math.max(1, planet.getPeriod());

        angle += (360.0 * (getTime() % period)) / period;
        angle %= 360;

        // Switch to counter clockwise.
        return (360 - angle) % 360;
    }

    /**
     * Draws labels for a planet. For a planet, this is the name, type and distance from the
     * primary star. For planetoids within an asteroid belt, we just show the name. Since the
     * only time we display a Moon is when it's a planetoid, we can use this to determine
     * the planet's status.
     *
     * @param x         X pixels planet was drawn at.
     * @param y         Y pixels planet was drawn at.
     * @param angle     Angle around the star planet was drawn (in degrees).
     * @param planet    Planet to show information on.
     */
    protected void drawPlanetLabel(SimpleImage image, int x, int y, double angle, Planet planet) {
        String text, dtext;

        if (planet.isMoon()) {
            // This is a planetoid inside an asteroid belt. We only display the name of
            // planetoid in this case.
            text = planet.getName().replaceAll(".* ", "");
            dtext = "";
        } else {
            text = planet.getName().replaceAll(".* ", "");
            text += " / " + planet.getType();

            long distance = planet.getDistance();

            if (distance > 1_000_000_000) {
                DecimalFormat format = new DecimalFormat("#,### AU");
                dtext = format.format(distance / 150_000_000);
            } else if (distance >= 10_000_000) {
                DecimalFormat format = new DecimalFormat("#,### Mkm");
                dtext = format.format(distance / 1_000_000);
            } else {
                DecimalFormat format = new DecimalFormat("#,### km");
                dtext = format.format(distance);
            }
        }

        int fontSize = 12;
        int textWidth = image.getTextWidth(text, 0, fontSize);
        int padding = 10;

        if (angle < 90) {
            // Bottom right.
            image.text(x + padding, y + padding, text, 0, fontSize, "#000000");
            image.text(x + padding, y + (int)(padding * 2.5), dtext, 0, fontSize, "#000000");
        } else if (angle < 180) {
            // Bottom left.
            image.text(x - padding - textWidth, y + padding, text, 0, fontSize, "#000000");
            image.text(x - padding - textWidth, y + (int)(padding * 2.5), dtext, 0, fontSize, "#000000");
        } else if (angle < 270) {
            // Top left.
            image.text(x - padding - textWidth, y - padding, text, 0, fontSize, "#000000");
            image.text(x - padding - textWidth, y + (int)(padding * 0.5), dtext, 0, fontSize, "#000000");
        } else {
            // Top right.
            image.text(x + padding, y - padding, text, 0, fontSize, "#000000");
            image.text(x + padding, y + (int)(padding * 0.5), dtext, 0, fontSize, "#000000");
        }

    }

    /**
     * Is this tile water, and if so, does it border on land? Checks East, West and the adjacent tile.
     *
     * @param x     X position of Tile.
     * @param y     Y position of Tile.
     * @return      True iff this tile is water, and has at least one neighbouring non-water tile.
     */
    protected boolean isShallows(int x, int y) {
        if (!getTile(x, y).isWater()) {
            return false;
        }

        if (!getTile(getEast(x, y), y).isWater()) {
            return true;
        }
        if (!getTile(getWest(x, y), y).isWater()) {
            return true;
        }
        Point p = getUpDown(x, y);
        return !getTile(p.x, p.y).isWater();
    }

    /**
     * Is this tile land, and if so, does it border on water? Checks East, West and the adjacent tile.
     *
     * @param x     X tile to check.
     * @param y     Y tile to check.
     * @return      True iff this tile is land, and at least one of neighbours is water.
     */
    protected boolean isShore(int x, int y) {
        if (getTile(x, y).isWater()) {
            return false;
        }

        if (getTile(getEast(x, y), y).isWater()) {
            return true;
        }
        if (getTile(getWest(x, y), y).isWater()) {
            return true;
        }
        Point p = getUpDown(x, y);
        return getTile(p.x, p.y).isWater();
    }

    /**
     * Modifies the spread of moisture according to the amount of water available on the planet. Lower
     * levels of water mean that the spread of moisture is reduced. The number returned ranges from
     * 0.00 for water worlds, and increases for dryer worlds.
     *
     * @return      Modifier to subtract from the spread factor.
     */
    private double getHydrographicsWetnessReduction() {
        if (planet.getHydrographics() < 30) {
            return 0.06;
        } else if (planet.getHydrographics() < 50) {
            return 0.05;
        } else if (planet.getHydrographics() < 65) {
            return 0.04;
        } else if (planet.getHydrographics() < 75) {
            return 0.03;
        } else if (planet.getHydrographics() < 85) {
            return 0.02;
        } else if (planet.getHydrographics() < 95) {
            return 0.01;
        }
        return 0.0;
    }

    /**
     * Modifies the spread of moisture according to the atmospheric pressure. The lower the pressure, the
     * reduced chance of moisture to be spread around the planet. For no atmosphere the modifier is 0.10.
     *
     * @return  Modifier to subtract from the spread factor.
     */
    private double getPressureWetnessReduction() {
        if (planet.getPressure() < Physics.STANDARD_PRESSURE) {
            double modifier = Math.pow((Physics.STANDARD_PRESSURE - planet.getPressure()) / 1_000, 2);
            return modifier / 100_000;
        }
        return 0.0;
    }

    /**
     * Generate map of rainfall patterns across the world. Each tile has a wetness from 0 to 100, with water
     * and shore tiles automatically having a wetness of 100. The further away from water sources, the greater
     * the reduction in wetness.
     *
     * Mountains block wetness. Also, try to apply weather patterns above and below the equator.
     * Above 60°, winds will tend to push moisture west.
     * Between 30° and 60° winds will tend to push moisture east.
     * Between 10° and 30° wills will push moisture west.
     * Beneath 10° there is no particular direction.
     */
    public void generateWetness() {
        boolean moreToDo = true;
        int     tropicSize = Die.d8() - 1;
        while (moreToDo) {
            int[][] wet = new int[getNumRows()][];
            int     numberSet = 0;
            int     tropics = tropicSize + Die.d3();

            for (int y = 0; y < getNumRows(); y++) {
                wet[y] = new int[getWidthAtY(y)];
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (getTile(x, y).getWetness() == 0 && !getTile(x,y).isMountain()) {
                        if (isShore(x, y)) {
                            getTile(x, y).setWetness(100);
                        } else {
                            int e = getTile(getEast(x, y), y).getWetness();
                            int w = getTile(getWest(x, y), y).getWetness();
                            int u = getTile(getUpDown(x, y).x, getUpDown(x, y).y).getWetness();

                            double  factor = 0.99;

                            if (planet.hasFeature(Terrestrial.TerrestrialFeature.Dry)) {
                                factor = 0.9;
                            }

                            factor -= getHydrographicsWetnessReduction();
                            factor -= getPressureWetnessReduction();

                            int latitude = getLatitude(y);
                            if (latitude < 20 || planet.hasFeature(Terrestrial.TerrestrialFeature.Wet)) {
                                // Equator (Intertropical convergence zone)
                                factor += 0.02;
                            } else if (latitude < 30+tropics) {
                                // Tropical Easterlies (wind blowing East to West)
                                factor += 0.01;
                                e *= 0.75;
                                w *= 0.5;
                                u *= 0.75;
                                factor -= 0.3;
                            } else if (latitude < 50-tropics) {
                                // Subtropical high (winds are quiet)
                                e *= 0.9;
                                w *= 0.9;
                                u *= 0.9;
                                factor -= 0.10;
                            } else if (latitude < 50) {
                                // Prevailing Westerlies (wind blowing West to East)
                                factor += 0.01;
                                e *= 0.75;
                            } else if (latitude < 70) {
                                // Subpolar low
                                // No modifiers
                            } else {
                                // Polar Eastelies (wind blowing East to West)
                                w *= 0.75;
                            }

                            int max = Math.max(u, Math.max(e, w));
                            if (max > 0 && latitude > 50 && latitude < 80) {
                                // Try and ensure we have some vegetation cover around the 60°-70° latitudes.
                                // We need a (max > 0) check above, otherwise we set wetness to a small non-zero
                                // value on the first iteration, which prevents any further propagation from lower
                                // latitudes.
                                max += Math.max(0, (15 - Math.abs(65 - latitude)) * 5 - getHeight(x, y) / 10);
                            }
                            wet[y][x] = (int) Math.min(100, (max * factor));
                            if (wet[y][x] > 0) {
                                // Keep track of how many tiles we have updated this iteration. We stop
                                // iterating when we've updated no more tiles.
                                numberSet++;
                            }
                        }
                    }
                }
            }

            for (int y = 0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    getTile(x, y).setWetness(Math.max(wet[y][x], getTile(x, y).getWetness()));
                    wet[y][x] = 0;
                }
            }
            moreToDo = (numberSet > 0);
        }
    }

    /**
     * Clean the height map so it produces better results for the bump mapper.
     * If there is too much variation, then the bump mapper just produces a mess,
     * so modifies the heights so they fall into three categories (low, medium
     * and high).
     */
    protected void cleanBumpMap() {
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                if (h < 1 || getTile(x, y).isWater()) {
                    h = 0;
                } else if (h < 11) {
                    h = 5;
                } else if (h < 21) {
                    h = 15;
                } else if (h < 90) {
                    h = 50;
                } else {
                    h = 100;
                }
                setHeight(x, y, h);
            }
        }
    }

    public void debugWetness(boolean withHeights) {
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (withHeights) {
                    setTile(x, y, new Wetness(getTile(x, y), getHeight(x, y)));
                } else {
                    setTile(x, y, new Wetness(getTile(x, y)));
                }
            }
        }
    }

    public void setTemperatureMap() {
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {

                if (getTile(x,y).isWater()) {
                    setTile(x, y, new Tile("Water", "#000000"));
                    continue;
                }
                int k = getTemperature(x, y);

                if (k < 268) {
                    setTile(x, y, new Tile("Cold", "#000044"));
                } else if (k < 273) {
                    setTile(x, y, new Tile("Cold", "#000088"));
                } else if (k < 278) {
                    setTile(x, y, new Tile( "Cold", "#0000CC"));
                } else if (k < 283) {
                    setTile(x, y, new Tile( "Cool", "#0044FF"));
                } else if (k < 288) {
                    setTile(x, y, new Tile("Cool", "#0088FF"));
                } else if (k < 293) {
                    setTile(x, y, new Tile("Moderate", "#44CC88"));
                } else if (k < 298) {
                    setTile(x, y, new Tile("Moderate", "#88FF00"));
                } else if (k < 303) {
                    setTile(x, y, new Tile("Warm", "#CCFF00"));
                } else if (k < 308) {
                    setTile(x, y, new Tile("Warm", "#FFFF00"));
                } else if (k < 313) {
                    setTile(x, y, new Tile("Hot", "#FF8800"));
                } else if (k < 318) {
                    setTile(x, y, new Tile("Hot", "#FF4400"));
                } else {
                    setTile(x, y, new Tile("Very Hot", "#FF0000"));
                }
            }
        }
    }

    /**
     * Apply biomes to each tile on the map.
     */
    public void generateBiomes() {
        if (planet.getLife().isSimplerThan(Life.SimpleLand)) {
            // No biomes will be present on land.
            return;
        }
        Biomes biomes = new Biomes(planet);

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                Tile tile = getTile(x, y);
                if (!tile.isWater() && !tile.equals(getIce())) {
                    int w = tile.getWetness();
                    int t = getTemperature(x, y);
                    int h = getHeight(x, y);

                    biomes.setBiomes(w, t, h);
                    List<String> colours = biomes.getColours();

                    Climate climate = new Climate(getTile(x, y));
                    climate.list(colours, getHeight(x, y));
                    setTile(x, y, climate);
                }
            }
        }
    }

    protected static void testOutput(Planet planet, PlanetGenerator pg, PlanetMapper mapper) throws IOException {
        testOutput(planet, pg, mapper, "output/" + planet.getType());
    }

    protected static void testOutput(Planet planet, PlanetGenerator pg, PlanetMapper mapper, String directory) throws IOException {
        System.out.println(planet.getType() + ":");
        int WIDTH = 2048;

        TextGenerator generator = new TextGenerator(planet);
        generator.setGenerator(pg);
        String text = generator.getFullDescription();
        mapper.generate();

        File dir = new File(directory);
        dir.mkdirs();
        String fileRoot = dir.getAbsolutePath() + "/" + planet.getName();

        SimpleImage img = mapper.draw(WIDTH);
        img.save(new File(fileRoot + ".png"));
        PlanetMapper.stretchImage(img, WIDTH).save(new File(fileRoot + "-Stretch.png"));
        mapper.debugWetness(true);
        mapper.draw(WIDTH).save(new File(fileRoot + "-Wet.png"));
        mapper.setTemperatureMap();
        mapper.draw(WIDTH).save(new File(fileRoot + "-Heat.png"));
        img = mapper.drawHeightMap(WIDTH);
        img.save(new File(fileRoot + "-Height.png"));
        PlanetMapper.stretchImage(img, WIDTH).save(new File(fileRoot + "-HeightStretch.png"));

        if (mapper.hasCloudMap) {
            mapper.drawClouds(WIDTH).get(0).save(new File(fileRoot + "-Clouds.png"));
        }

        FileWriter writer = new FileWriter(fileRoot + ".html");
        writer.write("<body>\n");
        writer.write(text);
        writer.write("\n</body>\n");
        writer.close();

        System.out.println(text);
    }
}
