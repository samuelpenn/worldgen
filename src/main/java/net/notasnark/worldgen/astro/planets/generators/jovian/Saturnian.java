/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Jovian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Jovian.JovianFeature.*;

/**
 * Saturnian, Jovian of class Dwarf-Jovian.
 * Dwarf-Jovian worlds mass from 0.06 to 0.8 times that of Jupiter. The greatest portion of their
 * mass are concentrated within their gaseous envelopes, but they still have a low enough gravity
 * to swell from stellar heating. The more massive examples will have layers of liquid metallic
 * hydrogen or helium surrounding their cores.
 *
 * Saturnian's orbit beyond the snowline, and have dynamic atmospheres, though they are often obscured
 * by methane and ammonia.
 */
public class Saturnian extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Saturnian.class);

    public Saturnian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        if (planet.getTemperature() < Temperature.MethaneBoils.getKelvin()) {
            if (Die.d2() == 1) planet.addFeature(MethaneClouds);
        } else if (planet.getTemperature() < Temperature.AmmoniaBoils.getKelvin()) {
            if (Die.d2() == 1) planet.addFeature(AmmoniaClouds);
        } else if (planet.getTemperature() > Temperature.WaterFreezes.getKelvin()) {
            if (Die.d2() == 1) planet.addFeature(WaterClouds);
        }
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(45000 + Die.die(5000, 4));

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11:
                planet.setMagneticField(MagneticField.Strong);
                break;
            case 12:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
        }

        planet.setTemperature((int) (planet.getTemperature() * 1.2));
        planet.setNightTemperature((int) (planet.getNightTemperature() * 1.2));

        // Set default day length to be around 10 hours.
        planet.setDayLength(9 * 3600 + Die.die(3600, 2));

        generateFeatures(planet);

        addPrimaryResource(planet, Hydrogen);
        addTertiaryResource(planet, Helium);
        addTertiaryResource(planet, OrganicGases);
        addTertiaryResource(planet, Water);

        return planet;
    }
}
