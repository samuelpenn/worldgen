/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.FerricOre;
import static net.notasnark.worldgen.astro.commodities.CommodityName.SilicateOre;

/**
 * JaniLithic worlds are tidally locked to their parent star, existing at the inner edge of where
 * planets can form.
 */
public class JaniLithic extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(JaniLithic.class);

    public JaniLithic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.JaniLithic);

        planet.setHabitability(4);
        planet.setHydrographics(0);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(Die.die(60_000) + 10_000);
        planet.setLife(Life.None);
        planet.setDayLength(planet.getPeriod());
        planet.setTemperature((int) (planet.getTemperature() * 1.4));
        planet.setNightTemperature(planet.getTemperature() / 2);

        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, FerricOre);

        planet.addTradeCode(TradeCode.De, TradeCode.Ba);
        planet.addFeature(GeneralFeature.TideLocked);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
