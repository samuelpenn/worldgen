/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * MesoGaian worlds are Tectonic Terrestrial worlds similar to an early Earth. Their biology has evolved
 * far enough for oxygen to be present in their atmospheres. They are warm worlds, which tend to have a
 * high atmospheric pressure. Oxygen content is still quite low.
 */
public class MesoGaian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(MesoGaian.class);

    public MesoGaian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Early stage Meso Gaian worlds have an Aerobic level of life. Organisms are beginning to
     * evolve rapidly as the availability of oxygen increases.
     */
    private void earlyStage(Planet planet, int modifier) {
        planet.addFeature(TerrestrialFeature.EarlyStage);
        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setAtmosphere(Atmosphere.LowOxygen);
                break;
            case 4: case 5:
                planet.setAtmosphere(Atmosphere.HighCarbonDioxide);
                planet.setHabitability(3);
                break;
            case 6:
                planet.setAtmosphere(Atmosphere.OrganicToxins);
                planet.setHabitability(3);
                break;
        }
        planet.setPressure((int)(planet.getPressure() * 1.5));

        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);
        addTertiaryResource(planet, Oxygen);
        planet.setTemperature( (planet.getTemperature() + 350) / 2 - modifier * 5);
    }

    /**
     * Late Stage Meso Gaian worlds have complex ocean life, with a wide range of aquatic vertebrates.
     * There might be simple life, such as lichens, starting to gain a foothold on land.
     */
    private void lateStage(Planet planet, int modifier) {
        planet.addFeature(TerrestrialFeature.LateStage);
        switch (Die.d8()) {
            case 1: case 2:
                planet.setAtmosphere(Atmosphere.LowOxygen);
                break;
            case 3:
                planet.setAtmosphere(Atmosphere.HighCarbonDioxide);
                planet.setHabitability(3);
                break;
            case 4: case 5:
                planet.setAtmosphere(Atmosphere.OrganicToxins);
                planet.setHabitability(3);
                break;
            case 6: case 7: case 8:
                planet.setAtmosphere(Atmosphere.Standard);
                break;
        }
        planet.setPressure((int) (planet.getPressure() * 1.1));
        switch (Die.d6() + modifier * 2) {
            case 1: case 2: case 3: case 4: case 5:
                planet.setLife(Life.ComplexOcean);
                break;
            default:
                planet.setLife(Life.SimpleLand);
                break;
        }

        addSecondaryResource(planet, Oxygen);
        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);
        planet.setTemperature( (planet.getTemperature() + 320) / 2 - modifier * 5);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.MesoGaian);

        setTerrestrialProperties(planet);
        planet.setHabitability(2);
        planet.setHydrographics(55 + Die.d12(2));

        int modifier = 0;
        if (planet.hasFeature(GeneralFeature.VeryHighLife)) {
            modifier = 2;
        } else if (planet.hasFeature(GeneralFeature.HighLife)) {
            modifier = 1;
        }
        switch (Die.d6() + modifier) {
            case 1: case 2: case 3:
                earlyStage(planet, modifier);
                break;
            default:
                lateStage(planet, modifier);
        }
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));
        setFromUWP(planet);

        if (planet.getHydrographics() < 30) {
            planet.addFeature(TerrestrialFeature.Dry);
        }

        generateLife(planet);
        addFeatures(planet);

        addPrimaryResource(planet, Water);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
