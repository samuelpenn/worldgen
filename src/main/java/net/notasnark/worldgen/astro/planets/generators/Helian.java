/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.*;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.utils.rpg.Die;

import java.util.ArrayList;
import java.util.List;

/**
 * Helian worlds range in size between 3 and 17 Earth Masses. They tend to have Helium atmospheres, but
 * are mostly rocky or icy rather than being gas worlds.
 */
public class Helian extends PlanetGenerator {

    public enum HelianFeature implements PlanetFeature {
        LightVolcanic,
        MediumVolcanic,
        HeavyVolcanic
    }

    public Helian(WorldGen worldgen, StarSystem system, Star primary, Planet previous, long distance) {
        super(worldgen, system, primary, previous, distance);
    }

    /**
     * Get a generated planet. Can't be called directly on the Belt class, because
     * we don't know exactly what type of planet to create.
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }


    @Override
    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(6_000 + Die.d6(2) * 5000 + Die.die(1_000));

        // Set default day length to be around 16 hours.
        planet.setDayLength((Die.d6(2) + 8) * 86400 + Die.die(3600, 2));

        addPrimaryResource(planet, CommodityName.Helium);

        return planet;
    }

    /**
     * Randomly determine how many moons this planet has.
     *
     * @return  Number of moons, up to a dozen or so.
     */
    protected int getNumberOfMoons() {
        return Die.d6();
    }

    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<Planet>();

        int numberOfMoons = getNumberOfMoons() - (primary.hasFeature(GeneralFeature.TideLocked)?5:0);
        if (numberOfMoons < 1) {
            // Nothing to do.
            return moons;
        }

        logger.info(String.format("Creating %d moons for [%s]", numberOfMoons, primary.getName()));

        int  kelvin = Physics.getTemperatureOfOrbit(star, primary.getDistance());
        long distance = primary.getRadius() * (Die.d2(2));

        int  moonOrbit = 1;
        int  ringOrbit = 1;
        for (int m=0; m < numberOfMoons; m++) {
            String name = StarSystemFactory.getMoonName(primary.getName(), m + 1);

            logger.info("Adding moon " + name);

            List<PlanetFeature> features = new ArrayList<PlanetFeature>();
            features.add(MoonFeature.Moon);
            if (primary.hasFeature(GeneralFeature.TideLocked)) {
                features.add(MoonFeature.SmallMoon);
            }

            PlanetType type = PlanetFactory.determineSmallBodyType(kelvin);
            if (Die.die(m + 1) == 1 && Physics.getTemperatureOfOrbit(star, primary.getDistance()) < 250) {
                type = PlanetType.IceRing;
                name = StarSystemFactory.getRingName(primary.getName(), ringOrbit++);
            } else {
                if (Die.die(m) < 4) {
                    type = PlanetType.Vulcanian;
                }
                name = StarSystemFactory.getMoonName(primary.getName(), moonOrbit++);
            }

            if (type == null) {
                logger.error("Trying to create a moon of type null, bailing out.");
                break;
            }

            Planet moon = factory.createMoon(system, star, name, type,
                    distance, primary,
                    features.toArray(new PlanetFeature[0]));

            moons.add(moon);
            distance += Die.die((int) primary.getRadius(), 3);
        }

        return moons;
    }

}
