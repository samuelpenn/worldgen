/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * EoGaian worlds are Tectonic Terrestrial worlds similar to an early Earth. They have very high atmospheric
 * pressure, and high surface temperatures. However, they have a considerable amount of surface water that is
 * kept liquid by the intense pressures.
 *
 * The atmosphere of EoGaian worlds is undergoing rapid change, dropping in pressure and temperature over
 * time, and changing in composition as it interacts with the water, rocks and volcanic activity. Organic
 * chemicals are being formed, eventually leading to the abiogensis of bacteria.
 *
 * The next step is MesoGaian.
 */
public class EoGaian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(EoGaian.class);

    public EoGaian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Early stage EoGaian worlds have a Primordial or Methane atmosphere.
     * Atmospheric pressure is generally over 10x standard, surface temperature is hot.
     *
     * @param planet    Planet to update.
     */
    private void earlyStage(Planet planet) {
        planet.setHydrographics(60 + Die.d12(2));
        planet.setLife(Life.None);
        planet.setAtmosphere(Atmosphere.Primordial);
        // Ensure high temperature and pressure.
        planet.setPressure(planet.getPressure() * (Die.d6() + 6));
        planet.setTemperature( (planet.getTemperature() + 600) / 2);

        switch (Die.d6(2)) {
            case 2: case 3:
                // Primordial atmosphere has recently formed. Life is non-existent.
                planet.addFeature(TerrestrialFeature.VolcanicFlats);
                addSecondaryResource(planet, OrganicGases);
                addSecondaryResource(planet, Hydrogen);
                addTertiaryResource(planet, OrganicChemicals);
                planet.setPressure((int) (planet.getPressure() * 1.2));
                planet.setTemperature( (planet.getTemperature() + 700) / 2);
                break;
            case 4: case 5: case 6:
                // Organic matter, no significant life yet.
                planet.addFeature(TerrestrialFeature.EarlyStage);
                planet.addFeature(TerrestrialFeature.Volcanoes);
                addPrimaryResource(planet, OrganicGases);
                addSecondaryResource(planet, OrganicChemicals);
                addTertiaryResource(planet, Hydrogen);
                planet.setLife(Life.Organic);
                planet.setTemperature( (planet.getTemperature() + 550) / 2);
                break;
            default:
                // Single celled life. Planet is cooling and atmosphere pressure is dropping.
                planet.addFeature(TerrestrialFeature.EarlyStage);
                planet.addFeature(TerrestrialFeature.Volcanoes);
                addPrimaryResource(planet, OrganicGases);
                addPrimaryResource(planet, OrganicChemicals);
                addTraceResource(planet, Hydrogen);
                planet.setLife(Life.Archaean);
                planet.setPressure((int) (planet.getPressure() * 0.75));
                planet.setTemperature( (planet.getTemperature() + 450) / 2);
                break;
        }
    }

    /**
     * Late stage EoGaian worlds have a Nitrogen and Carbon Dioxide atmosphere.
     *
     * @param planet    Planet to update.
     */
    private void lateStage(Planet planet) {
        planet.addFeature(TerrestrialFeature.LateStage);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setHydrographics(55 + Die.d12(2));
        planet.setPressure((int) (planet.getPressure() * (Die.d4() + 4)));
        planet.setLife(Life.Archaean);

        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                // Single celled life. Planet is cooling and atmosphere pressure is dropping.
                addPrimaryResource(planet, OrganicChemicals);
                addSecondaryResource(planet, OrganicGases);
                addTraceResource(planet, Hydrogen);
                planet.setTemperature( (planet.getTemperature() + 425) / 2);
                break;
            case 5: case 6: case 7: case 8:
                addPrimaryResource(planet, OrganicChemicals);
                addSecondaryResource(planet, OrganicGases);
                planet.setPressure((int) (planet.getPressure() * 0.75));
                planet.setTemperature( (planet.getTemperature() + 400) / 2);
                break;
            default:
                addSecondaryResource(planet, OrganicChemicals);
                addSecondaryResource(planet, OrganicGases);
                addTraceResource(planet, Oxygen);
                planet.setPressure((int) (planet.getPressure() * 0.50));
                planet.setTemperature( (planet.getTemperature() + 375) / 2);
                break;
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EoGaian);

        setTerrestrialProperties(planet);
        planet.setHabitability(5);

        // Volcanic activity.
        planet.setTemperature((int) (planet.getTemperature() * 1.05));
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));

        if (planet.getTemperature() < 275) {
            planet.addFeature(TerrestrialFeature.Cold);
        } else if (planet.getTemperature() > 295) {
            planet.addFeature(TerrestrialFeature.Warm);
        }

        switch (Die.d3()) {
            case 1: case 2:
                earlyStage(planet);
                break;
            case 3:
                lateStage(planet);
                break;
        }
        // The thick atmosphere means that temperatures are pretty much consistent across the globe.
        planet.setNightTemperature(planet.getTemperature());

        generateLife(planet);

        // Define resources for this world.
        addPrimaryResource(planet, SilicateOre);
        addPrimaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, CarbonicOre);
        addSecondaryResource(planet, CarbonicCrystals);

        if (planet.hasFeature(TerrestrialFeature.Light)) {
            addTertiaryResource(planet, FerricOre);
            addTraceResource(planet, HeavyMetals);
            addTraceResource(planet, Radioactives);
            addTraceResource(planet, PreciousMetals);
        } else if (planet.hasFeature(TerrestrialFeature.Dense)) {
            addSecondaryResource(planet, FerricOre);
            addSecondaryResource(planet, HeavyMetals);
            addSecondaryResource(planet, Radioactives);
            addTertiaryResource(planet, RareMetals);
            addTertiaryResource(planet, PreciousMetals);
        } else {
            addSecondaryResource(planet, FerricOre);
            addTertiaryResource(planet, HeavyMetals);
            addTertiaryResource(planet, Radioactives);
            addTraceResource(planet, RareMetals);
            addTraceResource(planet, PreciousMetals);
        }
        addPrimaryResource(planet, Water);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

}
