/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Arean;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.planets.tiles.Cratered;
import net.notasnark.worldgen.astro.planets.tiles.Speckled;

/**
 * An Arean world type encompasses several sub-types, include EoArean, MesoArean, EuArean and AreanLacustric.
 * They belong to the Dwarf Terrestrial Group, and generally lack the size to hold onto their atmospheres
 * for more than a few billion years.
 */
public abstract class AreanMapper extends PlanetMapper {
    protected final static int DEFAULT_FACE_SIZE = 24;
    protected final Tile MOUNTAINS = new Tile("Mountains", "#906045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#D08055", false, 3);
    protected final Tile BASIN = new Tile("Basin", "#D08055", false, 3);
    protected final Tile DESERT = new Tile("Desert", "#F09050", false, 2);
    protected final Tile SALT_DESERT = new Tile("SaltDesert", "#F2F2F2", false, 2);

    protected final Tile RIFT = new Tile("Rift", "#805030", false, 1);

    private final Tile WATER    = new Tile("Water", "#7777FF").water(true).random(2);
    private final Tile ICE      = new Tile("Ice", "#FEFEFE").water(false).random(3);

    enum CoverType {
        Jungle,
        Forest,
        Plains,
        Scrub,
        Barren,
        Heavy,
        Light
    }

    class CoverDefs {
        String      heavy = "#007700";
        String      light = "#00AA00";
        int         density = 0;

        public CoverDefs() {

        }

        public CoverDefs(String heavy, String light, int density) {
            this.heavy = heavy;
            this.light = light;
            this.density = density;
        }

        public CoverDefs heavy(String heavy) {
            this.heavy = heavy;
            return this;
        }
        public CoverDefs light(String light) {
            this.light = light;
            return this;
        }
        public CoverDefs density(int density) {
            this.density = density;
            return this;
        }
    }


    public AreanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public AreanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected Tile getDesert() { return DESERT; };
    protected Tile getPlains() { return PLAINS; };
    protected Tile getMountains() { return MOUNTAINS; };
    protected Tile getWater() { return WATER; };
    protected Tile getIce() { return ICE; };

    protected Tile setLowPlains(int x, int y) {
        if (planet.hasFeature(Arean.AreanFeature.WhiteDesert)) {
            return setTile(x, y, new Speckled(getDesert().copy().shaded(100 - getHeight(x, y) / 2)).colour("#FEFEFE"));
        } else {
            return setTile(x, y, getDesert().copy().shaded(100 - getHeight(x, y) / 2));
        }
    };

    protected Tile setHighPlains(int x, int y) {
        if (planet.hasFeature(Arean.AreanFeature.WhiteDesert)) {
            return setTile(x, y, new Speckled(getPlains().copy().shaded(100 - getHeight(x, y) / 2)).colour("#FEFEFE"));
        } else {
            return setTile(x, y, getPlains().copy().shaded(100 - getHeight(x, y) / 2));
        }
    }

    protected Tile setMountains(int x, int y) {
        return setTile(x, y, getMountains().copy().shaded(150 - getHeight(x, y)));
    }

    protected Tile setWater(final int x, final int y) {
        setHeight(x, y, 0);
        return setTile(x, y, getWater().copy());
    };

    protected Tile setIce(int x, int y) {
        return setTile(x, y, getIce().copy().shaded(100 - getHeight(x, y) / 2));
    }


    protected Tile getRandomPlain(int tileX, int tileY) {
        int h = getHeight(tileX, tileY);

        if (h < 15) {
            return setLowPlains(tileX, tileY);
        } else {
            return setHighPlains(tileX, tileY);
        }
    }

    protected void setColours() {
        if (planet.hasFeature(Arean.AreanFeature.GreyDesert)) {
            getDesert().mix("#808080", 2);
            getPlains().mix("#707070", 2);
            getMountains().rgb("#505050");
        }
        if (planet.hasFeature(Arean.AreanFeature.RedDesert)) {
            getDesert().mix("#F0A080", 2);
            getPlains().mix("#F07070", 2);
            getMountains().rgb("#A06040");
        }
        if (planet.hasFeature(Arean.AreanFeature.BrownDesert)) {
            getDesert().mix("#705000", 2);
            getPlains().mix("#705000", 2);
            getMountains().rgb("#504000");
        }
        if (planet.hasFeature(Arean.AreanFeature.YellowDesert)) {
            getDesert().mix("#FFFF60", 3);
            getPlains().mix("#F0F070", 2);
            getMountains().rgb("#909040");
        }
        if (planet.hasFeature(Arean.AreanFeature.PurpleDesert)) {
            getDesert().mix("#A000A0", 2);
        }
        if (planet.hasFeature(Arean.AreanFeature.PinkDesert)) {
            getDesert().mix("#C0A0A0", 2);
        }
        if (planet.hasFeature(Arean.AreanFeature.WhiteDesert)) {
            getDesert().rgb("#FEFEFE");
            getPlains().rgb("#FAFAD0");
            getMountains().rgb("#F0F0F0");
        }
        if (planet.hasFeature(Arean.AreanFeature.SaltDesert)) {
            getDesert().rgb("#F0F0F0");
            getPlains().mix("#F0F0F0");
        }
        if (planet.hasFeature(Arean.AreanFeature.DarkSea)) {
            getWater().mix("#000000");
        }
        if (planet.hasFeature(Arean.AreanFeature.BlackSea)) {
            getWater().rgb("#010110");
        }
        if (planet.hasFeature(Arean.AreanFeature.BlackRocks)) {
            getMountains().rgb("#303040");
        }
        if (planet.hasFeature(Arean.AreanFeature.WhiteRocks)) {
            getMountains().rgb("#C0C0A0");
        }
    }

    public void generate() {
        setColours();
        Icosahedron parent = new Icosahedron(3);
        parent.fractal();

        double modifier = 1.0;
        for (int y= parent.getFaceSize() * 2 - 1; y < parent.getFaceSize() * 3; y++) {
            modifier *= 0.9;
            for (int x=0; x < parent.getWidthAtY(y); x++) {
                parent.setHeight(x, y, (int)(parent.getHeight(x, y) * modifier));
            }
        }

        generateHeightMap(parent,24, getFaceSize());

        // Equatorial Ridge
        if (planet.hasFeature(Dwarf.DwarfFeature.EquatorialRidge)) {
            int y = getNumRows()/2;
            int cy = y;
            for (int x=0; x < getWidthAtY(y); x++) {
                setHeight(x, cy-2, getHeight(x, cy) + Die.d12(1));
                setHeight(x, cy-1, getHeight(x, cy) + Die.d12(3));
                setHeight(x, cy, getHeight(x, cy) + Die.d12(5));
                setHeight(x, cy+1, getHeight(x, cy) + Die.d12(3));
                setHeight(x, cy+2, getHeight(x, cy) + Die.d12(1));

                cy += Die.dieV(2);
                if (cy < y-5) {
                    cy++;
                } else if (cy > y+5) {
                    cy--;
                }
                if (x > getWidthAtY(y) - 8) {
                    if (cy < y) {
                        cy++;
                    } else if (cy > y) {
                        cy--;
                    }
                }
            }
        }

        int mountainLevel = getSeaLevel(90);

        // Basic barren landscape. Set mountains at high elevations.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getHeight(x, y) >= mountainLevel) {
                    setTile(x, y, setMountains(x, y));
                } else {
                    setTile(x, y, getRandomPlain(x, y));
                }
            }
        }

        if (planet.hasFeature(Arean.AreanFeature.NorthernSea)) {
            // Not actually a sea, but a dry sea basin in Northern hemisphere.
            int y = getNumRows()/4 + Die.dieV(12);
            int x = getWidthAtY(y) + Die.dieV(getWidthAtY(y) / 5);

            setTile(x, y, BASIN);
            floodToPercentage(BASIN, 10 + Die.d6(2), true);
            for (y=0; y < getNumRows(); y++) {
                for (x=0; x < getWidthAtY(y); x++) {
                    if (getTile(x, y) == BASIN) {
                        setHeight(x, y, Die.d12());
                    }
                }
            }

        }

        // If a tile is completely surrounded by mountains, make it a mountain.
        int count = 1;
        while (count > 0) {
            count = 0;
            for (int y = 0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (!getTile(x, y).isMountain() &&
                            getTile(getWest(x, y), y).isMountain() &&
                            getTile(getEast(x, y), y).isMountain() &&
                            getTile(getUpDown(x, y).x, getUpDown(x, y).y).isMountain()) {
                        setTile(x, y, setMountains(x, y));
                        count++;
                    }
                }
            }
        }

        // Expand light and dark areas.
        /*
        flood(getLowPlains(), 6);
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).equals(getLowPlains())) {
                    setTile(x,y, getTile(x,y).getShaded(75 + getHeight(x, y)));
                }
            }
        }
        */

        int iceLine = 80 + (planet.getTemperature() - 270) / 2;

        // Apply craters, frequency depending on tile type.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getLatitude(y) + getHeight(x, y) / 5 > iceLine) {
                    setIce(x, y);
                } else if (getLatitude(y) + getHeight(x, y) / 5 > iceLine) {
                        setTile(x, y, getTile(x, y).getMix(getIce()).getMix(getIce()));
                } else if (getTile(x, y).equals(getPlains())) {
                    if (Die.d20() == 1) {
                        setTile(x, y, new Cratered(getTile(x, y)));
                    }
                } else if (getTile(x, y).equals(getDesert())) {
                    if (Die.d6() == 1) {
                        setTile(x, y, new Cratered(getTile(x, y)));
                    }
                } else if (Die.d12() == 1) {
                    setTile(x, y, new Cratered(getTile(x, y)));
                }
            }
        }
        hasHeightMap = true;

        if (planet.getHydrographics() > 0) {
            if (planet.hasFeature(Arean.AreanFeature.EquatorialSea)) {
                int y = getNumRows() / 2;
                for (int x = 0; x < getWidthAtY(y); x++) {
                    setTile(x, y, getWater());
                    setHeight(x, y, 0);
                }
            } else if (planet.hasFeature(Arean.AreanFeature.NorthernSea)) {
                for (int x = 0; x < getWidthAtY(0); x++) {
                    setTile(x, 0, getWater());
                    setHeight(x, 0, 0);
                }
            } else if (planet.hasFeature(Arean.AreanFeature.SouthernSea)) {
                for (int x = 0; x < getWidthAtY(getNumRows() - 1); x++) {
                    setTile(x, getNumRows() - 1, getWater());
                    setHeight(x, getNumRows() - 1, 0);
                }
            } else if (planet.hasFeature(Arean.AreanFeature.SmallContinents)) {
                for (int i = 0; i < planet.getHydrographics() * 2; i++) {
                    int y = Die.rollZero(getNumRows() / 2) + getNumRows() / 4;
                    int x = Die.rollZero(getWidthAtY(y));
                    setTile(x, y, getWater());
                    setHeight(x, y, 0);
                }
                for (int i = 0; i < 15; i++) {
                    int y = Die.rollZero(DEFAULT_FACE_SIZE);
                    setTile(Die.rollZero(getWidthAtY(y)), y, getWater());
                    setTile(Die.rollZero(getWidthAtY(getNumRows() - 1 - y)), getNumRows() - 1 - y, getWater());
                }
            } else {
                for (int i = 0; i < planet.getHydrographics() / 3; i++) {
                    int y = Die.rollZero(getNumRows() / 2) + getNumRows() / 4;
                    int x = Die.rollZero(getWidthAtY(y));
                    setTile(x, y, getWater());
                    setHeight(x, y, 0);
                }
                if (Die.d2() == 1) {
                    setTile(0, 0, getWater());
                }
                if (Die.d2() == 1) {
                    setTile(0, getNumRows() - 1, getWater());
                }
            }
            floodToPercentage(getWater(), planet.getHydrographics(), true);
        }
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 0);
                } else if (isShore(x, y) && getTile(x, y).isMountain()) {
                    getTile(x, y).mountain(false);
                }
            }
        }
        setIceCaps(getIce(), getIce());
        generateWetness();
        generateBiomes();
    }
}
