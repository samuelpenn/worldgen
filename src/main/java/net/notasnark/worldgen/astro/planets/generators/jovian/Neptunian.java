/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Jovian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.jovian.Neptunian.NeptunianFeatures.*;

/**
 * Neptunian worlds are cold gas giants that are found in the outer solar system.
 */
public class Neptunian extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Neptunian.class);

    public enum NeptunianFeatures implements PlanetFeature {
        DarkClouds,
        GreenClouds,
        SapphireClouds,
        WhiteSpots,
        DarkSpots,
        WhiteBands
    }

    public Neptunian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        if (Die.d2() == 1) {
            planet.addFeature(new Roller<PlanetFeature>(GreenClouds, SapphireClouds).add(DarkClouds, 3).roll());
        }

        if (Die.d4() == 1) {
            planet.addFeature(WhiteBands);
        }
    }

    public Planet getPlanet(String name) {
        Planet planet =  definePlanet(name, PlanetType.Neptunian);
        planet.setRadius(20_000 + Die.die(5_000, 4));

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);

        if (planet.getDistance() < 0) {
            throw new IllegalStateException("Planet " + planet.getName() + " has negative distance");
        }

        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
            case 11: case 12:
                planet.setMagneticField(MagneticField.Standard);
            default:
                planet.setMagneticField(MagneticField.Strong);
                break;
        }

        // Set default day length to be hundreds of days.
        planet.setDayLength(Die.d100(5) * Physics.STANDARD_DAY + Die.die(Physics.STANDARD_DAY));

        generateFeatures(planet);

        addPrimaryResource(planet, OrganicGases);
        addSecondaryResource(planet, Hydrogen);
        addSecondaryResource(planet, Helium);

        return planet;
    }
}
