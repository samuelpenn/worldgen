/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.BathyGaian;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BathyPelagicMapper extends TerrestrialMapper {
    public BathyPelagicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected static final Tile MESOGAIAN = new Tile("Land", "#604030", false, 3);
    protected static final Tile SANDY = new Tile("Land", "#806050", false, 3);
    protected static final Tile VOLCANO = new Tile("Volcano", "#F05050", false, 3);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        WATER.rgb(0, 0x10 + Die.dieV(16), 0x80 + Die.dieV(32));
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                setTile(x, y, WATER.getShaded(getHeight(x, y) / 4 + 75));
                setHeight(x, y, 50);
            }
        }

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = false;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        String cloudColour = "#FEFEFE";
        int lowerLimit = 45;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, h);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("BathyPelagic");
        planet.setType(PlanetType.BathyPelagic);
        planet.setTemperature(350);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(1_000_000);
        planet.setHydrographics(100);

        planet.setLife(Life.ComplexOcean);

        testOutput(planet, new BathyGaian(Server.getWorldGen(), null, null, null, 0),
                new BathyPelagicMapper(planet));
    }
}
