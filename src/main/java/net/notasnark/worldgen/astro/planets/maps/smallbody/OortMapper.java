/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.smallbody;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.maps.SmallBodyMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;

/**
 * An icy asteroid, with some rocky parts.
 */
public class OortMapper extends SmallBodyMapper {
    public OortMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public OortMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private static final Tile ICE = new Tile("Ice", "#F0F0F0", false);
    private static final Tile SOOT = new Tile("Soot", "#101010", false);

    public void generate() {
        super.generate();

        int baseGrey = 60;
        for (int y = 0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int h = getHeight(x, y);
                setTile(x, y, new Rough(ICE.getShaded(50 + h / 2)));
            }
        }

        // Larger asteroids tend to be more spherical.
        int heightDividor = 1 + (int) (planet.getRadius() / 80);

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        smoothHeights(planet, heightDividor);
    }
}
