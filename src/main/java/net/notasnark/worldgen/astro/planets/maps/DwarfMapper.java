/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps;

import net.notasnark.worldgen.astro.planets.Planet;

/**
 * A Dwarf Terrestrial world.
 */
public class DwarfMapper extends PlanetMapper {
    protected static final int    DEFAULT_FACE_SIZE = 24;

    public DwarfMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public DwarfMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {
        generateHeightMap(24, DEFAULT_FACE_SIZE);
    }

    /**
     * Smooth out the surface height map. This reduces the chance of a broken surface mesh,
     * which happens if neighbouring tiles are too different. The amount of smoothing is
     * controlled by the heightDividor, which is normally based on the size of the planet.
     * The larger the planet (heightDividor), the more spherical it will be.
     *
     * Also applies a deformation transform on the surface, if the planet has any shape
     * features defined.
     *
     * @param planet            Planet to smooth.
     * @param heightDividor     Number to divide height by.
     */
    protected void smoothHeights(Planet planet, int heightDividor) {
        if (heightDividor > 1) {
            heightDividor = Math.max(3, heightDividor);
            for (int y = 0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    int h = getHeight(x - 1, y) + getHeight(x, y) + getHeight(x + 1, y);
                    if (heightDividor > 3) {
                        h += 50 * (heightDividor - 3);
                    }
                    setHeight(x, y, h / heightDividor);
                }
            }
        }

        for (int x=0; x < getWidthAtY(0); x++) {
            setHeight(x, 0, getHeight(0, 0));
            setHeight(x, getNumRows() - 1, getHeight(0, getNumRows() - 1));
        }
    }

}
