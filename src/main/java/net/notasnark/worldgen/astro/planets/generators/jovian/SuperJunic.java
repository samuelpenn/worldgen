/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Jovian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * Junic worlds are large gas planets in epistellar orbits around their star.
 */
public class SuperJunic extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(SuperJunic.class);

    public SuperJunic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet =  definePlanet(name, PlanetType.SuperJunic);
        planet.setRadius(65_000 + Die.die(6_000, 4));

        planet.setAtmosphere(Atmosphere.Exotic);
        planet.setPressure(Pressure.SuperDense);
        planet.setHabitability(6);

        // Junic worlds are normally tidally locked.
        planet.setDayLength(planet.getPeriod());
        int k = (int)(planet.getTemperature() * 1.5);
        planet.setTemperature(k);
        planet.setNightTemperature(k / 2);

        // Slightly weaker magnetic fields due to the lack of rotation.
        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.setMagneticField(MagneticField.Strong);
                break;
            default:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
        }

        addPrimaryResource(planet, Hydrogen);
        addSecondaryResource(planet, Helium);
        addTertiaryResource(planet, HeavyMetals);
        addSecondaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, ExoticCrystals);

        return planet;
    }
}
