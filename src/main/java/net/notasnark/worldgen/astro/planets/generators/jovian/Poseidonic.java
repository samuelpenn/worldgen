/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Jovian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Jovian.JovianFeature.*;

/**
 * Poseidonic, sub-Jovian world, within the snow line with lots of water vapour. Tends to have whitish clouds.
 */
public class Poseidonic extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Poseidonic.class);

    public enum PoseidonicFeatures implements PlanetFeature {
        WhiteMarble,
        BlueMarble
    }
    public Poseidonic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        planet.addFeature(WaterClouds);

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.addFeature(PoseidonicFeatures.BlueMarble);
                break;
            case 4: case 5:
                planet.addFeature(PoseidonicFeatures.WhiteMarble);
                break;
        }
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(17_000 + Die.die(4000, 4));

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
            case 10: case 11: case 12:
                planet.setMagneticField(MagneticField.Standard);
            default:
                planet.setMagneticField(MagneticField.Strong);
                break;
        }

        // Set default day length to be around 20 hours.
        planet.setDayLength(16 * 3600 + Die.die(3600, 8));
        planet.setTemperature((planet.getTemperature() + 300)/2);
        planet.setNightTemperature((planet.getTemperature()  - Die.d6(3)));

        generateFeatures(planet);

        addPrimaryResource(planet, Hydrogen);
        addSecondaryResource(planet, Water);
        addTertiaryResource(planet, Helium);

        return planet;
    }
}
