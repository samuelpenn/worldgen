/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

public enum GeneralFeature implements PlanetFeature {
    // Features due to age.
    VeryYoung,  // Less than a 100 million years old.
    Young,      // Less than a billion years old.
    Old,        // More than 10 billion years old.
    Migrant,    // Has migrated from another system.
    TideLocked,
    FewMoons,
    NoMoons,
    // Star system metallicity
    LowMetals,
    HighMetals,
    NoMetals,
    HighLife,
    VeryHighLife,
    UWP;

}
