/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.planemo;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Planemo;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * These are Planemo worlds massive enough to be considered gas giants.
 */
public class Odyssian extends Planemo {
    private static final Logger logger = LoggerFactory.getLogger(Odyssian.class);

    public Odyssian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public enum OdyssianFeature implements PlanetFeature {
        GreenClouds,
        BlueClouds,
        YellowClouds,
        PaleClouds,
        DarkClouds;

        public int getNotability() {
            return 100;
        }
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.setRadius(20_000 + Die.die(5_000, 2));
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 5: case 6: case 7: case 8:
                planet.setRadius(20_000 + Die.die(10_000, 2));
                planet.setMagneticField(MagneticField.Strong);
                break;
            case 9: case 10:
                planet.setRadius(25_000 + Die.die(10_000, 2));
                planet.setMagneticField(MagneticField.Strong);
                break;
            case 11:
                planet.setRadius(30_000 + Die.die(15_000, 2));
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
            case 12:
                planet.setRadius(40_000 + Die.die(20_000, 2));
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
        }

        switch (Die.d6(2)) {
            case 2:
                planet.addFeature(OdyssianFeature.GreenClouds);
                break;
            case 3: case 4:
                planet.addFeature(OdyssianFeature.BlueClouds);
                break;
            case 5: case 6: case 7: case 8: case 9:
                planet.addFeature(OdyssianFeature.DarkClouds);
                break;
            case 10: case 11:
                planet.addFeature(OdyssianFeature.PaleClouds);
                break;
            case 12:
                planet.addFeature(OdyssianFeature.YellowClouds);
                break;
        }

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);

        planet.setDayLength(6 * Physics.HOUR + Die.die(60) * Physics.HOUR + Die.die(Physics.HOUR));

        planet.setTemperature(100 + Die.d100());
        planet.setNightTemperature(planet.getTemperature());

        addPrimaryResource(planet, Hydrogen);
        addPrimaryResource(planet, Helium);

        return planet;
    }
}
