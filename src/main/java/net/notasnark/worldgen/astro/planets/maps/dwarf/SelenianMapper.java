/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Selenian;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * Selenian worlds are barren, grey, rocky worlds. They have darker 'seas' of recent lava plains.
 */
public class SelenianMapper extends DwarfMapper {
    protected static final Tile MARIA = new Tile("Seas", "#404040", false, 2);
    protected static final Tile HIGHLANDS = new Tile("Highlands", "#8B8B88", false, 2);

    public SelenianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public SelenianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h <= seaLevel) {
                    setTile(tileX, tileY, MARIA);
                } else {
                    setTile(tileX, tileY, HIGHLANDS.getShaded(50 + (h / 2)));
                }
            }
        }

        // Flood the maria out to their final coverage. Then set the height shading on them.
        // This ensures that the height shading is consistent between original maria seeds
        // and the flooded areas.
        floodToPercentage(MARIA, 20, false);
        growBorder(MARIA, 2, 2);
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (getTile(tileX, tileY).equals(MARIA)) {
                    setTile(tileX, tileY, MARIA.getShaded(75 + h/2));
                } else {
                    setTile(tileX, tileY, new Rough(getTile(tileX, tileY)));
                }
            }
        }
        cleanBumpMap();
        createCraters(2, 20);
        createCraters(-1, 250);
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Moon-Gabriel");
        planet.setType(PlanetType.Selenian);
        planet.setTemperature(298);
        testOutput(planet, new Selenian(Server.getWorldGen(), null, null, null, 0),
                new SelenianMapper(planet));
    }
}
