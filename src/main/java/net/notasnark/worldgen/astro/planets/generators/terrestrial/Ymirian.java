/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Terrestrial.TerrestrialFeature.IceWorld;

/**
 * Lutian worlds are cold and barren Terrestrial worlds with no extant geological cycle. They will
 * probably be covered in ice, though unlike GaianGelidian worlds they were never warm enough for life.
 * They have enough of an atmosphere to produce erosion and snow storms.
 */
public class Ymirian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(Ymirian.class);

    public Ymirian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Ymirian);

        planet.setRadius(5000 + Die.die(1000, 2));
        planet.setHabitability(4);
        planet.setHydrographics(0);
        planet.setAtmosphere(Atmosphere.Nitrogen);
        planet.setPressure(Die.die(50_000) + 20_000);
        planet.setLife(Life.None);
        planet.setDayLength(30_000 + Die.die(30_000, 2));

        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, FerricOre);

        if (Die.d2() == 1) {
            planet.addFeature(IceWorld);
            addPrimaryResource(planet, Water);
        }

        planet.addTradeCode(TradeCode.Ba);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
