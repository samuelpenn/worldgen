/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.PlanetGenerator;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.utils.rpg.Die;

import java.util.ArrayList;
import java.util.List;

/**
 * Planemo worlds are rogue planets not attached to any star system. They are found in the space between
 * stars, often cold and unknown.
 */
public class Planemo extends PlanetGenerator {

    public Planemo(WorldGen worldgen, StarSystem system, Star primary, Planet previous, long distance) {
        super(worldgen, system, primary, previous, distance);
    }

    /**
     * Get a generated planet. Can't be called directly on the Belt class, because
     * we don't know exactly what type of planet to create.
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }


    @Override
    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(6_000 + Die.d6(2) * 5000 + Die.die(1_000));

        // Set default day length to be around 16 hours.
        planet.setDayLength((Die.d6(2) + 8) * 86400 + Die.die(3600, 2));

        addPrimaryResource(planet, CommodityName.Helium);

        return planet;
    }

    /**
     * Most planemo types don't have moons.
     */
    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        return new ArrayList<>();
    }

}
