/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.astro.planets.tiles.Snow;
import net.notasnark.worldgen.text.TextGenerator;

import java.io.File;
import java.io.IOException;

/**
 * Gelidian worlds are icy and dormant.
 */
public class EuropanMapper extends DwarfMapper {
    protected static final Tile ICE_PLAINS = new Tile("Ice Plains", "#EAE9D0", false, 2);
    protected static final Tile MINERALS = new Tile("Minerals", "#C06030", false, 2);

    public EuropanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public EuropanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h < 25) {
                    setTile(tileX, tileY, new Snow(MINERALS.getShaded(80 + (h / 5))));
                } else {
                    setTile(tileX, tileY, new Snow(ICE_PLAINS.getShaded(70 + (h / 3))));
                }
            }
        }

        cleanBumpMap();
        createCraters(0, 20);
        createCraters(-1, 50);
        flood(MINERALS, 5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                setTile(tileX, tileY, new Snow(getTile(tileX, tileY).getShaded(70 + (h/3))));
                if (h < 45) {
                    setHeight(tileX, tileY, 10);
                } else if (h < 75) {
                    setHeight(tileX, tileY, 50);
                } else {
                    setHeight(tileX, tileY, 90);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Foo I");
        planet.setType(PlanetType.Europan);
        planet.setRadius(1300);
        EuropanMapper p = new EuropanMapper(planet);

        p.generate();
        SimpleImage img = p.draw(2048);
        img.save(new File("/home/sam/tmp/europan.png"));

        TextGenerator tg = new TextGenerator(planet);
        System.out.println("[" + tg.getFullDescription() + "]");

    }
}
