/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.stars.Star;

import java.util.List;
import java.util.Random;

/**
 * Mapper for a disc of dust that encircles a star. This has no surface map, but does provide an
 * orbit map which is used by the star system mapper.
 */
public class DustDiscMapper extends PlanetMapper {
    protected static final Logger logger = LoggerFactory.getLogger(DustDiscMapper.class);

    public DustDiscMapper(final Planet planet, final int size) {
        super(planet, size);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public DustDiscMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public void generate() {
        // Nothing do do here.
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random  random = new Random(planet.getId() * 7);
        long    distance = planet.getDistance();
        int     numSpokes = 0;
        int     deg = 0;

        if (planet.hasTradeCode(TradeCode.Bsp)) {
            int[] spokes = new int[] { 2, 3, 3, 4, 4, 4, 6, 6, 8, 12, 15, 18, 20, 24, 30, 36 };
            numSpokes = spokes[Math.abs(random.nextInt())%spokes.length];
            deg = random.nextInt(360);
        }

        logger.info(String.format("Drawing orbit for [%s] at [%d]km scale [%d]", planet.getName(), distance, kmPerPixel));

        long    d = distance - planet.getRadius();
        while (d < distance + planet.getRadius()) {
            String colour = planet.getType().getColour();

            // Abuse the Tile class to perform some colour mixing.
            if (planet.hasTradeCode(TradeCode.Brd)) {
                colour = new Tile("Tile", colour).mix("#FF0000", 1).getRGB();
            } else if (planet.hasTradeCode(TradeCode.Bgr)) {
                colour = new Tile("Tile", colour).mix("#00FF00", 1).getRGB();
            } else if (planet.hasTradeCode(TradeCode.Bbl)) {
                colour = new Tile("Tile", colour).mix("#0000FF", 1).getRGB();
            } else if (planet.hasTradeCode(TradeCode.Bbk)) {
                colour = new Tile("Tile", colour).mix("#000000", 1).getRGB();
            }

            switch (random.nextInt(3)) {
                case 0:
                    colour = SimpleImage.getDarker(colour, 6 + random.nextInt(6));
                    break;
                case 1:
                    colour = SimpleImage.getLighter(colour, 6 + random.nextInt(6));
                    break;
                default:
                    // Colour remains unchanged.
            }
            int width = 1;
            int rnd = (int) (planet.getRadius() / (kmPerPixel * 10));
            if (rnd > 0) {
                width += random.nextInt(rnd);
            }
            int alpha = (int) (planet.getDensity() * Math.sqrt(d * 1.0 / planet.getDistance() ));

            alpha = Math.max(20, Math.min(255, alpha));
            image.circleOutline(cx, cy, (int) (d / kmPerPixel), String.format("%s%02X", colour, alpha), width);
            if (numSpokes > 0) {
                // There are visible 'spokes' in the dust disc.
                String spokeColour = colour;
                int inc = 360 / numSpokes;
                int arc = inc / 4;
                for (int s=0; s < numSpokes; s++) {
                    image.arc(cx, cy, (int) (d / kmPerPixel), String.format("%s%02X", spokeColour, (alpha*2 + 255)/3), deg + inc * s, arc);
                }
            }
            if (planet.hasTradeCode(TradeCode.Bva) && random.nextInt(4) == 1) {
                // There are obvious variable clumps of matter.
                String clumpColour = SimpleImage.getDarker(colour, 2 + random.nextInt(4));
                double offset = getAngleOffset(star, d);
                int arc = random.nextInt(20);
                image.arc(cx, cy, (int) (d / kmPerPixel), String.format("%s%02X", clumpColour, 255), (int)offset, (int) arc);
            }
            d += 1 + random.nextInt((int) (planet.getRadius() / 10));
        }

        if (moons != null) {
            for (Planet moon : moons) {
                random = new Random(moon.getId());
                d = planet.getDistance() + moon.getDistance();
                double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
                int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
                int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

                int radius = 4;
                image.circle(x, y, radius, planet.getType().getColour());

                drawPlanetLabel(image, x, y, angle, moon);
            }
        }
    }
}
