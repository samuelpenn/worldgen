/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A GeoCyclic MesoArean world of the Dwarf Terrestrial Group. GeoCylic worlds go through
 * a cycle of active/passive periods. A MesoArean world is in the active part of its cycle.
 */
public class MesoArean extends Arean {
    private static final Logger logger = LoggerFactory.getLogger(MesoArean.class);

    public MesoArean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
        switch (Die.d6(2)) {
            case 5:
                planet.addFeature(DwarfFeature.SouthCrater);
                break;
            case 6:
                planet.addFeature(DwarfFeature.EquatorialRidge);
                break;
            case 7:
                planet.addFeature(DwarfFeature.GreatRift);
                break;
            case 8:
                planet.addFeature(DwarfFeature.NorthCrater);
                break;
            case 9:
                planet.addFeature(
                        new Roller<AreanFeature>(
                                Arean.AreanFeature.YellowDesert,
                                Arean.AreanFeature.PinkDesert,
                                Arean.AreanFeature.PurpleDesert,
                                Arean.AreanFeature.BlackRocks,
                                Arean.AreanFeature.GreenRocks).roll());
            default:
                // No special features.
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.MesoArean);
        int radius = 2800 + Die.die(500, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(20_000 + Die.die(30_000, 2));
        planet.setTemperature((planet.getTemperature() + 320) / 2);
        planet.setMagneticField(MagneticField.Minimal);

        int pascals = planet.getPressure();
        // Ironically, the chance of life increases as the atmosphere thins and the seas evaporate. It's
        // a race between life evolving and the planet dying.
        int lifeBonus = 0;
        if (pascals < 40_000) {
            planet.setMagneticField(MagneticField.VeryWeak);
            planet.setHydrographics(Die.d6(3));
            planet.setTemperature(Math.max(planet.getTemperature(), 275 + Die.d6()));
            lifeBonus = 0;
        } else if (pascals < 70_000) {
            planet.setMagneticField(MagneticField.Weak);
            planet.setHydrographics(Die.d8(3));
            planet.setTemperature(Math.max(planet.getTemperature(), 280 + Die.d6(3)));
            lifeBonus = 2;
        } else {
            planet.setMagneticField(MagneticField.Weak);
            planet.setHydrographics(Die.d12(5));
            planet.setTemperature(Math.max(planet.getTemperature(), 285 + Die.d6(5)));
            lifeBonus = 5;
        }
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));

        if (planet.hasFeature(GeneralFeature.VeryHighLife)) {
            lifeBonus += 6;
        } else if (planet.hasFeature(GeneralFeature.HighLife)) {
            lifeBonus += 3;
        }

        switch (Die.d6(2) + lifeBonus) {
            case 2:
                planet.setLife(Life.None);
                break;
            case 3: case 4:
                planet.setLife(Life.Organic);
                break;
            case 5: case 6:
                planet.setLife(Life.Archaean);
                if (Die.d2() == 1) {
                    addTraceResource(planet, Oxygen);
                }
                break;
            case 7: case 8:
                planet.setLife(Life.Aerobic);
                planet.setAtmosphere(Atmosphere.LowOxygen);
                planet.setHabitability((planet.getPressure() < Pressure.Thin.getPascals())?3:2);
                addSecondaryResource(planet, Oxygen);
                break;
            case 9: case 10:
                planet.setLife(Life.ComplexOcean);
                planet.setAtmosphere(Atmosphere.Standard);
                planet.setHabitability((planet.getPressure() < Pressure.VeryThin.getPascals())?3:2);
                addPrimaryResource(planet, Oxygen);
                break;
            default:
                planet.setLife(Life.SimpleLand);
                planet.setAtmosphere(Atmosphere.Standard);
                planet.setHabitability((planet.getPressure() < Pressure.VeryThin.getPascals())?3:2);
                addPrimaryResource(planet, Oxygen);
                break;
        }
        addFeatures(planet);

        // Define basic resources for this world. Biological resources will be defined in the text template.
        addMineralResources(planet);
        generateLife(planet);
        addTertiaryResource(planet, Water);

        return planet;
    }
}
