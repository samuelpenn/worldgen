/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.Commodity;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.commodities.Frequency;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.web.Server;
import net.notasnark.worldgen.astro.planets.generators.dwarf.EuArean;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NecroGaianMapper extends TerrestrialMapper {
    public NecroGaianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected static final Tile NECROGAIAN = new Tile("Land", "#605030", false, 3);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();
        setWater();

        int continents = 4 + Die.d6();
        createContinents(continents);

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y) + 200) / 3));
                    setHeight(x, y, 0);
                } else {
                    setTile(x, y, new Rough(NECROGAIAN.getShaded((getHeight(x, y) + 100) / 2)));
                }
            }
        }

        createCraters(0, 30);
        setIceCaps();
        cleanBumpMap();

        generateFeatures(planet.getFeatures());

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int modifier = planet.getPressure() / Physics.STANDARD_PRESSURE;

        String cloudColour = "#FEFEFE";

        int lowerLimit = 45;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, modifier + (h * 2) / 3);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }


    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Berlichingen");
        planet.setType(PlanetType.NecroGaian);
        planet.setTemperature(320);
        planet.setAtmosphere(Atmosphere.Primordial);
        planet.setPressure(100_000);
        planet.setHydrographics(15);

        planet.setLife(Life.Organic);
        planet.addResource(new Commodity() {{
            setName(CommodityName.OrganicChemicals.getName());
            setFrequency(Frequency.COMMON);
        }}, 500);

        PlanetMapper p = new NecroGaianMapper(planet);

        testOutput(planet, new EuArean(Server.getWorldGen(), null, null, null, 0),
                new NecroGaianMapper(planet));
    }
}
