/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Enceladusian;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * Gelidian worlds are icy and dormant.
 */
public class EnceladusianMapper extends DwarfMapper {
    protected static final Tile ICE_PLAINS = new Tile("Ice Plains", "#FAFAFA", false, 2);

    public EnceladusianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public EnceladusianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                setTile(tileX, tileY, ICE_PLAINS.getShaded(70 + (h / 3)));
            }
        }

        cleanBumpMap();
        createCraters(-4, 50);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h < 45) {
                    setHeight(tileX, tileY, 35);
                } else if (h < 75) {
                    setHeight(tileX, tileY, 50);
                } else {
                    setHeight(tileX, tileY, 65);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Gloire VI v");
        planet.setType(PlanetType.Enceladusian);
        planet.setRadius(1300);
        EnceladusianMapper p = new EnceladusianMapper(planet);

        testOutput(planet, new Enceladusian(Server.getWorldGen(), null, null, null, 0),
                new EnceladusianMapper(planet));
    }
}
