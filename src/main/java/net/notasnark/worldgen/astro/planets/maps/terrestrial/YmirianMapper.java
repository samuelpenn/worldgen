/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.Ymirian;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.SnowCover;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class YmirianMapper extends TerrestrialMapper {
    public YmirianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected final Tile MOUNTAINS = new Tile("Mountains", "#906045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#D08055", false, 3);
    protected final Tile ICE = new Tile("Desert", "#FEFEFE", false, 2);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (planet.hasFeature(Terrestrial.TerrestrialFeature.IceWorld)) {
                    if (getHeight(x, y) > 85) {
                        setTile(x, y, new SnowCover(MOUNTAINS.getShaded(getHeight(x, y))).cover(50));
                    } else {
                        setTile(x, y, new Rough(ICE.getShaded((getHeight(x, y) + 100) / 2)));
                    }
                } else {
                    if (getHeight(x, y) > 85) {
                        setTile(x, y, MOUNTAINS.getShaded(getHeight(x, y)));
                    } else {
                        setTile(x, y, PLAINS.getShaded((getHeight(x, y) + 100) / 2));
                        if (Die.d100() > getHeight(x, y)) {
                            setTile(x, y, new SnowCover(getTile(x, y)).cover(50 - getHeight(x, y)));
                        }
                    }
                }
            }
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 0);
                } else {
                    int h = getHeight(x, y);
                    if (h < 90) {
                        h = 50;
                    } else {
                        h = 100;
                    }
                    setHeight(x, y, h);
                }
            }
        }
        createCraters(1, 10);

        generateFeatures(planet.getFeatures());
//        cleanBumpMap();

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        int modifier = planet.getPressure() / Physics.STANDARD_PRESSURE;

        String cloudColour = "#F0C0C0";

        int lowerLimit = 5;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, modifier + h / 2);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }


    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }


    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("Ymirian");
        planet.setType(PlanetType.Ymirian);
        planet.setHydrographics(0);
        planet.setTemperature(250);
        planet.setPressure(50_000);
        planet.setAtmosphere(Atmosphere.Nitrogen);
        planet.setLife(Life.None);

        testOutput(planet, new Ymirian(Server.getWorldGen(), null, null, null, 0),
                new YmirianMapper(planet));
    }
}
