/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.tiles;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;

/**
 * Add a vegetation layer to a tile. This normally displays as greens, but can vary based on type of
 * vegetation and solar type.
 */
public class Vegetation extends Tile {
    private String  heavyColour = "#004400";
    private int     heavyCover = 50;

    private String  lightColour = "#44AA44";
    private int     lightCover = 75;

    public Vegetation(Tile tile) {
        super(tile);
    }

    public Vegetation heavy(String colour, int level) {
        this.heavyColour = colour;
        this.heavyCover = level;

        return this;
    }

    public Vegetation light(String colour, int level) {
        this.lightColour = colour;
        this.lightCover = level;

        return this;
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        int height = Math.abs(h);
        for (int yy = 0; yy <= height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx <= width; xx++) {
                if (Die.d100() <= heavyCover) {
                    image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), heavyColour);
                } else if (Die.d100() <= lightCover) {
                    image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), lightColour);
                }
            }
        }
    }
}
