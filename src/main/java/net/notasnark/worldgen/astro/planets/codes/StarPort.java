/*
 * Copyright (c) 2007, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.codes;

/**
 * Starport classification for a world. This uses the standard Traveller
 * classifications, from A (best) to E (worst) and X as no starport at all.
 *
 * Starports also have a minimum Tech Level, which is required to support a
 * starport of the given type.
 *
 * @author Samuel Penn
 */
public enum StarPort {
	// Starports
	A(10, 1_000_000, "A", "Major"),
	Ao(10, 1_000_000, "A", "Major Orbital"),
	B(9,  100_000, "B", "Large"),
	Bo(9, 100_000, "B", "Large Orbital"),
	C(8,  10_000, "C", "Medium"),
	Co(8, 10_000, "C", "Medium Orbital"),
	D(7,  1_000, "D", "Small"),
	Do(7, 1_000, "D", "Small Orbital"),
	E(5,  0, "E", "Minimal"),
	Eo(5, 0, "E", "Minimal Orbital"),
	// In-system spaceports
	F(7, 0, "F", "Spaceport"),
	Fo(9, 0, "F", "Spaceport Orbital"),
	G(6, 0, "G", "Spaceport"),
	Go(8, 0, "G", "Spaceport Orbital"),
    H(5, 0, "H", "Spaceport"),
    Ho(7, 0, "H", "Spaceport Orbital"),
	// Nothing available
	X(0,  0, "X", "None");

	final private int		minTechLevel;
	final private int		minPopulation;
	final private String    code;
	final private String	description;

	StarPort(int minTechLevel, int minPopulation, String code, String description) {
		this.minTechLevel = minTechLevel;
		this.minPopulation = minPopulation;
		this.code = code;
		this.description = description;
	}

	/**
	 * Get the minimum tech level that is required to support a starport of this
	 * type. Large starports generally require a good technology base to be
	 * built upon, plus provide the resources to boost the planet's own
	 * technology.
	 */
	public int getMinimumTechLevel() {
		return minTechLevel;
	}

	/**
	 * Large starports not only require a large number of people to keep them
	 * working, they attract a large number of people as well, since they tend
	 * to be trade hubs.
	 */
	public int getMinimumPopulation() {
		return minPopulation;
	}

	/**
	 * Gets the star port type one better than this one. A Star port type of A
	 * will return A.
	 */
	public StarPort getBetter() {
		switch (this) {
            case A:
            case B:
                return A;
            case C:
                return B;
            case D:
                return C;
            case E:
                return D;
			case F:
				return D;
			case G:
				return F;
            case H:
                return G;
            case X:
                return H;
            case Ao:
            case Bo:
                return Ao;
            case Co:
                return Bo;
            case Do:
                return Co;
            case Eo:
                return Do;
			case Fo:
				return Do;
			case Go:
				return Fo;
            case Ho:
                return Go;
        }

		return E;
	}

	/**
	 * Gets the star port type one worse than this one. A star port type of X
	 * will return X.
	 */
	public StarPort getWorse() {
		switch (this) {
            case A:
                return B;
            case B:
                return C;
            case C:
                return D;
            case D:
                return E;
            case E:
                return G;
			case F:
				return G;
			case H:
				return X;
            case Ao:
                return Bo;
            case Bo:
                return Co;
            case Co:
                return Do;
            case Do:
                return Eo;
			case Eo:
				return Go;
			case Fo:
				return Go;
			case Go:
				return Ho;
			case Ho:
				return X;
        }

		return X;
	}

	/**
	 * Get the local spaceport equivalent of a starport. If it is already a spaceport, just returns
	 * itself. Otherwise converts a Starport (A-E) into a Spaceport (F-H).
	 *
	 * @return		Local equivalent of this starport.
	 */
	public StarPort getLocal() {
		switch (this) {
			case A:
			case B:
			case C:
				return F;
			case D:
				return G;
			case E:
				return H;
			case Ao:
			case Bo:
			case Co:
				return Fo;
			case Do:
				return G;
			case Eo:
				return H;
			case X:
				return X;
		}
		return this;
	}

	/**
	 * Gets the standard rating for this type of starport, without any suffix.
	 *
	 * @return	Get standard rating code.
	 */
	public StarPort getStandard() {
		return StarPort.valueOf(getCode());
	}

	public boolean isBetterThan(StarPort port) {
		return ordinal() < port.ordinal();
	}

	public boolean isWorseThan(StarPort port) {
		return ordinal() > port.ordinal();
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
	    return code;
    }
}
