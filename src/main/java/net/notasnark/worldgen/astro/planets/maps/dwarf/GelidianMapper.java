/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Gelidian;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.Snow;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * Gelidian worlds are icy and dormant.
 */
public class GelidianMapper extends DwarfMapper {
    protected static final Tile ICE_PLAINS = new Tile("Ice Plains", "#D2D0CB", false, 2);
    protected static final Tile HIGHLANDS = new Tile("Highlands", "#E0E0E0", false, 2);

    public GelidianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public GelidianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h > 70) {
                    setTile(tileX, tileY, HIGHLANDS.getShaded(80 + (h / 5)));
                } else {
                    setTile(tileX, tileY, new Snow(ICE_PLAINS.getShaded(70 + (h / 3))));
                }
            }
        }

        cleanBumpMap();
        createCraters(2, 5);
        createCraters(1, 20);
        createCraters(-1, 50);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                if (h < 15) {
                    setTile(tileX, tileY, new Rough(ICE_PLAINS.getShaded(70 + (h / 3))));
                    setHeight(tileX, tileY, 10);
                } else if (h < 70) {
                    setHeight(tileX, tileY, 50);
                } else {
                    setHeight(tileX, tileY, 90);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Wildeman");
        planet.setType(PlanetType.Gelidian);
        planet.setRadius(1600);

        testOutput(planet, new Gelidian(Server.getWorldGen(), null, null, null, 0),
                new GelidianMapper(planet));

    }
}
