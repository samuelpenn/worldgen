/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.belt;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Belt;
import net.notasnark.worldgen.astro.planets.generators.SmallBody;

import java.util.ArrayList;
import java.util.List;

import static net.notasnark.worldgen.astro.planets.generators.Belt.BeltFeature.*;

/**
 * Oort Clouds are found out in the far reaches of a solar system, possibly even outside what is normally
 * considered the solar system. They consist of billions, or trillions, of small icy proto-comets.
 */
public class OortCloud extends Belt {
    private static final Logger logger = LoggerFactory.getLogger(OortCloud.class);

    public enum OortCloudFeatures implements PlanetFeature {
        InnerOort,
        OuterOort;
    }

    public OortCloud(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
    }

    public Planet getPlanet(String name) {
        Planet  planet = definePlanet(name, PlanetType.OortCloud);
        long    radius = planet.getDistance() / 2;
        int     density = 15 + Die.dieV(10);

        planet.setTemperature(Physics.getTemperatureOfOrbit(star, distance));

        addFeatures(planet);

        radius = checkDistance(planet, radius);

        planet.setRadius(radius);
        planet.setDensity(density);

        addPrimaryResource(planet, CommodityName.Water);
        addTraceResource(planet, CommodityName.CarbonicOre);

        return planet;
    }

    /**
     * Gets a list of moons for this belt. In a belt, a 'moon' is a significantly larger
     * asteroid that is part of the main belt, rather than something that orbits the
     * primary.
     *
     * @param primary   Belt the moons belong to.
     * @return          Array of planets (moons), may be empty.
     */
    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<Planet>();

        int numberOfMoons = Die.d6(3) - 14;

        logger.info(String.format("getMoons (OortCloud): Creating [%d] moons for [%s]", numberOfMoons, primary.getName()));
        if (numberOfMoons > 0) {
            long variance = primary.getRadius() / numberOfMoons;
            long distance = 0 - primary.getRadius();
            switch (numberOfMoons) {
                case 1:
                    distance = Die.dieV(primary.getRadius());
                    variance = primary.getRadius() / 100;
                    break;
                case 2:
                    distance = 0 - Die.die(primary.getRadius());
                    variance = primary.getRadius() / 2;
                    break;
                default:
                    // As above.
            }

            for (int m=0; m < numberOfMoons; m++) {
                String name = StarSystemFactory.getPlanetoidName(primary.getName(), m + 1);

                logger.info("Adding moon " + name);

                // We only want to call out an asteroid as a 'moon' if it is particularly large,
                // so ensure that the planetoid generated is of sufficient size to be interesting.
                List<PlanetFeature> features = new ArrayList<PlanetFeature>();
                switch (Die.d6(2) - (primary.hasFeature(Planetoids)?2:0)) {
                    case 0:
                        // Not really an asteroid at this size.
                        features.add(SmallBody.SmallBodyFeature.Gigantic);
                        break;
                    case 1: case 2: case 3: case 4:
                        // About Ceres sized.
                        features.add(SmallBody.SmallBodyFeature.Huge);
                        break;
                    default:
                        // About Vesta sized.
                        features.add(SmallBody.SmallBodyFeature.Large);
                        break;
                }

                PlanetType type = PlanetType.Oort;

                Planet moon = factory.createMoon(system, star, name, type,
                        distance + Die.dieV((int) (variance / 5)), primary,
                        features.toArray(new PlanetFeature[0]));

                moons.add(moon);
                distance += variance;
            }
        }

        return moons;
    }
}
