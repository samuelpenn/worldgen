/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Dwarf.DwarfFeature.*;
import static net.notasnark.worldgen.astro.planets.generators.Dwarf.DwarfFeature.MetallicSea;

/**
 * ProtoLithian worlds are still in a process of forming. This may be due to being very young, or
 * because they haven't had the opportunity to cool down due to intense solar radiation. They may
 * have an atmosphere due to it being replenished due to outgassing.
 */
public class ProtoLithian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(ProtoLithian.class);

    public ProtoLithian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
        if (planet.getTemperature() > Temperature.SilicatesMelt.getKelvin()) {
            planet.addFeature(DwarfFeature.MoltenSurface);
        } else if (planet.getTemperature() > Temperature.IronMelts.getKelvin()) {
            planet.addFeature(DwarfFeature.MoltenMetals);
        } else if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            planet.addFeature(DwarfFeature.MetallicSea);
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.ProtoLithian);
        int radius = 2000 + Die.die(500, 2);

        if (planet.hasFeature(DwarfFeature.Small)) {
            radius *= 0.67;
        } else if (planet.hasFeature(DwarfFeature.Large)) {
            radius *= 1.5;
        }

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.addTradeCode(TradeCode.Ba);
        planet.setPressure(0);
        planet.setHabitability(5);

        if (planet.hasFeature(GeneralFeature.VeryYoung)) {
            planet.setDayLength(30_000 + Die.die(30_000, 2));
        } else if (planet.hasFeature(GeneralFeature.Young)) {
            planet.setDayLength(50_000 + Die.die(100_000, 2));
        } else {
            switch (Die.d6()) {
                case 1:
                    planet.setDayLength((int) (planet.getPeriod() * 1.5));
                    break;
                case 2:
                    planet.setDayLength((int) (planet.getPeriod() * (2.0 / 3.0)));
                    break;
                default:
                    planet.setDayLength(planet.getPeriod() / 3 + Die.die((int) planet.getPeriod()) / 2);
                    break;
            }
        }
        modifyTemperatureByRotation(planet);

        // This is a hot world, regardless of where it is relative to its star.
        if (planet.hasFeature(GeneralFeature.VeryYoung)) {
            int kelvin = Temperature.SilicatesMelt.getKelvin() + Die.die(500, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));
            planet.setHabitability(6);

            planet.setAtmosphere(Die.d3() == 1?Atmosphere.Exotic:Atmosphere.Hydrogen);
            planet.setPressure(75_000 + Die.die(200_000));
        } else if (planet.hasFeature(GeneralFeature.Young)) {
            int kelvin = Temperature.IronMelts.getKelvin() + Die.die(300, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));
            planet.setHabitability(6);

            planet.setAtmosphere(Die.d2() == 1?Atmosphere.Exotic:Atmosphere.Hydrogen);
            planet.setPressure(50_000 + Die.die(100_000));
        } else {
            int kelvin = Temperature.LeadMelts.getKelvin() + Die.die(150, 2);
            planet.setTemperature(Math.max(kelvin, planet.getTemperature()));
            planet.setNightTemperature(Math.max(kelvin - Die.d6(2), planet.getNightTemperature()));

            planet.setAtmosphere(Atmosphere.Exotic);
            planet.setPressure(10_000 + Die.die(50_000));
        }

        // Shift night temperature to be more like the day, just in case it isn't. Enough convection
        // processes in the mantle to shift heat around.
        planet.setNightTemperature((planet.getTemperature() + planet.getNightTemperature()) / 2);

        setAutomaticFeatures(planet);
        addFeatures(planet);

        // Molten metallic core provides some degree of magnetic field.
        switch (Die.d6(3)) {
            case 3: case 4: case 5:
                planet.setMagneticField(MagneticField.Weak);
                break;
            case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.VeryWeak);
                break;
            case 9: case 10: case 11:
                planet.setMagneticField(MagneticField.Minimal);
            default:
                planet.setMagneticField(MagneticField.None);
        }

        // Define resources for this world.
        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addSecondaryResource(planet, FerricOre);
        if (planet.hasFeature(MetallicSea) || planet.hasFeature(MoltenMetals)) {
            addPrimaryResource(planet, HeavyMetals);
            addTertiaryResource(planet, Radioactives);
            addTertiaryResource(planet, PreciousMetals);
        } else {
            addSecondaryResource(planet, HeavyMetals);
        }

        return planet;
    }
}
