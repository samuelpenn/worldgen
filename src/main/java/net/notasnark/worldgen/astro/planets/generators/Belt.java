/**
 * Belt.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.PlanetGenerator;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;

/**
 * A Belt is the high level Group for all types of asteroid belts, planetary rings and
 * similar natural objects which consist of a large collection of very small bodies.
 */
public class Belt extends PlanetGenerator {

    public enum BeltFeature implements PlanetFeature {
        ThinRing,
        WideRing,
        WideSparseRing,
        Planetoids,
        HotGas,
        GasOnly,
        Dust,
        ProtoPlanets
    }

    public Belt(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Checks that the width of the belt is not greater than the distance to the primary.
     * If it is, returns a reduced belt width which is compatible with the distance.
     *
     * @param planet    Belt that is being checked.
     * @param radius    Radius of the belt in kilometres.
     *
     * @return          New suggested radius in kilometres.
     */
    protected long checkDistance(Planet planet, long radius) {
        long previousDistance = getPreviousDistance();

        if (previousDistance + (radius * 1.3) > planet.getDistance()) {
            planet.setDistance(previousDistance + (long)(radius * 1.3));
        }

        return Math.min(radius, planet.getDistance() / 4);
    }

    /**
     * Get a generated planet. Can't be called directly on the Belt class, because
     * we don't know exactly what type of planet to create.
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet = definePlanet(name, type);

        // Radius of a Belt is in km, and represents its width.
        long radius = (Die.d6(3) * Physics.MKM);

        if (distance < 10 * Physics.MKM) {
            radius /= 2;
        } else if (distance < 25 * Physics.MKM) {
            // No modifier.
        } else if (distance < 100 * Physics.MKM) {
            radius *= 3;
            radius += Die.d3(); // Not always a multiple of 3.
        } else {
            radius *= 5;
            radius += Die.d10() * Physics.MKM; // Not always a multiple of 5.
        }
        radius = checkDistance(planet, radius);
        planet.setRadius(radius);

        return planet;
    }
}
