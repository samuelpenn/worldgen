/**
 * JovicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.utils.rpg.Roller;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.jovian.Neptunian;
import net.notasnark.worldgen.astro.planets.generators.jovian.Poseidonic;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A cold Jovian world, found in the outer solar system.
 */
public class PoseidonicMapper extends JovianMapper {

    protected static final Tile BLUE = new Tile("Blue", "#A0A0F0", false, 6);
    protected static final Tile BLUISH = new Tile("Bluish", "#C0C0F0", false, 6);
    protected static final Tile LIGHT = new Tile("Light", "#D0D0FF", false, 4);
    protected static final Tile WHITE = new Tile("White", "#F0F0F0", false, 2);

    public PoseidonicMapper(final Planet planet, final int size) {
        super(planet, size);
    }
    public PoseidonicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        if (planet.hasFeature(Poseidonic.PoseidonicFeatures.WhiteMarble)) {
            return WHITE;
        } else if (planet.hasFeature(Poseidonic.PoseidonicFeatures.BlueMarble)) {
            return new Roller<Tile>(BLUE, BLUISH, BLUISH, LIGHT).roll();
        } else {
            return new Roller<Tile>(BLUISH, BLUISH, BLUISH, WHITE, WHITE, LIGHT).roll();
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            return nextColour;
        }
        if (previousColour == null || Die.d3() == 1) {
            return getRandomColour();
        }
        return previousColour.getVariant(Die.d8() - Die.d8());
    }

    private static final int H = 52;

    public void cloudFormations() {
        if (planet.hasFeature(Neptunian.NeptunianFeatures.WhiteBands)) {
            int s = getNumRows() / 2 + Die.dieV(20);
            int h = Die.d4(2);

            Tile mix = getTile(0, s);
            for (int y = s; y < s + h; y++) {
                mix = mix.getMix(WHITE);
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
            for (int y = s + h; y < s + h * 2; y++) {
                mix = mix.getMix(getTile(0, y));
                for (int x=0; x < getWidthAtY(y); x++) {
                    setHeight(x, y, H);
                    setTile(x, y, mix);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Poseidonic");
        planet.setType(PlanetType.Poseidonic);
        planet.setRadius(25_000);
        //planet.addFeature(BlueMarble);

        testOutput(planet, new Poseidonic(Server.getWorldGen(), null, null, null, 0),
                new PoseidonicMapper(planet));
    }
}
