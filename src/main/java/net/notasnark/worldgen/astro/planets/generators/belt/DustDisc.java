/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.belt;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Temperature;
import net.notasnark.worldgen.astro.planets.generators.Belt;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;
import static net.notasnark.worldgen.astro.planets.generators.Belt.BeltFeature.*;

/**
 * A DustDisc is a circumstellar class object of the Belt group. It represents a wide ring
 * of dust, gas and planetesimals that doesn't contain a significant number of objects of
 * sufficient size to be named or numbered. The majority of objects are in the millimetre
 * or centimetre size, rather than metres or kilometres.
 */
public class DustDisc extends Belt {
    public DustDisc(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.DustDisc);
        long radius = (int) (Die.d6(3) * Math.pow(distance, 0.8));

        // If the radius is too close relative to the orbital distance, then it causes all sorts of weirdness
        // in temperature differences, so try and avoid that.
        if (radius > distance / 3) {
            radius = distance / 3;
        }

        // Move the belt out, if it would collide with a previous planet. Need to recalculate base temperature
        // if we do this.
        if (getPreviousDistance() + 5_000_000 > distance - radius) {
            distance = (int) (getPreviousDistance() + 5_000_000 + radius * 1.2);
            setTemperature(planet);
        }

        // Temperature will be slightly hotter due to left over heat after creation.
        int k = planet.getTemperature();
        if (planet.hasFeature(HotGas)) {
            k *= 3.0;
            if (k < Temperature.IronMelts.getKelvin()) {
                planet.removeFeature(HotGas);
                planet.addFeature(GasOnly);
            }
        } else if (planet.hasFeature(GasOnly)) {
            k *= 1.5;
        } else if (planet.hasFeature(Dust)) {
            k *= 1.2;
        } else if (planet.hasFeature(ProtoPlanets)) {
            k *= 1.2;
        } else {
            planet.addFeature(Dust);
            k *= 1.2;
        }
        planet.setTemperature((int) k);
        planet.setNightTemperature(planet.getTemperature());

        radius = (int) Physics.round(radius, 4);
        planet.setRadius(radius);

        // What is the density a representation of?
        planet.setDensity((int) (100 * Math.sqrt(star.getMass()) * Math.sqrt((1.0 * Physics.AU) / planet.getDistance()) ));

        // Resources
        if (planet.hasFeature(HotGas)) {
            addPrimaryResource(planet, Hydrogen);
            if (planet.hasFeature(GeneralFeature.NoMetals)) {
                // Nothing
            } else if (planet.hasFeature(GeneralFeature.LowMetals)) {
                addTertiaryResource(planet, ExoticGases);
            } else {
                addPrimaryResource(planet, ExoticGases);
            }
        } else if (planet.hasFeature(GasOnly)) {
            addPrimaryResource(planet, Hydrogen);
            if (planet.hasFeature(GeneralFeature.HighMetals)) {
                addTertiaryResource(planet, ExoticGases);
            } else if (planet.hasFeature(GeneralFeature.LowMetals)) {
                addTraceResource(planet, ExoticGases);
            }
        }


        if (planet.getTemperature() < 300 && Die.d2() == 1) {
            addSecondaryResource(planet, Water);
        } else if (planet.getTemperature() < 400 && Die.d2() == 1) {
            addTertiaryResource(planet, Water);
        } else if (planet.getTemperature() < 400) {
            addTraceResource(planet, Water);
        }
        if (Die.d3() == 1) {
            addTraceResource(planet, SilicateOre);
        }
        if (Die.d6() == 1) {
            addTraceResource(planet, CarbonicOre);
        }

        return planet;
    }
}
