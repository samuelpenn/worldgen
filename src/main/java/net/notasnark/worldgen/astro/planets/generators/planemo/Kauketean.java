/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.planemo;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Planemo;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;

/**
 * These are large, rocky Planemo worlds with a dense atmosphere and scorched surfaces.
 */
public class Kauketean extends Planemo {
    private static final Logger logger = LoggerFactory.getLogger(Kauketean.class);

    public enum KauketeanFeature implements PlanetFeature {
        WaterOcean
    }

    public Kauketean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(8_000 + Die.die(2_500, 3));

        planet.setAtmosphere(Atmosphere.InertGases);
        planet.setPressure(Physics.STANDARD_PRESSURE * Die.d6(5) + Die.die(Physics.STANDARD_PRESSURE) * 5);
        planet.setMagneticField(MagneticField.Standard);
        planet.setHabitability(6);

        planet.setDayLength(9 * Physics.HOUR + Die.die(60) * Physics.HOUR + Die.die(Physics.HOUR));

        planet.setTemperature(350 + Die.d100(2));
        planet.setNightTemperature(planet.getTemperature());
        if (Die.d6() == 1) {
            planet.setHydrographics(Die.d20(3));
            planet.addFeature(KauketeanFeature.WaterOcean);
        }

        addPrimaryResource(planet, CommodityName.Helium);
        addPrimaryResource(planet, CommodityName.SilicateOre);
        addSecondaryResource(planet, CommodityName.FerricOre);
        addTraceResource(planet, CommodityName.CarbonicOre);
        addTertiaryResource(planet, CommodityName.Radioactives);
        addSecondaryResource(planet, CommodityName.HeavyMetals);
        addTraceResource(planet, CommodityName.PreciousMetals);
        addTraceResource(planet, CommodityName.ExoticMetals);

        planet.addTradeCode(TradeCode.Ba);

        return planet;
    }
}
