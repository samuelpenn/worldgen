/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Bathic;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.web.Server;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Temperature;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Bathic worlds are water covered.
 */
public class BathicMapper extends DwarfMapper {
    protected static final Tile WATER = new Tile("Water", "#9090F0", true, 2);
    protected static final Tile ICE = new Tile("Ice", "#FEFEFE", true, 2);

    public BathicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public BathicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);

        // Sea scape, with possible ice.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                int k = getTemperature(tileX, tileY);
                int lat = getLatitude(tileY);

                // Planet is either completely frozen, or no ice at all.
                if (planet.hasFeature(Bathic.BathicFeature.Frozen)) {
                    k = 0;
                } else if (planet.hasFeature(Bathic.BathicFeature.PolarCaps)) {
                    k -= (lat - 75);
                } else if (planet.hasFeature(Bathic.BathicFeature.Cold)) {
                    k -= (lat - 55) ;
                } else if (planet.hasFeature(Bathic.BathicFeature.MostlyFrozen)) {
                    if (lat > 15) {
                        k -= (lat - 15);
                    } else {
                        k += (15 - lat);
                    }
                    if (Die.d20() == 1) {
                        k -= Die.d10();
                    }
                } else {
                    k = 500;
                }

                if (k < Temperature.WaterFreezes.getKelvin()) {
                    setTile(tileX, tileY, ICE.getShaded(80 + h / 5));
                    setHeight(tileX, tileY, 50 + h/2);
                } else {
                    h = 80 + h / 5;
                    setTile(tileX, tileY, WATER.getShaded(h));
                    setHeight(tileX, tileY, 0);
                }
            }
        }

        cleanBumpMap();

        hasHeightMap = true;
        hasCloudMap = true;

    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();
        cloudHeight = 5;

        if (planet.getPressure() < 1_000) {
            // No significant atmosphere, so don't show anything.
            return clouds;
        }

        Icosahedron cloud = getCloudLayer();
        int modifier = planet.getPressure() / 2_500;

        String cloudColour = "#F5F5F5";

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y) / 2;
                if (h > 35) {
                    cloud.setHeight(x, y, (h + modifier) / 2);
                } else {
                    cloud.setHeight(x, y, 0);
                }
            }
        }
        clouds.add(Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width));

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Bathic");
        planet.setType(PlanetType.Bathic);
        planet.setRadius(2600);
        planet.setTemperature(280);
        planet.setAtmosphere(Atmosphere.LowOxygen);
        planet.setLife(Life.Aerobic);
        planet.setPressure(70000);

        testOutput(planet, new Bathic(Server.getWorldGen(), null, null, null, 0),
                new BathicMapper(planet));

    }
}
