/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;

import static net.notasnark.worldgen.astro.commodities.CommodityName.SilicateCrystals;
import static net.notasnark.worldgen.astro.commodities.CommodityName.SilicateOre;
import static net.notasnark.worldgen.astro.commodities.CommodityName.Water;

/**
 * Cerean worlds are a subtype of Dwarf Terrestrial Lithic worlds. They are rocky silicate worlds
 * rich in ices and other volatiles.
 */
public class Cerean extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Cerean.class);

    public Cerean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Cerean);
        int radius = 350 + Die.die(200);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.setPressure(Pressure.None);
        planet.setMagneticField(MagneticField.None);

        setAutomaticFeatures(planet);

        // Define resources for this world.
        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, Water);
        addTertiaryResource(planet, SilicateCrystals);

        return planet;
    }
}
