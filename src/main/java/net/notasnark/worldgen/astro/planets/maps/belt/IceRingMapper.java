/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.stars.Star;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Mapper for an ice ring around a planet.
 */
public class IceRingMapper extends AsteroidBeltMapper {
    protected static final Logger logger = LoggerFactory.getLogger(IceRingMapper.class);

    public IceRingMapper(final Planet planet, final int size) {
        super(planet, size);

        hasMainMap = false;
        hasOrbitMap = true;
        hasRingMap = true;
    }

    public IceRingMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);

        hasMainMap = false;
        hasOrbitMap = true;
        hasRingMap = true;
    }

    public void generate() {
        // Nothing do do here.
    }

    public SimpleImage drawRing(int width) {
        SimpleImage image = new SimpleImage(width, width);

        int opacity = 0;
        boolean gap = false;
        for (int y=0; y < width; y++) {
            if (gap || y > width - 200) {
                opacity = Math.max(0, opacity - Die.d2());
            } else if (opacity < 250) {
                opacity = Math.min(250, opacity + Die.d2());
            }

            if (!gap && opacity > 240 && Die.die(200) == 1) {
                gap = true;
            } else if (gap && opacity < 150 && Die.d12() == 1) {
                gap = false;
            }
            String colour = String.format("#%02x%02x%02x%02x",
                    230 + Die.d20(), 230 + Die.d20(), 200 + Die.d20(), opacity);
            image.line(0, y, width - 1, y, colour);
        }

        return image;
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random  random = new Random(planet.getId());

        String colour = planet.getType().getColour();
        if (isFaded) {
            colour = SimpleImage.getLighter(colour, 3);
        }
        image.circleOutline(cx, cy, (int) (planet.getDistance() / kmPerPixel), colour, planet.getRadius() / kmPerPixel);
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        PlanetMapper p = new IceRingMapper(planet,12);

        p.generate();
        SimpleImage img = p.drawRing(2048);
        img.save(new File("/home/sam/tmp/IceRing.png"));

    }
}
