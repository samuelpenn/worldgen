/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

public class Arean extends Dwarf {

    public enum AreanFeature implements PlanetFeature {
        RedDesert,
        YellowDesert,
        PurpleDesert,
        BrownDesert,
        GreyDesert,
        WhiteDesert,
        PinkDesert,
        BlackRocks,
        WhiteRocks,
        GreenRocks,
        SaltDesert,
        DarkSea,
        BlackSea,
        MurkySea,
        SaltPlains,
        EquatorialSea,
        SouthernSea,
        NorthernSea, SmallContinents;
    }

    public Arean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Set organic resources for a world with a life level of None.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeNone(Planet planet) {
        switch (Die.d6()) {
            case 6:
                // Some basic organics. Not enough to count as 'Organic'.
                addTraceResource(planet, OrganicChemicals);
                break;
        }
    }

    /**
     * Set organic resources for a world with a life level of Organic. They will be rich in the precursors to
     * life, and in their later stages may have formed basic uni-cellular organisms, though they are not yet
     * widespread enough for the world to be considered 'Archaean'.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeOrganic(Planet planet) {
        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5: case 6:
                // None: Just simple organic chemicals.
                addSecondaryResource(planet, OrganicChemicals);
                break;
            case 7: case 8: case 9: case 10:
                // None: Just simple organic chemicals.
                addPrimaryResource(planet, OrganicChemicals);
                break;
            case 11: case 12:
                addPrimaryResource(planet, OrganicChemicals);
                addTraceResource(planet, Prokaryotes);
                break;
        }
    }

    /**
     * Archaean worlds have basic life, but haven't evolved into oxygen breathing life forms yet. The formation
     * of Cyanobacteria, which begins the process of oxygen generation, is relatively rapid when it does occur.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeArchaean(Planet planet) {
        addSecondaryResource(planet, OrganicChemicals);
        addPrimaryResource(planet, Prokaryotes);

        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5: case 6:
                addSecondaryResource(planet, Prokaryotes);
                break;
            case 7: case 8: case 9:
                addPrimaryResource(planet, Prokaryotes);
                if (Die.d8() == 1) {
                    planet.addFeature(Terrestrial.TerrestrialFeature.BacterialMats);
                }
                break;
            case 10: case 11:
                addPrimaryResource(planet, Prokaryotes);
                addTraceResource(planet, Algae);
                break;
            case 12:
                addPrimaryResource(planet, Prokaryotes);
                if (Die.d8() == 1) {
                    if (Die.d3() == 1) {
                        planet.addFeature(Terrestrial.TerrestrialFeature.BorderedInBlack);
                    } else {
                        planet.addFeature(Terrestrial.TerrestrialFeature.BorderedInGreen);
                    }
                    addPrimaryResource(planet, Algae);
                } else {
                    addSecondaryResource(planet, Algae);
                }
                break;
        }
    }

    /**
     * Aerobic worlds have a significant amount of Oxygen in their atmospheres.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeAerobic(Planet planet) {
        addPrimaryResource(planet, Prokaryotes);

        switch (Die.d6(2)) {
            case 2: case 3:
                addTertiaryResource(planet, Protozoa);
                break;
            case 4: case 5:
                addSecondaryResource(planet, Protozoa);
                break;
            case 6: case 7:
                addPrimaryResource(planet, Protozoa);
                addTertiaryResource(planet, Metazoa);
                break;
            case 8:
                addPrimaryResource(planet, Protozoa);
                addSecondaryResource(planet, Metazoa);
                break;
            case 9:
                addPrimaryResource(planet, Metazoa);
                addSecondaryResource(planet, Protozoa);
                addTertiaryResource(planet, Algae);

                if (Die.d3() == 1) addTraceResource(planet, Sponges);
                if (Die.d6() == 1) addTraceResource(planet, Molluscs);
                if (Die.d6() == 1) addTraceResource(planet, Crawlers);
                if (Die.d4() == 1) addTraceResource(planet, Jellies);
                if (Die.d3() == 1) addTraceResource(planet, Kelp);
                break;
            case 12:
                addTraceResource(planet, Fish);
                // Fall through.
            default:
                addPrimaryResource(planet, Metazoa);
                addTertiaryResource(planet, Protozoa);
                addSecondaryResource(planet, Algae);
                if (Die.d2() == 1) addTertiaryResource(planet, Sponges);
                if (Die.d6() == 1) addTertiaryResource(planet, Molluscs);
                if (Die.d4() == 1) addTertiaryResource(planet, Crawlers);
                if (Die.d3() == 1) addTertiaryResource(planet, Jellies);

                // Determine primary life type.
                switch (Die.d4(2)) {
                    case 2:
                        addSecondaryResource(planet, Molluscs);
                        break;
                    case 3: case 4:
                        addSecondaryResource(planet, Sponges);
                        break;
                    case 5:
                        addSecondaryResource(planet, Jellies);
                        break;
                    case 6:
                        addSecondaryResource(planet, Crawlers);
                        break;
                    default:
                        // Nothing.
                }
                if (Die.d6() == 1) {
                    addSecondaryResource(planet, Kelp);
                } else {
                    addTertiaryResource(planet, Kelp);
                }
                break;
        }
    }

    /**
     * Aerobic worlds have a significant amount of Oxygen in their atmospheres.
     *
     * @param planet    Planet to set resources on.
     */
    private void lifeComplexOcean(Planet planet) {
        addTertiaryResource(planet, Algae);
        addTertiaryResource(planet, Metazoa);
        addTraceResource(planet, Protozoa);
        addTraceResource(planet, Sponges);

        switch (Die.d6(2)) {
            case 2: case 3: case 4: case 5: case 6:
                addSecondaryResource(planet, Fish);
                addSecondaryResource(planet, Molluscs);
                addSecondaryResource(planet, Crawlers);
                addSecondaryResource(planet, Jellies);
                addSecondaryResource(planet, Metazoa);
                addTertiaryResource(planet, Protozoa);
                break;
            case 7: case 8: case 9:
                addPrimaryResource(planet, Fish);
                addTertiaryResource(planet, Kelp);
                break;
            case 10: case 11:
                addPrimaryResource(planet, Fish);
                addTertiaryResource(planet, Crawlers);
                addTertiaryResource(planet, Kelp);
                break;
            case 12:
                addPrimaryResource(planet, Fish);
                addSecondaryResource(planet, Crawlers);
                addTertiaryResource(planet, Kelp);
                if (Die.d2() == 1) {
                    addTraceResource(planet, Fungus);
                }
                break;
        }
    }
    private void lifeSimpleLand(Planet planet) {
        addTraceResource(planet, Algae);
        addTraceResource(planet, Sponges);
        addTertiaryResource(planet, Molluscs);
        addTertiaryResource(planet, Jellies);
        addTertiaryResource(planet, Crawlers);
        addTertiaryResource(planet, Kelp);
        addPrimaryResource(planet, Fish);

        addSecondaryResource(planet, Arthropods);
        addTertiaryResource(planet, Lichen);
        addTertiaryResource(planet, SlimeMold);
        addSecondaryResource(planet, Fungus);
    }


    /**
     * Set the organic resources found on Terrestrial worlds, given their life level.
     *
     * @param planet    The planet to generate biological resources for.
     */
    protected void generateLife(Planet planet) {
        switch (planet.getLife()) {
            case None:
                lifeNone(planet);
                break;
            case Organic:
                lifeOrganic(planet);
                break;
            case Archaean:
                lifeArchaean(planet);
                break;
            case Aerobic:
                lifeAerobic(planet);
                break;
            case ComplexOcean:
                lifeComplexOcean(planet);
                break;
            case SimpleLand:
            case ComplexLand:
            case Extensive:
                lifeSimpleLand(planet);
                break;
        }
    }

    protected void addMineralResources(Planet planet) {
        addPrimaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addTertiaryResource(planet, FerricOre);

        addTraceResource(planet, HeavyMetals);
        addTraceResource(planet, Radioactives);

        switch (Die.d6(2)) {
            case 2:
                addSecondaryResource(planet, HeavyMetals);
                addTertiaryResource(planet, Radioactives);
                addTertiaryResource(planet, PreciousMetals);
                break;
            case 3:
                addTertiaryResource(planet, HeavyMetals);
                addTertiaryResource(planet, Radioactives);
                break;
            case 4:
                addSecondaryResource(planet, FerricOre);
                addTraceResource(planet, PreciousMetals);
                break;
            case 5:
                addTraceResource(planet, ExoticCrystals);
                break;
            case 12:
                // Something special.
                switch (Die.d3()) {
                    case 1:
                        addPrimaryResource(planet, PreciousMetals);
                        break;
                    case 2:
                        addPrimaryResource(planet, HeavyMetals);
                        break;
                    case 3:
                        addPrimaryResource(planet, Radioactives);
                        break;
                }
                break;
        }

    }

}
