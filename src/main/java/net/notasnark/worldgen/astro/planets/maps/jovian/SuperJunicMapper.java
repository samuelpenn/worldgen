/**
 * JunicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.jovian.SuperJunic;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A hot jovian world, close to its star. Similar in size to Jupiter. Tend to be dark reddish in colour, with
 * some dark purples and blues.
 */
public class SuperJunicMapper extends JovianMapper {

    protected static final Tile RED = new Tile("Red", "#A02020", false, 6);
    protected static final Tile DARK_BROWN = new Tile("Dark Brown", "#A05050", false, 4);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#C07070", false, 4);
    protected static final Tile PURPLE = new Tile("Yellow", "#905090", false, 6);

    public SuperJunicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public SuperJunicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        switch (Die.d6()) {
            case 1:
                return SuperJunicMapper.PURPLE;
            case 2:case 3:
                return SuperJunicMapper.RED;
            case 4:case 5:
                return SuperJunicMapper.LIGHT_BROWN;
            case 6:
                return SuperJunicMapper.DARK_BROWN;
        }
        throw new IllegalStateException("getRandomColour: Invalid switch value.");
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d6() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.dieV(6));
        }
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("BigWorld");
        planet.setType(PlanetType.SuperJunic);
        planet.setTemperature(1400);
        planet.setRadius(55_000);
        testOutput(planet, new SuperJunic(Server.getWorldGen(), null, null, null, 0),
                new SuperJunicMapper(planet));
    }
}
