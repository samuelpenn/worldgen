/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.planets.generators.Jovian;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.worldgen.astro.planets.Planet;

/**
 * Jovic worlds are large gas giant worlds similar to Jupiter.
 */
public class Jovic extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Jovic.class);

    public enum JovicFeature implements PlanetFeature {
        RedBands,
        GreenBands,
        BlueBands,
        BlackSpots,
        BlackBands,
        DarkClouds,
        InternalHeat;

        public int getNotability() { return 100; }
    }

    public Jovic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void generateFeatures(Planet planet) {
        if (planet.getTemperature() > 150) {
            planet.addFeature(Jovian.JovianFeature.WaterClouds);
        } else {
            planet.addFeature(JovianFeature.AmmoniaClouds);
        }

        switch (Die.d6(3)) {
            case 3:
                planet.addFeature(JovianFeature.DarkClouds);
                break;
            case 4:
                planet.addFeature(JovicFeature.BlackBands);
                break;
            case 5:
                planet.addFeature(JovicFeature.GreenBands);
                break;
            case 6:
                planet.addFeature(JovicFeature.BlueBands);
                break;
            case 7: case 8:
                planet.addFeature(JovicFeature.RedBands);
                break;
            case 16:
                planet.addFeature(JovicFeature.InternalHeat);
                break;
            case 18:
                planet.addFeature(JovicFeature.DarkClouds);
        }
    }

    public Planet getPlanet(String name) {
        Planet planet =  definePlanet(name, PlanetType.Jovic);
        planet.setRadius(45000 + Die.die(5000, 4));

        generateFeatures(planet);

        planet.setAtmosphere(Atmosphere.Hydrogen);
        planet.setPressure(Pressure.SuperDense);
        planet.setHabitability(6);

        if (planet.hasFeature(JovicFeature.InternalHeat)) {
            // This world is considerably hotter than it should be.
            planet.setTemperature((int) (planet.getTemperature() * 1.3));
        } else {
            // Jovic worlds generally generate some of their own heat.
            planet.setTemperature((int) (planet.getTemperature() * 1.1));
        }
        planet.setNightTemperature(planet.getTemperature() - Die.d4(2));

        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.Strong);
                break;
            case 12:
                planet.setMagneticField(MagneticField.Intense);
                break;
            default:
                planet.setMagneticField(MagneticField.VeryStrong);
                break;
        }

        // Set default day length to be around 10 hours.
        planet.setDayLength(9 * 3600 + Die.die(3600, 2));

        switch (Die.d6(3)) {
            case 3:
                planet.setLife(Life.Aerobic);
                addTertiaryResource(planet, CommodityName.Metazoa);
                addTertiaryResource(planet, CommodityName.Prokaryotes);
                addSecondaryResource(planet, CommodityName.OrganicChemicals);
                break;
            case 4:
                planet.setLife(Life.Archaean);
                addTertiaryResource(planet, CommodityName.Prokaryotes);
                addSecondaryResource(planet, CommodityName.OrganicChemicals);
                break;
            case 5:
                planet.setLife(Life.Organic);
                addSecondaryResource(planet, CommodityName.OrganicChemicals);
                break;
        }

        addPrimaryResource(planet, CommodityName.Hydrogen);
        addSecondaryResource(planet, CommodityName.Helium);
        addTertiaryResource(planet, CommodityName.OrganicGases);
        addTertiaryResource(planet, CommodityName.Water);
        if (planet.hasFeature(JovicFeature.GreenBands)) {
            addTertiaryResource(planet, CommodityName.HeavyMetals);
        }
        if (planet.hasFeature(JovicFeature.DarkClouds)) {
            addSecondaryResource(planet, CommodityName.OrganicGases);
        }

        return planet;
    }
}
