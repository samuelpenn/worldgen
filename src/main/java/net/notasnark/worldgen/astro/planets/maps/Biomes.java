/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps;

import net.notasnark.worldgen.astro.planets.Planet;

import java.util.ArrayList;
import java.util.List;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

public class Biomes {
    public Biome JUNGLE = new Biome(Woods, "#004400");
    public Biome FOREST = new Biome(Woods, "#006600");
    public Biome SHRUB = new Biome(Shrubs, "#448800");
    public Biome PLAIN = new Biome(Grasses, "#66AA00");
    public Biome DESERT = new Biome(null, "#C0C000");
    public Biome TUNDRA = new Biome(Grasses,"#A8A880");
    public Biome ICE = new Biome(null, "#FEFEFE");
    public Biome MOUNTAIN = new Biome(null,"#505020");

    private List<Biome> all;
    private Planet planet;

    public Biomes(Planet planet) {
        this.planet = planet;

        all = new ArrayList<>() {{
            add(JUNGLE);
            add(FOREST);
            add(SHRUB);
            add(PLAIN);
            add(DESERT);
            add(TUNDRA);
            add(ICE);
            add(MOUNTAIN);
        }};
    }


    private void setJungle(int w, int t, int h) {
        JUNGLE.density = (w < 50)?0:1+(w-50)/5;

        if (t < 293) {
            JUNGLE.density = 0;
        } else if (t >= 308) {
            JUNGLE.density *= 4;
        } else if (t >= 303) {
            JUNGLE.density *= 3;
        } else if (t > 298) {
            JUNGLE.density *= 2;
        }
    }

    private void setForest(int w, int t, int h) {
        FOREST.density = (w < 25)?0:1+(w-25)/10;

        if (t < 278) {
            FOREST.density = 0;
        } else if (t >= 298) {
            FOREST.density *= 2;
        } else if (t >= 288) {
            FOREST.density *= 1;
        } else if (t >= 278) {
            FOREST.density /= 2;
        }
    }

    private void setShrub(int w, int t, int h) {
        SHRUB.density = 0;

        if (w >= 20 && w < 30) SHRUB.density = 1;
        if (w >= 30 && w < 40) SHRUB.density = 2;
        if (w >= 40 && w < 50) SHRUB.density = 4;
        if (w >= 50 && w < 60) SHRUB.density = 3;
        if (w >= 60) SHRUB.density = 2;

        if (t >= 278) {
            SHRUB.density *= 2;
        } else if (t < 268) {
            SHRUB.density = 0;
        }
    }

    private void setPlain(int w, int t, int h) {
        PLAIN.density = 0;

        if (w >= 5 && w < 15) PLAIN.density = 2;
        if (w >= 15 && w < 25) PLAIN.density = 4;
        if (w >= 25 && w < 35) PLAIN.density = 6;
        if (w >= 35 && w < 45) PLAIN.density = 5;
        if (w >= 45 && w < 55) PLAIN.density = 4;
        if (w >= 55) PLAIN.density = 2;

        if (t < 268) PLAIN.density /= 2;
        if (t > 293) PLAIN.density /= 2;
    }

    private void setDesert(int w, int t, int h) {
        DESERT.density = 0;

        if (w < 5) DESERT.density = 6;
        else if (w < 10) DESERT.density = 3;
        else if (w < 20) DESERT.density = 1;

        if (t < 293) DESERT.density = 0;
        else if (t < 303) DESERT.density *= 1;
        else if (t < 313) DESERT.density *= 2;
        else DESERT.density *= 3;
    }

    private void setTundra(int w, int t, int h) {
        TUNDRA.density = Math.max(0, (283 - t) / 3);
    }

    private void setIce(int w, int t, int h) {
        ICE.density = Math.max(0, (275 - t) / 2);

        if (h > 80) {
            ICE.density += Math.pow((h - 75) / 5, 2);
        }
    }

    private void setMountain(int w, int t, int h) {
        MOUNTAIN.density = Math.max(0, (h-50) / 3);
    }

    public void setBiomes(int w, int t, int h) {
        setJungle(w, t, h);
        setForest(w, t, h);
        setShrub(w, t, h);
        setPlain(w, t, h);
        setDesert(w, t, h);
        setTundra(w, t, h);
        setIce(w, t, h);
        setMountain(w, t, h);
    }

    public List<String> getColours() {
        List<String> list = new ArrayList<>();

        for (Biome b : all) {
            b.colours(list, planet);
        }

        return list;
    }
}
