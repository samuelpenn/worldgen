/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.belt;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Belt;

import static net.notasnark.worldgen.astro.planets.generators.Belt.BeltFeature.*;

/**
 * An Ice Ring is a type of planetary ring. Found around planets, often Jovian worlds.
 * Planetary rings do not have 'moons' or similar bodies.
 */
public class IceRing extends Belt {
    private static final Logger logger = LoggerFactory.getLogger(IceRing.class);

    public IceRing(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    private void addFeatures(Planet planet) {
        switch (Die.d6(2)) {
            case 2: case 3: case 4:
                planet.addFeature(ThinRing);
                break;
            case 10: case 11: case 12:
                planet.addFeature(WideRing);
                break;
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.IceRing);
        long radius = 5_000 + Die.die(5_000, 3);
        int density = 2_000 + Die.dieV(500);

        if (planet.getFeatures().size() != 0) {
            addFeatures(planet);
        }

        if (planet.hasFeature(ThinRing)) {
            radius /= 3;
            density /= 2;
        } else if (planet.hasFeature(WideRing)) {
            radius *= 5;
        }

        if (parentPlanet != null) {
            planet.setTemperature(Physics.getTemperatureOfOrbit(star, parentPlanet.getDistance()));
        } else {
            planet.setTemperature(Physics.getTemperatureOfOrbit(star, distance));
        }
        planet.setNightTemperature(planet.getTemperature());

        addFeatures(planet);

        radius = checkDistance(planet, radius);

        planet.setRadius(radius);
        planet.setDensity(density);

        addPrimaryResource(planet, CommodityName.Water);

        return planet;
    }
}
