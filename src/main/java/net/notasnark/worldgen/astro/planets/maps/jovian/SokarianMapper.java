/**
 * SaturnianMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Jovian;
import net.notasnark.worldgen.astro.planets.generators.jovian.Saturnian;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A Sub-Jovian world (0.03 to 0.48 Jupiter masses) locked into a tight solar orbit. They may have silicate clouds.
 */
public class SokarianMapper extends JovianMapper {

    protected static final Tile RED = new Tile("Red", "#A02020", false, 2);
    protected static final Tile ORANGE = new Tile("Orange", "#C08020", false, 5);
    protected static final Tile YELLOW = new Tile("Yellow", "#C0C020", false, 6);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#D3AD6E", false, 3);
    protected static final Tile DARK_BROWN = new Tile("Dark Brown", "#CCCC99", false, 4);

    public SokarianMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public SokarianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }


    /**
     * Gets a random band colour for the clouds, based on the type of clouds prominent in the atmosphere.
     */
    private Tile getRandomColour() {
        switch (Die.d8() + (planet.hasFeature(Jovian.JovianFeature.SilicateClouds)?3:0)) {
            case 1: case 2: case 3:
                return LIGHT_BROWN;
            case 4: case 5:
                return DARK_BROWN;
            case 6:
                return YELLOW;
            case 7:
                return ORANGE;
            default:
                return RED;
        }
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d3() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.dieV(8));
        }
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Sokarian");
        planet.setType(PlanetType.Sokarian);
        planet.setRadius(22_000);
        planet.addFeature(Jovian.JovianFeature.SilicateClouds);

        testOutput(planet, new Saturnian(Server.getWorldGen(), null, null, null, 0),
                new SokarianMapper(planet));
    }
}
