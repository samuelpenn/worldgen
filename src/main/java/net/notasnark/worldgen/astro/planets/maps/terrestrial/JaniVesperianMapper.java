/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.JaniVesperian;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.SnowCover;
import net.notasnark.worldgen.astro.planets.tiles.Vegetation;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.List;

/**
 * Tidally locked world, with possibly ice on the night side and a roasted daylight side.
 */
public class JaniVesperianMapper extends TerrestrialMapper {
    public JaniVesperianMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected final Tile MOUNTAINS = new Tile("Mountains", "#706045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#B08055", false, 3);
    protected final Tile DESERT = new Tile("Desert", "#D09050", false, 2);
    protected final Tile RED = new Tile("Red", "#FF0000");
    protected final Tile GREEN = new Tile("Green", "#00FF00");

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getHeight(x, y) > 85) {
                    setTile(x, y, MOUNTAINS.getShaded(getHeight(x, y)));
                } else if (getHeight(x, y) < 25) {
                    setTile(x, y, PLAINS.getShaded((getHeight(x, y) + 100) / 2));
                } else {
                    setTile(x, y, DESERT.getShaded((getHeight(x, y) + 100) / 2));
                }
            }
        }

        for (int i = 0; i < planet.getHydrographics() / 2; i++) {
            int y = Die.rollZero(getNumRows() / 3) + getNumRows() / 3;
            int x = Die.rollZero(getWidthAtY(y));
            while (getLongitude(x, y) < 90) {
                x = Die.rollZero(getWidthAtY(y));
            }
            setTile(x, y, getWater());
            setHeight(x, y, 0);
        }
        if (Die.d2() == 1) {
            setTile(0, 0, getWater());
        }
        if (Die.d2() == 1) {
            setTile(0, getNumRows() - 1, getWater());
        }
        floodToPercentage(getWater(), planet.getHydrographics(), true);

        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int l = Math.abs(getLongitude(x, y));
                if (l < 90) {
                    if (getTile(x, y).isWater()) {
                        setTile(x, y, PLAINS);
                    }
                    int shade = (int) (Math.sqrt(l) * 4) + 10;
                    //setTile(x, y, getTile(x, y).getShaded(shade));
                } else if (l > 120) {
                    if (getTile(x, y).isWater()) {
                        setTile(x, y, getIce());
                    }
                }
            }
        }
        // Recalculate actually how much water there is.
        int totalTiles = 0;
        for (int y=0; y < getNumRows(); y++) {
            totalTiles += getWidthAtY(y);
        }
        int flooded = countTilesOfType(getWater()) + countTilesOfType(getIce());
        planet.setHydrographics((100 * flooded) / totalTiles);

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater() || getTile(x,y) == getIce() ) {
                    setHeight(x, y, 0);
                } else {
                    int h = getHeight(x, y);
                    if (h < 50) {
                        setHeight(x, y, 25);
                    } else if (h < 90) {
                        setHeight(x, y, 50);
                    } else {
                        setHeight(x, y, 100);
                    }
                    if (Math.abs(getLongitude(x, y)) > 100) {
                        setTile(x, y, new SnowCover(getTile(x, y)).cover(h / 3 + Math.abs(getLongitude(x, y)) - 120));
                    } else {
                        setTile(x, y, new Rough(getTile(x, y)));
                    }
                }
            }
        }
        createBiome();

        generateFeatures(planet.getFeatures());
        cleanBumpMap();

        hasHeightMap = true;
    }

    protected void createBiome() {
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int l = Math.abs(getLongitude(x, y));
                if (l > 90 &&  l < 100 && !getTile(x, y).isWater()) {
                    setTile(x, y, new Vegetation(getTile(x, y)));
                }
            }
        }
    }


    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("JaniVesperian");
        planet.setType(PlanetType.JaniVesperian);
        planet.setHydrographics(20);
        planet.setTemperature(550);
        planet.setNightTemperature(250);
        planet.setPressure(90_000);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setLife(Life.SimpleLand);

        testOutput(planet, new JaniVesperian(Server.getWorldGen(), null, null, null, 0),
                new JaniVesperianMapper(planet));
    }
}
