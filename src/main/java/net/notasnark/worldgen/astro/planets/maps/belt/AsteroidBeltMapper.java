/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.belt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.stars.Star;

import java.util.List;
import java.util.Random;

/**
 * Mapper for a belt of asteroids that encircles a star. This has no surface map, but does provide an
 * orbit map which is used by the star system mapper.
 */
public class AsteroidBeltMapper extends PlanetMapper {
    protected static final Logger logger = LoggerFactory.getLogger(AsteroidBeltMapper.class);

    public AsteroidBeltMapper(final Planet planet, final int size) {
        super(planet, size);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public AsteroidBeltMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);

        hasMainMap = false;
        hasOrbitMap = true;
    }

    public void generate() {
        // Nothing do do here.
    }


    /**
     * Gets the number of planetesimals to draw for a belt. Based on the area of the belt,
     * and the defined density for that belt. This is the number drawn, not the actual
     * number of asteroids present.
     *
     * @param belt      Belt to calculate numbers for.
     * @return          Number of objects to draw.
     */
    protected int getNumberOfPlanetesimals(Planet belt, long kmPerPixel) {
        // The area is equal to the area within the outer rim of the belt, minus the area
        // within the inner rim of the belt. We ignore constants. Gives area in km^2
        double area = Math.pow((belt.getDistance() + belt.getRadius()), 2);
        area -= Math.pow((belt.getDistance() - belt.getRadius()), 2);

        long number = (long) ((area * belt.getDensity()) / Math.pow(kmPerPixel, 2));

        logger.debug("Area of belt: [" + area + "], Density: [" + belt.getDensity() +
                "], kmPerPixel: [" + kmPerPixel + "], Number: [" + number + "]");

        return (int) Math.sqrt(number); //(number / 100_000);
    }

    public void drawOrbit(SimpleImage image, Star star, int cx, int cy, long kmPerPixel, List<Planet> moons) {
        Random  random = new Random(planet.getId());

        final int number = getNumberOfPlanetesimals(planet, kmPerPixel);
        int     s = (int) ((planet.getDistance() / kmPerPixel) / 1000);
        logger.info("draw" + planet.getType() + " " + number);

        for (int a = 0; a < number; a++) {
            double  d = planet.getDistance() + random.nextFloat() * planet.getRadius() - random.nextFloat() * planet.getRadius();
            double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
            int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
            int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

            // Determine size class of this asteroid, based on random distribution.
            int     size = random.nextInt(100) + a;
            if (size < 10) {
                // Giant asteroid, > 100km radius.
                image.rectangleFill(x - 1 - s, y - 1 - s, 3 + s * 2, 3 + s * 2, "#000000");
            } else if (size < 30) {
                // Large asteroid, > 30km radius.
                image.rectangleFill(x - s, y - s, 2 + s * 2, 2 + s * 2, "#000000");
            } else if (size < 100) {
                // Medium asteroid, > 10km radius.
                image.rectangleFill(x - s / 2, y - s / 2, 2 + s, 2 + s, SimpleImage.getDarker(planet.getType().getColour()));
            } else if (size < 1000) {
                // Small asteroid, > 3km radius.
                image.rectangleFill(x - s / 4, y - s / 4, 2 + s / 2, 2 + s / 2, planet.getType().getColour());
            } else {
                // Don't draw anything.
                image.rectangleFill(x, y, 1, 1, SimpleImage.getLighter(planet.getType().getColour()));
            }
        }

        if (moons != null) {
            for (Planet moon : moons) {
                random = new Random(moon.getId());
                double  d = planet.getDistance() + moon.getDistance();
                double  angle = random.nextDouble() * 360.0 + getAngleOffset(star, d);
                int     x = cx + (int) (Math.cos(Math.toRadians(angle)) * d / kmPerPixel);
                int     y = cy + (int) (Math.sin(Math.toRadians(angle)) * d / kmPerPixel);

                int radius = 4;
                image.circle(x, y, radius, planet.getType().getColour());

                drawPlanetLabel(image, x, y, angle, moon);
            }
        }
    }

}
