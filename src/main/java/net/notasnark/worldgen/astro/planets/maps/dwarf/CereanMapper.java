/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.dwarf;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.dwarf.Cerean;
import net.notasnark.worldgen.astro.planets.maps.DwarfMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * Cerean worlds are barren, grey, rocky worlds. They have darker 'seas' of recent lava plains.
 */
public class CereanMapper extends DwarfMapper {
    protected static final Tile MARIA = new Tile("Seas", "#404040", false, 2);
    protected static final Tile SILICATES = new Tile("Highlands", "#8B8B88", false, 2);

    public CereanMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public CereanMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    public void generate() {

        generateHeightMap(24, DEFAULT_FACE_SIZE);
        int seaLevel = getSeaLevel(5);

        // Basic barren landscape.
        for (int tileY=0; tileY < getNumRows(); tileY++) {
            for (int tileX=0; tileX < getWidthAtY(tileY); tileX++) {
                int h = getHeight(tileX, tileY);
                setTile(tileX, tileY, new Rough(SILICATES.getShaded(50 + (h / 2))));
            }
        }

        cleanBumpMap();
        createCraters(2, 20);
        createCraters(1, 50);
        createCraters(-1, 250);
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("Sophia");
        planet.setType(PlanetType.Cerean);
        planet.setRadius(2600);
        planet.setTemperature(275);

        testOutput(planet, new Cerean(Server.getWorldGen(), null, null, null, 0),
                new CereanMapper(planet));
    }
}
