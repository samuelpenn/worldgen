/**
 * PlanetFactory.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.codes.PlanetClass;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.CircumstellarZone;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.astro.systems.UWP;
import net.notasnark.worldgen.civ.Facility;
import net.notasnark.worldgen.exceptions.NoSuchObjectException;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Factory class to create, store and find planets.
 */
public class PlanetFactory {
    private static final Logger logger = LoggerFactory.getLogger(PlanetFactory.class);
    private final EntityManager session;
    private final WorldGen worldgen;
    private int   ambient = 0;

    private UWP uwp = null;

    /**
     * Constructor using a session object.
     *
     * @param session Persistence session to use.
     */
    public PlanetFactory(final WorldGen worldgen, final EntityManager session) {
        if (session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Session object must be open and non-null.");
        }
        this.worldgen = worldgen;
        this.session = session;
    }

    /**
     * Gets the planet defined by its unique id.
     *
     * @param id Unique id of planet.
     * @return Planet if it exists.
     * @throws NoSuchPlanetException Thrown if planet does not exist.
     */
    public Planet getPlanet(int id) throws NoSuchPlanetException {
        if (id < 1) {
            throw new IllegalArgumentException("Planet Id must be strictly positive.");
        }
        Planet planet = session.find(Planet.class, id);
        if (planet == null) {
            throw new NoSuchPlanetException(id);
        }

        return planet;
    }

    /**
     * Gets a list of all the planets in the given star system. Returned ordered by the star
     * they are orbiting and their distance from it.
     *
     * @param system Star system to look in.
     * @return List of planets, may be an empty list. Ordered by parent and distance.
     */
    public List<Planet> getPlanets(StarSystem system) {
        ArrayList<Planet> planets;

        Query query = session.createQuery("FROM Planet WHERE systemId = :systemId ORDER BY parentId, distance");
        query.setParameter("systemId", system.getId());

        planets = (ArrayList<Planet>) query.getResultList();

        return planets;
    }

    /**
     * Gets a list of all the planets around a given star.
     *
     * @param star Star to get planets for.
     * @return List of planets, may be an empty list. Ordered by distance.
     */
    public List<Planet> getPlanets(Star star) {
        ArrayList<Planet> planets;
        Query query = session.createQuery("FROM Planet WHERE parentId = :starId ORDER BY distance");
        query.setParameter("starId", star.getId());

        planets = (ArrayList<Planet>) query.getResultList();

        return planets;
    }

    /**
     * Get the moons of the given planet. If the planet has no moons, then an empty list will be returned.
     *
     * @param planet Planet to get moons for.
     * @return List of moons (planets), may be empty. Ordered by distance.
     */
    public List<Planet> getMoons(Planet planet) {
        ArrayList<Planet> planets;

        Query query = session.createQuery("FROM Planet WHERE moonOf = :planetId ORDER BY distance");
        query.setParameter("planetId", planet.getId());

        planets = (ArrayList<Planet>) query.getResultList();

        return planets;

    }

    public void persist(Planet planet) {
        if (planet != null && planet.getId() > 0) {
            session.persist(planet);
        }
    }

    /**
     * Set the ambient temperature for this system. Ambient temperatures are used in binary
     * systems where a second star increases the effective temperature.
     */
    public void setAmbient(int kelvin) {
        this.ambient = kelvin;
    }

    public int getAmbient() {
        return ambient;
    }

    /**
     * Gets a count of the total number of planets.
     *
     * @return Number of planets.
     */
    public int getPlanetCount() {
        Query query = session.createNativeQuery("SELECT COUNT(*) FROM planets");
        List<BigInteger> count = query.getResultList();

        return count.get(0).intValue();
    }

    public void setUWP(UWP uwp) {
        this.uwp =uwp;
    }

    public List<Facility> getFacilities(Planet planet) {
        Query query = session.createQuery("FROM Facility WHERE planetId = :planetId ORDER BY id");
        query.setParameter("planetId", planet.getId());

        return (ArrayList<Facility>) query.getResultList();
    }

    public void setFacility(Facility facility) {
        session.persist(facility);
    }

    public void setFacilities(List<Facility> facilities) {
        for (Facility f : facilities) {
            session.persist(f);
        }
    }

    /**
     * Stores the map for a given planet. Each map is stored as its own entry in the database,
     * it isn't part of the Planet object itself. Each planet can have several named maps
     * associated with it.
     *
     * @param planetId      Id of planet to store map for.
     * @param name          Name of this map.
     * @param image         Image of the map itself.
     * @throws IOException  If unable to store map data.
     */
    public void setPlanetMap(int planetId, String name, SimpleImage image) throws IOException {
        ByteArrayOutputStream stream = image.save();

        Query query = session.createQuery("FROM PlanetMap G WHERE planetId = :planetId AND name=:name");
        query.setParameter("planetId", planetId);
        query.setParameter("name", name);

        PlanetMap map;
        try {
            map = (PlanetMap) query.getSingleResult();
            map.setData(stream.toByteArray());
        } catch (NoResultException e) {
            map = new PlanetMap(planetId, name, stream.toByteArray());
        }

        session.persist(map);
    }

    public SimpleImage getPlanetMap(int planetId, String name) {
        Query query = session.createQuery("FROM PlanetMap G WHERE planetId = :planetId AND name=:name");
        query.setParameter("planetId", planetId);
        query.setParameter("name", name);
        PlanetMap map = (PlanetMap) query.getSingleResult();

        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(map.getData());
            return new SimpleImage(ImageIO.read(bais));
        } catch (IOException e) {
            logger.warn(String.format("Unable to create image from data (%s)", e.getMessage()));
        }
        return null;
    }

    /**
     * Get list of maps available for this planet.
     *
     * @param planetId  Plant to get list of maps for.
     * @return          List of map names.
     */
    public List<String> getPlanetMaps(int planetId) {
        Query query = session.createNativeQuery("SELECT name FROM planet_maps WHERE planet_id=:id");
        query.setParameter("id", planetId);
        return query.getResultList();
    }

    private static final String GENERATOR_PREFIX = "net.notasnark.worldgen.astro.planets.generators.";

    private static Class getGeneratorClass(String name, PlanetType type) throws UnsupportedException {
        try {
            return Class.forName(GENERATOR_PREFIX + type.getGroup().name().toLowerCase() + "." + type.name());
        } catch (Throwable e) {
            logger.warn(String.format("Unable to find generator class for planet type [%s]", type.name()));
        }

        // Failed to get a generator for the given type, so look to see if there is a parent
        // class defined for this type.
        PlanetClass classification = type.getClassification();
        try {
            return Class.forName(GENERATOR_PREFIX + type.getGroup().name().toLowerCase() + "." + classification.name());
        } catch (Throwable e) {
            logger.warn(String.format("Unable to find generator class for planet type [%s.%s]",
                    classification.name(), type.name()));
        }

        // Failed to get a generator for either the specific type or classification, so look
        // for a top level group generator for this type.
        PlanetGroup group = classification.getGroup();
        try {
            return Class.forName(GENERATOR_PREFIX + group.name());
        } catch (Throwable e) {
            logger.error(String.format("Unable to find generator class for planet type [%s.%s.%s]",
                    group.name(), classification.name(), type.name()));
        }
        throw new UnsupportedException(String.format("Planet [%s] has unsupported type [%s]", name, type.name()));
    }

    public List<Planet> createPlanet(StarSystem system, Star star, String name, PlanetType type, long distance,
                                     PlanetFeature... features) throws UnsupportedException {
        return createPlanet(system, star, name, type, distance, null, features);
    }

    /**
     * Creates a new planet in the given star system. Calls one of the specific generators for the given
     * type of planet. May return more than one planet if the planet has moons. The primary planet will
     * be the first in the list, followed by the moons from inner to outer.
     *
     * @param system    System to create planet in.
     * @return          List containing the planet and any moons.
     */
    public List<Planet> createPlanet(StarSystem system, Star star, String name, PlanetType type, long distance,
                                     Planet previous, PlanetFeature... features) throws UnsupportedException {
        Class genClass = getGeneratorClass(name, type);
        PlanetGenerator generator;
        List<Planet>    planets = new ArrayList<Planet>();

        logger.info(String.format("Creating planet [%s] of type [%s] distance [%d]km",
                name, type.name(), distance));

        try {
            Constructor c = genClass.getConstructor(WorldGen.class, StarSystem.class, Star.class, Planet.class, Long.TYPE);
            generator = (PlanetGenerator) c.newInstance(worldgen, system, star, previous, distance);
            if (ambient > 0) {
                generator.setAmbientTemperature(ambient);
            }

            if (features != null && features.length > 0) {
                for (PlanetFeature feature : features) {
                    generator.addFeature(feature);
                }
            }

            Planet planet;
            try {
                planet = generator.getPlanet(name);
            } catch (UnsupportedException e) {
                planet = generator.getPlanet(name, type);
            }
            if (uwp != null) {
                if (uwp.getSize() > 0) {
                    planet.setRadius(uwp.getSize() / 2 + Die.dieV(400));
                }
                planet.setStarPort(uwp.getStarPort());
                planet.setPopulation(uwp.getPopulation());
                planet.setTechLevel(uwp.getTechLevel());
                planet.setLawLevel(uwp.getLawLevel());
                planet.setGovernment(uwp.getGovernment());
                planet.setAtmosphere(uwp.getAtmosphere());
                planet.setPressure(uwp.getPressure());

                uwp = null;
            }

            generator.generateDescription(planet);

            session.persist(planet);
            session.flush();
            planets.add(planet);

            if (type.getGroup() != PlanetGroup.Belt) {
                Map<String,SimpleImage> maps = generator.getPlanetMaps(planet);

                for (String mapType : maps.keySet()) {
                    setPlanetMap(planet.getId(), mapType, maps.get(mapType));
                }
            }
            if (!planet.hasFeature(GeneralFeature.NoMoons)) {
                List<Planet> moons = generator.getMoons(planet, this);
                if (moons.size() > 0) {
                    logger.info(String.format("Planet [%s] has %d moons", name, moons.size()));
                    for (Planet moon : moons) {
                        session.persist(moon);
                    }
                    planets.addAll(moons);
                }
            }

            return planets;
        } catch (NoSuchMethodException e) {
            logger.error(String.format("No such method for [%s]", type.name()), e);
        } catch (IllegalAccessException e) {
            logger.error(String.format("Illegal access for [%s]", type.name()), e);
        } catch (InstantiationException e) {
            logger.error(String.format("Cannot instantiate for [%s]", type.name()), e);
        } catch (InvocationTargetException e) {
            logger.error(String.format("Cannot invoke target for [%s]", type.name()), e);
        } catch (IOException e) {
            logger.error(String.format("Error generating/storing image map [%s]", type.name()), e);
        }

        return null;
    }

    /**
     * Create a moon around a parent planet.
     *
     * @param system    System planet and moon are in.
     * @param star      Star that the parent planet is orbiting.
     * @param name      Name to give to the moon.
     * @param type      Planet Type for this moon.
     * @param distance  Distance (km) of the moon from its parent.
     * @param parent    Parent planet.
     * @param features  Optionally, any features to apply.
     *
     * @return  Created moon.
     */
    public Planet createMoon(StarSystem system, Star star, String name, PlanetType type,
                             long distance, Planet parent, PlanetFeature... features) {
        Class genClass = getGeneratorClass(name, type);
        PlanetGenerator generator;

        logger.info(String.format("Creating moon [%s] of type [%s] at [%d]km", name, type.name(), distance));

        try {
            Constructor c = genClass.getConstructor(WorldGen.class, StarSystem.class, Star.class, Planet.class, Long.TYPE);
            generator = (PlanetGenerator) c.newInstance(worldgen, system, star, null, distance);

            if (features != null && features.length > 0) {
                for (PlanetFeature f : features) {
                    generator.addFeature(f);
                }
            }
            generator.setMoonOf(parent);
            Planet moon;
            try {
                moon = generator.getPlanet(name);
            } catch (UnsupportedException e) {
                moon = generator.getPlanet(name, type);
            }
            if (uwp != null) {
                if (uwp.getSize() > 0) {
                    moon.setRadius(uwp.getSize() / 2 + Die.dieV(400));
                }
                moon.setStarPort(uwp.getStarPort());
                moon.setPopulation(uwp.getPopulation());
                moon.setTechLevel(uwp.getTechLevel());
                moon.setLawLevel(uwp.getLawLevel());
                moon.setGovernment(uwp.getGovernment());
                moon.setAtmosphere(uwp.getAtmosphere());
                moon.setPressure(uwp.getPressure());

                uwp = null;
            }
            moon.updateExtras(parent);
            generator.generateDescription(moon);

            session.persist(moon);
            session.flush();

            Map<String,SimpleImage> maps = generator.getPlanetMaps(moon);

            for (String mapType : maps.keySet()) {
                setPlanetMap(moon.getId(), mapType, maps.get(mapType));
            }

            if (parent.getParentId() < 1) {
                moon.setParentId(parent.getParentId());
                session.persist(moon);
                session.flush();
            }
            return moon;
        } catch (NoSuchMethodException e) {
            logger.error(String.format("No such method for [%s]", type.name()), e);
        } catch (IllegalAccessException e) {
            logger.error(String.format("Illegal access for [%s]", type.name()), e);
        } catch (InstantiationException e) {
            logger.error(String.format("Cannot instantiate for [%s]", type.name()), e);
        } catch (InvocationTargetException e) {
            logger.error(String.format("Cannot invoke target for [%s]", type.name()), e);
        } catch (IOException e) {
            logger.error(String.format("Error generating/storing image map [%s]", type.name()), e);
        }

        return null;
    }

    public void addMoon(Planet parent, PlanetType type, PlanetFeature... features) throws NoSuchObjectException {
        List<Planet> moons = getMoons(parent);

        long distance = 0;
        int orbit = 1;

        if (moons.size() > 0) {
            for (Planet moon : moons) {
                distance = moon.getDistance();
                orbit++;
                /*
                if (type.getGroup() == PlanetGroup.Belt && moon.getType().getGroup() == PlanetGroup.Belt) {
                    orbit++;
                } else if (type.getGroup() != PlanetGroup.Belt && moon.getType().getGroup() == PlanetGroup.Belt) {
                    orbit++;
                }
                */
            }
            if (distance < 1) {
                distance += Die.d6(3) * 10_000 + Die.die(10_000);
            } else if (distance < 100_000) {
                distance *= 1.5;
            } else {
                distance += Die.d6(3) * 5_000 + Die.die(1000);
            }
        } else {
            distance = parent.getRadius() * 10;
        }

        StarSystem system = worldgen.getStarSystemFactory().getStarSystem(parent.getSystemId());
        Star star;
        if (parent.getParentId() == 0) {
            star = null;
        } else if (parent.getParentId() == -1) {
            star = system.getStars().get(0);
        } else {
            star = worldgen.getStarFactory().getStar(parent.getParentId());
        }
        String name = StarSystemFactory.getMoonName(parent.getName(), orbit);

        createMoon(system, star, name, type, distance, parent);
    }



    /**
     * Determine a suitable type of world given the orbital temperature.
     *
     * @param kelvin    Temperature of this orbit.
     * @return          Randomly selected planet type.
     */
    public static PlanetType determinePlanetType(int kelvin) {
        CircumstellarZone zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Exclusion:
                return null;
            case Epistellar:
                return PlanetType.ProtoLithian;
            case Hot:
                switch (Die.d3()) {
                    case 1 :
                        return PlanetType.Ferrinian;
                    default:
                        return PlanetType.Hermian;
                }
            case Inner:
                return PlanetType.Cytherean;
            case Middle:
                return PlanetType.EoGaian;
            case Outer:
                return PlanetType.EuArean;
            case Cold:
                return PlanetType.Jovic;
            case Stygia:
                return null;
            case Tartarus:
                return null;
        }

        return null;
    }

    public static PlanetType determineRockyType(int kelvin) {
        CircumstellarZone  zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Exclusion:
                return null;
            case Epistellar:
                return PlanetType.ProtoLithian;
            case Hot:
                switch (Die.d3()) {
                    case 1 :
                        return PlanetType.Ferrinian;
                    default:
                        return PlanetType.Hermian;
                }
            case Inner:
                switch (Die.d6()) {
                    case 1:
                        return PlanetType.Cytherean;
                    case 2:
                        return PlanetType.NecroGaian;
                    case 3:
                        return PlanetType.EoGaian;
                    case 4:
                        return PlanetType.MesoGaian;
                    case 5:
                        return PlanetType.Selenian;
                    case 6:
                        return PlanetType.Hermian;
                }
            case Middle:
                switch (Die.d6()) {
                    case 1:
                        return PlanetType.Cytherean;
                    case 2:
                        return PlanetType.NecroGaian;
                    case 3:
                        return PlanetType.EoGaian;
                    case 4:
                        return PlanetType.MesoGaian;
                    case 5:
                        return PlanetType.Selenian;
                    case 6:
                        return PlanetType.EoArean;
                }
            case Outer:
                switch (Die.d6(3)) {
                    case 3: case 4:
                        return PlanetType.NecroGaian;
                    case 5: case 6:
                        return PlanetType.Selenian;
                    case 7: case 8: case 9:
                        return PlanetType.EuArean;
                    case 10: case 11: case 12:
                        return PlanetType.MesoArean;
                    case 13: case 14:
                        return PlanetType.EoArean;
                    case 15:
                        return PlanetType.EoGaian;
                    case 16:
                        return PlanetType.MesoGaian;
                    case 17: case 18:
                        return PlanetType.AsteroidBelt;
                }
            case Cold:
                return null;
            case Stygia:
                return null;
            case Tartarus:
                return null;
        }

        return null;
    }

    public static PlanetType determineDwarfType(int kelvin) {
        CircumstellarZone  zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Epistellar:
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        return PlanetType.Ferrinian;
                    case 4: case 5: case 6:
                        return PlanetType.Hermian;
                }
                break;
            case Hot:
                switch (Die.d6()) {
                    case 1: case 2:
                        return PlanetType.Ferrinian;
                    case 3: case 4: case 5: case 6:
                        return PlanetType.Hermian;
                }
                break;
            case Inner: case Middle: case Outer:
                return PlanetType.Selenian;
            case Cold: case Stygia: case Tartarus:
                switch (Die.d6()) {
                    case 1: case 2:
                        return PlanetType.Europan;
                    case 3: case 4:
                        return PlanetType.Enceladusian;
                    case 5: case 6:
                        return PlanetType.Gelidian;
                }
                break;
        }

        return null;
    }

    /**
     * Determine a suitable small body type for the given orbital temperature. This is often used
     * to determine small moons or planetoids rather than for primary planets.
     *
     * @param kelvin    Temperature of this orbit.
     * @return          Randomly selected small body planet type.
     */
    public static PlanetType determineSmallBodyType(int kelvin) {
        CircumstellarZone  zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Epistellar:
                switch (Die.d6()) {
                    case 1: case 2:
                        return PlanetType.Metallic;
                    case 3: case 4: case 5: case 6:
                        return PlanetType.Vulcanian;
                }
                break;
            case Hot:
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        return PlanetType.Vulcanian;
                    case 4: case 5:
                        return PlanetType.Silicaceous;
                    case 6:
                        return PlanetType.Metallic;
                }
                break;
            case Inner:
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        return PlanetType.Silicaceous;
                    case 4: case 5:
                        return PlanetType.Carbonaceous;
                    case 6:
                        return PlanetType.Vulcanian;
                }
                break;
            case Middle: case Outer:
                switch (Die.d6()) {
                    case 1: case 2: case 3:
                        return PlanetType.Silicaceous;
                    case 4: case 5:
                        return PlanetType.Carbonaceous;
                    case 6:
                        return PlanetType.Gelidaceous;
                }
                break;
            case Cold: case Stygia: case Tartarus:
                return PlanetType.Gelidaceous;
        }

        return null;
    }

    public static PlanetType determineJovianType(int kelvin) {
        CircumstellarZone zone = CircumstellarZone.getZone(kelvin);

        switch (zone) {
            case Epistellar:
                return PlanetType.Sokarian;
            case Hot:
                return PlanetType.Junic;
            case Inner: case Middle:
                return PlanetType.Poseidonic;
            case Outer:
            case Cold:
                return PlanetType.Saturnian;
            default:
                return PlanetType.Neptunian;
        }
    }
}
