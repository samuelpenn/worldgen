/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.JaniLithic;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.tiles.Lava;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.astro.planets.tiles.SnowCover;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.List;

/**
 * Tidally locked world, with possibly ice on the night side and a roasted daylight side.
 */
public class JaniLithicMapper extends TerrestrialMapper {
    public JaniLithicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected final Tile MOUNTAINS = new Tile("Mountains", "#706045", false, 2);
    protected final Tile PLAINS = new Tile("Plains", "#B08055", false, 3);
    protected final Tile DESERT = new Tile("Desert", "#D09050", false, 2);
    protected final Tile RED = new Tile("Red", "#FF0000");
    protected final Tile GREEN = new Tile("Green", "#00FF00");

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getHeight(x, y) > 85) {
                    setTile(x, y, MOUNTAINS.getShaded(getHeight(x, y)));
                } else if (getHeight(x, y) < 25) {
                    setTile(x, y, PLAINS.getShaded((getHeight(x, y) + 100) / 2));
                } else {
                    setTile(x, y, DESERT.getShaded((getHeight(x, y) + 100) / 2));
                }
                int l = Math.abs(getLongitude(x, y));
                if (l < 90) {
                    int shade = (int)(Math.sqrt(l) * 2) + 70;
                    setTile(x, y, getTile(x, y).getShaded(shade));
                }
            }
        }

        // After finishing with the height map, set it to more consistent values
        // so that the bump mapper can use it cleanly.
        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setHeight(x, y, 0);
                } else {
                    int h = getHeight(x, y);
                    if (h < 90) {
                        h = 50;
                    } else {
                        h = 100;
                    }
                    setHeight(x, y, h);
                }
                int l = Math.abs(getLongitude(x, y));
                if (l < 45 && Die.d20() == 1 && getLatitude(y) < 45 && getHeight(x, y) < 60) {
                    setTile(x, y, new Lava(getTile(x, y)));
                } else {
                    setTile(x, y, new Rough(getTile(x, y)));
                }
            }
        }
        createCraters(1, 10);

        // Volatiles on the night side.
        for (int y=0; y < getNumRows(); y++) {
            for (int x = 0; x < getWidthAtY(y); x++) {
                int l = Math.abs(getLongitude(x, y));
                if (l > 100 && getLatitude(y) < 60) {
                    int cover = l - 160 + planet.getPressure() / 1_500 - getLatitude(y) / 2;
                    setTile(x, y, new SnowCover(getTile(x, y)).cover(cover));
                }
            }
        }
        generateFeatures(planet.getFeatures());
//        cleanBumpMap();

        hasHeightMap = true;
    }


    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("JaniLithic");
        planet.setType(PlanetType.JaniLithic);
        planet.setHydrographics(0);
        planet.setTemperature(550);
        planet.setNightTemperature(250);
        planet.setPressure(40_000);
        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setLife(Life.None);

        testOutput(planet, new JaniLithic(Server.getWorldGen(), null, null, null, 0),
                new JaniLithicMapper(planet));
    }
}
