/*
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.*;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.exceptions.UnsupportedException;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.Pressure;

import java.util.ArrayList;
import java.util.List;

/**
 * Generator for the Dwarf Terrestrial group of planets.
 */
public class Dwarf extends PlanetGenerator {

    // Features common to Dwarf planets.
    public enum DwarfFeature implements PlanetFeature {
        // Features defining the size of the world.
        Small,        // Smaller than normal.
        Large,        // Larger than normal.
        // Features defining the average day time surface temperature.
        UltraCold,    // Temperature below 50K.
        VeryCold,     // Temperature below 150K.
        Cold,         // Temperature between 150K and 260K.
        Standard,     // Temperature within 260K - 300K.
        Hot,          // Temperature between 300K and 500K.
        VeryHot,      // Temperature above 500K.
        RedHot,       // Temperature above 750K.
        WhiteHot,     // Temperature above 1,000K.
        UltraHot,     // Temperature above 1,500K.
        // Orbital features.
        ColdNight,
        TideLocked,
        Resonance,
        // Heat features.
        MoltenSurface(100),
        MoltenMetals(100),
        // General features.
        GreatRift,
        BrokenRifts,
        MetallicSea(100),
        MetallicLakes,
        NaturalHoneyComb(100),
        ArtificialHoneyComb(100),
        IceCaps,
        NorthCrater,
        SouthCrater,
        EquatorialRidge,
        NightsideIce,
        ReMelted;

        final int notability;
        final int primaryNotability;

        DwarfFeature() {
            this.notability = 50;
            this.primaryNotability = 100;
        }

        DwarfFeature(final int notability) {
            this.notability = notability;
            this.primaryNotability = 100;
        }

        public int getNotability() {
            return notability;
        }
    }

    public Dwarf(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Get a generated planet. Can't be called directly on the Belt class, because
     * we don't know exactly what type of planet to create.
     * Call getPlanet(String, PlanetType) instead.
     *
     * @param name  Name of planet to be generated.
     * @return      Always throws UnsupportedException().
     */
    public Planet getPlanet(String name) {
        throw new UnsupportedException("Must define planet type");
    }

    @Override
    public Planet getPlanet(String name, PlanetType type) {
        Planet planet = definePlanet(name, type);
        planet.setRadius(Die.d6(3) * 100);

        return planet;
    }

    /**
     * Set some features automatically on the planet. Currently this consists of some temperature
     * related features.
     *
     * @param planet    Planet to have features automatically set.
     */
    protected void setAutomaticFeatures(Planet planet) {
        if (planet.getTemperature() > 1500) {
            planet.addFeature(DwarfFeature.UltraHot);
        } else if (planet.getTemperature() > 1000) {
            planet.addFeature(DwarfFeature.WhiteHot);
        } else if (planet.getTemperature() > 750) {
            planet.addFeature(DwarfFeature.RedHot);
        } else if (planet.getTemperature() > 500) {
            planet.addFeature(DwarfFeature.VeryHot);
        } else if (planet.getTemperature() > 300) {
            planet.addFeature(DwarfFeature.Hot);
        } else if (planet.getTemperature() > 260) {
            planet.addFeature(DwarfFeature.Standard);
        } else if (planet.getTemperature() > 150) {
            planet.addFeature(DwarfFeature.Cold);
        } else if (planet.getTemperature() > 50) {
            planet.addFeature(DwarfFeature.VeryCold);
        } else {
            planet.addFeature(DwarfFeature.UltraCold);
        }

        if (planet.getTemperature() > 400 && planet.getNightTemperature() < 270) {
            planet.addFeature(DwarfFeature.ColdNight);
        }

        if (planet.hasFeature(MoonFeature.SmallMoon, DwarfFeature.Small)) {
            planet.setRadius((int) (planet.getRadius() * 0.67));
        } else if (planet.hasFeature(MoonFeature.LargeMoon, DwarfFeature.Large)) {
            planet.setRadius((int)(planet.getRadius() * 1.5));
        }

        if (planet.hasFeature(MoonFeature.TidallyLocked)) {
            planet.setDayLength(planet.getPeriod());
        } else if (planet.hasFeature(MoonFeature.AlmostLocked)) {
            planet.setDayLength(planet.getPeriod() * (94 + Die.dieV(6)) / 100);
        }
    }

    protected void setSiderealPeriod(Planet planet) {
        long weeks = planet.getPeriod() / Physics.STANDARD_WEEK;

        if (Die.d8(2) > weeks) {
            // Tidally locked.
            planet.setDayLength(planet.getPeriod());
            planet.addFeature(DwarfFeature.TideLocked);
        } else if (Die.d8(4) > weeks) {
            // Orbital resonance due to tidal locking.
            switch (Die.d6()) {
                case 1:
                    planet.setDayLength((int) (planet.getPeriod() * 1.5));
                    break;
                case 2: case 3:
                    planet.setDayLength((int) (planet.getPeriod() * (2.0/3.0)));
                    break;
                case 4: case 5:
                    planet.setDayLength(planet.getPeriod() / 2);
                    break;
                default:
                    planet.setDayLength(planet.getPeriod() / (Die.d6() + 2));
                    break;
            }
            planet.addFeature(DwarfFeature.Resonance);
        }
    }

    /**
     * Randomly determine atmospheric pressure of a Dwarf world, from Trace to Thin, with VeryThin
     * being the most likely. If the modifier is negative, then Trace atmospheres become more likely,
     * and positive modifiers make Thin atmospheres more likely.
     *
     * A modifier of 5 either way forces a Trace or Thin atmosphere.
     *
     * @param modifier      Modifier to the thickness (normally -3 to +3).
     */
    protected Pressure determinePressure(int modifier) {
        int roll = Die.d6() + modifier;

        if (roll < 2) {
            return Pressure.Trace;
        } else if (roll < 6) {
            return Pressure.VeryThin;
        } else {
            return Pressure.Thin;
        }
    }

    protected void setAreanFeatures(Planet planet) {
        planet.setRadius(2500 + Die.die(500, 2));

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setMagneticField(MagneticField.Minimal);
                break;
            case 4: case 5:
                planet.setMagneticField(MagneticField.None);
                break;
            case 6:
                planet.setMagneticField(MagneticField.VeryWeak);
                break;
        }

        planet.setAtmosphere(Atmosphere.CarbonDioxide);
        planet.setPressure(5_000 + Die.die(10_000, 2));
    }

    /**
     * Randomly determine how many moons this planet has.
     *
     * @return  Between 0 and 4 moons.
     */
    private int getNumberOfMoons() {
        int numberOfMoons;

        switch (Die.d6(2)) {
            case 2:
                numberOfMoons = 4;
                break;
            case 3:
                numberOfMoons = 3;
                break;
            case 4: case 5:
                numberOfMoons = 2;
                break;
            case 6: case 7:
                numberOfMoons = 1;
                break;
            default:
                numberOfMoons = 0;
                break;
        }
        return numberOfMoons;
    }

    public List<Planet> getMoons(Planet primary, PlanetFactory factory) {
        List<Planet> moons = new ArrayList<Planet>();

        int numberOfMoons = getNumberOfMoons();
        if (numberOfMoons == 0) {
            // Nothing to do.
            return moons;
        }

        logger.info(String.format("Creating %d moons for [%s]", numberOfMoons, primary.getName()));

        int  kelvin = Physics.getTemperatureOfOrbit(star, primary.getDistance());
        long distance = primary.getRadius() * (Die.d3(2));

        for (int m=0; m < numberOfMoons; m++) {
            String name = StarSystemFactory.getMoonName(primary.getName(), m + 1);

            logger.info("Adding moon " + name);

            List<PlanetFeature> features = new ArrayList<PlanetFeature>();

            switch (Die.d6()) {
                case 1: case 2: case 3:
                    features.add(SmallBody.SmallBodyFeature.Small);
                    break;
                case 4: case 5:
                    features.add(SmallBody.SmallBodyFeature.Medium);
                    break;
                default:
                    features.add(SmallBody.SmallBodyFeature.Tiny);
                    break;
            }
            features.add(SmallBody.SmallBodyFeature.Tiny);
            PlanetType type = PlanetFactory.determineSmallBodyType(kelvin);

            if (type == null) {
                logger.error("Trying to create a moon of type null, bailing out.");
                break;
            }

            Planet moon = factory.createMoon(system, star, name, type,
                    distance, primary,
                    features.toArray(new PlanetFeature[0]));

            moons.add(moon);
            distance += Die.die((int) primary.getRadius(), 3);
        }

        return moons;
    }


}
