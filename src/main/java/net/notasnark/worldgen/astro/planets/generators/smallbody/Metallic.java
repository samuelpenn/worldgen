/*
 * Copyright (c) 2017, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.smallbody;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Temperature;
import net.notasnark.worldgen.astro.planets.generators.SmallBody;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A Metallic asteroid with metal content greater than 50%.
 */
public class Metallic extends SmallBody {
    public Metallic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        return getPlanet(name, PlanetType.Metallic);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet = definePlanet(name, type);
        planet.setRadius(getRadius(planet));

        if (planet.getTemperature() > Temperature.SilicatesBoil.getKelvin()) {
            planet.addFeature(SmallBodyFeature.Boiling);
            planet.setPressure(Die.die(1000, 5));
            planet.setAtmosphere(Atmosphere.Exotic);
        } else if (planet.getTemperature() > Temperature.SilicatesMelt.getKelvin()) {
            planet.addFeature(SmallBodyFeature.Molten);
            planet.setPressure(Die.die(100, 5));
            planet.setAtmosphere(Atmosphere.Exotic);
        } else if (planet.getTemperature() > Temperature.IronMelts.getKelvin()) {
            if (Die.d3() == 1) {
                planet.addFeature(SmallBodyFeature.Molten);
                planet.setPressure(Die.die(100, 5));
                planet.setAtmosphere(Atmosphere.Exotic);
            } else {
                planet.addFeature(SmallBodyFeature.MoltenMetals);
            }
        } else if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            planet.addFeature(SmallBodyFeature.MoltenMetals);
        }

        addPrimaryResource(planet, FerricOre);
        addSecondaryResource(planet, HeavyMetals);
        if (Die.d2() == 1) {
            addSecondaryResource(planet, RareMetals);
        }
        if (Die.d2() == 1) {
            addSecondaryResource(planet, PreciousMetals);
        }
        if (Die.d2() == 1) {
            addTertiaryResource(planet, Radioactives);
        }

        return planet;
    }
}
