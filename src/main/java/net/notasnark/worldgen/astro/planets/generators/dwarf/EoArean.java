/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A GeoCyclic EoArean world of the Dwarf Terrestrial Group. GeoCylic worlds go through
 * a cycle of active/passive periods. An EoArean world is at the beginning of its cycles,
 * and has yet to settle into a regular cycle.
 *
 * It has a thick atmosphere, mostly of carbon dioxide, and possible water cover. It is too
 * young to have formed life, so such worlds are still lifeless.
 */
public class EoArean extends Arean {
    private static final Logger logger = LoggerFactory.getLogger(EoArean.class);

    public EoArean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EoArean);
        setAreanFeatures(planet);
        planet.setMagneticField(planet.getMagneticField().getStronger().getStronger());

        planet.setAtmosphere(Atmosphere.Primordial);

        if (planet.hasFeature(GeneralFeature.Young)) {
            planet.setHabitability(5);
            planet.setPressure(120_000 + Die.die(50_000, 5));
            planet.setHydrographics(40 + Die.d20(2));
            planet.setTemperature(380 + Die.d20(3));
            planet.setNightTemperature(planet.getTemperature() - Die.d3());
            addSecondaryResource(planet, OrganicGases);
        } else {
            planet.setHabitability(5);
            planet.setPressure(80_000 + Die.die(30_000, 3));
            planet.setHydrographics(20 + Die.d20(2));
            planet.setTemperature(320 + Die.d20(2));
            planet.setNightTemperature(planet.getTemperature() - Die.d6());
            addPrimaryResource(planet, OrganicGases);
            if (Die.d2() == 1) {
                planet.setLife(Life.Organic);
                addSecondaryResource(planet, OrganicChemicals);
            }
        }

        // Define resources for this world.
        addMineralResources(planet);
        addPrimaryResource(planet, Water);

        return planet;
    }
}
