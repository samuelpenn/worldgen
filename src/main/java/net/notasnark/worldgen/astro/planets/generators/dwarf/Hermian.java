/**
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.dwarf;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.planets.generators.Dwarf;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates a Hermian world. Dwarf Terrestrial Group, GeoPassive Lithic.
 * These are hot, silicate worlds with large metalic cores and relatively thin crusts.
 * They may have lost much of their mass through an earlier collision.
 */
public class Hermian extends Dwarf {
    private static final Logger logger = LoggerFactory.getLogger(Hermian.class);

    public Hermian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Add one or more random features to the planet.
     */
    private void addFeatures(Planet planet) {
        if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            logger.info("Surface temperature is very hot.");
            switch (Die.d6()) {
                case 1:
                    planet.addFeature(DwarfFeature.MetallicSea);
                    logger.info("Adding metallic sea.");
                    return;
                case 2: case 3:
                    planet.addFeature(DwarfFeature.MetallicLakes);
                    logger.info("Adding metallic lakes.");
                    return;
                default:
                    // Add nothing.
            }
        }
        switch (Die.d6(2)) {
            case 5:
                planet.addFeature(DwarfFeature.SouthCrater);
                break;
            case 6:
                planet.addFeature(DwarfFeature.EquatorialRidge);
                break;
            case 8:
                planet.addFeature(DwarfFeature.GreatRift);
                break;
            case 9:
                planet.addFeature(DwarfFeature.NorthCrater);
                break;
            case 10:
                planet.addFeature(DwarfFeature.BrokenRifts);
                break;
            case 11:
                planet.addFeature(DwarfFeature.ReMelted);
                break;
            default:
                // No special features.
        }
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Hermian);
        int radius = 2000 + Die.die(500, 2);

        planet.setRadius(radius);
        planet.setAtmosphere(Atmosphere.Vacuum);
        planet.addTradeCode(TradeCode.Va);
        planet.addTradeCode(TradeCode.Ba);
        planet.setPressure(0);
        planet.setHabitability(5);

        switch (Die.d6()) {
            case 1:
                planet.setDayLength((int) (planet.getPeriod() * 1.5));
                break;
            case 2: case 3:
                planet.setDayLength((int) (planet.getPeriod() * (2.0/3.0)));
                break;
            default:
                planet.setDayLength(planet.getPeriod() / 2);
                break;
        }
        modifyTemperatureByRotation(planet);
        if (planet.getTemperature() > Temperature.LeadMelts.getKelvin()) {
            int kelvin = planet.getTemperature() - Temperature.LeadMelts.getKelvin();
            kelvin = Temperature.LeadMelts.getKelvin() + kelvin / 2;
            planet.setTemperature(kelvin);

            if (planet.getNightTemperature() > kelvin) {
                kelvin = planet.getNightTemperature() - (kelvin - Temperature.LeadMelts.getKelvin());
                planet.setNightTemperature(kelvin);
            }

            planet.setHabitability(6);
        }

        switch (Die.d6(3)) {
        case 3: case 4:
            planet.setMagneticField(MagneticField.VeryWeak);
            if (Die.d3() == 1) {
                planet.setAtmosphere(Atmosphere.InertGases);
                planet.setPressure(50 + Die.die(100, 1));
            }
            break;
        case 5: case 6: case 7:
            planet.setMagneticField(MagneticField.Minimal);
            break;
        default:
            planet.setMagneticField(MagneticField.None);
        }

        setAutomaticFeatures(planet);
        addFeatures(planet);

        if (planet.hasFeature(DwarfFeature.MetallicSea) || planet.hasFeature(DwarfFeature.MetallicLakes)) {
            if (Die.d2() == 1) {
                // Outgassing from the liquid metal on the surface.
                planet.setAtmosphere(Atmosphere.Exotic);
                planet.setPressure(50 + Die.die(100, 2));
            }
        }

        // Define resources for this world.
        addPrimaryResource(planet, CommodityName.SilicateOre);
        addSecondaryResource(planet, CommodityName.SilicateCrystals);
        addSecondaryResource(planet, CommodityName.FerricOre);
        if (planet.hasFeature(DwarfFeature.MetallicSea) || planet.hasFeature(DwarfFeature.MetallicLakes)) {
            addPrimaryResource(planet, CommodityName.HeavyMetals);
        } else {
            addSecondaryResource(planet, CommodityName.HeavyMetals);
        }
        addTertiaryResource(planet, CommodityName.Radioactives);
        addTertiaryResource(planet, CommodityName.PreciousMetals);

        return planet;
    }
}
