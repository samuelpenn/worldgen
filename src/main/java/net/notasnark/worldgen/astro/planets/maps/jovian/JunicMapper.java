/**
 * JunicMapper.java
 *
 * Copyright (C) 2017 Samuel Penn, sam@notasnark.net
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.maps.jovian;

import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.jovian.Junic;
import net.notasnark.worldgen.astro.planets.maps.JovianMapper;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;

/**
 * A hot jovian world, close to its star. Similar in size to Jupiter. Tend to be dark reddish in colour, with
 * some dark purples and blues.
 */
public class JunicMapper extends JovianMapper {

    protected static final Tile RED = new Tile("Red", "#A04040", false, 6);
    protected static final Tile DARK_BROWN = new Tile("Dark Brown", "#C69E60", false, 4);
    protected static final Tile LIGHT_BROWN = new Tile("Light Brown", "#D07070", false, 4);
    protected static final Tile YELLOW = new Tile("Yellow", "#B08080", false, 6);

    public JunicMapper(final Planet planet, final int size) {
        super(planet, size);
    }

    public JunicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    private Tile getRandomColour() {
        switch (Die.d6()) {
            case 1:
                return JunicMapper.YELLOW;
            case 2:case 3:
                return JunicMapper.RED;
            case 4:case 5:
                return JunicMapper.LIGHT_BROWN;
            case 6:
                return JunicMapper.DARK_BROWN;
        }
        throw new IllegalStateException("getRandomColour: Invalid switch value.");
    }

    protected Tile getBandColour(Tile previousColour, Tile nextColour) {
        if (nextColour != null) {
            // We've already chosen the next colour, so return that.
            return nextColour;
        }
        if (previousColour == null) {
            return getRandomColour();
        }
        if (Die.d3() == 1) {
            return getRandomColour();
        } else {
            return previousColour.getVariant(Die.dieV(10));
        }
    }

    public static void main(String[] args) throws IOException {
        Planet       planet = new Planet();
        planet.setName("BigWorld");
        planet.setType(PlanetType.Junic);
        planet.setTemperature(1400);
        planet.setRadius(55_000);
        testOutput(planet, new Junic(Server.getWorldGen(), null, null, null, 0),
                new JunicMapper(planet));
    }
}
