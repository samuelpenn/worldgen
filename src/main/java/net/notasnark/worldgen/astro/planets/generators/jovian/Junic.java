/*
 * Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.jovian;

import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.Pressure;
import net.notasnark.worldgen.astro.planets.generators.Jovian;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Junic worlds are large gas planets in epistellar orbits around their star.
 */
public class Junic extends Jovian {
    private static final Logger logger = LoggerFactory.getLogger(Junic.class);

    public Junic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet =  definePlanet(name, PlanetType.Junic);
        planet.setRadius(50_000 + Die.die(5000, 4));

        planet.setAtmosphere(Atmosphere.Exotic);
        planet.setPressure(Pressure.SuperDense);
        planet.setHabitability(6);

        // Junic worlds are normally tidally locked.
        planet.setDayLength(planet.getPeriod());
        int k = (int)(planet.getTemperature() * 1.4);
        planet.setTemperature(k);
        planet.setNightTemperature(k / 2);

        // Slightly weaker magnetic fields due to the lack of rotation.
        switch (Die.d6(2)) {
            case 2: case 3:
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 10: case 11: case 12:
                planet.setMagneticField(MagneticField.VeryStrong);
            default:
                planet.setMagneticField(MagneticField.Strong);
                break;
        }

        addPrimaryResource(planet, CommodityName.Hydrogen);
        addSecondaryResource(planet, CommodityName.Helium);
        addTertiaryResource(planet, CommodityName.HeavyMetals);
        addTertiaryResource(planet, CommodityName.SilicateCrystals);
        addTertiaryResource(planet, CommodityName.ExoticCrystals);

        return planet;
    }
}
