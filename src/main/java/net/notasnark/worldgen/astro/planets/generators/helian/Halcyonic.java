package net.notasnark.worldgen.astro.planets.generators.helian;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.GeneralFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Helian;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * These worlds are close to their star. They tend to be massive terrestrial worlds, with a thick cloudless
 * atmosphere of Helium. They tend to have heavy volcanic activity.
 */
public class Halcyonic extends Helian {
    private static final Logger logger = LoggerFactory.getLogger(Halcyonic.class);

    public Halcyonic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(9_000 + Die.die(3_000, 2));

        int k = planet.getTemperature();
        planet.setTemperature(k * 2);
        planet.setNightTemperature(k);
        planet.setHabitability(5);
        planet.setAtmosphere(Atmosphere.InertGases);
        int sizeMod = (int) planet.getRadius() / 1000 - 8;
        planet.setPressure(Physics.STANDARD_PRESSURE * Die.d6(sizeMod) + Die.die(Physics.STANDARD_PRESSURE));

        switch (Die.d6(2) + sizeMod) {
            case 2: case 3: case 4:
                planet.setMagneticField(MagneticField.Standard);
                planet.addFeature(HelianFeature.LightVolcanic);
                break;
            case 5: case 6: case 7: case 8:
                planet.setMagneticField(MagneticField.Strong);
                planet.addFeature(HelianFeature.MediumVolcanic);
                break;
            default:
                planet.setMagneticField(MagneticField.VeryStrong);
                planet.addFeature(HelianFeature.HeavyVolcanic);
                break;
        }

        // This is probably tidally locked.
        if (Die.d4() != 1) {
            planet.addFeature(GeneralFeature.TideLocked);
            planet.setDayLength(planet.getPeriod());
        }

        addPrimaryResource(planet, SilicateOre);
        addPrimaryResource(planet, SilicateCrystals);
        addTertiaryResource(planet, FerricOre);
        addTertiaryResource(planet, ExoticCrystals);
        addTertiaryResource(planet, Hydrogen);

        return planet;
    }
}
