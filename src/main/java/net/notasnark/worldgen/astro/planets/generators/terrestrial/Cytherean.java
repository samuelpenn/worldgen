/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.*;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * A hot terrestrial world with a super dense atmosphere, no water or appreciable plate tectonics.
 * Such worlds are very similar to Venus.
 *
 * There are two subtypes. The Hydro subtype still has considerable surface water, and the world is
 * just starting the process of moving to full Cytherean. The Xeric subtype is further along in the
 * process, but hasn't fully completed the evolution yet.
 */
public class Cytherean extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(Cytherean.class);

    public enum CythereanFeature implements PlanetFeature {
        RedClouds,
        PinkClouds,
        DarkClouds,
        CythereanHydro,
        CythereanXeric
    }

    public Cytherean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.Cytherean);

        switch (Die.d6(2)) {
            case 2:
                planet.addFeature(CythereanFeature.CythereanHydro);
                break;
            case 3: case 4:
                planet.addFeature(CythereanFeature.CythereanXeric);
                break;
            default:
                // Standard Cytherean world.
        }

        planet.setRadius(5700 + Die.d100(2) * 5);

        if (planet.hasFeature(CythereanFeature.CythereanHydro)) {
            // A world on the cusp of becoming Cytherean. Still has seas, but they are sulphuric acid.
            // Pressure is between 5 and 15 times normal.
            planet.setTemperature((int) (planet.getTemperature() * 1.5));
            planet.setNightTemperature(planet.getTemperature());
            planet.setHydrographics(20 + Die.d10(2));
            planet.setAtmosphere(Atmosphere.CarbonDioxide);
            planet.setPressure(500_000 + Die.die(500_000, 2));
            planet.setHabitability(5);

            switch (Die.d6(2)) {
                case 2:
                    planet.setLife(Life.Aerobic);
                    if (Die.d2() == 1) {
                        planet.setAtmosphere(Atmosphere.SulphurCompounds);
                    } else {
                        planet.setAtmosphere(Atmosphere.HighCarbonDioxide);
                    }
                    break;
                case 3: case 4:
                    planet.setLife(Life.Archaean);
                    break;
                case 5: case 6:
                    planet.setLife(Life.Organic);
                    break;
                default:
                    planet.setLife(Life.None);
            }

        } else if (planet.hasFeature(CythereanFeature.CythereanXeric)) {
            // World that is on the way to becoming full Cytherean, and is past the tipping point.
            // Pressure is between 15 and 35 times normal.
            planet.setTemperature((int) (planet.getTemperature() * 2.0));
            planet.setNightTemperature(planet.getTemperature());
            planet.setHydrographics(Die.d4() - 1);
            planet.setAtmosphere(Atmosphere.CarbonDioxide);
            planet.setPressure(1_500_000 + Die.die(1_000_000, 2));
            planet.setHabitability(6);

            if (planet.getHydrographics() > 0) {
                switch (Die.d6(2)) {
                    case 2: case 3:
                        planet.setLife(Life.Archaean);
                        break;
                    case 4: case 5:
                        planet.setLife(Life.Organic);
                        break;
                    default:
                        planet.setLife(Life.None);
                }
            }
        } else {
            planet.setTemperature((int) (planet.getTemperature() * 2.5));
            planet.setNightTemperature(planet.getTemperature());
            planet.setHydrographics(0);
            planet.setAtmosphere(Atmosphere.CarbonDioxide);
            // Calculate atmospheric pressure, based on planet's temperature. The hotter the
            // temperature, the higher the pressure. Pressure is in Pascals.
            int pressure = (int) (Math.sqrt(planet.getTemperature() * (9 + Die.dieV(3))) * Physics.STANDARD_PRESSURE);
            planet.setPressure(pressure);
            planet.setHabitability(6);
        }
        if (planet.getHydrographics() > 0) {
            planet.addTradeCode(TradeCode.Fl);
        }

        addFeatures(planet);

        switch (Die.d6()) {
            case 1: case 2: case 3:
                planet.setDayLength(3600 * 20 + Die.die(3600 * 4, 2));
                planet.setMagneticField(MagneticField.Standard);
                break;
            case 4: case 5:
                planet.setDayLength(3600 * 10 + Die.die(3600 * 8));
                planet.setMagneticField(MagneticField.Strong);
                break;
            case 6:
                planet.setDayLength(Physics.STANDARD_DAY + Die.die((int) Physics.STANDARD_DAY * 10));
                planet.setMagneticField(MagneticField.Weak);
                break;
        }

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        // Define resources for this world.
        addSecondaryResource(planet, SilicateOre);
        addSecondaryResource(planet, SilicateCrystals);
        addTertiaryResource(planet, CarbonicOre);
        addTertiaryResource(planet, FerricOre);

        return planet;
    }

    private void addFeatures(Planet planet) {
        switch (Die.d6(2)) {
            case 2:
                planet.addFeature(CythereanFeature.PinkClouds);
                break;
            case 3: case 4:
                planet.addFeature(CythereanFeature.RedClouds);
                break;
            case 11: case 12:
                planet.addFeature(CythereanFeature.DarkClouds);
                break;
        }
    }
}
