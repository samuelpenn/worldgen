/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.tiles;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;

/**
 * Based on the Rough texture, there is a small possibility of there being ice/snow present on the
 * surface, as well as normal shading variation. Initially added for Gelidaceous asteroids.
 */
public class Icy extends Tile {

    public Icy(Tile tile) {
        super(tile);
    }

    public void addDetail(SimpleImage image, int x, int y, int w, int h) {
        String dark = getShiftedColour(0.9);
        String light = getShiftedColour(1.1);
        String snow = "#F0F0F0";

        int height = Math.abs(h);
        for (int yy = 0; yy < height; yy++) {
            int width = (int) ((1.0 * w * (height - yy)) / (1.0 * height));
            for (int xx = -width; xx < width; xx++) {
                String colour = getRGB();
                switch (Die.d6(2)) {
                    case 2: case 3: case 4: case 5: case 11:
                        // No change.
                        break;
                    case 6: case 7:
                        colour = light;
                        break;
                    case 8: case 9: case 10:
                        colour = dark;
                        break;
                    case 12:
                        colour = snow;
                        break;
                }
                image.dot(x + xx + w, y + (int)(Math.signum(h) * yy), colour);
            }
        }
    }
}
