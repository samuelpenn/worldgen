/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * BathyGaian worlds are Tectonic Terrestrial worlds covered in deep oceans and with a thick, hot, atmosphere.
 * There is normally life in the oceans, though the ocean floors are anoxic and lifeless.
 */
public class EuPelagic extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(EuPelagic.class);

    public EuPelagic(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EuPelagic);

        planet.setHabitability(6);
        planet.setHydrographics(100);
        if (Die.d3() == 1) {
            planet.setAtmosphere(Atmosphere.Oxygen);
        } else {
            planet.setAtmosphere(Atmosphere.HighOxygen);
        }
        planet.setPressure(50_000 + Die.d6(2) * Die.die(25_000)  + Die.die(25_000));
        planet.setLife(Life.ComplexOcean);

        planet.setTemperature( 300 + Die.d100());
        planet.setNightTemperature(planet.getTemperature());

        addPrimaryResource(planet, Fish);

        addPrimaryResource(planet, Water);
        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
