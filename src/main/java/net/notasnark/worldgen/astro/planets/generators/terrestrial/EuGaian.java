/*
 * Copyright (c) 2018, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.generators.terrestrial;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.text.TextGenerator;

import static net.notasnark.worldgen.astro.commodities.CommodityName.*;

/**
 * EuGaian worlds are Tectonic Terrestrial worlds similar to an early Earth.
 */
public class EuGaian extends Terrestrial {
    private static final Logger logger = LoggerFactory.getLogger(EuGaian.class);

    public EuGaian(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    /**
     * Late Stage Meso Gaian worlds have complex ocean life, with a wide range of aquatic vertebrates.
     * There might be simple life, such as lichens, starting to gain a foothold on land.
     */
    private void typical(Planet planet) {
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setPressure(80_000 + Die.die(20_000, 2));
        planet.setLife(Life.Extensive);

        addSecondaryResource(planet, Oxygen);
        addSecondaryResource(planet, OrganicChemicals);
        addTertiaryResource(planet, OrganicGases);
        planet.setTemperature( (planet.getTemperature() + 300) / 2);
    }

    public Planet getPlanet(String name) {
        Planet planet = definePlanet(name, PlanetType.EuGaian);

        setTerrestrialProperties(planet);
        planet.setHabitability(1);
        planet.setHydrographics(55 + Die.d12(2));

        typical(planet);
        planet.setNightTemperature(planet.getTemperature() - Die.d6(2));

        setFromUWP(planet);

        if (planet.getHydrographics() < 30) {
            planet.addFeature(TerrestrialFeature.Dry);
        }

        addFeatures(planet);
        generateLife(planet);

        addPrimaryResource(planet, Water);

        TextGenerator text = new TextGenerator(planet);
        planet.setDescription(text.getFullDescription());

        return planet;
    }

    private void addFeatures(Planet planet) {
    }
}
