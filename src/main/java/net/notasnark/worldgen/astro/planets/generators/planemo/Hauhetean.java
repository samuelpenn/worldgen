/**
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets.generators.planemo;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Planemo;
import net.notasnark.worldgen.astro.stars.Star;
import net.notasnark.worldgen.astro.systems.StarSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.MagneticField;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;

/**
 * These are Planemo worlds which retain their hydrogen and helium atmospheres, but are not
 * massive enough to generate enough internal heating from their own geology. They tend to
 * be smaller than Earth-sized
 */
public class Hauhetean extends Planemo {
    private static final Logger logger = LoggerFactory.getLogger(Hauhetean.class);

    public Hauhetean(WorldGen worldgen, StarSystem system, Star star, Planet previous, long distance) {
        super(worldgen, system, star, previous, distance);
    }

    public Planet getPlanet(String name, PlanetType type) {
        Planet planet =  definePlanet(name, type);
        planet.setRadius(3_500 + Die.die(750, 3));

        planet.setAtmosphere(Atmosphere.InertGases);
        planet.setPressure(Physics.STANDARD_PRESSURE * Die.d4(2) + Die.die(Physics.STANDARD_PRESSURE));
        planet.setMagneticField(MagneticField.None);

        planet.setDayLength(6 * Physics.HOUR + Die.die(60) * Physics.HOUR + Die.die(Physics.HOUR));

        if (Die.d4() >0) {
            planet.setHydrographics(Die.d12(4));
            addPrimaryResource(planet, CommodityName.Water);
        }
        planet.setTemperature(50 + Die.d100());
        planet.setNightTemperature(planet.getTemperature());

        addPrimaryResource(planet, CommodityName.Hydrogen);
        addPrimaryResource(planet, CommodityName.Helium);
        addPrimaryResource(planet, CommodityName.SilicateOre);
        addSecondaryResource(planet, CommodityName.CarbonicOre);
        addTertiaryResource(planet, CommodityName.FerricOre);

        planet.addTradeCode(TradeCode.Ba, TradeCode.Ic);

        return planet;
    }
}
