/*
 * Copyright (c) 2021, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro.planets.maps.terrestrial;

import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.utils.graphics.Tile;
import net.notasnark.utils.rpg.Die;
import net.notasnark.worldgen.astro.commodities.Commodity;
import net.notasnark.worldgen.astro.commodities.CommodityName;
import net.notasnark.worldgen.astro.commodities.Frequency;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.PlanetFeature;
import net.notasnark.worldgen.astro.planets.codes.Atmosphere;
import net.notasnark.worldgen.astro.planets.codes.Life;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.generators.Terrestrial;
import net.notasnark.worldgen.astro.planets.generators.terrestrial.GaianPelagic;
import net.notasnark.worldgen.astro.planets.maps.TerrestrialMapper;
import net.notasnark.worldgen.astro.planets.tiles.Rough;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * GaianPelagic worlds are EuGaian worlds with more than 80% water cover.
 */
public class GaianPelagicMapper extends TerrestrialMapper {
    public GaianPelagicMapper(final Planet planet) {
        super(planet, DEFAULT_FACE_SIZE);
    }

    protected static final Tile MESOGAIAN = new Tile("Land", "#604030", false, 3);
    protected static final Tile SANDY = new Tile("Land", "#806050", false, 3);
    protected static final Tile VOLCANO = new Tile("Volcano", "#F05050", false, 3);
    protected static final Tile SHALLOWS = new Tile("Shallows", "#8080C0").water(true);

    private void generateFeatures(List<PlanetFeature> features) {
    }

    public void generate() {
        super.generate();

        if (planet.hasFeature(Terrestrial.TerrestrialFeature.CyanSeas)) {
            WATER.rgb("#00F0E0");
        }
        if (planet.hasFeature(Terrestrial.TerrestrialFeature.VolcanicFlats, Terrestrial.TerrestrialFeature.Volcanoes)) {
            LAND_ICE.rgb("#E0E0E0");
            SEA_ICE.rgb("#E0E0F0");
            SANDY.rgb("#101010");
        }

        if (planet.hasFeature(Terrestrial.TerrestrialFeature.FractalIslands)) {
            setWater(WATER);
            int height = getSeaLevel(planet.getHydrographics());
            for (int y = 0; y < getNumRows(); y++) {
                for (int x = 0; x < getWidthAtY(y); x++) {
                    if (getHeight(x, y) >= height) {
                        setTile(x, y, LAND);
                    }
                }
            }
        } else if (planet.hasFeature(Terrestrial.TerrestrialFeature.Shallows) && planet.getHydrographics() == 100) {
            setWater(WATER);
            planet.setHydrographics(85);
            createContinents(3 + Die.d3());
            planet.setHydrographics(100);
        } else {
            setWater();

            if (planet.hasFeature(Terrestrial.TerrestrialFeature.ManyIslands)) {
                createContinents(12 + Die.d6(3));
            } else {
                createContinents(6 + Die.d6());
            }
        }


        for (int y=0; y < getNumRows(); y++) {
            for (int x=0; x < getWidthAtY(y); x++) {
                if (getTile(x, y).isWater()) {
                    setTile(x, y, getTile(x, y).getShaded((getHeight(x, y) + 200) / 3));
                    setHeight(x, y, 0);
                } else if (planet.hasFeature(Terrestrial.TerrestrialFeature.Shallows)) {
                    setTile(x, y, SHALLOWS.getShaded(getHeight(x, y) / 2 + 75));
                    setHeight(x, y, 0);
                } else {
                    if (getHeight(x, y) > 75 && planet.hasFeature(Terrestrial.TerrestrialFeature.Volcanoes) && Die.d10() == 1) {
                        setTile(x, y, new Rough(VOLCANO));
                    } else if (getHeight(x, y) > 50) {
                        setTile(x, y, new Rough(MESOGAIAN.getShaded((getHeight(x, y) + 100) / 2)));
                    } else {
                        setTile(x, y, new Rough(SANDY.getShaded((getHeight(x, y) + 100) / 2)));
                    }
                }
            }
        }

        generateFeatures(planet.getFeatures());
        setIceCaps(getIce(), getIce());
        generateWetness();
        generateBiomes();
        cleanBumpMap();

        // Mark world as having clouds.
        hasCloudMap = true;
        hasHeightMap = true;
    }

    /**
     * Lower cloud layer is almost completely opaque.
     */
    private SimpleImage drawLowerCloudLayer(int width) throws IOException {
        Icosahedron cloud = getCloudLayer();

        String cloudColour = "#FEFEFE";
        int lowerLimit = 45;

        for (int y=0; y < cloud.getNumRows(); y++) {
            for (int x=0; x < cloud.getWidthAtY(y); x++) {
                int h = cloud.getHeight(x, y);
                if (h < lowerLimit) {
                    cloud.setHeight(x, y, 0);
                } else {
                    cloud.setHeight(x, y, h);
                }
            }
        }

        return Icosahedron.stretchImage(cloud.drawTransparency(cloudColour, width), width);
    }

    public List<SimpleImage> drawClouds(int width) {
        List<SimpleImage>  clouds = new ArrayList<>();

        cloudHeight = 5;

        try {
            clouds.add(drawLowerCloudLayer(width));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return clouds;
    }

    public static void main(String[] args) throws IOException {
        Planet planet = new Planet();
        planet.setName("GaianPelagic");
        planet.setType(PlanetType.GaianPelagic);
        planet.setTemperature(295);
        planet.setAtmosphere(Atmosphere.Standard);
        planet.setPressure(150_000);
        planet.setHydrographics(80+Die.d20());
//        planet.addFeature(Shallows);
//        planet.addFeature(Volcanoes);
//        planet.addFeature(FractalIslands);

        planet.setLife(Life.Extensive);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Woods.getName());
            setFrequency(Frequency.COMMON);
        }}, 800);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Grasses.getName());
            setFrequency(Frequency.COMMON);
        }}, 800);
        planet.addResource(new Commodity() {{
            setName(CommodityName.Shrubs.getName());
            setFrequency(Frequency.COMMON);
        }}, 500);

        testOutput(planet, new GaianPelagic(Server.getWorldGen(), null, null, null, 0),
                new GaianPelagicMapper(planet));
    }
}
