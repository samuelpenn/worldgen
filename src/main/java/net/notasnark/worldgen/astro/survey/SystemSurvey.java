/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.survey;

import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.systems.StarSystemType;

import java.util.ArrayList;
import java.util.List;

/**
 * Survey data for a star system. Designed to be used as a JSON object by the APIs, so light on methods.
 * It contains data that is known to players, so will be incomplete. It is never saved to the database.
 */
public class SystemSurvey {
    public String   name = "Unknown";
    public int      x = 0;
    public int      y = 0;

    public StarSystemType   type = StarSystemType.EMPTY;
    public SystemDensity    density = SystemDensity.BARREN;

    public int      scan = 0;
    public int      techLevel = 0;
    public StarPort starPort = StarPort.X;

    public List<COG> cogs = new ArrayList<>();

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getScan() {
        return scan;
    }

    public StarSystemType getType() {
        return type;
    }

    public SystemDensity getDensity() {
        return density;
    }

    public List<COG> getCogs() {
        return cogs;
    }

    public int getTechLevel() {
        return techLevel;
    }

    public StarPort getStarPort() {
        return starPort;
    }
}
