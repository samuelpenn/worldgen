/*
 * Copyright (C) 2007 Samuel Penn, sam@notasnark.net
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation version 2.
 * See the file COPYING.
 *
 * $Revision: 1.2 $
 * $Date: 2007/01/01 11:04:14 $
 */
package net.notasnark.worldgen.astro.survey;

import java.awt.Font;
import java.io.*;
import java.util.*;

import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.codes.StarPort;
import net.notasnark.worldgen.astro.sectors.NoSuchSectorException;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.sectors.SectorFactory;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.web.Server;

/**
 * Create a bitmap image of a subsector map. Designed for display on a webpage,
 * to allow a scrollable view. Also designed to have varying levels of detail.
 * The maps are supposed to stitch together, so edges can appear truncated since
 * these hexes will be half drawn on the neighbouring map. 
 * 
 * @author Samuel Penn
 *
 */
public class SurveyImage {
	private int				sectorId = 0;
	private int				ssx = 0;
	private int				ssy = 0;
	
	private SimpleImage image = null;
	private int				scale = 32;
	private int				verbosity = 0;
	private int				minScan = 0;
	private boolean			god = false;

	// Some simple constants.
	static final double COS30 = Math.sqrt(3.0)/2.0;
	static final double COS60 = 0.5;
	static final double SIN60 = Math.sqrt(3.0)/2.0;
	static final double SIN30 = 0.5;
	static final double ROOT_TWO = Math.sqrt(2.0);
	
	enum HexSides { Top, TopRight, BottomRight, Bottom, BottomLeft, TopLeft };
	
	static String SYMBOL_BASE = "file:/home/sam/src/traveller/webapp/images/symbols/";

	private WorldGen		worldGen;
	
	private int		leftMargin = 0;
	private int		topMargin = 0;
	
	public static void setSymbolBase(String base) {
		System.out.println("setSymbolBase: ["+base+"]");
		SYMBOL_BASE = base;
	}
	
	public enum SubSector {
		A(0, 0), B(1, 0), C(2, 0), D(3, 0),
		E(0, 1), F(1, 1), G(2, 1), H(3, 1),
		I(0, 2), J(1, 2), K(2, 2), L(3, 2),
		M(0, 3), N(1, 3), O(2, 3), P(3, 3);
		
		private int		x = 0;
		private int		y = 0;
		
		private SubSector(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public int getX() { return x; }
		public int getY() { return y; }
		
	}
	
	
	/**
	 * Create a new SubSectorImage for the given sector and sub sector
	 * coordinates. Coordinates are from top-left corner of the sector.
	 * 
	 * @param sectorId		Id of the sector to draw subsector map for.
	 * @param x			X coordinate of subsector (0-3).
	 * @param y			Y coordinate of subsector (0-3).
	 */
	public SurveyImage(WorldGen worldGen, int sectorId, int x, int y) {
		this.worldGen = worldGen;
		this.sectorId = sectorId;
		this.ssx = x;
		this.ssy = y;
	}
	
	public SurveyImage(WorldGen worldGen, int sectorId, SubSector subSector) {
		this.worldGen = worldGen;
		this.sectorId = sectorId;
		this.ssx = subSector.getX();
		this.ssy = subSector.getY();
	}

	public SurveyImage minScan(int minScan) {
		this.minScan = minScan;
		return this;
	}
	
	/**
	 * Set the scale of the maps to be generated. This is the width
	 * of each hex. 64 gives a good sized hex, 48 is medium and
	 * 32 is considered small.
	 * 
	 * @param scale	Size of the map.
	 */
	public void setScale(int scale) {
		this.scale = scale;
	}
	
	public int getScale() {
		return scale;
	}

	public void setGodMode(boolean god) {
		this.god = god;
	}

	/**
	 * Get actual coordinate of the hexagon specified by x and y index.
	 * 
	 * @param x		X index of hexagon.
	 * @param y		Y index of hexagon.
	 * @return			X coordinate of top left of hexagon.
	 */
	public double getX(int x, int y) {
		return leftMargin + ((x-1)*(scale * 1.5));
	}

	/**
	 * Get actual coordinate of the hexagon specified by x and y index.
	 * 
	 * @param x		X index of hexagon.
	 * @param y		Y index of hexagon.
	 * @return			Y coordinate of top left of hexagon.
	 */
	public double getY(int x, int y) {
		return (topMargin + (SIN60*2*scale) +  (Math.abs(x-1)%2)*(scale*SIN60) + (y-1)*(SIN60*2*scale));
	}
	
	/**
	 * Angles:
	 *  x = x * cos(a) - y * sin(a)
	 *  y = x * sin(a) + y * cos(a)
	 */
	private void plotHexagon(final double ox, final double oy, EnumSet<HexSides> flags) {
		double		topLeft_x, top_y, topRight_x, right_x, middle_y, bottom_y, left_x;
		double		size = scale;
		
		// Work out basic positions.
		topLeft_x = ox;
		top_y = oy;
		topRight_x = ox + size;
		right_x = topRight_x + (size * COS60 - 0 * SIN60);
		middle_y = oy - (size * SIN60 + 0 * COS60);
		bottom_y = oy - 2 * (size * SIN60 + 0 * COS60);
		left_x = ox - (size * COS60 - 0 * SIN60);
		
		String		normal = "#000000";
		String		emphasis = "#FF0000";
		float		width = (float)(scale / 64.0);
		float		emWidth = (float)(scale / 16.0);

		// Now draw the hexagon. The hexagon is actually upside down, so the
		// Top/Bottom flags aren't quite what you'd expect them to be.
		image.line(topLeft_x, top_y, topRight_x, top_y, (flags.contains(HexSides.Bottom)?emphasis:normal), (flags.contains(HexSides.Bottom)?emWidth:width));
		image.line(topRight_x, top_y, right_x, middle_y, (flags.contains(HexSides.BottomRight)?emphasis:normal), flags.contains(HexSides.BottomRight)?emWidth:width);
		image.line(right_x, middle_y, topRight_x, bottom_y, (flags.contains(HexSides.TopRight)?emphasis:normal), flags.contains(HexSides.TopRight)?emWidth:width);
		image.line(topRight_x, bottom_y, topLeft_x, bottom_y, (flags.contains(HexSides.Top)?emphasis:normal), flags.contains(HexSides.Top)?emWidth:width);
		image.line(topLeft_x, bottom_y, left_x, middle_y, (flags.contains(HexSides.TopLeft)?emphasis:normal), flags.contains(HexSides.TopLeft)?emWidth:width);
		image.line(left_x, middle_y, topLeft_x, top_y, (flags.contains(HexSides.BottomLeft)?emphasis:normal), flags.contains(HexSides.BottomLeft)?emWidth:width);		
	}

	private void plotHexagonFill(final double ox, final double oy, String colour) {
		int			topLeft_x, top_y, topRight_x, right_x, middle_y, bottom_y, left_x;
		double		size = scale;

		// Work out basic positions.
		topLeft_x = (int)ox;
		top_y = (int)oy;
		topRight_x = (int)(ox + size);
		right_x = (int)(topRight_x + (size * COS60 - 0 * SIN60));
		middle_y = (int)(oy - (size * SIN60 + 0 * COS60));
		bottom_y = (int)(oy - 2 * (size * SIN60 + 0 * COS60));
		left_x = (int)(ox - (size * COS60 - 0 * SIN60));

		float		width = (float)(scale / 64.0);
		float		emWidth = (float)(scale / 16.0);

		int[] x = { topLeft_x, topRight_x, right_x, topRight_x, topLeft_x, left_x, topLeft_x };
		int[] y = { top_y, top_y, middle_y, bottom_y, bottom_y, middle_y, top_y };

		image.polygonFill(x, y, colour);
	}
	
	private void plotText(double x, double y, String text, int style, int size, String colour) {
		image.text((int)x, (int)y, text, style, size, colour);
	}
	
	private void plotText(double x, double y, int hx, int hy) {
		String		text = "";
		
		//if (hx == 8) return;
		
		if (hx < 1) {
			text += 32 + hx;
			return;
		} else if (hx < 10) {
			text += "0" + hx;
		} else {
			text += hx;
		}

		if (hy < 1) {
			text += 40 + hy;
			return;
		} else if (hy < 10) {
			text += "0" + hy;
		} else {
			text += hy;
		}
		
		plotText(x + scale*0.1, y - scale*1.4, text, 0, (int)(scale * 0.3), "#000000");
	}
	
	/**
	 * Draw a subsector map onto an image buffer. The basic hexagons are drawn,
	 * together with all the star systems and related data.
	 * 
	 */
	private void drawBaseMap() throws NoSuchSectorException, NoSuchStarSystemException {
		int		hexWidth = (int)(scale + 1.0 * scale * COS60);
		int		hexHeight = (int)(2.0 * scale * SIN60);
		//image = new SimpleImage(hexWidth * 8 + (int)(scale * COS60), hexHeight * 10, "FFFFFF");
		image = new SimpleImage(hexWidth * 8, hexHeight * 10 + (int)(scale * SIN30 * 0.25), "FFFFFF");
		
		leftMargin = (int)(scale * 0.5);
		topMargin = 0;
		
		int		baseX = ssx*8;
		int		baseY = ssy*10;

		SectorFactory sectorFactory = worldGen.getSectorFactory();
		StarSystemFactory systemFactory = worldGen.getStarSystemFactory();
		SurveyFactory surveyFactory = worldGen.getSurveyFactory();
		StarSystem system = null;
		
		// In case we need to display surrounding sectors, work out where we
		// are in the galaxy.

		for (int x = -1; x < 9; x++) {
			for (int y = -1; y < 12; y++) {
				int			id = sectorId;
				int			sx = baseX + x;
				int			sy = baseY + y;
				EnumSet<HexSides>	flags = EnumSet.noneOf(HexSides.class);
				Sector sector = sectorFactory.getSector(sectorId);
				int					sectorX = sector.getX();
				int					sectorY = sector.getY();

				if (sx == 1) {
					flags.add(HexSides.TopLeft);
					flags.add(HexSides.BottomLeft);
				}
				if (sy == 1) {
					flags.add(HexSides.Top);
					if (sx%2 != 0) {
						flags.add(HexSides.TopLeft);
						flags.add(HexSides.TopRight);
					}
				}
				if (sx == 32) {
					flags.add(HexSides.TopRight);
					flags.add(HexSides.BottomRight);
				}
				if (sy > 39) {
					flags.add(HexSides.Bottom);
					if (sx%2 == 0) {
						flags.add(HexSides.BottomLeft);
						flags.add(HexSides.BottomRight);
					}
				}

				plotHexagon(getX(x, y), getY(x, y), flags);
				plotText(getX(x, y), getY(x, y), baseX+x, baseY+y);		

				try {
					if (sx < 1 && sy < 1) {
						// Top left
						sector  = sectorFactory.getSector(sectorX -1, sectorY - 1);
						id = sector.getId();
						sx += 32;
						sy += 40;
					} else if (sx < 1) {
						// Left
						sector  = sectorFactory.getSector(sectorX -1, sectorY);
						id = sector.getId();
						sx += 32;
					} else if (sy < 1) {
						// Top
						sector  = sectorFactory.getSector(sectorX, sectorY - 1);
						id = sector.getId();
						sy += 40;
					}
				} catch (NoSuchSectorException e) {
					// There isn't a neighbouring sector, so skip.
					continue;
				}

				try {
					system = systemFactory.getStarSystem(sector, sx, sy);
				} catch (NoSuchStarSystemException e) {
					continue;
				}
				int scan = Math.max(surveyFactory.getSurvey(system.getId()).getScan(), minScan);
				if (scan > 0) {
					String  background = system.getColour();
					if (background != null) {
						plotHexagonFill(getX(x, y), getY(x, y), background);
						plotHexagon(getX(x, y), getY(x, y), flags);
						plotText(getX(x, y), getY(x, y), baseX+x, baseY+y);
					}

					SystemSurvey survey = surveyFactory.getSystemSurvey(system, minScan);
					int		cx = (int)(getX(x, y) + scale*0.5); 		// X coordinate of centre.
					int		cy = (int)(getY(x, y) - (scale * SIN60));	// Y coordinate of centre.

					if (survey != null) {
						String portTL = "";
						if (survey.starPort != null && survey.starPort.isBetterThan(StarPort.X)) {
							portTL = "" + survey.starPort;
						} else {
							portTL = "X";
						}
						if (survey.techLevel > 0) {
							portTL += "/" + survey.techLevel;
						}
						double	fontSize = scale * 0.25;
						int		len = 0;

						len = image.getTextWidth(portTL, Font.PLAIN, (int)fontSize);
						plotText(getX(x, y) + scale * 0.5 - (len/2), getY(x, y) - scale * 1.2,
								portTL, Font.PLAIN, (int)fontSize, "#000000");
					}

					int numCogs = (survey != null && survey.cogs != null)?survey.cogs.size():0;

					switch (numCogs) {
						case 0:
							System.out.println("No cogs");
							// Do nothing.
							continue;
						case 1:
							drawCog(survey.cogs.get(0), cx, cy);
							break;
						case 2:
							drawCog(survey.cogs.get(0), cx, cy - 10);
							drawCog(survey.cogs.get(1), cx, cy + 10);
							break;
					}

					String	colour = "#000000";
					if (system.getZone() == Zone.RED) {
						colour = "#FF0000";
					} else if (system.getZone() == Zone.AMBER) {
						colour = "#FF8000";
					}

					double	fontSize = scale * 0.2;
					int		len = 0;

					len = image.getTextWidth(survey.density.getCode(), Font.PLAIN, (int)fontSize);
					plotText(getX(x, y) + scale * 0.5 - (len/2), getY(x, y) - scale * 0.05,
							survey.density.getCode(), Font.PLAIN, (int)fontSize, "#000000");

					len = image.getTextWidth(system.getName(), Font.BOLD, (int)fontSize);
					plotText(getX(x, y) + scale * 0.5 - (len/2), getY(x, y) - scale * 0.2, 
							 system.getName(), Font.BOLD, (int)fontSize, colour);
				}
			}
		}
	}

	private void drawCog(COG cog, int cx, int cy) {
		StarSurvey star = null;

		if (cog.stars != null && cog.stars.size() > 0) {
			star = cog.stars.get(0);
			int sz = scale / 5;
			String rgbOutline = "#404040";
			String rgbFill = "#E0E0E0";

			if (cog.planets != null && cog.planets.size() > 0) {
				cx -= scale / 2;
			}
			if (star.luminosity != null) {
				sz = (int) (0.1 * scale * star.luminosity.getSize());
				rgbFill = "#909090";
			}
			if (star.type != null) {
				rgbFill = star.type.getRGBColour();
				rgbOutline = "#000000";
			}
			image.circle(cx, cy, sz, rgbFill);
			image.circleOutline(cx, cy, (int) (sz * 1.1), rgbOutline);
			cx += scale / 3;
		}

		for (PlanetSurvey planet : cog.planets) {
			int h = 1;
			int w = 1;
			String rgb = "#000000";
			String water = null;

			if (planet.pclGroup != null) {
				switch (planet.pclGroup) {
					case Jovian:
						w = 5; h = 6;
						break;
					case Helian: case Terrestrial:
						w = 3; h = 4;
						break;
					case Dwarf:
						w = 3; h = 2;
						break;
					default:
						w = 1; h = 1;
						break;
				}
			}
			if (planet.atmosphere != null && planet.atmosphere.getHabitability() <= 2) {
				rgb = "#20A020";
			}
			if (planet.hydrographics > 50) {
				water = "#4040B0";
			} else if (planet.hydrographics > 0) {
				water = "#8080E0";
			}

			if (water != null) {
				image.line(cx, cy, cx, cy+h, water, w);
			}
			image.line(cx,  cy-h, cx, cy, rgb, w);
			cx += scale / 8;
		}

	}
	
	public SimpleImage getImage() throws NoSuchSectorException, NoSuchStarSystemException {
		drawBaseMap();
		return image;
	}
		
	public static void main(String[] args) throws Exception {

		WorldGen wg = Server.getWorldGen();

		SurveyImage sub = new SurveyImage(wg, 1, 0, 0);
		sub.setScale(64);
		sub.getImage().save(new File("/home/sam/tmp/sub.png"));


	}

}
