/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.survey;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds information on the centre of gravity of a star system. Single star systems will have one, multiple
 * star systems may have more than one, each one having a single star. A close binary pair will count as a
 * single COG, with two stars and multiple planets around it.
 */
public class COG {
    public long getDistance() {
        return distance;
    }

    public List<StarSurvey> getStars() {
        return stars;
    }

    public List<PlanetSurvey> getPlanets() {
        return planets;
    }

    public long     distance;
    public List<StarSurvey> stars = new ArrayList<>();
    public List<PlanetSurvey> planets = new ArrayList<>();
}
