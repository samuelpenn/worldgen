/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.survey;

import net.notasnark.worldgen.astro.stars.Luminosity;
import net.notasnark.worldgen.astro.stars.SpectralType;

import java.text.NumberFormat;

public class StarSurvey {
    public String   name = "Unknown";
    public long     distance = 0;
    public long     diameter = 0;

    public Luminosity   luminosity = null;
    public SpectralType type = null;

    public String getName() {
        return name;
    }

    public long getDistance() {
        return distance;
    }

    public String getDistanceText() {
        return NumberFormat.getNumberInstance().format(distance) + "AU";
    }

    public long getDiameter() {
        return diameter;
    }

    public String getDiameterText() {
        return NumberFormat.getNumberInstance().format(diameter) +"km";
    }

    public Luminosity getLuminosity() {
        return luminosity;
    }

    public SpectralType getType() {
        return type;
    }
}
