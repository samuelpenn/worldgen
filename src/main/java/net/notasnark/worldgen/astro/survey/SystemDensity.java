package net.notasnark.worldgen.astro.survey;

public enum SystemDensity {
    BARREN("?"),
    EXTREMELY_SPARSE("---"),
    VERY_SPARSE("--"),
    SPARSE("-"),
    NORMAL("^"),
    DENSE("+"),
    VERY_DENSE("++"),
    EXTREMELY_DENSE("+++"),
    ANOMALOUS("!!!");

    private String code;
    private SystemDensity(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static SystemDensity getByDensity(int density) {
        switch (density) {
            case 0:
                return BARREN;
            case 1: case 2: case 3:
                return EXTREMELY_SPARSE;
            case 4: case 5: case 6:
                return VERY_SPARSE;
            case 7: case 8: case 9:
                return SPARSE;
            case 10: case 11: case 12:
                return NORMAL;
            case 13: case 14: case 15:
                return DENSE;
            case 16: case 17: case 18:
                return VERY_DENSE;
            case 19: case 20: case 21:
                return EXTREMELY_DENSE;
            default:
                return ANOMALOUS;
        }
    }
}
