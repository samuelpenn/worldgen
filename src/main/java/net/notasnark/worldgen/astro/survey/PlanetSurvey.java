/*
 * Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.survey;

import net.notasnark.worldgen.astro.planets.codes.*;

import java.text.NumberFormat;

public class PlanetSurvey {
    public long     distance;
    public long     diameter;

    public PlanetGroup pclGroup = null;
    public PlanetClass pclClass = null;
    public PlanetType  pclType = null;

    public Atmosphere atmosphere = null;
    public Pressure pressure = null;

    public int hydrographics = 0;
    public long population = 0;
    public int techLevel = 0;
    public StarPort starPort = StarPort.X;

    public long getDistance() {
        return distance;
    }

    public String getDistanceText() {
        return NumberFormat.getNumberInstance().format(distance) + "AU";
    }

    public long getDiameter() {
        return diameter;
    }

    public String getDiameterText() {
        return NumberFormat.getNumberInstance().format(diameter) + "km";
    }

    public PlanetGroup getPclGroup() {
        return pclGroup;
    }

    public PlanetClass getPclClass() {
        return pclClass;
    }

    public PlanetType getPclType() {
        return pclType;
    }

    public Atmosphere getAtmosphere() {
        return atmosphere;
    }

    public Pressure getPressure() {
        return pressure;
    }

    public int getHydrographics() {
        return hydrographics;
    }

    public long getPopulation() {
        return population;
    }

    public int getTechLevel() {
        return techLevel;
    }

    public StarPort getStarPort() {
        return starPort;
    }


}
