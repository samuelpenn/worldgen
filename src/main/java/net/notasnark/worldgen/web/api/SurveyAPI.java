/**
 * SectorAPI.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.api;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.sectors.NoSuchSectorException;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.survey.SurveyImage;
import net.notasnark.worldgen.astro.survey.SystemSurvey;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.astro.systems.Survey;
import net.notasnark.worldgen.astro.systems.SurveyFactory;
import net.notasnark.worldgen.web.Controller;
import net.notasnark.worldgen.web.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import net.notasnark.worldgen.astro.planets.PlanetFactory;
import net.notasnark.worldgen.astro.sectors.SectorFactory;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.get;

/**
 * Defines REST APIs for accessing Sector level information. This also provides image
 * maps and thumbnails for sectors.
 */
public class SurveyAPI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(SurveyAPI.class);

    public void setupEndpoints() {
        logger.info("Setting up endpoints for SurveyAPI");
        get("/api/survey/:sector/map", (request, response) -> getSubSectorMap(request, response));
        get("/api/survey/:sector/data", (request, response) -> getSectorData(request, response), json());
        get("/api/survey/:sector/data/:xy", (request, response) -> getSystem(request, response), json());
        get("/api/scan/:sector/:xy", (request, response) -> scanSystem(request, response), json());
        get("/api/scan/:sector", (request, response) -> scanSector(request, response), json());
        get("/api/farscan/:sector/:xy", (request, response) -> scanSystems(request, response), json());
    }

    /**
     * Gets a list of all the known sectors. Returned as a list.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              Array of all the currently known sectors.
     */
    public Object getSystem(Request request, Response response) {
        try (WorldGen worldGen = Server.getWorldGen()) {
            response.type("application/json");

            String id = request.params(":sector");
            String xy = request.params(":xy");
            int x = StarSystemFactory.getXCoord(xy);
            int y = StarSystemFactory.getYCoord(xy);

            logger.info("Getting survey data for [" + id + "] at ["+x+","+y+"]");

            SectorFactory sectorFactory = worldGen.getSectorFactory();
            StarSystemFactory systemFactory = worldGen.getStarSystemFactory();
            SurveyFactory surveyFactory = worldGen.getSurveyFactory();

            Sector sector = sectorFactory.getSectorByIdentifier(id);
            StarSystem system = systemFactory.getStarSystem(sector, x, y);
            SystemSurvey survey = surveyFactory.getSystemSurvey(system);

            logger.info("Getting survey data for [" + system.getId() +"] [" + system.getName() + "]");

            return survey;
        } catch (Exception e) {
            logger.error(String.format("getSystem throws exception (%s)", e.getMessage()), e);
        }
        return null;
    }

    public Object getSubSectorMap(Request request, Response response) {
        try (WorldGen worldGen = Server.getWorldGen()) {
            response.type("image/png");

            String  id = request.params(":sector");
            int     x = getIntParamWithDefault(request, "x", 0);
            int     y = getIntParamWithDefault(request, "y", 0);
            int     scale = getIntParamWithDefault(request, "scale", 64);
            int     scan = getIntParamWithDefault(request, "scan", 0);
            boolean god = getBooleanParamWithDefault(request, "god", false);

            logger.info(String.format("getSubSectorMap: [%s] %d,%d Scale %d", id, x, y, scale));

            SectorFactory sectorFactory = worldGen.getSectorFactory();
            Sector sector = sectorFactory.getSectorByIdentifier(id);

            if (x < 0 || y < 0 || x > 3 || y > 3) {
                int sx = sector.getX();
                int sy = sector.getY();

                if (x < 0) {
                    sx--;
                    x += 4;
                }
                if (y < 0) {
                    sy--;
                    y += 4;
                }
                if (x > 3) {
                    sx++;
                    x -= 4;
                }
                if (y > 3) {
                    sy++;
                    y += 4;
                }
                sector = sectorFactory.getSector(sx, sy);
                if (sector == null) {
                    return null;
                }
            }

            SurveyImage image = new SurveyImage(worldGen, sector.getId(), x, y).minScan(scan);
            image.setGodMode(god);
            if (god) {
                image.minScan(12);
            }

            //image.setStandalone(true);
            image.setScale(scale);

            logger.info("Getting image map for [" + sector.getId() +"] [" + sector.getName() + "]");

            return image.getImage().save(false).toByteArray();
        } catch (Exception e) {
            logger.error(String.format("getSystem throws exception (%s)", e.getMessage()), e);
        }
        return null;
    }

    public Object getSectorData(Request request, Response response) {
        try (WorldGen worldGen = Server.getWorldGen()) {
            response.type("application/json");

            String id = request.params(":sector");

            SectorFactory sectorFactory = worldGen.getSectorFactory();
            SurveyFactory factory = worldGen.getSurveyFactory();

            Sector sector = sectorFactory.getSectorByIdentifier(id);

            StarSystemFactory sysFactory = worldGen.getStarSystemFactory();
            PlanetFactory planetFactory = worldGen.getPlanetFactory();

            List<StarSystem> list = sysFactory.getStarSystems(sector);

            List<SystemSurvey> listData = new ArrayList<>();
            for (StarSystem system : list) {
                SystemSurvey data = factory.getSystemSurvey(system);
                if (data != null) {
                    listData.add(data);
                }
            }


            return listData;
        } catch (NoSuchSectorException e) {
            response.status(404);
            return "No such sector";
        }
    }

    /**
     * Scan a single star system. Sets the survey value of the star system to the number given.
     * This is for when a single system is being given a detailed scan.
     *
     * Returns the survey results for the system after the scan.
     */
    public Object scanSystem(Request request, Response response) {
        try (WorldGen wg = Server.getWorldGen()) {
            String sectorId = getStringParam(request, "sector");
            String xy = getStringParam(request, "xy");
            int effect = getIntParamWithDefault(request, "scan", 3);

            SectorFactory sectorFactory = wg.getSectorFactory();
            Sector        sector = sectorFactory.getSectorByIdentifier(sectorId);
            int           x = StarSystemFactory.getXCoord(xy);
            int           y = StarSystemFactory.getYCoord(xy);

            StarSystem    system = wg.getStarSystemFactory().getStarSystem(sector, x, y);

            logger.info(String.format("scanSystem: [%s] [%s] [%d]", sector.getName(), system.getName(), effect));

            SurveyFactory surveyFactory = wg.getSurveyFactory();
            Survey survey = surveyFactory.getSurvey(system.getId());
            survey.setScan(effect);
            surveyFactory.persist(survey);

            return surveyFactory.getSystemSurvey(system);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Scan a whole sector. Sets the survey value for all systems in the sector. This is for pre-setting
     * survey information for large area of the map.
     */
    public Object scanSector(Request request, Response response) {
        try (WorldGen worldGen = Server.getWorldGen()) {
            response.type("application/json");

            String id = request.params(":sector");
            int    scan = getIntParamWithDefault(request, "scan", 3);
            int    distance = getIntParamWithDefault(request, "distance", 10);

            SectorFactory sectorFactory = worldGen.getSectorFactory();
            Sector sector = sectorFactory.getSectorByIdentifier(id);

            logger.info(String.format("scanSector: [%s] Scan %d, Distance %dpc",
                    sector.getName(), scan, distance));

            SurveyFactory factory = worldGen.getSurveyFactory();

            factory.scanSector(sector, scan, distance);
        } catch (Exception e) {
            logger.warn("scanSector: Unable to scan sector", e);
            return "NOTOK";
        }
        return "OK";
    }

    /**
     * Scan multiple star systems within range.
     *
     * @param request
     * @param response
     * @return
     */
    public Object scanSystems(Request request, Response response) {
        try (WorldGen wg = Server.getWorldGen()) {
            String sectorId = getStringParam(request, "sector");
            String xy = getStringParam(request, "xy");
            int radius = getIntParamWithDefault(request, "radius", 3);
            int effect = getIntParamWithDefault(request, "effect", 3);

            System.out.println(sectorId);
            System.out.println(xy);

            SectorFactory sectorFactory = wg.getSectorFactory();
            Sector        sector = sectorFactory.getSectorByIdentifier(sectorId);
            int           x = StarSystemFactory.getXCoord(xy);
            int           y = StarSystemFactory.getYCoord(xy);

            SurveyFactory surveyFactory = wg.getSurveyFactory();
            int num = surveyFactory.performSurvey(sector, x, y, radius, effect);

            System.out.println("Performed");
            return "Done on " + num;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
