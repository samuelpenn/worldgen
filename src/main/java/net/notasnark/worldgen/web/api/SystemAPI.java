/**
 * ImageAPI.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.api;

import net.notasnark.worldgen.astro.planets.*;
import net.notasnark.worldgen.astro.stars.NoSuchStarException;
import net.notasnark.worldgen.astro.stars.Star;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import net.notasnark.utils.graphics.Icosahedron;
import net.notasnark.utils.graphics.SimpleImage;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.maps.PlanetMapper;
import net.notasnark.worldgen.astro.systems.NoSuchStarSystemException;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.astro.systems.StarSystemImage;
import net.notasnark.worldgen.exceptions.ApiException;
import net.notasnark.worldgen.web.Controller;
import net.notasnark.worldgen.web.Server;

import java.io.IOException;
import java.util.List;

import static spark.Spark.get;
import static spark.Spark.put;

/**
 * Defines the REST API for accessing information about a star system.
 */
public class SystemAPI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(SystemAPI.class);

    public void setupEndpoints() {
        logger.info("Setting up endpoints for SystemAPI");
        get("/api/system/:id/map", (request, response) -> getSystemMap(request, response));
        get("/api/system/:id/planets", (request, response) -> getPlanets(request, response), json());
        get("/api/star/:id/planets", (request, response) -> getPlanetsAroundStar(request, response), json());
        get("/api/planet/:id/map", (request, response) -> getPlanetMap(request, response));
        get("/api/planet/:id/maps", (request, response) -> getPlanetMaps(request, response), json());
        get("/api/planet/:id/orbitmap", (request, response) -> getOrbitMap(request, response));
    }


    /**
     * Gets a map of the star system as a JPEG image.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              Image data of type image/jpg.
     */
    public Object getSystemMap(Request request, Response response) {
        try {
            int  id = getIdParam(request, "id");
            int  width = getIntParamWithDefault(request,"width", 2048);
            int  scale = getIntParamWithDefault(request, "scale", -1);
            long time = getTimeParamWithDefault(request,"time", "0");
            boolean zones = getBooleanParamWithDefault(request, "zones", false);
            int  zoom = getIntParamWithDefault(request, "zoom", 0);
            int  planet = getIntParamWithDefault(request, "planet", 0);
            boolean mask = getBooleanParamWithDefault(request, "mask", false);
            int shadowX = getIntParamWithDefault(request, "shadowX", 0);
            int shadowY = getIntParamWithDefault(request, "shadowY", 0);
            boolean radar = getBooleanParamWithDefault(request, "radar", true);

            // Width should be between 64px and 4096px.
            width = Math.min(Math.max(64, width), 4096);

            try (WorldGen worldGen = Server.getWorldGen()) {

                StarSystemFactory factory = worldGen.getStarSystemFactory();

                StarSystem system = factory.getStarSystem(id);
                StarSystemImage image = new StarSystemImage(worldGen, system);


                response.type("image/jpg");

                image.setWidth(width);
                image.setScale(scale);
                image.setZones(zones);
                image.setZoom(zoom);
                image.setRadar(radar);
                if (time > 0) {
                    image.setTime(time);
                }
                if (planet > 0) {
                    image.setPlanet(planet);
                }
                if (mask) {
                    image.setShadows(mask, shadowX, shadowY);
                }

                return image.draw().save().toByteArray();
            } catch (NoSuchStarSystemException e) {
                throw new ApiException(404, String.format("There is no star system with id [%d]", id));
            } catch (IOException e) {
                throw new ApiException(500, String.format("Inteneral error drawing map (%s)", e.getMessage()));
            }
        } catch (ApiException e) {
            logger.error(String.format("getSystemMap: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());
            return e.getMessage();
        }
    }

    /**
     * Gets a list of all the planets that are part of the specified star system.
     *
     * @param request       HTTP Request object.
     * @param response      HTTP Response object.
     * @return              List of planets.
     */
    public List<Planet> getPlanets(Request request, Response response) {
        try {
            int  id = getIdParam(request, "id");

            logger.info(String.format("getPlanets: [%d]", id));

            try (WorldGen worldGen = Server.getWorldGen()) {
                StarSystem    system = worldGen.getStarSystemFactory().getStarSystem(id);
                PlanetFactory factory = worldGen.getPlanetFactory();

                response.type("application/json");

                List<Planet> planets = factory.getPlanets(system);
                for (Planet p : planets) {
                    if (p.isMoon()) {
                        Planet parent = planets.stream().filter(pp -> pp.getId() == p.getMoonOf()).findFirst().get();
                        p.updateExtras(parent);
                    } else {
                        p.updateExtras();
                    }
                }
                return planets;
            } catch (NoSuchStarSystemException e) {
                e.printStackTrace();
            }
        } catch (ApiException e) {
            logger.error(String.format("getPlanets: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());

            return null;
        }
        return null;
    }

    /**
     * Gets a list of all the planets that orbit the specified star.
     *
     * @param request       HTTP Request object.
     * @param response      HTTP Response object.
     * @return              List of planets.
     */
    public List<Planet> getPlanetsAroundStar(Request request, Response response) {
        try {
            int  id = getIdParam(request, "id");

            logger.info(String.format("getPlanetsAroundStar: [%d]", id));

            try (WorldGen worldGen = Server.getWorldGen()) {
                Star star = worldGen.getStarFactory().getStar(id);
                PlanetFactory factory = worldGen.getPlanetFactory();

                response.type("application/json");
                List<Planet> planets = factory.getPlanets(star);
                for (Planet p : planets) {
                    if (p.isMoon()) {
                        Planet parent = planets.stream().filter(pp -> pp.getId() == p.getMoonOf()).findFirst().get();
                        p.updateExtras(parent);
                    } else {
                        p.updateExtras();
                    }
                }
                return planets;
            } catch (NoSuchStarException e) {
                e.printStackTrace();
            }
        } catch (ApiException e) {
            logger.error(String.format("getPlanetsAroundStar: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());

            return null;
        }
        return null;
    }

    public Object getPlanetMap(Request request, Response response) {
        try {
            int id = getIdParam(request, "id");
            String name = getStringParamWithDefault(request, "name", PlanetMap.MAIN);
            boolean stretch = getBooleanParamWithDefault(request, "stretch", false);
            int width = getIntParamWithDefault(request, "width", 1024);

            logger.info(String.format("getPlanetMap: [%d] [%s]", id, name));

            try (WorldGen worldGen = Server.getWorldGen()) {
                PlanetFactory factory = worldGen.getPlanetFactory();

                SimpleImage image = factory.getPlanetMap(id, name);

                if (stretch) {
                    logger.info("Stretching the image");
                    image = Icosahedron.stretchImage(image, width);
                }

                response.type("image/png");
                return image.save(!stretch).toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ApiException e) {
            logger.error(String.format("getPlanetMap: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());
        }
        return null;
    }

    public Object getOrbitMap(Request request, Response response) {
        try {
            int id = getIdParam(request, "id");
            int width = getIntParamWithDefault(request, "width", 1024);
            int height = getIntParamWithDefault(request, "height", width / 2);
            long time = getTimeParamWithDefault(request,"time", "0");
            int size = Math.min(width, height);

            logger.info(String.format("getOrbitMap: [%d] [%d]", id, width));

            try (WorldGen worldGen = Server.getWorldGen()) {
                PlanetFactory factory = worldGen.getPlanetFactory();

                SimpleImage image = new SimpleImage(width, height, "#FFFFFF");

                Planet  planet = factory.getPlanet(id);
                Star    star = null;
                long    km = planet.getDistance() + planet.getRadius() * 2;

                long    kmPerPixel = km / (size / 2);

                if (planet.getParentId() > 0) {
                    star = worldGen.getStarFactory().getStar(planet.getParentId());
                } else {
                    StarSystem system = worldGen.getStarSystemFactory().getStarSystem(planet.getSystemId());
                    double mass = 0;
                    for (Star s : system.getStars()) {
                        if (s.getParentId() == planet.getParentId()) {
                            mass += s.getMass();
                        }
                    }
                    star = new Star();
                    star.setMass(mass);
                }

                PlanetMapper mapper = PlanetGenerator.getMapper(planet);
                if (time > 0) {
                    mapper.setTime(time);
                } else {
                    mapper.setTime(worldGen.getCurrentTime());
                }
                mapper.drawOrbit(image, star, width / 2, height / 2, kmPerPixel, factory.getMoons(planet));

                response.type("image/png");
                return image.save().toByteArray();
            } catch (IOException | NoSuchPlanetException | NoSuchStarException e) {
                e.printStackTrace();
            } catch (NoSuchStarSystemException e) {
                e.printStackTrace();
            }
        } catch (ApiException e) {
            logger.error(String.format("getPlanetMap: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());
        }
        return null;
    }

    /**
     * Gets a list of all the maps available for this planet.
     *
     * @param request       HTTP Request object.
     * @param response      HTTP Response object.
     * @return              List of available map names, as a string array.
     */
    public List<String> getPlanetMaps(Request request, Response response) {

        try {
            int id = getIdParam(request, "id");

            logger.info(String.format("getPlanetMaps: [%d]", id));

            try (WorldGen worldGen = Server.getWorldGen()) {
                PlanetFactory factory = worldGen.getPlanetFactory();

                return factory.getPlanetMaps(id);
            }
        } catch (ApiException e) {
            logger.error(String.format("getPlanetMap: %s", e.getMessage()));

            response.status(e.getStatusCode());
            response.body(e.getMessage());
        }
        return null;
    }
}
