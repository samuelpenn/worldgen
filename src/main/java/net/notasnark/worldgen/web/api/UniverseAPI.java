/*
 * Copyright (c) 2019, Samuel Penn (sam@notasnark.net).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.api;

import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.Universe;
import net.notasnark.worldgen.web.Controller;
import net.notasnark.worldgen.web.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import java.util.Map;

import static spark.Spark.get;

/**
 * API endpoint for providing high level information about the universe.
 */
public class UniverseAPI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(UniverseAPI.class);


    public void setupEndpoints() {
        logger.info("Setting up endpoints for UniverseAPI");
        get("/api/universe", (request, response) -> getUniverse(request, response), json());
        get("/api/statistics", (request, response) -> getStatistics(request, response), json());
    }

    /**
     * Gets basic meta data on this universe.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              JSON data.
     */
    public Object getUniverse(Request request, Response response) {
        logger.info("getUniverse:");

        try (WorldGen worldGen = Server.getWorldGen()) {
            Universe u = worldGen.getUniverse();

            return u;

        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Gets statistics on the data in this universe.
     *
     * @param request       Request object.
     * @param response      Response object.
     * @return              JSON data.
     */
    public Object getStatistics(Request request, Response response) {
        logger.info("getStatistics:");

        try (WorldGen worldGen = Server.getWorldGen()) {
            Map<String,Long> statistics = worldGen.getStatistics();

            return statistics;
        } catch (Exception e) {

        }

        return null;
    }
}
