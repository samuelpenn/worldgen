/**
 * SystemUI.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.ui;

import net.notasnark.worldgen.astro.Physics;
import net.notasnark.worldgen.astro.stars.Star;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.velocity.VelocityTemplateEngine;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetGroup;
import net.notasnark.worldgen.astro.sectors.NoSuchSectorException;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.sectors.SectorFactory;
import net.notasnark.worldgen.astro.systems.NoSuchStarSystemException;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemFactory;
import net.notasnark.worldgen.exceptions.ApiException;
import net.notasnark.worldgen.web.Controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.get;
import static net.notasnark.worldgen.Main.getWorldGen;

/**
 * User interface controller for displaying star systems. Calls the system page template, and
 * provides a model for the selected star system.
 *
 * Star systems can be selected using one of two methods:
 *
 * /system/:id - Specify the star system by its unique internal id.
 *
 * /system/:sector/:system - Specify the system by the sector it is in, and then a unique
 * identifier within that sector. The sector can be a sector name, unique id, or coordinate.
 * The system identifier can be the coordinates (XXYY) or the system name.
 */
public class SystemUI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(SystemUI.class);

    @Override
    public void setupEndpoints() {
        logger.info("Setting up endpoints for ConfigController");
        get("/system/:id", (request, response) -> showStarSystemById(request, response));
        get("/system/:id/animate", (request, response) -> animateStarSystem(request, response));
        get("/system/:sector/:system", (request, response) -> showStarSystemByName(request, response));
        get("/system/:sector/:system/survey", (request, response) -> surveyStarSystem(request, response));
    }

    private String toTitleCase(String text) {
        text = text.toLowerCase().replaceAll("_", " ").trim();

        String title = "";
        for ( String word : text.split(" ")) {
            title += word.substring(0, 1).toUpperCase() + word.substring(1) + " ";
        }

        return title.trim();
    }

    private Object showStarSystemByName(Request request, Response response) {
        try (WorldGen worldGen = getWorldGen()) {
            String sectorId = getStringParam(request, "sector");
            Sector sector = worldGen.getSectorFactory().getSectorByIdentifier(sectorId);

            String systemId = getStringParam(request, "system");
            StarSystem  system;
            if (StarSystemFactory.isCoord(systemId)) {
                system = worldGen.getStarSystemFactory().getStarSystem(sector,
                        StarSystemFactory.getXCoord(systemId),
                        StarSystemFactory.getYCoord(systemId));
            } else {
                system = worldGen.getStarSystemFactory().getStarSystem(sector, systemId);
            }

            return showStarSystem(worldGen, sector, system);
        } catch (ApiException e) {
            logger.error("Failed to display star system", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchSectorException e) {
            logger.error("No such sector", e);
            response.status(500);
            return "Sector not found (" + e.getMessage() + ")";
        } catch (NoSuchStarSystemException e) {
            logger.error("No such star system", e);
            response.status(404);
            return "System not found (" + e.getMessage() + ")";
        }
    }

    private Object showStarSystemById(Request request, Response response) {
        try (WorldGen worldGen = getWorldGen()) {

            int id = getIdParam(request, "id");
            StarSystem system = worldGen.getStarSystemFactory().getStarSystem(id);
            Sector sector = worldGen.getSectorFactory().getSector(system.getSectorId());


            return showStarSystem(worldGen, sector, system);
        } catch (ApiException e) {
            logger.error("Failed to display star system", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchStarSystemException e) {
            logger.error("No such star system", e);
            response.status(404);
            return "System not found (" + e.getMessage() + ")";
        } catch (NoSuchSectorException e) {
            logger.error("No such sector", e);
            response.status(500);
            return "Sector not found (" + e.getMessage() + ")";
        }

    }

    private Object showStarSystem(WorldGen worldGen, Sector sector, StarSystem system) {
        List<Planet>    planets = worldGen.getPlanetFactory().getPlanets(system);

        Map<String, Integer> count = new HashMap<>();
        for (Planet planet : planets) {
            if (!planet.isMoon()) {
                String group = planet.getType().getGroup().name();
                if (count.containsKey(group)) {
                    count.replace(group, count.get(group) + 1);
                } else {
                    count.put(group, 1);
                }
            }
        }

        Map<String,Object> model = new HashMap<>();
        model.put("id", system.getId());
        model.put("system", system);
        model.put("x", String.format("%02d", system.getX()));
        model.put("y", String.format("%02d", system.getY()));

        model.put("systemType", toTitleCase("" + system.getType()));
        model.put("systemZone", toTitleCase("" + system.getZone()));

        List<Map> starmap = new ArrayList<>();
        for (Star star : system.getStars()) {
            Map<String,Object> data = new HashMap<>();
            data.put("name", star.getName());
            data.put("luminosity", Physics.getSolarLuminosity(star));
            data.put("constant", Physics.getSolarConstant(star));
            DecimalFormat format = new DecimalFormat();
            long diam = star.getRadius() * 2;
            String units = "km";
            if (diam >= Physics.AU) {
                diam /= Physics.AU;
                units = "AU";
            } else if (diam >= 10_000_000) {
                diam /= 1_000_000;
                units = "Mkm";
            }

            data.put("diameter", format.format(Physics.round(diam, 2)) + units);
            starmap.add(data);
        }
        model.put("stardata", starmap);

        model.put("sector", sector);
        model.put("subsector", sector.getSubSectorName(system.getX(), system.getY()));
        model.put("count", count);
        model.put("version", WorldGen.getFullVersion());

        return new VelocityTemplateEngine().render(
                new ModelAndView(model, "templates/system.vm")
        );
    }

    private Object animateStarSystem(Request request, Response response) {
        try (WorldGen worldGen = getWorldGen()) {

            int id = getIdParam(request, "id");
            StarSystem system = worldGen.getStarSystemFactory().getStarSystem(id);
            Sector sector = worldGen.getSectorFactory().getSector(system.getSectorId());

            List<Planet>    planets = worldGen.getPlanetFactory().getPlanets(system);

            Map<String, Integer> count = new HashMap<>();
            for (Planet planet : planets) {
                if (!planet.isMoon()) {
                    String group = planet.getType().getGroup().name();
                    if (count.containsKey(group)) {
                        count.replace(group, count.get(group) + 1);
                    } else {
                        count.put(group, 1);
                    }
                }
            }

            Map<String,Object> model = new HashMap<>();
            model.put("id", system.getId());
            model.put("system", system);
            model.put("x", String.format("%02d", system.getX()));
            model.put("y", String.format("%02d", system.getY()));

            model.put("systemType", toTitleCase("" + system.getType()));
            model.put("systemZone", toTitleCase("" + system.getZone()));

            model.put("sector", sector);
            model.put("subsector", sector.getSubSectorName(system.getX(), system.getY()));
            model.put("count", count);
            model.put("version", WorldGen.getFullVersion());

            return new VelocityTemplateEngine().render(
                    new ModelAndView(model, "templates/animate.vm")
            );

        } catch (Exception e) {
            logger.error("Animation exception", e);
            response.status(500);
            return "Animation error (" + e.getMessage() + ")";
        }
    }

    /**
     * Display survey data on the specified star system.
     *
     * @param request
     * @param response
     * @return
     */
    private Object surveyStarSystem(Request request, Response response) {
        try (WorldGen worldGen = getWorldGen()) {
            String sectorId = getStringParam(request, "sector");
            Sector sector = worldGen.getSectorFactory().getSectorByIdentifier(sectorId);

            String systemId = getStringParam(request, "system");
            StarSystem  system;

            int scan = getIntParamWithDefault(request, "scan", 2);
            if (StarSystemFactory.isCoord(systemId)) {
                system = worldGen.getStarSystemFactory().getStarSystem(sector,
                        StarSystemFactory.getXCoord(systemId),
                        StarSystemFactory.getYCoord(systemId));
            } else {
                system = worldGen.getStarSystemFactory().getStarSystem(sector, systemId);
            }

            return surveyStarSystem(worldGen, sector, system, scan);
        } catch (ApiException e) {
            logger.error("Failed to display star system", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchSectorException e) {
            logger.error("No such sector", e);
            response.status(500);
            return "Sector not found (" + e.getMessage() + ")";
        } catch (NoSuchStarSystemException e) {
            logger.error("No such star system", e);
            response.status(404);
            return "System not found (" + e.getMessage() + ")";
        }
    }

    private Object surveyStarSystem(WorldGen worldGen, Sector sector, StarSystem system, int scan) {

        String systemName = system.getName();

        List<String> data = new ArrayList<>();

        if (scan >= 1) {
            // Presence of stars
            data.add("<b>Star System Type:</b> " + toTitleCase(""+system.getType()));

            for (Star star : system.getStars()) {
                String text = star.getName();
                long   au = star.getDistance() / Physics.AU;
                text += " (" + au + " AU)";
                if (scan >= 2) {
                    text += " (" + star.getLuminosity() + ")";
                }
                if (scan >= 3) {
                    text += " (" + star.getSpectralType() + ")";
                }
                if (scan >= 4) {
                    text += " (" + star.getRadius() * 2 + "km diameter)";
                }
                data.add(text);

                if (scan >= 4) {
                    List<Planet>    planets = worldGen.getPlanetFactory().getPlanets(system);
                    long minSize = 100_000;
                    long dist = 10;
                    if (scan == 5) {
                        minSize = 25_000;
                    } else if (scan == 6) {
                        minSize = 5_000;
                        dist = 5;
                    } else if (scan == 7) {
                        minSize = 1_000;
                        dist = 3;
                    } else if (scan == 8) {
                        minSize = 200;
                        dist = 1;
                    } else if (scan >= 9) {
                        minSize = 100;
                        dist = 1;
                    }

                    for (Planet p : planets) {
                        if (!p.isMoon() && p.getParentId() == star.getId()) {
                            if (p.getRadius() >= minSize) {
                                text = "";

                                // Get the distance, to within a given accuracy.
                                long d = ((p.getDistance() / Physics.AU + dist / 2) / dist) * dist;
                                long r = ((p.getRadius() + minSize / 2) / minSize) * minSize;

                                text = "" + p.getType().getGroup();
                                /*
                                if (p.getRadius() > minSize * 2) {
                                    text += "." + p.getType().getClassification();
                                }
                                if (p.getRadius() > minSize * 5) {
                                    text += "." + p.getType();
                                }
                                */
                                if (p.getRadius() > minSize * 2 && p.getType().getGroup() != PlanetGroup.Belt) {
                                    text += " (" + r * 2 + "km)";
                                }
                                if (Math.sqrt(p.getPressure()) > minSize) {
                                    text += " " + p.getPressureText();
                                    if (Math.sqrt(p.getPressure()) > minSize * 5) {
                                        text += " " + p.getAtmosphere();
                                    }
                                }

                                data.add("" + d + "AU " + text);
                            }
                        }
                    }
                }
            }
        }

        Map<String,Object> model = new HashMap<>();
        model.put("sectorName", sector.getName());
        model.put("sectorNumber", SectorFactory.getSectorNumber(sector.getX(), sector.getY()));
        model.put("systemName", systemName);
        model.put("x", String.format("%02d", system.getX()));
        model.put("y", String.format("%02d", system.getY()));

        model.put("data", data);

        return new VelocityTemplateEngine().render(
                new ModelAndView(model, "templates/system-survey.vm")
        );
    }
}
