/**
 * SystemUI.java
 *
 * Copyright (c) 2017, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.ui;

import net.notasnark.worldgen.Main;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.sectors.NoSuchSectorException;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.survey.SystemSurvey;
import net.notasnark.worldgen.astro.systems.*;
import net.notasnark.worldgen.exceptions.ApiException;
import net.notasnark.worldgen.web.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.velocity.VelocityTemplateEngine;
import net.notasnark.worldgen.astro.sectors.SectorFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.get;

/**
 * GUI controller for the Sector information page. Shows basic information on the
 * sector, simple navigation controls plus list of star systems.
 */
public class SurveyUI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(SurveyUI.class);

    @Override
    public void setupEndpoints() {
        logger.info("Setting up endpoints for SectorUI");
        get("/survey/:id/map", (request, response) -> surveySectorMap(request, response));
        get("/survey/:id/system/:system", (request, response) -> surveySystemData(request, response));
    }

    public Object surveySystemData(Request request, Response response) {
        try (WorldGen worldGen = Main.getWorldGen()) {
            String sectorId = getStringParam(request, "id");
            String systemId = getStringParam(request, "system");

            // Get information on this sector.
            SectorFactory factory = worldGen.getSectorFactory();
            Sector sector = null;
            int x, y;
            try {
                sector = factory.getSectorByIdentifier(sectorId);
                x = sector.getX();
                y = sector.getY();
            } catch (NoSuchSectorException e) {
                // No sector found. How we respond depends on how sector was requested.
                if (SectorFactory.isCoord(sectorId)) {
                    x = SectorFactory.getXCoord(sectorId);
                    y = SectorFactory.getYCoord(sectorId);
                    sector = new Sector("Uncharted Sector " + SectorFactory.getSectorNumber(x, y), x, y);
                } else {
                    throw e;
                }
            }

            // Populate model with information.
            Map<String,Object> model = new HashMap<>();
            model.put("sector", sector);
            model.put("sectorName", sector.getName());
            model.put("sectorNumber",SectorFactory.getSectorNumber(sector.getX(), sector.getY()));
            model.put("version", WorldGen.getFullVersion());

            StarSystemFactory systemFactory = worldGen.getStarSystemFactory();
            StarSystem system = null;

            try {
                if (StarSystemFactory.isCoord(systemId)) {
                    system = systemFactory.getStarSystem(sector,
                            StarSystemFactory.getXCoord(systemId),
                            StarSystemFactory.getYCoord(systemId));
                } else {
                    system = systemFactory.getStarSystem(sector, systemId);
                }
                SurveyFactory surveyFactory = worldGen.getSurveyFactory();
                Survey survey = surveyFactory.getSurvey(system.getId());

                Map<String, Object> data = new HashMap<>();
                if (survey.getScan() > 0) {
                    model.put("systemName", system.getName());
                    model.put("x", String.format("%02d", system.getX()));
                    model.put("y", String.format("%02d", system.getY()));

                    SystemSurvey systemSurvey = surveyFactory.getSystemSurvey(system);

                    model.put("density", systemSurvey.density);
                    model.put("type", systemSurvey.type);
                    model.put("scan", systemSurvey.scan);

                    model.put("survey", systemSurvey);
                    System.out.println("COGS:" + systemSurvey.cogs.size());
                }


            } catch (NoSuchStarSystemException e) {
                e.printStackTrace();
            }

            return new VelocityTemplateEngine().render(
                    new ModelAndView(model, "templates/system-survey.vm")
            );

        } catch (ApiException e) {
            logger.error("Failed to display star system", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchSectorException e) {
            logger.error("No such sector", e);
            response.status(404);
            return "Sector not found (" + e.getMessage() + ")";
        }
    }

    public Object surveySectorMap(Request request, Response response) {
        try (WorldGen worldGen = Main.getWorldGen()) {
            String  id = getStringParam(request, "id");
            int     size = getIntParamWithDefault(request, "size", 0);
            int     scan = getIntParamWithDefault(request, "scan", 0);
            boolean god = getBooleanParamWithDefault(request, "god", false);
            boolean border = getBooleanParamWithDefault(request, "border", false);

            // Get information on this sector.
            SectorFactory factory = worldGen.getSectorFactory();
            Sector sector = null;
            List<StarSystem> list = null;
            int x, y;
            try {
                sector = factory.getSectorByIdentifier(id);
                list = worldGen.getStarSystemFactory().getStarSystems(sector);
                x = sector.getX();
                y = sector.getY();
            } catch (NoSuchSectorException e) {
                // No sector found. How we respond depends on how sector was requested.
                if (SectorFactory.isCoord(id)) {
                    x = SectorFactory.getXCoord(id);
                    y = SectorFactory.getYCoord(id);
                    sector = new Sector("Uncharted Sector " + SectorFactory.getSectorNumber(x, y), x, y);
                } else {
                    throw e;
                }
            }


            // Populate model with information.
            Map<String,Object> model = new HashMap<>();
            model.put("sector", sector);
            model.put("name", sector.getName());
            model.put("version", WorldGen.getFullVersion());
            model.put("scan", scan);
            model.put("god", god);

            return new VelocityTemplateEngine().render(
                    new ModelAndView(model, "templates/sector-survey-map.vm")
            );

        } catch (ApiException e) {
            logger.error("Failed to display star system", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchSectorException e) {
            logger.error("No such sector", e);
            response.status(404);
            return "Sector not found (" + e.getMessage() + ")";
        }
    }
}
