/**
 * SystemUI.java
 *
 * Copyright (c) 2018, Samuel Penn.
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.web.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.velocity.VelocityTemplateEngine;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.exceptions.ApiException;
import net.notasnark.worldgen.exceptions.NoSuchObjectException;
import net.notasnark.worldgen.web.Controller;

import java.util.HashMap;
import java.util.Map;

import static spark.Spark.get;
import static net.notasnark.worldgen.Main.getWorldGen;

/**
 * User interface controller for displaying a planet.
 *
 * /planet/:id - Specify the planet by its unique internal id.
 */
public class PlanetUI extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(PlanetUI.class);

    @Override
    public void setupEndpoints() {
        logger.info("Setting up endpoints for PlantUI");
        get("/planet/:id", (request, response) -> showPlanetById(request, response));
    }

    private Object showPlanetById(Request request, Response response) {
        try (WorldGen worldGen = getWorldGen()) {

            int id = getIdParam(request, "id");
            Planet planet = worldGen.getPlanetFactory().getPlanet(id);


            Map<String,Object> model = new HashMap<>();
            model.put("id", planet.getId());
            model.put("version", WorldGen.getFullVersion());

            return new VelocityTemplateEngine().render(
                new ModelAndView(model, "templates/planet.vm")
            );
        } catch (ApiException e) {
            logger.error("Failed to display planet", e);
            response.status(500);
            return "Internal server error (" + e.getMessage() + ")";
        } catch (NoSuchObjectException e) {
            logger.error("No such planet", e);
            response.status(404);
            return "Planet not found (" + e.getMessage() + ")";
        }
    }
}
