package net.notasnark.worldgen.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import net.notasnark.worldgen.WorldGen;

import static spark.Spark.get;

public class IndexController extends Controller {
    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @Override
    public void setupEndpoints() {
        logger.info("Setting up endpoints for ConfigController");
        get("/image", (request, response) -> getGalaxyMap(request, response));
    }

    public Object getGalaxyMap(Request request, Response response) {
        try (WorldGen worldGen = Server.getWorldGen()) {
            response.type("image/png");

            return worldGen.getGalaxyMapGrid().save().toByteArray();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
