
function getPlural(value, text) {
    let num = parseInt((""+value).replace(/,/, ""));
    if (num === 0) {
        return "";
    } else if (num === 1) {
        return value + " " + text + " ";
    } else {
        return value + " " + text + "s ";
    }
}

function getNumber(number) {
    let formatter = new Intl.NumberFormat('en-GB', { });

    return formatter.format(parseInt(number));
}

function getDistance(km) {
    let formatter = new Intl.NumberFormat('en-GB', {
    });
    let result = km;
    let MILLION = 1000000;

    if (km < 10 * MILLION) {
        result = formatter.format(km) + " km";
    } else if (km < 1000 * MILLION) {
        km = parseInt(km / MILLION);
        result = formatter.format(km) + " Mkm";
    } else {
        km = parseInt(km / MILLION);
        result = formatter.format(km) + " Mkm";
        let au = parseInt(km / 150);
        result += " (" + formatter.format(au) + " AU)";
    }

    return result;
}

function getPressure(pascals) {
    let STANDARD = 100000;

    if (pascals >= 10 * STANDARD) {
        return parseInt(pascals / STANDARD) + " x Std";
    } else if (pascals >= 0.05 * STANDARD) {
        return parseInt((pascals * 100) / STANDARD) + "% Std";
    } else {
        let pct = parseInt((pascals * 1000) / STANDARD) / 10.0;
        return pct + "% Std";
    }
}

function getPeriod(seconds) {
    let period = "";
    let retrograde = false;

    seconds = parseInt(seconds);
    if (seconds < 0) {
        retrograde = true;
        seconds = Math.abs(seconds);
    } else if (seconds === 0) {
        return "&#x221e;";
    }

    let s = parseInt(seconds % 60);
    seconds /= 60;

    let m = parseInt(seconds % 60);
    seconds /= 60;

    let h = parseInt(seconds % 24);
    seconds /= 24;

    let d = parseInt(seconds % 365);
    let y = parseInt(seconds / 365);

    period += getPlural(getNumber(y), "year");

    if (y < 30) {
        // Include days if less than 30 years.
        period += getPlural(d, "day");

        if (y === 0 && d < 30) {
            // Include hours if less than 30 days.
            period += getPlural(h, "hour");

            if (y === 0 && d < 7) {
                // Include minutes if less than 7 days.
                period += getPlural(m, "minute");

                if (y === 0 && d === 0 && h < 10) {
                    // Include seconds if less than 10 hours.
                    period += getPlural(s, "second");
                }
            }
        }
    }

    return period.trim() + (retrograde?" (Retrograde)":"");
}

function getVelocity(ms) {
    let formatter = new Intl.NumberFormat('en-GB', {});

    let result = "";
    if (ms < 2500) {
        // Show in m/s.
        result = formatter.format(ms) + "ms<sup>-1</sup>";
    } else if (ms < 25000) {
        // Show in km/s to one decimal place.
        ms = parseInt(ms / 100);
        ms = ms / 10.0
        result = formatter.format(ms) + "kms<sup>-1</sup>";
    } else {
        // Show in km/s, as integer.
        ms = parseInt(ms / 1000);
        result = formatter.format(ms) + "kms<sup>-1</sup>";
    }
    return result;
}

starSystem = {};

function listPlanetsAroundStar(starId) {
    let $planetMenu = $("#planet-menu-"+starId);
    let $planetContent = $("#planet-content-"+starId);
    let first = true;
    for (let id in starSystem.planets[starId]) {
        // noinspection JSUnfilteredForInLoop
        let planet = starSystem.planets[starId][id];

        if (planet.moonOf === 0) {

            let suffix = "";
            if (planet.population > 0) {
                suffix = "*";
            } else {
                for (let m in starSystem.planets[starId]) {
                    if (starSystem.planets[starId][m].moonOf === planet.id) {
                        if (starSystem.planets[starId][m].population > 0) {
                            suffix = "*";
                            break;
                        }
                    }
                }
            }

            let icon = "/icons/systems/group_" + planet.extra["Group"] + ".png";
            let title = `<img src="${icon}" width="32px" align="center"/> ${planet.name.replace(/.* /, "")} ${suffix}`;

            if (first) {
                first = false;
                $planetMenu.append(`<li class="nav-item"><a class="nav-link active" data-toggle="pill" id="planet-${planet.id}-tab" href="#planet-${planet.id}" role="tab">${title}</a></li>`);
                $planetContent.append(`<div class="tab-pane show active planet" id="planet-${planet.id}" role="tabpanel"></div>`);
            } else {
                $planetMenu.append(`<li class="nav-item"><a class="nav-link" data-toggle="pill" id="planet-${planet.id}-tab" href="#planet-${planet.id}" role="tab">${title}</a></li>`);
                $planetContent.append(`<div class="tab-pane" id="planet-${planet.id}" role="tabpanel"></div>`);
            }

            //showPlanet(starId, planet);
            updatePlanet(starId, planet);
        }

    }
}

function showMap(id) {
    $(`#map-${id}`).html("");
    $(`#map-${id}`).append(`<img src="/api/planet/${id}/map" width="100%"/>`);
}

function showStretchedMap(id) {
    $(`#map-${id}`).html("");
    $(`#map-${id}`).append(`<img src="/api/planet/${id}/map?stretch=true" width="100%"/>`);
}

function showOrbitMap(id) {
    $(`#map-${id}`).html("");
    $(`#map-${id}`).append(`<img src="/api/planet/${id}/orbitmap?width=1024&height=512" height="100%"/>`);
}

function showPlanetSystemMap(systemId, planetId) {
    $(`#map-${planetId}`).html("");
    $(`#map-${planetId}`).append(`<img style="margin: 0 auto; display: block" src="/api/system/${systemId}/map?width=1024&height=512&planet=${planetId}" height="100%"/>`);
}

/**
 * Display a 3D spinning globe using this world map.
 * @param id          ID of the planet to be shown.
 * @param planet      Planet data.
 * @param cloudLayers Whether the planet has clouds.
 */
function showGlobe(id, planet, hasDeform, cloudLayers, moons) {
    $(`#map-${id}`).html("");

    let scene = new THREE.Scene();
    let camera = new THREE.PerspectiveCamera(75, 800 / 400, 0.1, 2000);
    let renderer = new THREE.WebGLRenderer();

    let radius = 10;
    let scale = 10 / planet.radius;

    renderer.setSize(800, 400);
    renderer.shadowMap.enabled = true;

    let viewPort = document.getElementById(`map-${id}`);
    viewPort.appendChild(renderer.domElement);
    let textureLoader = new THREE.TextureLoader();

    let geometry   = new THREE.SphereGeometry(radius, 32, 32);
    let mainTexture = textureLoader.load(`/api/planet/${id}/map?stretch=true&width=1024`);
    let heightTexture = textureLoader.load(`/api/planet/${id}/map?stretch=true&name=height&width=1024`);
    let material = null;

    if (hasDeform) {
        let deformTexture = textureLoader.load(`/api/planet/${id}/map?stretch=true&name=deform&width=1024`);
        material = new THREE.MeshPhongMaterial({
            map: mainTexture, bumpMap: heightTexture, bumpScale: 0.5,
            displacementMap: deformTexture, displacementBias: -0.5, shininess: 0.0, specular: 0xaaaaaa, displacementScale: 5
        });
    } else {
        material = new THREE.MeshPhongMaterial({
            map: mainTexture, bumpMap: heightTexture, bumpScale: 0.5,
            shininess: 0.0, specular: 0xaaaaaa
        });
    }

    let world = new THREE.Mesh(geometry, material);
    world.receiveShadow = true;
    world.castShadow = true;

    let light = new THREE.AmbientLight(0x888888);
    scene.add(light);

    let albedo = parseFloat(planet.extra["albedo"]);
    let starLight = new THREE.PointLight(0xffffff, 1, 1000 * albedo, 2);
    starLight.position.set(radius, 0, radius * 10);
    //starLight.position.multiplyScalar(3);
    starLight.castShadow = true;
    scene.add(starLight);

    scene.add(world);
    let clouds = [];

    if (cloudLayers.length > 0) {
        let baseRadius = radius + 0.1;

        // Needs to loop backwards. Not entirely certain why. May be due to order objects
        // are rendered in.
        for (let layer=cloudLayers.length-1; layer >= 0; layer--) {
        //for (let layer = 0; layer < cloudLayers.length; layer++) {
            let height = parseInt((""+cloudLayers[layer]).replace(/[^0-9]/g, ""));
            let r = baseRadius + height / 100.0;
            let cloudGeometry = new THREE.SphereGeometry(r, 32, 32);
            let cloudTexture = textureLoader.load(`/api/planet/${id}/map?name=${cloudLayers[layer]}&width=1024`);

            let cloudMaterial = new THREE.MeshPhongMaterial({ map: cloudTexture, transparent: true, side: THREE.DoubleSide });
            let cloudLayer = new THREE.Mesh(cloudGeometry, cloudMaterial);

            clouds.push(cloudLayer);
            scene.add(cloudLayer);
        }

    }

    let rings = [];
    if (moons.length > 0) {
        for (let m=0; m < moons.length; m++) {
            let moon = moons[m];
            if (moon.type.indexOf("Ring") > -1) {
                let ringTexture = textureLoader.load(`/api/planet/${moon.id}/map?name=ring&width=1024`);

                let inner = (moon.distance - moon.radius) * scale;
                let outer = (moon.distance + moon.radius) * scale;

                let ringGeometry = new THREE.RingGeometry(inner, outer, 64, 64);
                ringGeometry.rotateX(Math.PI / 2);
                let ringMaterial = new THREE.MeshBasicMaterial({color: 0xffffdd, side: THREE.DoubleSide});
                ringMaterial = new THREE.MeshPhongMaterial({
                    map: ringTexture, bumpMap: heightTexture, bumpScale: 0.5,
                    shininess: 0.0, specular: 0xaaaaaa, side: THREE.DoubleSide, transparent: true
                });

                let ring = new THREE.Mesh(ringGeometry, ringMaterial);
                //ring.receiveShadow = true;
                ring.castShadow = true;
                scene.add(ring);
                rings.push(ring);
            }
        }

    }

    camera.position.z = radius * 3;

    let cloudRotation = 0;
    let pauseRotation = 0;
    viewPort.addEventListener('mousemove', function(event) {
        var mouse = { x: 0, y: 0};
        mouse.x = (event.clientX - renderer.domElement.offsetLeft) / renderer.domElement.width - 0.5;
        mouse.y = (event.clientY - renderer.domElement.offsetTop) / renderer.domElement.height - 0.5;

        world.rotation.x = Math.PI * 2 * mouse.y;
        world.rotation.y = Math.PI * 2 * mouse.x;
        if (rings.length > 0) {
            for (let r=0; r < rings.length; r++) {
                rings[r].rotation.x = Math.PI * 2 * mouse.y;
                rings[r].rotation.y = Math.PI * 2 * mouse.x;
            }
        }

        pauseRotation = 200;
    }, false);

    viewPort.addEventListener('wheel', function(event) {
        let e = window.event;
        if (e.wheelDelta < 0) {
            camera.position.z *= 1.1;
        } else if (e.wheelDelta > 0) {
            camera.position.z *= 0.9;
        }
        if (camera.position.z < radius + 1) {
            camera.position.z = radius + 1;
        }
        event.preventDefault();
        return false;
    }, false);

    let animate = function () {
        requestAnimationFrame( animate );

        if (pauseRotation) {
            pauseRotation--;

            if (clouds.length > 0) {
                for (let l=0; l < clouds.length; l++) {
                    clouds[l].material.opacity = (1 - (pauseRotation / 200));
                }
            }

        } else {
            world.rotation.y += 0.001;
        }
        if (clouds.length > 0) {
            cloudRotation += 0.001;
            if (cloudRotation > 2 * Math.PI) {
                cloudRotation -= 2 * Math.PI;
            }
            for (let l=0; l < clouds.length; l++) {
                clouds[l].rotation.x = world.rotation.x;
                clouds[l].rotation.y = world.rotation.y + (cloudRotation * (l+1));
            }
        }

        renderer.render(scene, camera);
    };
    animate();

}

function getHabitability(planet) {
    switch (planet.habitability) {
        case 1:
            return "I (Comfortable)";
        case 2:
            return "II (Harsh)";
        case 3:
            return "III (Mask)";
        case 4:
            return "IV (Vac Suit)";
        case 5:
            return "V (Heavy Vac Suit)";
        case 6:
            return "VI (Rigid Suit)";
    }
}

function toSpace(camel) {
    return camel.replace(/([A-Z])/g, " $1").trim();
}

function updatePlanet(starId, planet, parent) {
    let $planetInfo = $('#planet-' + planet.id);

    let distance = planet.distance;
    if (parent && parent.extra['Group'] === 'Belt') {
        distance += parent.distance;
    }

    $planetInfo.html("");
    $planetInfo.append(`<div id="outermap-${planet.id}"><div id='map-${planet.id}' class='planet-map'></div></div>`, '');
    $planetInfo.append(`<h4>${planet.name}</h4>`, '');
    $planetInfo.append(`<dl id='planet-data-${planet.id}' class='data-block'></dl>`, '');

    let $data = $('#planet-data-'+planet.id);
    $data.append(`<dt>Type</dt><dd>${toSpace(planet.type)} (${toSpace(planet.extra['Group'])})</dd>`);
    $data.append(`<dt>Habitability</dt><dd>${getHabitability(planet)}</dd>`);
    $data.append(`<dt>Distance</dt><dd>${getDistance(distance)}</dd>`);
    $data.append(`<dt>${parent?"Orbit period":"Year"}</dt><dd>${getPeriod(planet.period)}</dd>`);
    $data.append(`<dt>Radius</dt><dd>${getDistance(planet.radius)}</dd>`);
    if (planet.dayLength > 0) {
        $data.append(`<dt>Length of Day</dt><dd>${getPeriod(planet.extra["solarDay"])}</dd>`);
        let solar = parseInt(planet.extra["solarDay"]);
        let sidereal = parseInt(planet.dayLength);
        if (Math.abs(solar - sidereal) > 3600) {
            $data.append(`<dt>Sidereal Day</dt><dd>${getPeriod(planet.dayLength)}</dd>`);
        }
    }
    if (planet.extra['Group'] !== 'Belt') {
        $data.append(`<dt>Gravity</dt><dd>${planet.extra["g"]}ms<sup>-2</sup> (V<sub>e</sub> = ${getVelocity(planet.extra["escapeVelocity"])})</dd>`);
        if (planet.extra["spinGravity"]) {
            $data.append(`<dt>Spin Gravity</dt><dd>${planet.extra["spinGravity"]}ms<sup>-2</sup></dd>`);
        }
    } else {
        $data.append(`<dt>Density</dt><dd>${planet.density}</dd>`);
    }
    $data.append(`<dt>Atmosphere</dt><dd>${planet.pressure === 0?"":getPressure(planet.pressure)} ${toSpace(planet.atmosphere)}</dd>`);
    $data.append(`<dt>Temperature</dt><dd>${getNumber(planet.temperature)} K (${getNumber(planet.temperature - 273)} °C)</dd>`);
    if (planet.nightTemperature < planet.temperature - 10) {
        $data.append(`<dt>Nightside</dt><dd>${getNumber(planet.nightTemperature)} K (${getNumber(planet.nightTemperature - 273)} °C)</dd>`);
    }
    if (planet.hydrographics > 0) {
        $data.append(`<dt>Hydrographics</dt><dd>${planet.hydrographics}%</dd>`);
    }
    if (planet.life != "None") {
        $data.append(`<dt>Life</dt><dd>${planet.life}</dd>`);
    }

    if (planet.population > 0) {
        $data.append(`<dt>Population</dt><dd>${getNumber(planet.population)}</dd>`);
        $data.append(`<dt>Government</dt><dd>${planet.government} (${planet.law})</dd>`);
        $data.append(`<dt>Star Port</dt><dd>${planet.port}</dd>`);
        $data.append(`<dt>Tech Level</dt><dd>${planet.techLevel}</dd>`);
    }

    $planetInfo.append(`<p style="clear:left">${planet.description}</p>`);

    $planetInfo.append(`<div id='resources-list-${planet.id}' class='planet-resources'></div>`);
    let $planetResources = $(`#resources-list-${planet.id}`);

    $planetResources.append(`<ul id="resources-${planet.id}" class="resource-list"></ul>`);
    let $list = $(`#resources-${planet.id}`);

    planet.resources.sort(function(a, b) { return b.density - a.density });
    var count = 0;
    $.each(planet.resources, function(r) {
        let resource = planet.resources[r];
        let density = resource.density / 10.0;
        if (density >= 100) {
            density = density.toPrecision(3);
        } else {
            density = density.toPrecision(2);
        }
        if (++count > 12) {
            return;
        }
        $list.append(`<li><img src="/icons/${resource.commodity.image}.svg" title="${resource.commodity.name}" 
                width="64px"/><br/><span>${density}%</span></li>`)
    });

    updateMaps(planet);

    $planetInfo.append("<div style='clear:both;'></div>");
    for (let p in starSystem.planets[starId]) {
        let moon = starSystem.planets[starId][p];
        if (moon.moonOf === planet.id) {
            $planetInfo.append(`<div id="planet-${moon.id}" class="moon"></div>`);
            updatePlanet(starId, moon, planet);
        }
    }
}

function updateMaps(planet) {

    var hasMoons = false;
    var moonList = [];
    for (let p in starSystem.planets[planet.parentId]) {
        let moon = starSystem.planets[planet.parentId][p];
        if (moon.moonOf === planet.id) {
            hasMoons = true;
            moonList.push(starSystem.planets[planet.parentId][p]);
        }
    }

    $.getJSON(`/api/planet/${planet.id}/maps`, function(mapList) {
        let hasMain = false, hasDeform = false;
        let cloudLayers = [];

        for (let i=0; i < mapList.length; i++) {
            if (mapList[i] === "main") {
                hasMain = true;
            } else if (mapList[i] === "deform") {
                hasDeform = true;
            } else if (mapList[i].startsWith("cloud-")) {
                // A planet can have multiple cloud layers.
                cloudLayers.push(mapList[i]);
            }
        }
        if (hasMain) {
            $(`#map-${planet.id}`).before(`<div id='ctrls-${planet.id}' class='btn-group planet-controls' data-toggle='buttons'></div>`);

            $(`#ctrls-${planet.id}`).append(`<label class="btn btn-secondary btn.sm active" ><input type="radio" value="map" checked/>Map</label>`);
            $(`#ctrls-${planet.id}`).append(`<label class="btn btn-secondary btn.sm"><input type="radio" value="globe">Globe</label>`);
            $(`#ctrls-${planet.id}`).append(`<label class="btn btn-secondary btn.sm"><input type="radio" value="stretch">Stretched</label>`);

            if (hasMoons) {
                $(`#ctrls-${planet.id}`).append(`<label class="btn btn-secondary btn.sm"><input type="radio" value="moons">Moons</label>`);
            }

            $(`#ctrls-${planet.id} label`).click(function () {
                $(this).addClass('active').siblings().removeClass('active');

                let type = $(this).find("input").val();
                let id = ("" + $(this).parent().attr("id")).replace(/\D/g, '');

                if (type === "map") {
                    showMap(id);
                } else if (type === "stretch") {
                    showStretchedMap(id);
                } else if (type === "globe") {
                    showGlobe(id, planet, hasDeform, cloudLayers, moonList);
                } else if (type == "moons") {
                    showPlanetSystemMap(planet.systemId, planet.id);
                }
            });
            showMap(planet.id);
        } else if (planet.moonOf !== 0) {
            showPlanetSystemMap(planet.systemId, planet.id);
        } else {
            showOrbitMap(planet.id);
        }
    });
}
