#
# Copyright (c) 2020, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#

# Special properties of SmallBody worlds which can be applied to any type.
# These can be pulled in by a specific template.

SmallBody.Common=There are natural [empty pockets|caverns] deep under the surface, sealed off against the vacuum.

SmallBody.Common.1=The surface is covered with loose rubble, left over fragments of impacts.

SmallBody.Common.2=It is covered in [heavy|thick] layers of dust, which if [disturbed|stirred up] can hang \
  around for minutes in the low gravity.

SmallBody.Common.3=Holes cover the surface of $Name, mostly hidden in dark shadows within craters. Some lead \
  [tens of metres|hundreds of metres|deep|far] beneath the surface of the asteroid and are metres across.

SmallBody.UnCommon=There is plenty of loose rubble on the surface, which [sometimes|often] shifts as $Name \
  rotates.

SmallBody.Uncommon.1=[Strange|Unusual] looking scratch marks cover the surface [near the equator|around the poles] \
  and it [isn't clear|is unknown] whether they are natural or artificial.

SmallBody.Rare=The angle of rotation of [the world|$Name] shifts several [seconds|minutes] every few [months|years|decades], \
  possibly due to an imbalance in the core.

SmallBody.Rare.1=There is a fine shell of dust about [10km|30km|50km] above the surface of $Name. It's \
  [incredibly|very] unstable, so unlikely to last [for very|] long.

SmallBody.VeryRare=There are [strange|unusual|unrecognisable] [shapes|patterns] under the surface of $Name, \
  made out of [substance|metals|elements] that [shouldn't exist|can't have got here naturally|are artificial in nature]. \
  @+ExoticMetals

SmallBody.VeryRare.1=Tremors are [occasionally|sometimes|often] detected from deep within the core of \
  [this body|$Name], but they are [too regular] to be natural, and have no known means of generation.

SmallBody.VeryRare.2=There are [regular structures|regularities|patterns in the structure|structures] deep [near|in] \
  the core [of $Name|of this body|] which seem [artificial|regular|highly unusual] in nature, though nobody has explored \
  [deep|far] enough down [in order|] to [determine|figure out|find out] [just|exactly|] what they are.

SmallBody.VeryRare.3=[There are magnetic anomalies|Magnetic anomalies] are present [deep beneath|just under] \
  the [surface|crust] of $Name, [powerful|intense|abnormal] magnetic fields which are spread [randomly|regularly] \
  around the asteroid.