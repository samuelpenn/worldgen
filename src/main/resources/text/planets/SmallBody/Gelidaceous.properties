#
# Copyright (c) 2017, Samuel Penn (sam@notasnark.net).
# See the file LICENSE at the root of the project.
#

planet=<em>Gelidaceous</em> asteroids are a [mix|mixture] of [ice and rock|rock and ice], and \
  are generally found [in the outer solar system|beyond the snow line], where it is cold enough \
  for icy volatiles to remain frozen.

planet.1=[This|$Name] is a large asteroid comprised mostly of [water ice|ice], with a mixture \
  of silicate rocks. Formed in the outer system beyond the snow line, $Name is low on heavy metals but \
  [is|may be viewed as] a good source of water ice.

planet.2=An icy <em>Gelidaceous</em> asteroid [mixed with|with] [lumps|chunks] of \
  [rock|rocky debris|boulders], forming a [misshapen|sort of|cold|frozen] rocky ice ball.

planet.3=A rocky snowball of a type commonly found [beyond the snow line|in the outer system]. \
  [$Name|It] has a core of rocks and ice, with a mixed crust of silicate material and water ice.



planet.special=($3D6|3={VeryRare}|4,5={Rare}|6,7,8={Uncommon}|9,10,11,12={Common})


Common=The [exposed ice|icy surface|icy crust] of [this asteroid|$Name] is covered in [long|deep] scratches \
  which [trace|wind|stretch] across the surface in [regular|irregular|unusual] [lines|patterns].

Common.1=The surface is littered with rocky boulders [metres|many metres|dozens of metres] [across|wide|tall] \
  that [jut|poke] out of the [ice|icy surface|icy crust].

Common.2=[This|$Name] is a [very|particularly|unusually] large example of such an asteroid, being \
  [$Radius kilometres in radius|$Diameter kilometres across|$Diameter kilometres wide].

Common.3=The surface is [littered|covered|pock-marked] with [tiny|small] caters from ancient \
  [micro-meteorite collisions|collisions with micro-meteorites].

Common.4=Pockets of empty space, some [metres|many metres|dozens of metres|hundreds of metres|kilometres] \
  [wide|across|deep|long] can be found [near the core|just under the surface|throughout the asteroid].


# Special features

planet.Gelidaceous.feature.SnowCovered=The rocky surface of $Name is covered in a [thin|light] layer of \
  [snow|ice], the results of evaporation, settling and re-freezing of the icy layers in the past.


# Special core types which override the usual descriptions.

feature.RockSurface=[Asteroids|Planetoids|Small bodies] of the <em>Gelidaceous</em> type are \
  [normally|usually|generally] [found|located] in the [outer|colder] regions of a system, where the ice \
  they are mostly comprised of can exist in stable form. [This rock|This iceball|$Name] is instead way \
  too warm for ice to normally survive. [It's|A] thick [outer shell|crust] of [rock|silicates|silicate rock] \
  protects the [icy|watery|liquid] inner core from evaporation. The core is possibly super-heated water. \
  {special.extra}

feature.RockSurface.1=[This planetoid|This rock|$Name] has a rocky crust that [envelopes|hides] a \
  core of [liquid water|super heated water]. [The majority of|Over half] the mass of $Name is water, and \
  such objects are [rarely|unlikely to be] found in the inner system. The expected life [time|expectancy] \
  of [an object such as this|such an object] [is very short|may be as little as hundreds of years]. \
  {special.extra}

feature.RockSurface.2=A rocky [body|asteroid|planetoid] that [actually|surprisingly] [shields|hides] \
  [an inner|a central] core of [liquid water|warm water|water]. The [silicate|rocky] crust shields the core from \
  the stellar heat, and [limits|reduces] the rate at which it [can boil off|is boiling off]. $Name is \
  [most|very] [probably|likely] to be a recent migrant to this [orbit|orbit|position|location], and isn't \
  expected to remain as it is for very long. \
  {special.extra}

feature.SootSurface=[Rocks|Asteroids|Small bodies] of the <em>Gelidaceous</em> type are \
  [rare to find|rarely found|unlikely to be] in the inner system, so $Name is probably a recent migrant. \
  The [crust|outer] surface is covered in a [dense|thick|dark] layer of [sticky|sooty] material that \
  [protects|shields] the ice from being melted by the [solar|stellar] [heat|warmth|radiation]. The inner \
  core [is|is probably] a mixture of water and ice. \
  {special.extra}


# The usual automatic display of atmosphere details etc aren't used when overriding the core description,
# so add them back in since in this case we want them.
special.extra={planet.Gelidaceous.atmosphere.$Atmosphere} {planet.Gelidaceous.biosphere.$Life}


planet.atmosphere.WaterVapour=$Name is [enveloped|surrounded] by a trace atmosphere of water vapour, \
  [produced|formed] from [evaporation|outgassing] from its core. It is in a continuous cycle of losing the \
  vapour to space, and replenishing it from [inside|the core].

planet.atmosphere.WaterVapour.1=A extremely thin [cloud|envelope] of water vapour surrounds $Name, \
  continuously [added to|refreshed] from the evaporating core, and [just as quickly|also continuously] \
  being lost to space.


