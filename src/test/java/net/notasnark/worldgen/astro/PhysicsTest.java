/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */

package net.notasnark.worldgen.astro;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the Physics helper methods.
 */
public class PhysicsTest {
    @Test
    public void simpleRounding() {
        assertEquals(100, Physics.round(123, 1));
        assertEquals(120, Physics.round(123, 2));
        assertEquals(123, Physics.round(123, 3));
        assertEquals(123, Physics.round(123, 4));
    }

    @Test
    public void complexRounding() {
        assertEquals(-123, Physics.round(-123, 3));
        assertEquals(4570, Physics.round(4567, 3));
        assertEquals(456800000, Physics.round(456765432, 4));
    }

    @Test
    public void doubleRounding() {
        assertEquals(120, Physics.round(123.456, 2), 0.01);
        assertEquals(123, Physics.round(123.456, 3), 0.01);
        assertEquals(123.5, Physics.round(123.456, 4), 0.0001);
        assertEquals(123.46, Physics.round(123.456, 5), 0.0001);
        assertEquals(123.456, Physics.round(123.456, 6), 0.0001);
    }

    /**
     * Test that we get expected orbital periods for some common situations.
     * We are not looking for perfect results, but within 1% of expected is good enough.
     */
    @Test
    public void orbitalPeriods() {
        long earthPeriod = Physics.getOrbitalPeriod(Physics.SOL_MASS, Physics.AU * 1000);
        assertEquals(Physics.STANDARD_YEAR, earthPeriod, earthPeriod * 0.01);

        long marsPeriod = Physics.getOrbitalPeriod(Physics.SOL_MASS, 228 * Physics.MKM * 1000);
        assertEquals(Physics.STANDARD_DAY * 687, marsPeriod, marsPeriod * 0.01);

        long jupiterPeriod = Physics.getOrbitalPeriod(Physics.SOL_MASS, 778 * Physics.MKM * 1000);
        assertEquals(Physics.STANDARD_DAY * 4332, jupiterPeriod, jupiterPeriod * 0.01);
    }

    @Test
    public void temperature() {
        assertEquals(Physics.STANDARD_TEMPERATURE, Physics.getTemperatureOfOrbit(1.0, Physics.AU));
        assertEquals(Physics.AU, Physics.getOrbitWithTemperature(1.0, Physics.STANDARD_TEMPERATURE), 100_000);
    }

    @Test
    public void boilingPoint() {
        assertEquals(373, Physics.getBoilingPoint(Physics.STANDARD_PRESSURE));
        assertEquals(440, Physics.getBoilingPoint(Physics.STANDARD_PRESSURE * 4));
    }
}
