/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.astro.planets;

import net.notasnark.worldgen.astro.planets.codes.*;
import org.junit.Test;
import net.notasnark.worldgen.astro.planets.codes.*;

import static org.junit.Assert.*;

/**
 * Tests of the various enums available for planets.
 */
public class CodesTest {

    @Test
    public void atmosphere() {
        for (Atmosphere atmosphere : Atmosphere.values()) {
            assertTrue(atmosphere.getGreenhouse() > 0 && atmosphere.getGreenhouse() <= 1);
            assertTrue(atmosphere.getHabitability() >= 1 && atmosphere.getHabitability() <= 6);
        }

        assertTrue(Atmosphere.Chlorine.isNonWater());
        assertTrue(Atmosphere.Flourine.isNonWater());
        assertTrue(Atmosphere.Exotic.isNonWater());
        assertTrue(Atmosphere.SulphurCompounds.isNonWater());
        assertTrue(Atmosphere.NitrogenCompounds.isNonWater());
    }

    @Test
    public void life() {
        for (Life life : Life.values()) {
            assertTrue(life.getHabitability() >= 1 && life.getHabitability() <= 6);
        }

        assertTrue(Life.None.isSimplerThan(Life.Extensive));
        assertTrue(Life.Extensive.isMoreComplexThan(Life.None));
    }

    @Test
    public void government() {
        for (Government g : Government.values()) {
            assertTrue("Abbreviation for " + g.name(), g.getAbbreviation().length() == 2);
            assertTrue("Economy for " + g.name(), g.getEconomyModifier() >= -2 && g.getEconomyModifier() <= +2);
            assertTrue("Law for " + g.name(), g.getLawModifier() >= -2 && g.getLawModifier() <= +2);
            assertTrue("Tech for " + g.name(), g.getTechModifier() >= -2 && g.getTechModifier() <= +2);
        }
    }


    @Test
    public void pressure() {
        for (Pressure p : Pressure.values()) {
            assertTrue(p.getPascals() >= 0);
            assertTrue(p.getHabitability() >= 1 && p.getHabitability() <= 6);
            assertTrue(p.getGreenhouse() > 0 && p.getGreenhouse() <= 1);
        }

        assertEquals(Pressure.None, Pressure.getPressure(50));
        assertEquals(Pressure.Trace, Pressure.getPressure(5_000));
        assertEquals(Pressure.VeryThin, Pressure.getPressure(30_000));
        assertEquals(Pressure.Thin, Pressure.getPressure(50_000));
        assertEquals(Pressure.Standard, Pressure.getPressure(100_000));
        assertEquals(Pressure.Dense, Pressure.getPressure(180_000));
        assertEquals(Pressure.VeryDense, Pressure.getPressure(1_000_000));
        assertEquals(Pressure.SuperDense, Pressure.getPressure(10_000_000));
    }

    @Test
    public void temperature() {
        for (Temperature t : Temperature.values()) {
            assertTrue(t.getKelvin() >= 0);
        }
    }
}
