/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ.civilisation;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import net.notasnark.worldgen.WorldGen;
import net.notasnark.worldgen.astro.planets.Planet;
import net.notasnark.worldgen.astro.planets.codes.PlanetType;
import net.notasnark.worldgen.astro.planets.codes.TradeCode;
import net.notasnark.worldgen.astro.sectors.Sector;
import net.notasnark.worldgen.astro.systems.StarSystem;
import net.notasnark.worldgen.astro.systems.StarSystemType;
import net.notasnark.worldgen.astro.systems.Zone;
import net.notasnark.worldgen.civ.Facility;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class HermitTest {
    private WorldGen wg = null;
    private StarSystem system = null;

    @Before
    public void setup() {
        Sector sector = new Sector("Test", 0, 0);
        system = new StarSystem(sector, "Test", 12, 14, StarSystemType.SINGLE, Zone.GREEN);
    }

    @Test
    public void basicHermits() {
        Hermits hermits = new Hermits(wg, system);

        Planet planet = new Planet();
        List<Facility> facilities = new ArrayList<>();

        planet.setName("Test I");
        planet.setRadius(200);
        planet.setDayLength(86_400);
        planet.setType(PlanetType.Carbonaceous);
        planet.addTradeCode(TradeCode.Sg);
        system.addPlanet(planet);

        hermits.createRockHermits(planet, facilities);
        assertEquals(3.2, planet.getSpinGravity(), 0.01);
    }

    @Test
    public void hermitDescription() {
        Hermits hermits = new Hermits(wg, system);

        Planet planet = new Planet();
        List<Facility> facilities = new ArrayList<>();

        planet.setName("Test II");
        planet.setRadius(200);
        planet.setDayLength(43_200);
        planet.setType(PlanetType.Silicaceous);
        planet.addTradeCode(TradeCode.Sg);
        system.addPlanet(planet);

        hermits.createRockHermits(planet, facilities);
        assertEquals(5.2, planet.getSpinGravity(), 0.01);

        hermits.generate("RockHermits");
        System.out.println(planet.getDescription());
    }
}
