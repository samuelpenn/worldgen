/*
 * Copyright (c) 2018, Samuel Penn (sam@glendale.org.uk).
 * See the file LICENSE at the root of the project.
 */
package net.notasnark.worldgen.civ;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class CivilisationGeneratorTest {

    @Test
    public void abstractTests() {
        CivilisationGenerator civGen = new CivilisationGenerator(null, null) {
            @Override
            public void generate(String civ, CivilisationFeature... features) {
                // Do nothing.
                setFeatures(features);
            }
        };

        assertFalse(civGen.hasFeature(CivilisationFeature.Poor));
        civGen.setFeatures(CivilisationFeature.Poor);
        assertTrue(civGen.hasFeature(CivilisationFeature.Poor));
        civGen.setFeatures(CivilisationFeature.Infected, CivilisationFeature.HugePopulation);
        assertTrue(civGen.hasFeature(CivilisationFeature.Infected));
        assertTrue(civGen.hasFeature(CivilisationFeature.HugePopulation));

        Facility f = new Facility();
        f.setRating(100);
        assertEquals(100, f.getRating());
        civGen.qualityCheck(f);
        assertEquals(75, f.getRating());
    }

}
