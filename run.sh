#!/bin/bash

JARFILE=$(ls -t build/libs/*.jar | head -1)

if [ "$JARFILE" = "" ]
then
    echo "No jar file has been built. Run gradlew first."
    exit 2
fi

java -Dworldgen.config=wg.config -jar $JARFILE $* 2>&1 | grep -v "WARNING:"
