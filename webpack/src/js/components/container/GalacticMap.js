import React, { Component } from 'react';
import ReactDOM from 'react-dom';


class GalacticMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            minX: false,
            maxX: false,
            minY: false,
            maxY: false
        };
        this.state.minX = props.universe.minX;
        this.state.maxX = props.universe.maxX;
        this.state.minY = props.universe.minY;
        this.state.maxY = props.universe.maxY;
    }
    render() {
        if (this.state.minX || this.state.maxX) {

            var rows = [];

            for (let y = this.state.minY; y <= this.state.maxY; y++) {
                var row = [];
                for (let x = this.state.minX; x <= this.state.maxX; x++) {
                    let i = `/api/sector/${x},${y}/image`;
                    row.push(<td><img src={i} width="128" height="160"/></td>);
                }
                rows.push(<tr>{row}</tr>);
            }
            return (
                <table className="galaxy_map">
                    {rows}
                </table>
            )

        } else {
            return false;
        }
    }
}

export default GalacticMap;
