import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import ReactDOM from "react-dom";



import Header from './components/container/Header';


import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import allReducers from './reducers';

// STORE

// ACTION
const increment = () => {
    return {
        type: 'INCREMENT'
    }
};
const decrement = () => {
    return {
        type: 'DECREMENT'
    }
};
// REDUCER
const counter = (state = 0, action) => {
    switch (action.type) {
        case "INCREMENT":
            return state + 1;
        case "DECREMENT":
            return state - 1;
    }
};

// DISPATCH
store.dispatch(increment());




const store = createStore(allReducers, composeWithDevTools(
  applyMiddleware(...middleware),
  // other store enhancers if any
));



function Heading() {
    return (
        <AppBar position={"static"}>
            <Typography variant="h2" align={"left"} component={"h1"}>
                WorldGen Universe
            </Typography>
        </AppBar>
    )
}

function Copyright() {

    return (
        <Box my={4}>
            <Typography component={"h4"} align={"left"} gutterBottom>
                WorldGen
                {'Copyright (c) '}
                <Link color={"inherit"} href={"https://www.notasnark.net/worldgen/"}>
                    www.notasnark.net
                </Link>
                {' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        </Box>
    )
}

export default function WorldGenApp() {
    return (
        <Container maxWidth={"xl"}>
            <Header/>
            { ' ' }
            { Copyright() }
        </Container>
    )
}


ReactDOM.render(
    <Provider store={store}>
        <WorldGenApp />
    </Provider>,
    document.getElementById('root')
);
