# WorldGen

***WorldGen*** is a Java based application for generating worlds and civilisations
within a galaxy, using a style loosely inspired by *Traveller*. It provides a way
to create star systems and worlds, and a web application for displaying them.

For full details see the [WorldGen Wiki](https://www.notasnark.net/worldgen/). 
An example of the output produced for a world is below.

![Planet Data](docs/worldgen.png)

**Goals**

* To build something which could be used in a *Traveller*-like RPG, where
  players can easily travel between many worlds in an interstellar 
  civilisation;
* That makes use of the large amounts of processing power and storage now 
  readily available to most people;
* That makes use of the powerful internet enabled electronic devices that
  most gamers have available when at the table top;
* That can generate information for millions of worlds at a level of detail
  useful to a star-spanning RPG;
* That simulates trade between those worlds, to give the illusion of a
  living environment.

**Features (WIP)**

* Web based access to the data.  
* Multiple stars and worlds per system.
* Detailed world types, with each world having its own unique map and description.
* Detailed cultures and civilisations for each world.
* Trade simulated at the level of individual star ships.

The web application is built around [SparkJava](http://sparkjava.com/) . Data is stored 
in a database. It has been developed and tested against MySQL, other databases might 
work with a bit of effort.

## Documentation

There is documentation that is part of this repository.

  * [Web API](docs/api.md)
  * [Commands](docs/commands.md)
  * [Schema Notes](docs/schema.md)
  * [Text Templates](docs/templates.md)
  * [Planetary Classification List](https://www.notasnark.net/worldgen/pcl)

## Milestones

The current milestone (2.1) is named **Baryonic Blast**, which aims to put in basic
support for civilisations. This requires the definition of facilities to handle
agriculture, residential settlements, spaceports, factories and the like.

## Building

Use *gradle* to build the application. Depends on Java 11, SparkJava and MySQL.
On Linux, you can build it with:

```
./gradlew
```

Creates jar build/libs/worldgen-atomic-1.X-all.jar

Where X is the latest version of the current release. It expects a properties file 
to configure the database and web server.

```properties
database.url=jdbc:mysql://localhost:3306/worldgen?useSSL=false
database.username=world
database.password=world

server.port=4567

map.density.min=1
map.density.max=90

style.useRealStarColours=false

sim.frequency=60
sim.skipDowntime=true

```

Currently, you will need to dig out the contents of the schema.sql file and
import that into the database. Setup is not currently automated.

### ReactJS

There is the start of using React for the front end. The code for this is in
the webpack directory. To build this, use:

```
./build.sh
```

This will copy the needed Javascript files into the resource folder of the
project, which will be picked up when that is built. Currently the React
version of the site is at `/worldgen.html`.

## Running

There is a commandline application which can be used to run, control and report
on the application.

```
$ java -Dworldgen.config=wg.config -jar build/libs/worldgen-atomic-1.4-all.jar status
Universe: A Few Worlds
Created Date: 2018-03-04 22:21:32
Last Date: 2018-05-23 23:11:56
Current Time: 3.015 06:20
Number of Sectors: 3
Number of Systems: 175
Number of Planets: 392
```

Or, on Linux, just run `run.sh` which will find the latest built jar file and
run that. 

For a list of other commands, see [commands.md](docs/commands.md).


