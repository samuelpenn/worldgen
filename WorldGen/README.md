# WorldGen Docker Demo

Runs up WorldGen in a docker container, with some demo content.

If you have the code checked out, then you can build your own version of the
WorldGen container with the following command:

```
docker build -t notasnark/worldgen:2.8 .
```

You might want to give it a different name.

## Building the Docker environment

Docker require


Second, you need to create a database instance. This can be done using the
standard mysql docker container. We need both a shared network, and a
container, which can be created with the following:

```
docker network create worldgen

docker run -d --name database --network worldgen \
    --network-alias database  --env-file env mysql:latest

docker run -d --name worldgen --network worldgen \
    --network-alias worldgen --env-file env \
    -p 4567:4567 \
    notasnark/worldgen:2.8

```

There is an `env` file which contains basic configuration options, such
as passwords and users. If you want to point WorldGen at an existing MySQL
database, then you can change the MYSQL_HOST environment variables, and
you won't need the network or the mysql container.


Once the above has run, it should provide you with a web service running on
http://localhost:4567/ which you can point your browser at. It has only
minimal data setup, but there should be enough to see that things are working.

You can try creating some sectors and star systems with the following:

```
docker exec -it worldgen run.sh sector -4,0 Trojan Reach
docker exec -it worldgen run.sh sector -3,0 Reft
docker exec -it worldgen run.sh sectors
docker exec -it worldgen run.sh populate -3,0 J
```

For a list of the commands available, run:

```
docker exec -it worldgen run.sh
```

### Longterm Configuration

The above is great for getting something running quickly, but as soon as
you shutdown the docker containers, the database contents will be lost.
To avoid this, what you need to do is to map the database files onto an
external location. You do this with the `-v` option when setting up the
database container:


```
docker run -d --name database --network worldgen \
    --network-alias database --env-file env \
    -v /data/worldgen-mysql:/var/lib/mysql \
    mysql:latest \
    --character-set-server=utf8mb4 \
    --collation-server=utf8mb4_unicode_ci
```

The above would write the database files into /data/worldgen-mysql,
and next time the server is started it will pick up these files and
continue from where it left off. Feel free to change the location
to suit your system.

You can also optionally set the server's character set to be UTF8
with the final two options.

Similarly, you can mount the /root directory of the `worldgen` container
on an external directory, which makes it easier to upload/download files
and modify the configuration.

However, if you do this, then you need to ensure that the content of
the `files` directory is first copied into the mounted directory.

```
docker run -d --name worldgen --network worldgen \
    --network-alias worldgen --env-file env \
    -p 4567:4567 -v /data/worldgen:/root \
    notasnark/worldgen:2.8
```

## Building Worlds

The examples given above allow you to generate random data. There are
some other options though.

### Traveller Data

WorldGen recognises Traveller sector data files, and can be used to
generate star systems based on the UWP data. This allows you to generate
full star systems for whole sectors for a Traveller campaign.

To do this, you need a sector definition text file, which can be found
online or downloaded directly from the Traveller Map, as long as you use
the legacy file format.

For example, to download data for the Trojan Reach, and then generate
star systems from it, you can run the following:

```
docker exec -it worldgen run.sh sector -4,0 Trojan Reach
docker exec -it worldgen wget -O /root/Troj.txt https://travellermap.com/api/sec?sector=Troj&type=Legacy
docker exec -it worldgen run.sh sec /root/Troj.txt
```

These commands do the following:
* Create the Trojan Reach sector at the correct coordinates. Core sector is
defined as 0,0. Trailing is positive X, and Rimward is positive Y.
* Download the data file from the Traveller Map site.
* Generate sector data from the data file.

Generating a whole sector can take an hour or more, depending on the number
of star systems in the sector, and the speed of your computer.

### Deepnight Exploration

There are also some generators designed for use with the Deepnight Revelation
campaign. This is designed to generate a whole sector of systems with a low
level of habitation. Again, you need to ensure that the sector has been defined,
then you tell WorldGen to generate the data.

```
docker exec -it worldgen run.sh sector -3,0 Reft
docker exec -it worldgen run.sh deepnight -3,0
```

The systems are generated using the stellar density map.

#### Density Map

This is a greyscale image that defines what the galaxy looks like at a large
scale. Black means there is no chance of a star system at that location, whilst
white means there is guaranteed to be a star system. Shades of grey gives a
percentage chance.

30% gives a reasonable density of star systems, so setting a region to 30%
grey (#555555 in hex) will give 'normal' stellar distribution. The map included
with the software shows Chartered Space and everything around it.

![Galaxy Map](files/galaxy.png)

By default, the web interface only displays the core worlds, but this can be
expanded via the configuration page. You can also upload a new map via the
same page, or by running the `galaxy` command:

```
docker exec -it worldgen run.sh galaxy my_galaxy.png
```

In the latter case, the image needs to be inside the docker container.

## Time

WorldGen has its own internal clock, which is used to display the current
position of planets in a system. As time progresses, so everything moves
around in their orbits.

You can find the current time, as well as move backwards and forwards in
time by incrementing/decrementing the time:

```
docker exec -it worldgen run.sh time
docker exec -it worldgen run.sh time +0y7d
docker exec -it worldgen run.sh time -0y0d4h33m
```

## Database Hacks

All data is stored in the database, which can be accessed directly if you
need to (and have some knowledge of databases and SQL). You can either map
the Database port (3306) so that it is public and can be accessed using a
database client, or you can use the `mycli` program that is installed on the
worldgen container:

```
docker exec -it worldgen mycli -h database -u worldgen -p <passwd> worldgen
```

This will give you an interactive session. Some of the useful tables which
aren't immediately obvious as to their use are as follows:

* universe - some basic parameters, including the current time, and extent
of the galaxy which is displayed. Set `configured` column to 1 to start the
simulation clock running.
* constants - currently contains a single constant, `speed`, which is how
fast the internal clock runs at.



