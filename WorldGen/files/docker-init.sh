#!/bin/bash
#
# Used to start things up in the Docker container.
# If the database isn't configured, will try to configure the database first.
#

log() {
    echo $@
}

die() {
    echo $@
    exit 1
}

write_configuration() {
    if [ ! -f $CONFIG ]
    then
        cat << __EOF__ > $CONFIG
database.url=jdbc:mysql://$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE?useSSL=false
database.username=$MYSQL_USER
database.password=$MYSQL_PASSWORD

server.port=4567

map.density.min=5
map.density.max=950

style.useRealStarColours=false

sim.frequency=60
sim.skipDowntime=true

physics.jumpMask.useGravity=false
physics.jumpMask.gravityLimit=2000
__EOF__
    fi
}

database() {
    log "Checking the database with '$MYSQL_USER'"

    MYCLI="mycli -h $MYSQL_HOST -P $MYSQL_PORT -u $MYSQL_USER -p $MYSQL_PASSWORD"

    connection=0
    attempts=10
    while [ $attempts -gt 0 ]
    do
        echo "show databases" | $MYCLI && connection=1 && break
        attempts=$((attempts - 1))
        log ".. waiting for database ($attempts)"
        sleep 5
    done

    if [ $connection -eq 0 ]
    then
        die "Unable to connect to database ${MYSQL_USER}@${MYSQL_HOST}:${MYSQL_PORT}"
    fi
    log "Connecting to database"

    created=0
    echo "show tables" | $MYCLI $MYSQL_DATABASE | grep universe && created=1

    if [ $created -eq 0 ]
    then
        log "Creating database"
        $MYCLI --no-warn $MYSQL_DATABASE < /root/schema.sql

        write_configuration

        run.sh galaxy /root/galaxy.png

    else
        log "Database already created"
        write_configuration
    fi

    log "Database creation completed"

}

database

log "Starting server"
java -Dworldgen.config=$CONFIG -jar /root/worldgen-baryonic*.jar server
