#!/bin/bash

JARFILE=/root/worldgen*jar

if [ "$JARFILE" = "" ]
then
    echo "No jar file has been built. Run gradlew first."
    exit 2
fi

cd /root
java -Dworldgen.config=/root/worldgen.config -jar $JARFILE $* 2>&1 | grep -v "WARNING:"
